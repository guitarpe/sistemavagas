<?php
session_start();

include "includes/conexao.php";

$pagina = filter_input(INPUT_GET, 'pag');
$categoria = filter_input(INPUT_GET, 'categoria');
$ordenacao = filter_input(INPUT_GET, 'ordenacao');


$res_categorias = mysqli_query($con, "SELECT * FROM TB_VV_CATEGORIAS_BLOG WHERE status=1 ORDER BY nome ASC");

//paginação
$total_reg = 4;

if (!isset($pagina)) {
    $pag = "1";
} else {
    $pag = $pagina;
}

$ini = $pag - 1;
$inicio = $ini * $total_reg;

$where = "";
$order = "";
$urlparam = "?";

$sql = "SELECT
            bl.id_blog, bl.imagem, bl.titulo, bl.descricao
        FROM TB_VV_BLOGS bl
            INNER JOIN TB_VV_CATEGORIAS_BLOG cat ON cat.id_cat=bl.categoria
        WHERE bl.status=1 AND cat.status=1 ";

if (!empty($categoria)) {
    $where . " AND bl.categoria=$categoria ";
    $urlparam .= 'categoria=' . $categoria . '&';
}

if (!empty($ordenacao)) {
    if (intval($ordenacao) == 1) {
        $order = " DATE(bl.data) DESC ";
    } else {
        $order = " DATE(bl.data) ASC ";
    }
} else {
    $order = " bl.id_blog DESC";
}

$sql .= $where . " ORDER BY " . $order . " LIMIT $inicio, $total_reg";

$res_blog = mysqli_query($con, $sql) or die(mysqli_error($con));

/* TODOS */
$sql_todos = "SELECT
                count(*) as todos
            FROM TB_VV_BLOGS bl
                INNER JOIN TB_VV_CATEGORIAS_BLOG cat ON cat.id_cat=bl.categoria
            WHERE bl.status=1 AND cat.status=1 " . $where;
$res_todos = mysqli_query($con, $sql_todos) or die(mysqli_error($con));
$todos = mysqli_fetch_array($res_todos);
$tr = $todos['todos'];
$tp = $tr / $total_reg;

$num_paginas = ceil($tr / $total_reg);

?>
<html>
    <head>
        <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="container">
                <div class="row">
                    <section class="publicidade">
                        <div class="container">
                            <span>Publicidade</span>
                            <?php if (!empty($publi1['link'])) { ?>
                                <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </section>
                </div>
                <div class="row vagas">
                    <?php if (mysqli_num_rows($res_blog) > 0) { ?>
                        <div class="col-sm-12 m-bt-20">
                            <div class="col-sm-4 col-md-4 col-xs-6">
                                <label class="col-sm-3 form-label">Categoria</label>
                                <div class="col-sm-9">
                                    <select id="categoria" class="form-control slctpage">
                                        <option value="0">Todas</option>
                                        <?php while ($categorias = mysqli_fetch_array($res_categorias)) { ?>
                                            <option value="<?php echo $categorias['id_cat'] ?>" <?php echo $categoria == $categorias['id_cat'] ? 'selected=selected' : '' ?>><?php echo $categorias['nome'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-6">
                                <label class="col-sm-3 form-label">Ordenação</label>
                                <div class="col-sm-9">
                                    <select id="ordenacao" class="form-control slctpage">
                                        <option value="0" <?php echo!empty($ordenacao) ? intval($ordenacao) == 0 ? 'selected="selected"' : '' : '' ?>>Padrão</option>
                                        <option value="1" <?php echo!empty($ordenacao) ? intval($ordenacao) == 1 ? 'selected="selected"' : '' : '' ?>>Data Recente</option>
                                        <option value="2" <?php echo!empty($ordenacao) ? intval($ordenacao) == 2 ? 'selected="selected"' : '' : '' ?>>Data Antiga</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <div class="col-sm-9 col-xs-12 pagi">
                                    <div class="col-sm-3 col-xs-3">
                                        <label class="form-label">Página</label>
                                    </div>
                                    <?php
                                    $anterior = $pag - 1;
                                    $proximo = $pag + 1;

                                    $cont = 0;

                                    ?>
                                    <div class="col-sm-6 col-xs-6">
                                        <select id="pag1" class="form-control pg slctpage" select-group="first">
                                            <?php for ($i = 1; $i <= $num_paginas; $i++) { ?>
                                                <?php if ($i > ($pagina - 3) && $cont < 10) { ?>
                                                    <?php if ($i == $pagina) { ?>
                                                        <option selected><?php echo $i ?></option>
                                                    <?php } else { ?>
                                                        <option><?php echo $i ?></option>
                                                        <?php
                                                        $cont++;
                                                    }
                                                }

                                                ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 col-xs-3">
                                        <label class="form-label">de <?php echo $num_paginas ?></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <?php if ($pag > 1) { ?>
                                        <a href="<?php echo PATH_ALL . '/list-blogs.php' . $urlparam . 'pag=' . $anterior ?>" class="ant bt-nav-pag"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i></a>
                                    <?php } ?>
                                    <?php if ($pag < $tp) { ?>
                                        <a href="<?php echo PATH_ALL . '/list-blogs.php' . $urlparam . 'pag=' . $proximo ?>" class="prox bt-nav-pag" id="prox"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-offset-3 col-sm-6">
                            <?php while ($blog = mysqli_fetch_array($res_blog)) { ?>
                                <div class="blog">
                                    <figure>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $blog['imagem'] ?>" class="img-responsive">
                                    </figure>
                                    <div class="">
                                        <h2 class="ti"><?php echo $blog['titulo'] ?></h2>
                                        <p><?php echo $blog['descricao'] ?></p>
                                        <a href="<?php echo PATH_ALL . '/blog.php?id=' . $blog['id_blog'] ?>" class="btn btn-blog btn-icon">
                                            Leia Mais
                                            <i class="fa fa-angle-double-right"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-offset-8 col-sm-4 m-bot-10">
                            <div class="col-sm-9">
                                <div class="col-sm-3">
                                    <label class="form-label">Página</label>
                                </div>
                                <div class="col-sm-6">
                                    <?php
                                    $cont2 = 0;

                                    ?>
                                    <select id="pag1" class="form-control pg slctpage" select-group="first">
                                        <?php for ($i = 1; $i <= $num_paginas; $i++) { ?>
                                            <?php if ($i > ($pagina - 3) && $cont < 10) { ?>
                                                <?php if ($i == $pagina) { ?>
                                                    <option selected><?php echo $i ?></option>
                                                <?php } else { ?>
                                                    <option><?php echo $i ?></option>
                                                    <?php
                                                    $cont2++;
                                                }
                                            }

                                            ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="form-label">de <?php echo $num_paginas ?></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <?php if ($pag > 1) { ?>
                                    <a href="<?php echo PATH_ALL . '/list-blogs.php' . $urlparam . 'pag=' . $anterior ?>" class="ant bt-nav-pag"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i></a>
                                <?php } ?>
                                <?php if ($pag < $tp) { ?>
                                    <a href="<?php echo PATH_ALL . '/list-blogs.php' . $urlparam . 'pag=' . $proximo ?>" class="prox bt-nav-pag" id="prox"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-12 text-center">
                            <h3>Não existem itens cadastrados</h3>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <section class="publicidade">
                        <div class="container">
                            <span>Publicidade</span>
                            <?php if (!empty($publi2['link'])) { ?>
                                <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <?php include "includes/footer.php" ?>
    <?php include "includes/rodape.php" ?>
</body>
</html>
