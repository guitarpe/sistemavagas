<?php
include "../includes/conexao.php";

$func = new Funcoes();

$token = filter_input(INPUT_GET, 'token');

if (isset($token)) {

    $string = $func->dec_enc(2, $token);
    $dados = explode('|', $string);
    $nome = $dados[0];
    $email = $dados[1];

    $res = mysqli_query($con, "UPDATE TB_VV_USUARIOS SET status=1 WHERE email = '$email' AND tipo = 'candidato'");

    $situacao = 'msg-ativacao';
    $func->alert($situacao, 'acao');
    $func->redir("index.php");
} else {
    $func->redir("index.php");
}