<?php
session_start();
include "../includes/conexao.php";

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'candidato') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {
    $res = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATOS WHERE id = $_SESSION[id]");
    $dados = mysqli_fetch_array($res);

    ?>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-candidato.php"; ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="form-area acesso">
                                <h2 class="text-center">Alterar Senha</h2>
                                <form action="<?php echo PATH_ALL . '/candidatos/actions/recebe_alterasenha.php' ?>" method="post">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="input-group">
                                            <input class="form-control tamcampo" type="password" placeholder="Senha Atual" id="altsenha" name="altsenha" autocomplete="off" maxlength="6">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <span id="msg-senha" style="display:none">
                                            A senha deve ter no mínimo 6 caracteres
                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-3 margin-top-10">
                                        <div class="input-group">
                                            <input class="form-control txtSenha tamcampo" type="password" placeholder="Nova Senha" id="altnovasenha" name="senha" autocomplete="off" maxlength="6">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-3 margin-top-10">
                                        <div class="input-group">
                                            <input class="form-control confisenha nopaste tamcampo" type="password" placeholder="Confirmar Nova Senha" id="altcnovasenha" name="csenha" autocomplete="off" maxlength="6">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <span id="msg-csenha" style="display:none">
                                            As senhas não são iguais
                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-3 margin-top-10">
                                        <div class="text-center">
                                            <button class="btn btn-tipo button" id="atualizar-senha" type="submit">
                                                Atualizar
                                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
<?php } ?>