<nav class="off">
    <h4>
        <i class="fa fa-bars" aria-hidden="true"></i> Visualizar Menu do Candidato
    </h4>
    <ul>
        <li>
            <a href="<?php echo PATH_ALL . '/candidatos' ?>" id="item1">
                <h5>
                    <i class="fa fa-home" aria-hidden="true"></i> Painel do Candidato
                </h5>
            </a>
        </li>
        <li>
            <a href="<?php echo PATH_ALL . '/candidatos/perfil.php' ?>" id="item3">
                <h5>
                    <i class="fa fa-user" aria-hidden="true"></i> Perfil
                </h5>
            </a>
        </li>
        <li>
            <a href="<?php echo PATH_ALL . '/candidatos/candidaturas.php' ?>" id="item4">
                <h5>
                    <i class="fa fa-briefcase" aria-hidden="true"></i> Candidaturas
                </h5>
            </a>
        </li>
        <li>
            <a href="<?php echo PATH_ALL . '/buscar-vagas.php' ?>" id="item5">
                <h5>
                    <i class="fa fa-search" aria-hidden="true"></i> Buscar Vagas
                </h5>
            </a>
        </li>
    </ul>
</nav>