<?php
session_start();

include "../includes/conexao.php";

$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'candidato') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {


    $res_candidaturas = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATURAS WHERE id_candidato = $_SESSION[id] ORDER BY data ASC");

    ?>

    <!DOCTYPE html>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>

            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-candidato.php"; ?>
                            <script>$("#item4").addClass("active");</script>
                        </div>

                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="vagas">
                                <ul>
                                    <?php
                                    if (mysqli_num_rows($res_candidaturas)) {
                                        while ($candidatura = mysqli_fetch_array($res_candidaturas)) {
                                            $res = mysqli_query($con, "SELECT * FROM TB_VV_VAGAS WHERE id = $candidatura[id_vaga]");
                                            $vaga = mysqli_fetch_array($res);

                                            ?>
                                            <li>
                                                <div class="ttl">
                                                    <div class="rt">
                                                        <h6><?php echo $vaga["cargo"]; ?></h6>

                                                    </div>
                                                </div>

                                                <div class="info">
                                                    <p>
                                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                                        <?php echo $func->formataData($vaga['data_anuncio']); ?>
                                                        - Publicação da Vaga de Emprego
                                                    </p>
                                                    <p>
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        <?php echo $func->formataData($candidatura['data']); ?>
                                                        - Sua Inscrição
                                                    </p>
                                                    <p>
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        <?php echo $func->formataData($vaga['data_anuncio'], 2); ?>
                                                        - Encerramento das Inscrições
                                                    </p>
                                                    <a href="#" class="left m-top-10 m-right-20" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                        <i class="fa fa-times-rectangle-o cor-red"></i>
                                                    </a>
                                                    <a href="<?php echo PATH_ALL . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" class="oportunidade">
                                                        Visualizar Vaga <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    } else {

                                        ?>
                                        <li style="margin: 35px 0 15px 0">
                                            <div class="ttl">
                                                <div class="rt">
                                                    <h6>Candidaturas</h6>

                                                </div>
                                            </div>

                                            <div class="info">
                                                <p>
                                                    Você não possui candidaturas no momento!
                                                </p>
                                                <a href="../buscar-vagas.php" class="oportunidade">
                                                    Visualizar Vagas do Portal <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
<?php } ?>

