<?php
session_start();

include "../../includes/conexao.php";
include "../../includes/defines.php";

$func = new Funcoes();

ini_set('display_errors', 1);

error_reporting(E_ALL);

$to = "leonardolucas@oemprego.com.br, leonardo.lucas@gmail.com";
$to_txt = "Leonardo";
$subject = "Exclusão de cadastro de candidato no VAGAS&VAGAS";

$motivo = filter_input(INPUT_POST, 'motivo');

$message = '
	<html>
	<head>
	</head>
	<body>
		<div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
			<img src="' . HOME . '/images/top.png">
			<div class="container" style="padding:20px">
				<table>
					<tr>
						<td>
							<h2><b></b></h2>
							<br/>Olá Leonardo!
							<br/>
							<br/>
							O candidato ' . $_SESSION['nome'] . ', e-mail: ' . $_SESSION['email'] . ' acabou de deixar o portal Vagas&Vagas pelo seguinte motivo:
							<br/>
							<br/>
							"' . $motivo . '"
							<br/>
							<br/>
						</td>
					</tr>
				</table>
			</div>
			<img src="' . HOME . '/images/top.png">
		</div>
	</body>
	</html>
';

$dados = [
    'HOST' => $configuracoes_sis['SMTP_HOST'],
    'USER' => $configuracoes_sis['SMTP_USER'],
    'PASS' => $configuracoes_sis['SMTP_PASS'],
    'PORT' => $configuracoes_sis['SMTP_PORT'],
    'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
    'FROM_TXT' => 'Vagas e Vagas',
    'TO_MAIL' => $to,
    'TO_TXT' => $to_txt,
    'SUBJECT' => $subject,
    'MENSAGE' => $message
];

$query_cand = "DELETE FROM TB_VV_CANDIDATOS WHERE id=$_SESSION[id]";
$res_cand = mysqli_query($con, $query_cand);

if ($res_cand) {

    $query_us = "DELETE FROM TB_VV_USUARIOS WHERE id=$_SESSION[id] AND tipo like 'candidato'";
    $res_us = mysqli_query($con, $query_us);

    if ($res_us) {

        include_once '../../includes/enviar-email-to-redir.php';

        $func->redir("index.php");
    }
}