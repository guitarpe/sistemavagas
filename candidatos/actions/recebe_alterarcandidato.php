<?php
session_start();
include "../../includes/conexao.php";

$func = new Funcoes();

$id = filter_input(INPUT_GET, "id");
$tipo = intval(filter_input(INPUT_GET, "tipo"));

if ($tipo == 1) {
    /* ---------- DADOS DO USUÁRIO ---------- */
    $diretorio = "../images/";
    $nomeimagem = $_FILES['images']['name'];
    $tmp = $_FILES['images']['tmp_name'];
    $ext = substr($nomeimagem, -4, 4);
    $newnome = strtoupper(date("Ymdhis") . md5($nomeimagem));
    $nomefinalfoto = $newnome . $ext;
    $upload = $diretorio . $newnome . $ext;

    $dtnasc = filter_input(INPUT_POST, "datanasc");
    $nome = filter_input(INPUT_POST, "nome");
    $sexo = filter_input(INPUT_POST, "sexo");
    $filhos = filter_input(INPUT_POST, "filhos");
    $estadocivil = filter_input(INPUT_POST, "estadocivil");
    $cel = filter_input(INPUT_POST, "cel");
    $fone = filter_input(INPUT_POST, "fone");
    $pretensao_salarial = filter_input(INPUT_POST, "pretensao_salarial");
    $cep = filter_input(INPUT_POST, "cep");
    $logradouro = filter_input(INPUT_POST, "logradouro");
    $numero = filter_input(INPUT_POST, "numero");
    $complemento = filter_input(INPUT_POST, "complemento");
    $bairro = filter_input(INPUT_POST, "bairro");
    $estado = filter_input(INPUT_POST, "estado");
    $cidade = filter_input(INPUT_POST, "cidade");
    $viajar = filter_input(INPUT_POST, "viajar");
    $veiculo = filter_input(INPUT_POST, "veiculo");
    $objetivo = filter_input(INPUT_POST, "objetivo");

    $dtn = DateTime::createFromFormat('d/m/Y', $dtnasc);
    $datanasc = $dtn->format("Y-m-d");
}

//pega os dados do usuário
//exclui antiga foto do servidor
$sql_usu = "SELECT * FROM TB_VV_CANDIDATOS WHERE id=$id";
$res_usu = mysqli_query($con, $sql_usu);
$usu = mysqli_fetch_array($res_usu) or die(mysqli_error($con));

if ($tipo == 1) {
    if ($tmp != NULL) {

        if (move_uploaded_file($tmp, $upload)) {

            //exclui antiga foto do servidor
            if (mysqli_num_rows($res_usu)) {
                unlink("../images/" . $usu['foto']);
            }
        }

        $query_can = "UPDATE TB_VV_CANDIDATOS SET
                            nome = '$nome',
                            foto = '$nomefinalfoto',
                            nascimento = '$datanasc',
                            sexo = '$sexo',
                            filhos = '$filhos',
                            estado_civil = '$estadocivil',
                            celular = '$cel',
                            fone = '$fone',
                            pretensao_salarial = '$pretensao_salarial',
                            cep = '$cep',
                            logradouro = '$logradouro',
                            numero = '$numero',
                            complemento = '$complemento',
                            bairro = '$bairro',
                            estado = '$estado',
                            cidade = '$cidade',
                            viajar = $viajar,
                            veiculo = $veiculo,
                            obj_profissional = '$objetivo'
                            WHERE id = $id";
    } else {
        $query_can = "UPDATE TB_VV_CANDIDATOS SET
                        nome = '$nome',
                        nascimento = '$datanasc',
                        sexo = '$sexo',
                        filhos = '$filhos',
                        estado_civil = '$estadocivil',
                        celular = '$cel',
                        fone = '$fone',
                        pretensao_salarial = '$pretensao_salarial',
                        cep = '$cep',
                        logradouro = '$logradouro',
                        numero = '$numero',
                        complemento = '$complemento',
                        bairro = '$bairro',
                        estado = '$estado',
                        cidade = '$cidade',
                        viajar = $viajar,
                        veiculo = $veiculo,
                        obj_profissional = '$objetivo'
                        WHERE id = $id";
    }

    $res = mysqli_query($con, $query_can) or die(mysqli_error($con)) or die(mysqli_error($con));

    /* -------------- CNH ------------- */
    $cnh = filter_input(INPUT_POST, "cnh");
    if (intval($cnh) == 1) {

        $tipo = filter_input(INPUT_POST, "tipocnh");

        $res_cat_cnh = mysqli_query($con, "SELECT * FROM TB_VV_CNH_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));

        if (mysqli_num_rows($res_cat_cnh)) {
            $sql_cnh = "UPDATE TB_VV_CNH_CAND SET categoria='$tipo' WHERE id_candidato='$id'";
            $res_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));
        } else {
            $sql_cnh = "INSERT INTO TB_VV_CNH_CAND (id_candidato, categoria) VALUES ($id, '$tipo')";
            $res_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));
        }
    } else {
        $sql_cnh = "DELETE FROM TB_VV_CNH_CAND WHERE id_candidato = $id";
        $res_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));
    }

    /* -------------- DEFICIENCIAS ------------- */
    $deficiencia = filter_input(INPUT_POST, "deficiencia");
    if (intval($deficiencia) == 1) {

        $sql_def_can = "DELETE FROM TB_VV_DEFICIENCIAS_CAND WHERE id_candidato = $id";
        $res_def_can = mysqli_query($con, $sql_def_can) or die(mysqli_error($con));

        $defs = filter_input(INPUT_POST, 'deficiencias', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($defs[0])) {
            foreach ($defs as $key) {
                $sql_defs = "INSERT INTO TB_VV_DEFICIENCIAS_CAND (id_candidato, id_deficiencia) VALUES ($id, '$key') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', id_deficiencia='$key'";
                $res_defs = mysqli_query($con, $sql_defs) or die(mysqli_error($con));
            }
        }
    }

    /* -------------- CONHECIMENTOS INFO ------------- */
    $conhecimentos = filter_input(INPUT_POST, "informatica");

    if (intval($conhecimentos) == 1) {

        $sql_inf_can = "DELETE FROM TB_VV_INFORMATICA_CAND WHERE id_candidato = $id";
        $res_inf_can = mysqli_query($con, $sql_inf_can) or die(mysqli_error($con));

        $infos = filter_input(INPUT_POST, 'informaticas', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($infos[0])) {
            foreach ($infos as $key) {
                $sql_infos = "INSERT INTO TB_VV_INFORMATICA_CAND (id_candidato, id_informatica) VALUES ($id, '$key') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', id_informatica='$key'";
                $res_infos = mysqli_query($con, $sql_infos) or die(mysqli_error($con));
            }
        }
    }
}

if ($tipo == 2) {
    /* -------------- CARGOS ------------- */
    $cargos = filter_input(INPUT_POST, 'cargoprofissional', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($cargos[0])) {
        foreach ($cargos as $key) {
            if (!empty($key)) {
                $sql = "INSERT INTO TB_VV_CARGOS_CAND (id_candidato, cargo) VALUES ($id, '$key') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', cargo='$key'";
                $res = mysqli_query($con, $sql) or die(mysqli_error($con));
            }
        }
    }
}

if ($tipo == 3) {
    /* -------------- EXPERIÊNCIAS ------------- */
    $nomeempresa = filter_input(INPUT_POST, 'nomeempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $salario = filter_input(INPUT_POST, 'salario', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $cargoempresa = filter_input(INPUT_POST, 'cargoempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $atividades = filter_input(INPUT_POST, 'atividades', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $datainicio = filter_input(INPUT_POST, 'datainicio', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $datafim = filter_input(INPUT_POST, 'datafim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $empregoatual = filter_input(INPUT_POST, 'empregoatual', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($nomeempresa[0])) {

        foreach ($nomeempresa as $key => $value) {

            $valsalario = $func->returnValor($salario[$key]);
            $empatual = isset($empregoatual[$key]) ? intval($empregoatual[$key]) : 0;

            $sql = "INSERT INTO TB_VV_EXP_PROFISSIONAIS "
                . "(id_candidato, nome_empresa, cargo, atividades, inicio, fim, emprego_atual, salario) "
                . "VALUES "
                . "($id, '$nomeempresa[$key]', '$cargoempresa[$key]', '$atividades[$key]', '$datainicio[$key]', '$datafim[$key]', $empatual, '$valsalario') "
                . "ON DUPLICATE KEY UPDATE id_candidato='$id', nome_empresa='$nomeempresa[$key]', cargo='$cargoempresa[$key]', atividades='$atividades[$key]', "
                . "inicio='$datainicio[$key]', fim='$datafim[$key]', emprego_atual=$empatual, salario='$valsalario'";

            $res = mysqli_query($con, $sql) or die(mysqli_error($con));
        }
    }
}

if ($tipo == 4) {
    /* ---------- FORMAÇÃO -------- */
    $formacao = filter_input(INPUT_POST, 'formacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $instituicao = filter_input(INPUT_POST, 'instituicao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $nomeformacao = filter_input(INPUT_POST, 'nomeformacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $anofim = filter_input(INPUT_POST, 'anofim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $semestre = filter_input(INPUT_POST, 'semestre', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $turno = filter_input(INPUT_POST, 'turno', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($formacao[0])) {

        foreach ($formacao as $key => $value) {

            $sql_formacao = "INSERT INTO TB_VV_FORMACOES_CAND "
                . "(id_candidato, nivel, instituicao, curso, semestre, situacao, fim, turno) "
                . "VALUES "
                . "($id, '$formacao[$key]', '$instituicao[$key]', '$nomeformacao[$key]', '$semestre[$key]', '$situacao[$key]', '$anofim[$key]', '$turno[$key]')";

            $res_formacao = mysqli_query($con, $sql_formacao) or die(mysqli_error($con));
        }
    }
}

if ($tipo == 5) {
    /* ---------- CURSOS -------- */
    $nomecurso = filter_input(INPUT_POST, 'nomecurso', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $instituicao2 = filter_input(INPUT_POST, 'instituicao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $anofim2 = filter_input(INPUT_POST, 'anofim2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $situacao2 = filter_input(INPUT_POST, 'situacao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $turnoextra = filter_input(INPUT_POST, 'turnoextra', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $diassemana = filter_input(INPUT_POST, 'diassemana', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $cargahoraria = filter_input(INPUT_POST, 'cargahoraria', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($nomecurso[0])) {

        foreach ($nomecurso as $key => $value) {
            $sql_curso = "INSERT INTO TB_VV_CURSOS_CAND "
                . "(id_candidato, nome, instituicao, situacao, turnoextra, diassemana, cargahoraria, anofim ) "
                . "VALUES "
                . "($id, '$nomecurso[$key]', '$instituicao2[$key]', '$situacao2[$key]', '$turnoextra[$key]', '$diassemana[$key]', '$cargahoraria[$key]', '$anofim2[$key]')";

            $res_curso = mysqli_query($con, $sql_curso) or die(mysqli_error($con));
        }
    }
}

if ($tipo == 6) {
    /* ---------- IDIOMAS -------- */
    $idioma = filter_input(INPUT_POST, 'idioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $nivelidioma = filter_input(INPUT_POST, 'nivelidioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($idioma[0])) {

        foreach ($idioma as $key => $value) {

            $sql_idioma = "INSERT INTO TB_VV_IDIOMAS_CAND "
                . "(id_candidato, idioma, nivel) "
                . "VALUES "
                . "($id, '$idioma[$key]', '$nivelidioma[$key]')";

            $res_idioma = mysqli_query($con, $sql_idioma) or die(mysqli_error($con));
        }
    }
}

$situacao = 'msg-atualizacao';
$func->alert($situacao, 'acao');
$func->redir("candidatos/perfil.php");
