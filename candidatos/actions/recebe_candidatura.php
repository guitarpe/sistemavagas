<?php
include "../../includes/conexao.php";
$func = new Funcoes();

$data = date("Y-m-d");

$idcandidato = filter_input(INPUT_GET, "usuario");
$idvaga = filter_input(INPUT_GET, "vaga");

$tipo = filter_input(INPUT_GET, "tipo");

$res_candidatura = mysqli_query($con, "INSERT INTO TB_VV_CANDIDATURAS (id_candidato, id_vaga, data) VALUES (" . $idcandidato . ", " . $idvaga . ", '$data')")or die(msqli_error($con));

$res_vagas = mysqli_query($con, "SELECT id, id_empresa, cargo, numero_vagas, cidade, estado FROM TB_VV_VAGAS WHERE id = " . $idvaga . "")or die(msqli_error($con));
$vag = mysqli_fetch_array($res_vagas);

if ($vag['numero_vagas'] > 1) {
    $titulo = strtoupper($vag['cargo'] . " (" . $vag['numero_vagas'] . " VAGAS) - " . $vag['cidade'] . "-" . $vag['estado']);
} else {
    $titulo = strtoupper($vag['cargo'] . " (" . $vag['numero_vagas'] . " VAGA) - " . $vag['cidade'] . "-" . $vag['estado']);
}

$res_empresa = mysqli_query($con, "SELECT email, nome_empresa  FROM TB_VV_EMPRESAS WHERE id=" . $vag['id_empresa']);
$emp = mysqli_fetch_array($res_empresa);

$res_candidato = mysqli_query($con, "SELECT nome FROM TB_VV_CANDIDATOS WHERE id=" . $idcandidato);
$cand = mysqli_fetch_array($res_candidato);

$msg = "Você se candidatou na vaga $titulo.
	Agora depende apenas da análise da empresa para ser chamado para entrevista ou não.
	De qualquer forma desejamos todo sucesso do mundo para você e que dê tudo certo.
	Equipe Vagas&Vagas.";

date_default_timezone_set('America/Sao_Paulo');
$datahoje = date("d/m/Y");
$hora = date('H:i');

$res = mysqli_query($con, "INSERT INTO TB_VV_MENSAGENS (id_candidato, titulo, mensagem, nome, email, data, hora, status) VALUES (" . $idcandidato . ", 'Candidatura - Vaga:'.$titulo, '$msg', 'Vagas&Vagas', 'vagasevagas.com.br', '$datahoje', '$hora', 0)");

$to = $emp['email'];
$empresa = $emp['nome_empresa'];
$vaga = $vag['id'];
$candidato = $cand['nome'];

$res_emp = mysqli_query($con, "SELECT *
                               FROM TB_VV_USUARIOS us
                                    INNER JOIN TB_VV_EMPRESAS emp ON emp.id=us.id
                                    INNER JOIN TB_VV_VAGAS vg ON vg.id_empresa=emp.id
                               WHERE vg.id=$vaga AND us.tipo='empresa'");
$emp = mysqli_fetch_array($res_emp);

$subject = 'Você tem uma nova candidatura em sua vaga no Vagas e Vagas!';

$codigocan = $func->dec_enc(1, $idcandidato);

if (intval($emp['candidaturas_curriculos']) == 1) {
    $linkcurriculo = '<a href="' . URL . PATH_ALL . '/empresas/curriculo-candidato.php?cod=' . $codigocan . '" target="_blank">Clique aqui</a> para visualizar o currículo do candidato.';
} else {
    $linkcurriculo = '';
}

$message = '
<html>
    <head>
    </head>
    <body>
        <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2; font-family: "Comic Sans MS", cursive, sans-serif">
             <img src="' . HOME . '/images/top.png">
            <div class="container" style="padding:20px">
                <table>
                    <tr>
                        <td>
                            <h2><b>Olá ' . $empresa . '</b></h2>
                            <br/>
                            <br/>Queremos dar os parabéns pois, a sua vaga ' . $emp['cargo'] . ' está chamando a atenção dos candidatos!
                            <br/>
                            <br/>' . $candidato . ' acabou de se candidatar ao cargo oferecido e, você poderá visualizar mais informações ou entrar em contato através do seu painel.
                            <br/>' . $linkcurriculo . '
                            <br/>Para acessar agora o portal Vagas e Vagas, acesse o link a seguir: <a href="' . HOME . '/empresas/" style="color:blue">' . HOME . '/empresas</a>
                            <br/>
                            <br/>
                            <br/>Muito obrigado,
                            <br/>Equipe VagaseVagas.
                        </td>
                        <td>
                            <img src="' . HOME . '/images/confirmado.png">
                        </td>
                    </tr>
                </table>
                <br/>
                <br/>
                <div class="row" align="right">
                    <a href="' . FACEBOOK . '" style="text-decoration:none">
                        <img src="' . HOME . '/images/facebook.png">
                    </a>
                    <a href="' . TWITTER . '" style="text-decoration:none">
                        <img src="' . HOME . '/images/twitter.png">
                    </a>
                    <a href="' . PLUS . '" style="text-decoration:none">
                        <img src="' . HOME . '/images/googlep.png">
                    </a>
                </div>
            </div>
            <img src="' . HOME . '/images/top.png">
            <div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $emp['id'] . '&tipo=empresa" target="_blank">Não quero receber mais mensagens</a></div>
        </div>
    </body>
</html>';

if (intval($emp['recebe_email']) == 1) {
    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $empresa,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    $situacao = 'candidatura-success';
    $func->alert($situacao, 'acao');

    if (intval($tipo) == 1) {
        include_once '../../includes/enviar-email-no-redir.php';
    } else {
        include_once '../../includes/enviar-email.php';
    }
} else {

    ?>
    <form class="active">
        <div class="full text-center">
            <i class="fa fa-check" style="font-size:40px; color: green; margin-bottom: 10px"></i>
        </div>
        <div class="full" style="margin-bottom: 20px; text-align:center;">
            <span>Seu currículo foi enviado com sucesso!</span>
        </div>
    </form>
    <script>
        $(document).ready(function () {
            console.log("teste");
            $(document).find("a.descandidatar").removeClass("hidden");
        });
    </script>
<?php } ?>

