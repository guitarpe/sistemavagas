<?php
include "../../includes/conexao.php";

$func = new Funcoes();

$tipo = filter_input(INPUT_GET, "tipo");

if ($tipo == 1) {
    $edit_cargo_id = filter_input(INPUT_POST, "edit_cargo_id");
    $edit_cargo_usuario = filter_input(INPUT_POST, "edit_cargo_usuario");
    $editcargo = filter_input(INPUT_POST, "editcargo");

    $sql = "UPDATE TB_VV_CARGOS_CAND SET cargo='$editcargo' WHERE id_candidato=$edit_cargo_usuario AND id=$edit_cargo_id";
    mysqli_query($con, $sql) or die(mysqli_error($con));
}

if ($tipo == 2) {

    $edit_exp_id = filter_input(INPUT_POST, "edit_exp_id");
    $edit_exp_usuario = filter_input(INPUT_POST, "edit_exp_usuario");
    $nomeempresa = filter_input(INPUT_POST, "nomeempresamod");
    $valsalario = filter_input(INPUT_POST, "salariomod");
    $cargoempresa = filter_input(INPUT_POST, "cargoempresamod");
    $cargoempresaoutro = filter_input(INPUT_POST, "cargoempresaoutromod");
    $atividades = filter_input(INPUT_POST, "atividadesmod");
    $datainicio = filter_input(INPUT_POST, "datainiciomod");
    $datafim = filter_input(INPUT_POST, "datafimmod");
    $empatual = filter_input(INPUT_POST, "empregoatualmod");
    $empregoatual = empty($empatual) ? 0 : 1;

    $salario = $func->returnValor($valsalario);
    $cargo = ($cargoempresa == 'Outro') ? $cargoempresaoutro : $cargoempresa;

    $sql = "UPDATE
                TB_VV_EXP_PROFISSIONAIS
            SET
                nome_empresa='$nomeempresa',
                salario='$salario',
                atividades='$atividades',
                cargo='$cargo',
                emprego_atual=$empregoatual,
                fim='$datafim',
                inicio='$datainicio'
            WHERE id_candidato=$edit_exp_usuario AND id=$edit_exp_id";

    mysqli_query($con, $sql) or die(mysqli_error($con));
}

if ($tipo == 3) {
    $edit_form_id = filter_input(INPUT_POST, "edit_form_id");
    $edit_form_usuario = filter_input(INPUT_POST, "edit_form_usuario");
    $formacao = filter_input(INPUT_POST, 'formacaomod');
    $instituicao = filter_input(INPUT_POST, 'instituicaomod');
    $nomeformacao = filter_input(INPUT_POST, 'nomeformacaomod');
    $situacaomod = filter_input(INPUT_POST, 'situacaomod');
    $anofim = filter_input(INPUT_POST, 'anofimmod');
    $semestre = filter_input(INPUT_POST, 'semestremod');
    $turno = filter_input(INPUT_POST, 'turnomod');

    $sql = "UPDATE
                TB_VV_FORMACOES_CAND
            SET
                nivel='$formacao',
                instituicao='$instituicao',
                curso='$nomeformacao',
                semestre='$semestre',
                situacao='$situacaomod',
                fim='$anofim',
                turno='$turno'
            WHERE id_candidato=$edit_form_usuario AND id=$edit_form_id";

    mysqli_query($con, $sql) or die(mysqli_error($con));
}

if ($tipo == 4) {

    $edit_cur_id = filter_input(INPUT_POST, 'edit_cur_id');
    $edit_cur_usuario = filter_input(INPUT_POST, 'edit_cur_usuario');
    $nomecurso = filter_input(INPUT_POST, 'nomecursomod');
    $instituicao2 = filter_input(INPUT_POST, 'instituicao2mod');
    $anofim2 = filter_input(INPUT_POST, 'anofim2mod');
    $situacao2 = filter_input(INPUT_POST, 'situacao2mod');
    $turnoextra = filter_input(INPUT_POST, 'turnoextramod');
    $diassemana = filter_input(INPUT_POST, 'diassemanamod');
    $cargahoraria = filter_input(INPUT_POST, 'cargahorariamod');

    $sql = "UPDATE
                TB_VV_CURSOS_CAND
            SET
                nome='$nomecurso',
                instituicao='$instituicao2',
                situacao='$situacao2',
                turnoextra='$turnoextra',
                diassemana='$diassemana',
                cargahoraria='$cargahoraria',
                anofim='$anofim2'
            WHERE id_candidato=$edit_cur_usuario AND id=$edit_cur_id";

    mysqli_query($con, $sql) or die(mysqli_error($con));
}

if ($tipo == 5) {
    $edit_idioma_id = filter_input(INPUT_POST, 'edit_idioma_id');
    $edit_idioma_usuario = filter_input(INPUT_POST, 'edit_idioma_usuario');
    $idioma = filter_input(INPUT_POST, 'idioma');
    $nivelidioma = filter_input(INPUT_POST, 'nivelidioma');

    $sql = "UPDATE TB_VV_IDIOMAS_CAND SET idioma='$idioma', nivel='$nivelidioma' WHERE id_candidato=$edit_idioma_usuario AND id=$edit_idioma_id";

    mysqli_query($con, $sql) or die(mysqli_error($con));
}

$func->redir("candidatos/perfil.php");

