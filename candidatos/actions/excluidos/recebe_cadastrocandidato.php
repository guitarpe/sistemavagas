<?php
session_start();

include "../../includes/conexao.php";
$func = new Funcoes();

$email = filter_input(INPUT_POST, "email");
$senha = filter_input(INPUT_POST, "senha");
$nome = filter_input(INPUT_POST, "nome");
$cpf = filter_input(INPUT_POST, "cpf");
$data = filter_input(INPUT_POST, "data");
$sexo = filter_input(INPUT_POST, "sexo");
$fone = filter_input(INPUT_POST, "fone");
$estado = filter_input(INPUT_POST, "estado");
$cidade = filter_input(INPUT_POST, "cidade");
$cargosprof = filter_input(INPUT_POST, "cargosprof");
$experiencias = filter_input(INPUT_POST, "experiencias");
$formacoes = filter_input(INPUT_POST, "formacoes");
$cursos = filter_input(INPUT_POST, "cursos");
$idiomas = filter_input(INPUT_POST, "idiomas");

$senhausuario = $func->dec_enc(1, 'candidato' . '|' . $senha . '|' . $email);

$sql_cand = "INSERT INTO TB_VV_CANDIDATOS (email, senha, nome, cpf, nascimento, sexo, fone, estado, cidade) VALUES ('$email', '$senhausuario', '$nome', '$cpf', '$data', '$sexo', '$fone', '$estado', '$cidade')";

$res_cand = mysqli_query($con, $sql_cand);

$id = mysqli_insert_id($con);

$sql = "INSERT INTO TB_VV_USUARIOS (id, nome, email, senha, tipo, status) VALUES ($id, '$nome', '$email', '$senhausuario', 'candidato', '0')";

$res = mysqli_query($con, $sql);

foreach ($cargosprof as $value) {
    $sql = "INSERT INTO TB_VV_CARGOS_CAND (id_candidato, cargo) VALUES ($id, '$value')";
    $res = mysqli_query($con, $sql);
}

foreach ($experiencias as $value) {
    $salario = $func->returnValor($value["salario"]);

    $sql = "INSERT INTO TB_VV_EXP_PROFISSIONAIS (id_candidato, nome_empresa, cargo, atividades, inicio, fim, emprego_atual, salario) VALUES ($id, '" . $value['nome_empresa'] . "', '" . $value['cargo_empresa'] . "', '" . $value['atividades'] . "', '" . $value['inicio'] . "', '" . $value['fim'] . "', " . $value['emprego_atual'] . ", '$salario')";

    $res = mysqli_query($con, $sql);
}

foreach ($formacoes as $value) {
    $sql = "INSERT INTO TB_VV_FORMACOES_CAND (id_candidato, nivel, instituicao, curso, semestre, situacao, fim, turno) VALUES ($id, '" . $value['nivel'] . "', '" . $value['instituicao'] . "', '" . $value['curso'] . "', '" . $value['semestre'] . "', '" . $value['situacao'] . "', '" . $value['fim'] . "', '" . $value['turno'] . "')";

    $res = mysqli_query($con, $sql);
}

foreach ($cursos as $value) {
    $sql = "INSERT INTO TB_VV_CURSOS_CAND (id_candidato, nome, instituicao, situacao, inicio, fim) VALUES ($id, '" . $value['curso'] . "', '" . $value['instituicao'] . "', '" . $value['situacao'] . "', '" . $value['inicio'] . "', '" . $value['fim'] . "')";

    $res = mysqli_query($con, $sql);
}

foreach ($idiomas as $value) {
    $sql = "INSERT INTO TB_VV_IDIOMAS_CAND (id_candidato, idioma, nivel) VALUES ($id, '" . $value['idioma'] . "', '" . $value['nivel'] . "')";

    $res = mysqli_query($con, $sql);
}

$_SESSION['nome'] = $nome;
$_SESSION['email'] = $email;
$_SESSION['senha'] = $senha;
