<?php
session_start();

include "../includes/conexao.php";

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'candidato') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $cont = 1;

    $res = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATOS WHERE id=" . $_SESSION['id']) or die(mysqli_error($con));

    $ct = 0;
    while ($cand = mysqli_fetch_array($res)) {
        if (empty($cand[$ct])) {
            $cont++;
        }
        $ct++;
    }

    $experiencias = mysqli_query($con, "SELECT * FROM TB_VV_EXP_PROFISSIONAIS WHERE id_candidato=" . $_SESSION['id']) or die(mysqli_error($con));
    if (mysqli_num_rows($experiencias)) {
        $cont += 5;
    }

    $cursos = mysqli_query($con, "SELECT * FROM TB_VV_CURSOS_CAND WHERE id_candidato=" . $_SESSION['id']) or die(mysqli_error($con));
    if (mysqli_num_rows($cursos)) {
        $cont += 5;
    }

    $idiomas = mysqli_query($con, "SELECT * FROM TB_VV_IDIOMAS_CAND WHERE id_candidato=" . $_SESSION['id']) or die(mysqli_error($con));
    if (mysqli_num_rows($idiomas)) {
        $cont += 5;
    }

    $res_candidaturas = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATURAS WHERE id_candidato=" . $_SESSION['id'] . " ORDER BY data ASC") or die(mysqli_error($con));

    $perc = ($cont / ($ct + 16)) * 100;
    $percent = explode(".", $perc)[0];

    $tipo_cad = "candidato";

    ?>

    <!DOCTYPE html>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-candidato.php"; ?>
                            <script>$("#item1").addClass("active");</script>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <section class="dash">
                                <h2>Olá, <?php echo $_SESSION["nome"] ?></h2>
                                <br/>
                                <div class="row" id="home">
                                    <div class="col-md-3">
                                        <a href="<?php echo PATH_ALL . '/candidatos/imprimir-curriculo.php' ?>" target="_blank" class="btn btn-home" id="imprimir">
                                            <i class="fa fa-print"></i> Imprimir Currículo
                                        </a>
                                        <a href="#" data-load-title="Suspender Informativos" class="btn btn-home break-line-btn-home" data-toggle="modal" data-target="#modal-default-md" data-load-url="<?php echo PATH_ALL ?>/modais/suspender_informativos.php">
                                            <i class="fa fa-ban"></i> Suspender <br class="hidden-xs"/>Informativos
                                        </a>
                                        <a href="#" class="btn btn-home hidden" id="ativar">
                                            <i class="fa fa-check-circle-o"></i> Ativar Informativos
                                        </a>
                                        <a href="<?php echo PATH_ALL . '/candidatos/dados-de-acesso.php' ?>" class="btn btn-home">
                                            <i class="fa fa-edit"></i> Alterar Senha
                                        </a>
                                        <a href="#" data-load-title="Atenção! Excluir Cadastro" class="btn btn-home" data-toggle="modal" data-target="#modal-default-md" data-load-url="<?php echo PATH_ALL ?>/modais/dialog_exclui_conta.php">
                                            <i class="fa fa-trash-o"></i> Excluir Cadastro
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-default height-270">
                                            <div class="panel-heading">
                                                <i class="fa fa-user"></i>
                                                <span>Seu Perfil</span>
                                            </div>
                                            <div class="panel-body">
                                                <span>Seu Perfil Profissional (CV) está</span>
                                                <h3><?php echo $percent . "%" ?></h3>
                                                <div class="progress">
                                                    <div data-percentage="0%" style="width: <?php echo $percent ?>%;" class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span>Completo.</span>
                                                <div class="row">
                                                    <a href="<?php echo PATH_ALL . '/candidatos/perfil.php' ?>" class="btn btn-refresh-perfil">
                                                        <i class="fa fa-refresh"></i>
                                                        Atualizar Perfil
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="panel panel-default candidaturas" style="">
                                            <div class="panel-heading">
                                                <i class="fa fa-briefcase"></i>
                                                <span>Candidaturas</span>
                                            </div>
                                            <div class="panel-body panel-oportunidades">
                                                <ul>
                                                    <?php
                                                    if (mysqli_num_rows($res_candidaturas)) {
                                                        while ($candidatura = mysqli_fetch_array($res_candidaturas)) {
                                                            $res = mysqli_query($con, "SELECT cargo, id FROM TB_VV_VAGAS WHERE id = $candidatura[id_vaga]");
                                                            $vaga = mysqli_fetch_array($res);

                                                            ?>
                                                            <li class="ul-list-candidaturas">
                                                                <div class="margin-bottom">
                                                                    <strong>
                                                                        <?php echo $vaga["cargo"]; ?>
                                                                        <a href="#" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                                            <i class="fa fa-times-circle cor-red"></i>
                                                                        </a>
                                                                    </strong>
                                                                </div>
                                                                <div>
                                                                    <a href="<?php echo PATH_ALL . '/ver-oportunidade.php?id=' . $vaga['id'] ?>">
                                                                        Visualizar Vaga <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                            <?php
                                                        }
                                                    } else {

                                                        ?>
                                                        <li class="margin-top-50">
                                                            <div><strong>Você não possui candidaturas no momento!</strong></div>
                                                            <div>
                                                                <a href="<?php echo PATH_ALL . '/buscar-vagas.php' ?>" class="oportunidade">
                                                                    Visualizar vagas disponiveis <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $res_msgs = mysqli_query($con, "SELECT * FROM TB_VV_MENSAGENS WHERE id_candidato=" . $_SESSION['id'] . " ORDER BY id DESC");
                                    if (mysqli_num_rows($res_msgs)) {

                                        ?>
                                        <div class="col-md-12" id="painel-msgs">
                                            <div class="panel panel-default candidaturas">
                                                <div class="panel-heading">
                                                    <i class="fa fa-envelope"></i>
                                                    <span>Mensagens</span>
                                                </div>

                                                <div class="panel-body painel-mensagens">
                                                    <ul>
                                                        <?php
                                                        if (mysqli_num_rows($res_msgs)) {
                                                            while ($msg = mysqli_fetch_array($res_msgs)) {

                                                                ?>
                                                                <table>
                                                                    <tr>
                                                                        <td width="150"><?php echo $msg["data"] . " " . $msg["hora"] ?></td>
                                                                        <td><a href="#" data-load-title="Visualizar Mensagem" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_ver_mensagem.php?id=<?php echo $msg["id"] ?>"><?php echo $msg["titulo"] ?></a></td>
                                                                        <td width="40">
                                                                            <?php if (intval($msg["status"]) == 0) { ?>
                                                                                <a href="#" data-load-title="Visualizar Mensagem" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_ver_mensagem.php?id=<?php echo $msg["id"] ?>">
                                                                                    <i class="fa fa-eye icon-msgs-gray"></i>
                                                                                </a>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td width="40">
                                                                            <a href="#" data-load-title="Apagar Mensagem" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_msg.php?id=<?php echo $msg["id"] ?>">
                                                                                <i class="fa fa-trash-o icon-msgs-red btn_apagar_msg"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <?php
                                                            }
                                                        }

                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </section>
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
    <?php
}
