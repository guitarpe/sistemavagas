<?php
include "../includes/conexao.php";
$func = new Funcoes();

$cand = filter_input(INPUT_POST, 'candidato');
$vag = filter_input(INPUT_POST, 'vaga');

$res_cand = mysqli_query($con, "SELECT nome, email FROM TB_VV_CANDIDATOS WHERE id = $cand");
$candidato = mysqli_fetch_array($res_cand);

$res_usu = mysqli_query($con, "SELECT nome, email FROM TB_VV_USUARIOS WHERE id=$cand AND tipo='candidato'");
$usuario = mysqli_fetch_array($res_usu);

$res_vaga = mysqli_query($con, "SELECT cargo, numero_vagas, cidade, estado FROM TB_VV_VAGAS WHERE id = $vag");
$vaga = mysqli_fetch_array($res_vaga);

if ($vaga['numero_vagas'] > 1) {
    $titulo = strtoupper("$vaga[cargo] ($vaga[numero_vagas] vagas) - $vaga[cidade] $vaga[estado]");
} else if ($vaga['numero_vagas'] == 1) {
    $titulo = strtoupper("$vaga[cargo] ($vaga[numero_vagas] vaga) - $vaga[cidade] $vaga[estado]");
}

$to = $candidato['email'];
$to_txt = $candidato['nome'];

$subject = "Novo cadastro de empresa no Vagas e Vagas";

$message = '
	<html>
	<head>
	</head>
	<body>
		<div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
			<img src="' . HOME . '/images/top.png">
			<div class="container" style="padding:20px">
				<table>
					<tr>
						<td>
							<h2><b></b></h2>
							<br/>Olá ' . $candidato['nome'] . '
							<br/>
							<br/>
							Temos a alegria de dizer que o seu currículo despertou interesse em uma empresa e está sendo analisado para a vaga ' . $titulo . '.
							<br/>
							<br/>
							Agora depende apenas da análise da empresa para ser chamado para entrevista ou não.
							<br/>
							<br/>
							De qualquer forma desejamos todo sucesso do mundo para você e que dê tudo certo.
							<br/>
							<br/>
							Equipe Vagas e Vagas.
						</td>
					</tr>
				</table>
				<div class="row" align="right">
					<a href="' . FACEBOOK . '" style="text-decoration:none">
						<img src="' . HOME . '/images/facebook.png">
					</a>
					<a href="' . TWITTER . '" style="text-decoration:none">
						<img src="' . HOME . '/images/twitter.png">
					</a>
					<a href="' . PLUS . '" style="text-decoration:none">
						<img src="' . HOME . '/images/googlep.png">
					</a>
				</div>
			</div>
			<img src="' . HOME . '/images/top.png">
		<div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $usuario['id'] . '&tipo=candidato" target="_blank">Não quero receber mais mensagens</a></div>
            </div>
	</body>
	</html>';

if (intval($usuario['recebe_email']) == 1) {
    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $to_txt,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    include_once '../includes/enviar-email.php';
}
