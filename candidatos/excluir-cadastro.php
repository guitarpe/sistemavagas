<?php
session_start();
include "../includes/conexao.php";

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'candidato') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    ?>

    <!DOCTYPE html>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php" ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-candidato.php"; ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="form-area" style="text-align:justify; padding:40px 80px 80px 40px;">
                                <h1>Ficaremos tristes se você decidir ir embora!</h1>
                                <br/>
                                <p>Olá <?php echo $_SESSION['nome'] ?>,</p>
                                <p>Parece que você está decidindo ir embora, saiba que ficaremos tristes, pois gostamos sempre de oferecer as empresas que divulgam suas vagasconosco, currículos tão bons quanto o seu.</p>
                                <p>Você ainda pode desistir clicando no botão <a href="<?php echo PATH_ALL . '/index.php' ?>"><strong>TIRE ME DAQUI!</strong></a> e continuar aproveitando tudo de bom que o portal Vagas & Vagas pode lhe oferecer. Saiba que todos os recursos do site são totalmente gratuitos e seus dados cadastrais estão completamente seguros conosco.</p>
                                <p>Mas se mesmo assim você quiser nos deixar, por favor nos informe o motivo.</p>

                                <form action="<?php echo PATH_CANDIDATOS . '/' . 'actions/recebe_excluircadastro.php' ?>" method="post">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <textarea class="form-control" name="motivo" placeholder="Descreva o motivo..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-success btn-md">
                                                <i class="fa fa-paper-plane"></i> Enviar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
<?php } ?>