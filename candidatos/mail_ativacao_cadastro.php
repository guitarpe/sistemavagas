<?php
session_start();
include "../includes/conexao.php";

$func = new Funcoes();

$user = $user = filter_input(INPUT_GET, 'user');

if (isset($nome) && isset($email) && isset($senha)) {

    $sql_us = "SELECT * FROM TB_VV_USUARIOS WHERE id=$user AND tipo='candidato'";
    $res_us = mysqli_query($con, $sql_us) or die(mysqli_error($con));
    $usuario = mysqli_fetch_array($res_us);

    $to = $usuario['email'];
    $to_txt = $usuario['nome'];

    $subject = 'Nova conta de ' . $nome . ' no portal Vagas e Vagas';

    $token = base64_encode('email=' . $email . '&nome=' . $nome);

    $message = '
            <html>
            <head>
            </head>
            <body>
                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                    <img src="' . HOME . '/images/top.png">
                    <div class="container" style="padding:20px">
                        <table>
                            <tr>
                                <td>
                                    <h2><b></b></h2>
                                    <br/>Olá ' . $nome . ', agora você já faz quase parte da família Vagas e Vagas!
                                    <br/>
                                    Um portal totalmente livre para pesquisar e inserir vagas!
                                    <br/>
                                    <br/>
                                    Bem, para que possa entrar no site, basta confirmar que seu e-mail está correto e, para isso, use o link abaixo:
                                    <br/>
                                    <br/>
                                    <a href="' . HOME . '/candidatos/activate.php?token=' . $token . '">' . HOME . '/candidatos/activate.php?token=' . $token . '</a>
                                    <br/>
                                    <br/>
                                    Aguardamos sua confirmação e torcemos muito para que encontre a vaga que deseja!
                                    <br/>
                                    <br/>
                                </td>
                                <td>
                                    <img src="' . HOME . '/images/confirmado.png">
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                        PS. Ajude divulgar este trabalho especial para seus colegas! É muito importante para todos.
                        <br/>
                        <br/>
                        Contamos muito com você!
                        <br/>
                        <br/>
                        Equipe Vagas e Vagas.
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <div class="row" align="right">
                            <a href="' . FACEBOOK . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/facebook.png">
                            </a>
                            <a href="' . TWITTER . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/twitter.png">
                            </a>
                            <a href="' . PLUS . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/googlep.png">
                            </a>
                        </div>
                    </div>
                    <img src="' . HOME . '/images/top.png">
                <div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $usuario['id'] . '&tipo=candidato" target="_blank">Não quero receber mais mensagens</a></div>
                </div>
            </body>
            </html>';

    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $to_txt,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    include_once '../includes/enviar-email.php';
}