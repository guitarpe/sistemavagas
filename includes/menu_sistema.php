<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
	<style type="text/css">
		.sidebar a:link {
			text-decoration: none;
		}
		.fa-circle {
			font-size: 10px;
		}
	</style>
</head>

<body>
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="layout/dist/img/adm.png" class="img-circle" alt="User Image">
				</div>
				<div class="pull-left info">
					<p>ADMINISTRAÇÃO</p>
					<a href=""><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>
			<br/>
			<!-- search form -->
			<!-- /.search form -->
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu" data-widget="tree">
				<li class="header">OPÇÕES DE CURRÍCULO</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-plus-circle"></i>
						<span>Adicionar</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="add-area-profissional.php"><i class="fa fa-circle"></i>Área Profissional</a></li>
						<li><a href="add-especializacao.php"><i class="fa fa-circle"></i>Especialização</a></li>
						<li><a href="add-nivel-ensino.php"><i class="fa fa-circle"></i>
						Nível Ensino</a></li>
						<li><a href="add-situacao.php"><i class="fa fa-circle"></i>
						Situação</a></li>
					</ul>
				</li>
				<li class="header">CANDIDATOS</li>
				<li><a href="list-candidatos.php"><i class="fa fa-list-alt"></i>Listar Candidatos</a></li>
				<li class="header">EMPRESAS</li>
				<li><a href="list-empresas.php"><i class="fa fa-list-alt"></i>Listar Empresas</a></li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-list-alt"></i>
						<span>Listar</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="list-area-profissional.php"><i class="fa fa-circle"></i>Área Profissional</a></li>
						<li><a href="list-especializacoes.php"><i class="fa fa-circle"></i>Especialização</a></li>
						<li><a href="list-nivel-ensino.php"><i class="fa fa-circle"></i>
						Nível Ensino</a></li>
						<li><a href="list-situacao.php"><i class="fa fa-circle"></i>
						Situação</a></li>
					</ul>
				</li>
				<br/>
				<li class="header">OPÇÕES DE VAGA</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-plus-circle"></i>
						<span>Adicionar</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="add-vaga.php"><i class="fa fa-circle"></i> Nova Vaga</a></li>
						<li><a href="add-experiencia-minima.php"><i class="fa fa-circle"></i> Experiência Mínima</a></li>
						<li><a href="add-ensino-minimo.php"><i class="fa fa-circle"></i>
						Ensino Mínimo</a></li>
						<li><a href="add-tipo-deficiencia.php"><i class="fa fa-circle"></i>
						Tipo Deficiência</a></li>
						<li><a href="add-deficiencia.php"><i class="fa fa-circle"></i>
						Deficiência</a></li>
						<li><a href="add-conhecimento-informatica.php"><i class="fa fa-circle"></i>Conhecimento Informatica</a></li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-list-alt"></i>
						<span>Listar</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="list-vagas.php"><i class="fa fa-circle"></i> Vagas</a></li>
						<li><a href="list-vagas-ativar.php"><i class="fa fa-circle"></i> Vagas a Ativar</a></li>
						<li><a href="list-vagas-estado.php"><i class="fa fa-circle"></i> Vagas por estado</a></li>
						<li><a href="list-vagas-cidade.php"><i class="fa fa-circle"></i> Vagas por cidade</a></li>
						<li><a href="list-vagas-empresa.php"><i class="fa fa-circle"></i> Vagas por empresa</a></li>
						<li><a href="list-experiencia-minima.php"><i class="fa fa-circle"></i> Experiência Mínima</a></li>
						<li><a href="list-ensino-minimo.php"><i class="fa fa-circle"></i>
						Ensino Mínimo</a></li>
						<li><a href="list-tipo-deficiencia.php"><i class="fa fa-circle"></i>
						Tipo Deficiência</a></li>
						<li><a href="list-deficiencia.php"><i class="fa fa-circle"></i>
						Deficiência</a></li>
						<li><a href="list-conhecimento-informatica.php"><i class="fa fa-circle"></i>Conhecimento Informatica</a></li>
					</ul>
				</li>
				<br/>
				<li class="header">OUTROS</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-newspaper-o"></i>
						<span>Notícias</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="cadastrar-noticia.php"><i class="fa fa-plus-circle"></i>Cadastrar</a></li>
						<li><a href="list-noticias.php"><i class="fa fa-list-alt"></i>Listar</a></li>
					</ul>
				</li>
                <li class="treeview">
					<a href="#">
						<i class="fa fa-newspaper-o"></i>
						<span>Blog</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="cadastrar-blog.php"><i class="fa fa-plus-circle"></i>Cadastrar Blog</a></li>
						<li><a href="list-blogs.php"><i class="fa fa-list-alt"></i>Listar</a></li>
                        <li><a href="cadastrar-cat-blog.php"><i class="fa fa-plus-circle"></i>Cadastrar Categoria Blog</a></li>
						<li><a href="list-cat-blogs.php"><i class="fa fa-list-alt"></i>Listar Categorias</a></li>
					</ul>
				</li>
                <li><a href="edit-institucional.php"><i class="fa fa-plus-circle"></i>Institucional</a></li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-bullhorn"></i>
						<span>Publicidades</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="cadastrar-publicidade.php"><i class="fa fa-plus-circle"></i>Cadastrar</a></li>
						<li><a href="list-publicidade.php"><i class="fa fa-list-alt"></i>Listar</a></li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-file-text"></i>
						<span>Perguntas Contato</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="cadastrar-pergunta.php"><i class="fa fa-plus-circle"></i>Cadastrar</a></li>
						<li><a href="list-perguntas.php"><i class="fa fa-list-alt"></i>Listar</a></li>
					</ul>
				</li>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
</body>
</html>
