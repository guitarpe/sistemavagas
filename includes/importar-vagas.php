<?php
include "conexao.php";
include "conexao2.php";

$func = new Funcoes();

$ini = filter_input(INPUT_GET, 'ini');
$fim = filter_input(INPUT_GET, 'fim');

$query_oemprego = "SELECT pj.id_pj, id_vaga, cargo_vaga, descricao_vaga, name_cidade, bairro_pj, sigla_estado, cep_pj, vg.id_experiencia,
                        name_experiencia, deficiencia_vaga, sexo_vaga, viajar_vaga, veiculo_vaga, cnh_vaga, idioma_vaga, informatica_vaga,
                        vg.id_ensino, name_ensino, salario_vaga, name_salario, name_turno, name_tipo_contratacao, name_profissional_area,
                        razao_social_pj, nome_fantasia_pj, default_name_pj, renovada_vaga, publish_date_vaga, renovada_date_vaga, privacidade_vaga
                    FROM oemp_vaga vg
                        INNER JOIN oemp_estado est ON est.id_estado=vg.id_estado
                        INNER JOIN oemp_cidade cid ON cid.id_cidade=vg.id_cidade
                        INNER JOIN oemp_pj pj ON pj.id_pj=vg.id_pj
                        INNER JOIN oemp_ramo_atividade ra ON ra.id_ramo_atividade=pj.id_ramo_atividade
                        LEFT OUTER JOIN oemp_experiencia exp ON exp.id_experiencia=vg.id_experiencia
                        INNER JOIN oemp_ensino ens ON ens.id_ensino=vg.id_ensino
                        INNER JOIN oemp_salario sal ON sal.id_salario=vg.id_salario
                        INNER JOIN oemp_turno tur ON tur.id_turno=vg.id_turno
                        INNER JOIN oemp_tipo_contratacao cont ON cont.id_tipo_contratacao=vg.id_tipo_contratacao
                        INNER JOIN oemp_profissional_area pa ON pa.id_profissional_area=vg.id_profissional_area
                    ORDER BY id_vaga DESC LIMIT $ini,$fim";

$res_oemprego = mysqli_query($con2, $query_oemprego) or die(mysqli_error($con2));

while ($row = mysqli_fetch_array($res_oemprego)) {

    //INSERIR A EMPRESA
    $query_emp = "SELECT pj.razao_social_pj, pj.cnpj_pj, ra.name_ramo_atividade, pj.nome_fantasia_pj, pj.email_pj,
                    pj.logo_pj, pj.descricao_pj, pj.nfuncionarios_pj, pj.site_pj, pj.email_pj, pj.telefone_pj,
                    pj.fax_pj, pj.cep_pj, pj.logradouro_pj, pj.numero_pj, pj.complemento_pj, pj.bairro_pj, cid.name_cidade, est.sigla_estado
                  FROM oemp_pj pj
                    INNER JOIN oemp_ramo_atividade ra ON ra.id_ramo_atividade=pj.id_ramo_atividade
                    INNER JOIN oemp_estado est ON est.id_estado=pj.id_estado
                    INNER JOIN oemp_cidade cid ON cid.id_cidade=pj.id_cidade WHERE id_pj=" . $row['id_pj'];

    $res_emp = mysqli_query($con2, $query_emp);
    $row_emp = mysqli_fetch_array($res_emp);

    $razao_social_pj = $row_emp['razao_social_pj'];
    $cnpj_pj = $row_emp['cnpj_pj'];
    $nome_ramo_atividade = $row_emp['name_ramo_atividade'];
    $nome_fantasia_pj = $row_emp['nome_fantasia_pj'];
    $email_pj = $row_emp['email_pj'];
    $logo_pj = $row_emp['logo_pj'];
    $descricao_pj = $row_emp['descricao_pj'];
    $nfuncionarios_pj = $row_emp['nfuncionarios_pj'];
    $site_pj = $row_emp['site_pj'];
    $telefone_pj = $row_emp['telefone_pj'];
    $fax_pj = $row_emp['fax_pj'];
    $cep_pj = $row_emp['cep_pj'];
    $logradouro_pj = $row_emp['logradouro_pj'];
    $numero_pj = $row_emp['numero_pj'];
    $complemento_pj = $row_emp['complemento_pj'];
    $bairro_pj = $row_emp['bairro_pj'];
    $name_cidade = $row_emp['name_cidade'];
    $sigla_estado = $row_emp['sigla_estado'];

    $verifica_emp = mysqli_query($con, "SELECT id FROM TB_VV_EMPRESAS WHERE cnpj='$cnpj_pj'") or die(mysqli_error($con));
    $exite_emp = mysqli_fetch_array($verifica_emp);

    if (empty($exite_emp['id'])) {

        $query_in_emp = 'INSERT INTO TB_VV_EMPRESAS(
                            nome_empresa, cnpj, ramo_atividade, nome_usuario, email, senha, imagem,
                            descricao, qtd_funcionarios, site, email_empresa, fone, fax,
                            cep, logradouro, numero, complemento, bairro, cidade, estado
                        )VALUES("' . $razao_social_pj . '", "' . $cnpj_pj . '", "' . $nome_ramo_atividade . '", "' . $nome_fantasia_pj . '", "' . $email_pj . '", "",
                            "' . $logo_pj . '", "' . $descricao_pj . '", "' . $nfuncionarios_pj . '", "' . $site_pj . '", "' . $email_pj . '", "' . $telefone_pj . '", "' . $fax_pj . '",
                            "' . $cep_pj . '", "' . $logradouro_pj . '", ' . $numero_pj . ', "' . $complemento_pj . '", "' . $bairro_pj . '", "' . $name_cidade . '", "' . $sigla_estado . '")';

        $res_in_emp = mysqli_query($con, $query_in_emp) or die(mysqli_error($con2));
        $id_empresa = mysqli_insert_id($con);
    } else {

        $id_empresa = $exite_emp['id'];
    }

    $cargo_vaga = $row['cargo_vaga'];
    $descricao_vaga = $row['descricao_vaga'];
//$name_cidade = $row['name_cidade'];
//$bairro_pj = $row['bairro_pj'];
//$sigla_estado = $row['sigla_estado'];
//$cep_pj = $row['cep_pj'];
    $name_experiencia = $row['name_experiencia'];
    $deficiencia_vaga = $row['deficiencia_vaga'];
    $sexo_vaga = $row['sexo_vaga'];
    $viajar_vaga = $row['viajar_vaga'];
    $veiculo_vaga = $row['veiculo_vaga'];
    $cnh_vaga = $row['cnh_vaga'];
    $idioma_vaga = $row['idioma_vaga'];
    $informatica_vaga = $row['informatica_vaga'];
    $name_ensino = $row['name_ensino'];
    $salario_vaga = $row['salario_vaga'];
    $name_salario = $row['name_salario'];
    $name_turno = $row['name_turno'];
    $name_tipo_contratacao = $row['name_tipo_contratacao'];
    $name_profissional_area = $row['name_profissional_area'];
//$razao_social_pj = $row['razao_social_pj'];
//$nome_fantasia_pj = $row['nome_fantasia_pj'];
    $default_name_pj = $row['default_name_pj'];
    $renovada_vaga = $row['renovada_vaga'];
    $publish_date_vaga = $row['publish_date_vaga'];
    $renovada_date_vaga = $row['renovada_date_vaga'];
    $privacidade_vaga = $row['privacidade_vaga'];

    //insere a vaga
    $query_in_vaga = 'INSERT INTO TB_VV_VAGAS (
        id_empresa, forma_contratacao, cargo, numero_vagas, nivel_hierarquico, turno, atividades,
        salario, faixa_salarial, sexo, experiencia_minima, ensino_minimo, vaga_outrolocal, cidade, estado,
        viajar, veiculo_proprio, visibilidade_dados, data_anuncio, status, numero_candidatos
        )VALUES(
            ' . $id_empresa . ', "' . $name_tipo_contratacao . '", "' . $func->trocarAspas($cargo_vaga) . '", "", "", "' . $name_turno . '", "' . addslashes($descricao_vaga) . '", "' . $salario_vaga . '", "' . $name_salario . '", "' . $sexo_vaga . '", "' . $name_experiencia . '", "' . $name_ensino . '", 0, "' . $name_cidade . '", "' . $sigla_estado . '", "' . $viajar_vaga . '", "' . $veiculo_vaga . '", 1, "' . $publish_date_vaga . '", 0, "") ';

    $res_in_vaga = mysqli_query($con, $query_in_vaga) or die(mysqli_error($con));
}