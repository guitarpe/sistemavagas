<div class="container">
    <div class="logo">
        <a href="<?php echo PATH_ALL . '/index.php' ?>">
            <img src="<?php echo PATH_ASSETS . '/img/' ?>logo-sem-fundo.png" class="img-responsive"/>
        </a>
    </div>
    <nav class="off">
        <button>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <ul>
            <li>
                <?php if ((isset($_SESSION['id']) || isset($_SESSION['nome'])) && isset($_SESSION['email']) && isset($_SESSION['senha']) && $_SESSION['tipo'] == 'empresa') { ?>
                    <a href="#" class="link-js" target="cadastro-vagas" title="Anunciar Vagas">
                        Anunciar Vagas
                    </a>
                <?php } else { ?>
                    <a href="<?php echo PATH_ALL ?>/cadastroempresa.php?op=1" title="Anunciar Vagas">
                        Anunciar Vagas
                    </a>
                <?php } ?>
            </li>
            <li>
                <a href="<?php echo PATH_ALL ?>/buscar-vagas.php" title="Buscar Vagas">
                    Buscar Vagas
                </a>
            </li>
            <li>
                <a href="<?php echo PATH_ALL ?>/list-blogs.php" title="Blog">
                    Blog
                </a>
            </li>
            <li>
                <a href="<?php echo PATH_ALL ?>/institucional.php" title="Institucional">
                    Institucional
                </a>
            </li>
            <li>
                <a href="<?php echo PATH_ALL ?>/contato-formulario.php" title="Fale Conosco">
                    Fale Conosco
                </a>
            </li>
        </ul>
    </nav>
    <?php if ((isset($_SESSION['id']) || isset($_SESSION['nome'])) && isset($_SESSION['email']) && isset($_SESSION['senha']) && isset($_SESSION['tipo'])) { ?>
        <a href="#" data-load-title="Sair" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_sair.php" class="button">
            <i class="fa fa-power-off"></i>
            Sair
        </a>
        <?php if ($_SESSION['tipo'] == 'candidato') { ?>
            <a href="<?php echo PATH_ALL ?>/candidatos" class="button">
                <i class="fa fa-user-circle"></i>
                Minha Conta
            </a>
            <?php
        } else {
            if ($_SESSION['tipo'] == 'empresa') {

                ?>
                <a href="<?php echo PATH_ALL ?>/empresas" class="button">
                    <i class="fa fa-user-circle"></i>
                    Minha Conta
                </a>
            <?php } else { ?>
                <a href="<?php echo PATH_ALL ?>/admin" title="Painel de Administração">
                    <i class="fa fa-gear" aria-hidden="true"></i>
                </a>
                <?php
            }
        }

        ?>
    <?php } else { ?>
        <a href="#" data-load-title="Faça seu login" data-toggle="modal" data-target="#modal-default-md" data-load-url="<?php echo PATH_ALL ?>/modais/entrar.php" class="button">
            Entrar
        </a>
        <a href="#" data-load-title="Cadastre-se" data-toggle="modal" data-target="#modal-default-md" data-load-url="<?php echo PATH_ALL ?>/modais/cadastro-tipo.php" class="button dark">
            Cadastre-se
        </a>
    <?php } ?>
</div>