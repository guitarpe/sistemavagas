<footer>
    <section class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <nav>
                        <ul class="menu hidden-xs">
                            <li>
                                <a href="#" data-load-title="Cadastre seu currículo" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_ALL ?>/modais/cadastro-curriculo.php">
                                    Cadastrar Currículo
                                </a>
                            </li>
                            <li>
                                <a href="#" data-load-title="Cadastre sua empresa" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_ALL ?>/modais/cadastro-empresa.php">
                                    Cadastrar Empresa
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo PATH_ALL ?>/institucional.php" title="Institucional">
                                    Institucional
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo PATH_ALL ?>/list-blogs.php" title="Blog">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <ul class="social">
                        <li>
                            <a href="#" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="footer-bot">
        <div class="container">
            <span class="text-center">
                Copyright © 2018 - Vagas&Vagas
            </span>
        </div>
    </section>
</footer>
<?php include "modais_dialogos.php"; ?>
