<?php

class SimpleXmlElementComplement extends SimpleXMLElement
{

    public function addCData($name, $value = null)
    {
        $child = $this->addChild($name);
        $node = dom_import_simplexml($child);
        $cdata = $node->ownerDocument->createCDATASection($value);
        $node->appendChild($cdata);
    }
}
