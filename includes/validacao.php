<?php
include "conexao.php";

$tipo = filter_input(INPUT_POST, "tipo");
$nundoc = filter_input(INPUT_POST, "numdoc");

if ($tipo == 1) {
    echo validar_cpf($nundoc) ? 1 : 0;
} else {
    echo validar_cnpj($nundoc) ? 1 : 0;
}

function validar_cpf($cpf)
{
    // Extrai somente os números
    $cpf = preg_replace('/[^0-9]/is', '', $cpf);

    // Verifica se foi informado todos os digitos corretamente
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }
    // Faz o calculo para validar o CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return false;
        }
    }
    return true;
}

function validar_cnpj($cnpj)
{
    $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
    // Valida tamanho
    if (strlen($cnpj) != 14)
        return false;

    // Lista de CNPJs inválidos
    $invalidos = [
        '00000000000000',
        '11111111111111',
        '22222222222222',
        '33333333333333',
        '44444444444444',
        '55555555555555',
        '66666666666666',
        '77777777777777',
        '88888888888888',
        '99999999999999'
    ];

    // Verifica se o CNPJ está na lista de inválidos
    if (in_array($cnpj, $invalidos)) {
        return false;
    }

    // Valida primeiro dígito verificador
    for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }
    $resto = $soma % 11;
    if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
        return false;
    // Valida segundo dígito verificador
    for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }
    $resto = $soma % 11;
    return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
}
if (isset($_POST["email"])) {
    $sql = "SELECT id FROM TB_VV_USUARIOS WHERE email LIKE '$_POST[email]'";
    $res = mysqli_query($con, $sql);
    $qregistro = mysqli_num_rows($res);

    if ($qregistro) {
        echo false;
    } else {
        echo true;
    }
}

if (isset($_POST["cnpj"])) {
    $sql = "SELECT id  FROM TB_VV_EMPRESAS WHERE cnpj = '$_POST[cnpj]'";
    $res = mysqli_query($con, $sql);
    $qregistro = mysqli_num_rows($res);

    if ($qregistro) {
        echo 401;
    } else {
        if (validar_cnpj($_POST["cnpj"])) {
            echo 200;
        } else {
            echo 400;
        }
    }
}

if (isset($_POST["cpf"])) {
    $sql = "SELECT id FROM TB_VV_CANDIDATOS WHERE cpf = '$_POST[cpf]'";
    $res = mysqli_query($con, $sql);
    $qregistro = mysqli_num_rows($res);

    if ($qregistro) {
        echo 401;
    } else {
        if (validar_cpf($_POST["cpf"])) {
            echo 200;
        } else {
            echo 400;
        }
    }
}

?>