<div id="modal-default" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg box">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body">
                <p>Carregando Visualização</p>
            </div>
        </div>
    </div>
</div>
<div id="modal-default-md" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md box small">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body">
                <p>Carregando Visualização</p>
            </div>
        </div>
    </div>
</div>
<div id="modal-default-mini" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm box small">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body">
                <p>Carregando Visualização</p>
            </div>
        </div>
    </div>
</div>
<div id="modal-default-aviso" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md box small">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<?php if (isset($_SESSION["acao"])) { ?>
    <script>
        $(document).ready(function () {
            $('#modal-default-alert').modal('show');
        })
    </script>
    <div id="modal-default-alert" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md box small">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <h2 class="modal-title">
                        <?php if ($_SESSION['acao-class'] == 'ativacao-cadastro') { ?>
                            Cadastro de Currículo
                        <?php } else if ($_SESSION['acao-class'] == 'msg-ativacao') { ?>
                            Ativação de Cadastro
                        <?php } else if ($_SESSION['acao-class'] == 'msg-cadastro-sucesso') { ?>
                            Cadastro
                        <?php } else if ($_SESSION['acao-class'] == 'msg-usuario-senha-inv') { ?>
                            Acesso
                        <?php } else if ($_SESSION['acao-class'] == 'alerta-alterarsenha') { ?>
                            Dados de Acesso
                        <?php } else if ($_SESSION['acao-class'] == 'alerta-alterarsenha-erro') { ?>
                            Dados de Acesso
                        <?php } else if ($_SESSION['acao-class'] == 'msg-sem-acesso') { ?>
                            Acesso
                        <?php } else if ($_SESSION['acao-class'] == 'candidatura-box') { ?>
                            Candidatura
                        <?php } else if ($_SESSION['acao-class'] == 'candidatura-success') { ?>
                            Candidatura
                        <?php } else if ($_SESSION['acao-class'] == 'msg-add-vaga-succ') { ?>
                            Cadastro de Vaga
                        <?php } else if ($_SESSION['acao-class'] == 'msg-atualizacao') { ?>
                            Atualização de Cadastro
                        <?php } else if ($_SESSION['acao-class'] == 'msg-up-vaga') { ?>
                            Atualização de Vaga
                        <?php } else if ($_SESSION['acao-class'] == 'excluir-msg') { ?>
                            Mensagem
                        <?php } else if ($_SESSION['acao-class'] == 'msg-publi-desp-vaga') { ?>
                            Atualizar Vaga
                        <?php } else if ($_SESSION['acao-class'] == 'msg-descarte-candidato') { ?>
                            Descatar
                        <?php } else if ($_SESSION['acao-class'] == 'msg-contratar-candidato') { ?>
                            Contratar Candidato
                        <?php } else if ($_SESSION['acao-class'] == 'msg-descontratar-candidato') { ?>
                            Remover Contratação
                        <?php } else if ($_SESSION['acao-class'] == 'msg-selecionar-candidato') { ?>
                            Selecionar Candidato
                        <?php } else if ($_SESSION['acao-class'] == 'msg-fale-conosco') { ?>
                            Enviado com Sucesso
                        <?php } else if ($_SESSION['acao-class'] == 'msg-fale-conosco-lim') { ?>
                            Limite Excedido
                        <?php } ?>
                    </h2>
                </div>
                <div class="modal-body">
                    <?php if ($_SESSION['acao-class'] == 'ativacao-cadastro') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-envelope" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:justify;">
                                <span>Um e-mail foi enviado em sua conta com um link para a ativação do seu cadastro. Não esqueça de verificar sua caixa de spam. Ative já a o seu cadastro para usar nossos serviços!</span>
                            </div>
                            <div class="full">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Entendi</button>
                                <button class="reenviar" type="button" data-id='<?php echo $_SESSION["acao-user"] ?>'>
                                    Reenviar e-mail
                                    <i class="fa fa-spinner fa-spin" id="spinner" style="display:none"></i>
                                    <i class="fa fa-check" id="check" style="display:none"></i>
                                </button>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-ativacao') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:justify;">
                                <span>Parabéns! Seu cadastro foi <b>Ativado</b> com sucesso! Faça seu login e aproveite o portal <b>Vagas&Vagas!</b></span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-cadastro-sucesso') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:40px; color: green; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Cadastro ativado com sucesso!<br/>Você já pode realizar o seu login</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-usuario-senha-inv') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-close" style="font-size:40px; color: red; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Usuário ou senha inválida!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'alerta-alterarsenha') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" class="mgs-alterar-check"></i>
                            </div>
                            <div class="full msg-alterar">
                                <span>Senha atualizada com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'alerta-alterarsenha-erro') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-close" style="font-size:40px; color: red; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>A senha informada não confere, por favor tente novamente!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-sem-acesso') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-close" style="font-size:40px; color: red; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Você não tem acesso à esta pagina!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'candidatura-box') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px">
                                <span>Sua candidatura para a Vaga de Emprego foi realizada com sucesso. Veja mais informações sobre sua condidatura em seu Painel.</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'candidatura-success') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:40px; color: green; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Seu currículo foi enviado com sucesso!</span>
                            </div>
                        </form>
                        <script>
                            $(document).ready(function () {
                                console.log("teste");
                                $(document).find("a.descandidatar").removeClass("hidden");
                            });
                        </script>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-add-vaga-succ') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Sua Vaga foi <b>cadastrada</b> com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-atualizacao') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Seus dados foram <b>Atualizados</b> com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-up-vaga') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Sua Vaga foi <b>Atualizada</b> com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'excluir-msg') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Mensagem excluída com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-publi-desp-vaga') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:58px; color: #710082; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Vaga atualizada com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-descarte-candidato') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-close" style="font-size:40px; color: red; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Candidato descartado com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-contratar-candidato') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:40px; color: green; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Candidato contratado com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-descontratar-candidato') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-close" style="font-size:40px; color: red; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Removida a contratação com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-selecionar-candidato') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" style="font-size:42px; color: green; margin-bottom: 10px"></i>
                            </div>
                            <div class="full" style="margin-bottom: 20px; text-align:center;">
                                <span>Candidato selecionado com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-fale-conosco') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-check" class="mgs-alterar-check"></i>
                            </div>
                            <div class="full msg-alterar">
                                <span>Enviado com sucesso!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php if ($_SESSION['acao-class'] == 'msg-fale-conosco-lim') { ?>
                        <form class="active">
                            <div class="full text-center">
                                <i class="fa fa-close" style="font-size:40px; color: red; margin-bottom: 10px"></i>
                            </div>
                            <div class="full msg-alterar">
                                <span>Você pode enviar apenas 3 mensagens por dia!</span>
                            </div>
                        </form>
                    <?php } ?>
                    <?php
                    unset($_SESSION["acao"]);
                    unset($_SESSION["acao-class"]);
                    unset($_SESSION["acao-user"]);

                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>