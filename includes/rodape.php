<script src="<?= PATH_ASSETS ?>/js/jquery/jquery-ui.js"></script>
<script src="<?= PATH_ASSETS ?>/js/jquery/jquery-1.12.4.js"></script>
<script src="<?= PATH_ASSETS ?>/js/jquery/jquery-1.10.1.min.js"></script>
<script src="<?= PATH_ASSETS ?>/plugins/Uikit/uikit-2.27.2.js" ></script>
<script src="<?= PATH_ASSETS ?>/plugins/Uikit/notify-2.27.2.js" ></script>
<script src="<?= PATH_ASSETS ?>/bootstrap/js/bootstrap.js"></script>
<script src="<?= PATH_ASSETS ?>/bootstrap/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/jquery/jquery.mask.min.js"></script>

<script>
    let caminho = '<?php echo DIRETORIO; ?>';
</script>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/custom.js"></script>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/mascaras.js"></script>

<?php if ($tipo_cad == "candidato") { ?>
    <script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/cadastrocandidato.js"></script>
    <script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/candidato.js"></script>
<?php } ?>

<?php if ($tipo_cad == "empresa") { ?>
    <script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/cadastroempresa.js"></script>
    <script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/empresa.js"></script>
    <script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/cadastrovagas.js"></script>
<?php } ?>


