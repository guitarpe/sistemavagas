<div class="filtro-mobile off">
    <h4>
        <i class="fa fa-filter" aria-hidden="true"></i> Filtro de Pesquisa
    </h4>

    <div class=" pesquisa-mobile">
        <form action="<?php echo PATH_ALL ?>/buscar-vagas.php?tipo=1" method="get">
            <div class="filter" id="filtros">
                <h5>
                    <i class="fa fa-filter" aria-hidden="true"></i> Filtros
                </h5>
                <ul class="filtros-list"></ul>
            </div>
            <div class="filter <?= !empty($cargo) ? 'open' : 'close' ?>" id="filtro1">
                <h5>
                    <i class="fa fa-briefcase" aria-hidden="true"></i> Cargo
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_cargo = mysqli_query($con, "SELECT
                                                cargo, sum(ct_cargos) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 AND cargo!='' " . $where . " group by cargo");
                            if (mysqli_num_rows($res_cargo) > 0) {
                                $ct0 = 0;
                                while ($row = mysqli_fetch_array($res_cargo)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($cargo) > 0 && sizeof($cargo) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="cargo[<?php echo $ct0 ?>]" value="<?php echo $row['cargo'] ?>" <?= in_array($row['cargo'], $cargo) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['cargo'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="cargo[<?php echo $ct0 ?>]" value="<?php echo $row['cargo'] ?>"/> <span><?php echo $row['cargo'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct0++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($estado) ? 'open' : 'close' ?>" id="filtro2">
                <h5>
                    <i class="fa fa-globe" aria-hidden="true"></i> Estado
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_estado = mysqli_query($con, "SELECT
                                                nome, sum(ct_estados) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 " . $where . " group by nome");
                            if (mysqli_num_rows($res_estado) > 0) {
                                $ct1 = 0;
                                while ($row = mysqli_fetch_array($res_estado)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($estado) > 0 && sizeof($estado) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="estado[<?php echo $ct1 ?>]" value="<?php echo $row['nome'] ?>" <?= in_array($row['nome'], $estado) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['nome'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="estado[<?php echo $ct1 ?>]" value="<?php echo $row['nome'] ?>"/> <span><?php echo $row['nome'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct1++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($cidade) ? 'open' : 'close' ?>" id="filtro3">
                <h5>
                    <i class="fa fa-map-marker" aria-hidden="true"></i> Cidade
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_cidades = mysqli_query($con, "SELECT
                                                                    cidade, sum(ct_cidade) as total
                                                                FROM VW_VAGAS_FILTRO WHERE 1=1 " . $where . " group by cidade");
                            if (mysqli_num_rows($res_cidades) > 0) {
                                $ct2 = 0;
                                while ($row = mysqli_fetch_array($res_cidades)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($cidade) > 0 && sizeof($cidade) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="cidade[<?php echo $ct2 ?>]" value="<?php echo $row['cidade'] ?>" <?= in_array($row['cidade'], $cidade) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['cidade'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="cidade[<?php echo $ct2 ?>]" value="<?php echo $row['cidade'] ?>"/> <span><?php echo $row['cidade'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct2++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($nivel) ? 'open' : 'close' ?>" id="filtro4">
                <h5>
                    <i class="fa fa-sort-amount-asc" aria-hidden="true"></i> Nível Hierárquico
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_niveis = mysqli_query($con, "SELECT
                                                nivel_hierarquico, sum(ct_nivel) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 AND nivel_hierarquico!='' " . $where . " group by nivel_hierarquico");
                            if (mysqli_num_rows($res_niveis) > 0) {
                                $ct3 = 0;
                                while ($row = mysqli_fetch_array($res_niveis)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($nivel) > 0 && sizeof($nivel) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="nivel[<?php echo $ct3 ?>]" value="<?php echo $row['nivel_hierarquico'] ?>" <?= in_array($row['nivel_hierarquico'], $nivel) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['nivel_hierarquico'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="nivel[<?php echo $ct3 ?>]" value="<?php echo $row['nivel_hierarquico'] ?>"/> <span><?php echo $row['nivel_hierarquico'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct3++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($faixa) ? 'open' : 'close' ?>" id="filtro5">
                <h5>
                    <i class="fa fa-dollar" aria-hidden="true"></i> Faixa Salarial
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_faixa = mysqli_query($con, "SELECT
                                                faixa_salarial, sum(ct_faixa) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 AND faixa_salarial!='' " . $where . " group by faixa_salarial");
                            if (mysqli_num_rows($res_faixa) > 0) {
                                $ct4 = 0;
                                while ($row = mysqli_fetch_array($res_faixa)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($faixa) > 0 && sizeof($faixa) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="faixa[<?php echo $ct4 ?>]" value="<?php echo $row['faixa_salarial'] ?>" <?= in_array($row['faixa_salarial'], $faixa) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['faixa_salarial'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="faixa[<?php echo $ct4 ?>]" value="<?php echo $row['faixa_salarial'] ?>"/> <span><?php echo $row['faixa_salarial'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct4++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($contrato) ? 'open' : 'close' ?>" id="filtro6">
                <h5>
                    <i class="fa fa-file-text" aria-hidden="true"></i> Tipo de Contrato
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_contratacao = mysqli_query($con, "SELECT
                                                forma_contratacao, sum(ct_contratacao) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 AND forma_contratacao!='' " . $where . " group by forma_contratacao");
                            if (mysqli_num_rows($res_contratacao) > 0) {
                                $ct5 = 0;
                                while ($row = mysqli_fetch_array($res_contratacao)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($contrato) > 0 && sizeof($contrato) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="contrato[<?php echo $ct5 ?>]" value="<?php echo $row['forma_contratacao'] ?>" <?= in_array($row['forma_contratacao'], $contrato) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['forma_contratacao'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="contrato[<?php echo $ct5 ?>]" value="<?php echo $row['forma_contratacao'] ?>"/> <span><?php echo $row['forma_contratacao'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct5++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($turno) ? 'open' : 'close' ?>" id="filtro9">
                <h5>
                    <i class="fa fa-clock-o" aria-hidden="true"></i> Turno
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_turnos = mysqli_query($con, "SELECT
                                                turno, sum(ct_turno) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 AND turno!='' " . $where . " group by turno");
                            if (mysqli_num_rows($res_turnos) > 0) {
                                $ct6 = 0;
                                while ($row = mysqli_fetch_array($res_turnos)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($turno) > 0 && sizeof($turno) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="turno[<?php echo $ct6 ?>]" value="<?php echo $row['turno'] ?>" <?= in_array($row['turno'], $turno) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['turno'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="turno[<?php echo $ct6 ?>]" value="<?php echo $row['turno'] ?>"/> <span><?php echo $row['turno'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct6++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter <?= !empty($escolaridade) ? 'open' : 'close' ?>" id="filtro11">
                <h5>
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i> Escolaridade
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_escolaridade = mysqli_query($con, "SELECT
                                                ensino_minimo, sum(ct_ensino) as total
                                            FROM VW_VAGAS_FILTRO WHERE 1=1 AND ensino_minimo!='' " . $where . " group by ensino_minimo");
                            if (mysqli_num_rows($res_escolaridade) > 0) {
                                $ct7 = 0;
                                while ($row = mysqli_fetch_array($res_escolaridade)) {
                                    if (intval($row['total']) > 0) {
                                        if (count($escolaridade) > 0 && sizeof($escolaridade) > 0) {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="escolaridade[<?php echo $ct7 ?>]" value="<?php echo $row['ensino_minimo'] ?>" <?= in_array($row['ensino_minimo'], $escolaridade) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['ensino_minimo'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        } else {

                                            ?>
                                            <li><label class="filtrar"><input type="checkbox" name="escolaridade[<?php echo $ct7 ?>]" value="<?php echo $row['ensino_minimo'] ?>"/> <span><?php echo $row['ensino_minimo'] . " (" . $row['total'] . ")" ?></span></label></li>
                                            <?php
                                        }
                                        $ct7++;
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filter" id="filtro13">
                <button class="btn btn-filtro" type="submit">
                    Filtrar
                </button>
            </div>
        </form>
    </div>
    <div>
        <section class="publicidade">
            <div class="container">
                <span>Publicidade</span>
                <?php if (!empty($publi3['link'])) { ?>
                    <a href="<?php echo $publi3['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi3['imagem'] ?>"></a>
                <?php } else { ?>
                    <img src="<?php echo PATH_IMAGENS . '/' . $publi3['imagem'] ?>">
                <?php } ?>
            </div>
        </section>
    </div>
</div>

