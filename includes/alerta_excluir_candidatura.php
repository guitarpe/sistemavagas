<div class="box small" id="excluir-candidatura">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>Candidatura</h2>
    <form class="active" action="<?php echo PATH_ALL . '/candidatos/actions/recebe_excluircandidatura.php' ?>" method="post">

        <div class="full text-center">
            <i class="fa fa-trash mgs-excluir-cand"></i>
        </div>

        <div class="full mgs-alterar-check">
            <h4>Deseja excluir sua candidatura para esta vaga ?</h4>
        </div>

        <div class="full text-center">
            <input type="hidden" id="del_id_vaga" name="del_id_vaga">
            <input type="hidden" id="del_id_cand" name="del_id_cand">
            <button type="submit">
                Excluir
                <i class="fa fa-angle-double-right"></i>
            </button>
            <a href="#" class="btn button cancelar">
                Cancelar
            </a>
        </div>
    </form>
</div>