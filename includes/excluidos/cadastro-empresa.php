<div class="box" id="cadastro-empresa">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>
        Cadastre sua empresa
    </h2>
    <div class="steps">
        <ul>
            <li class="active step1">
                <div class="op"></div>
                <i class="fa fa-address-card" aria-hidden="true"></i>
                <span>Dados Básico</span>
            </li>
            <li class="step2">
                <div class="op"></div>
                <i class="fa fa-briefcase" aria-hidden="true"></i>
                <span>Complementares</span>
            </li>
        </ul>
    </div>
    <form class="active">
        <div class="full msg" id="msg">
            <p>
                Olá, vamos ajudar você a preencher seu cadastro, começando pelo básico. Preencha as informações abaixo e clique em próximo.
            </p>
        </div>
        <label>
            <input type="text" placeholder="Nome da empresa" id="enome">
            <i class="fa fa-diamond" aria-hidden="true"></i>
        </label>
        <div class="full">
            <label class="half">
                <input class="cnpj" type="text" placeholder="CNPJ" id="ecnpj">
                <i class="fa fa-address-card" aria-hidden="true"></i>
                <span id="msg-ecnpj" style="display:none"></span>
            </label>
            <label class="half">
                <input type="text" placeholder="Ramo de atividade" id="eramo">
                <i class="fa fa-gear" aria-hidden="true"></i>
            </label>
        </div>
        <label>
            <input type="text" placeholder="Nome completo do usuário" id="unome">
            <i class="fa fa-user" aria-hidden="true"></i>
        </label>
        <label>
            <input type="text" placeholder="E-mail" id="eemail">
            <i class="fa fa-envelope" aria-hidden="true"></i>
            <span id="msg-eemail" style="display:none"></span>
        </label>
        <div class="full">
            <label class="half">
                <input type="password" placeholder="Senha" id="esenha" maxlength="6">
                <i class="fa fa-asterisk" aria-hidden="true"></i>
                <span id="msg-senha" style="display:none">
                    A senha deve ter no mínimo 6 caracteres
                </span>
            </label>
            <label class="half">
                <input type="password" placeholder="Confirmar senha" id="csenha" maxlength="6">
                <i class="fa fa-asterisk" aria-hidden="true"></i>
                <span id="msg-csenha" style="display:none">
                    As senhas não são iguais
                </span>
            </label>
        </div>
        <label id="labeltermos" class="col-md-6">
            <input type="checkbox" id="etermos">
            <span>Li e aceito os <a href="#">Termos e Condições</a> para uso de políticas de privacidade </span>
        </label>
        <div class="full">
            <a href="" target="entrar" class="link link-js">
                Já tem seu cadastro? Faça login agora!
            </a>
            <button class="link-js-step" type="button" id="epasso2">
                Próximo passo <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </div>
    </form>
    <form action="<?php echo PATH_ALL; ?>/empresas/upload.php" method="post" enctype="multipart/form-data">
        <div class="full">
            <label class="quad3 file" style="display:none">
                <input type="file" value="Selecionar imagem" id="imagem" name="imagem" class="ver_imagem">
            </label>
            <label class="quad" id="foto-perfil" title="Alterar Imagem" onclick="$('#imagem').click()">
                <div class="image">
                    <i class="fa fa-image" id="img-icone"></i>
                </div>
            </label>
            <label class="quad3" >
                <textarea placeholder="Descrição da empresa" id="descricao"></textarea>
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
            </label>
        </div>
        <div class="full">
            <label class="half">
                <input type="text" placeholder="Número de funcionários" id="nfuncionarios">
                <i class="fa fa-group" aria-hidden="true"></i>
            </label>
            <label class="half">
                <input type="text" placeholder="Site" id="site">
                <i class="fa fa-laptop" aria-hidden="true"></i>
            </label>
        </div>
        <label>
            <input type="text" placeholder="E-mail de relacionamento da empresa" id="emaile">
            <i class="fa fa-envelope" aria-hidden="true"></i>
            <span id="msg-emaile" style="display:none"></span>
        </label>
        <div class="full">
            <label class="half">
                <input class="tel" type="text" placeholder="Telefone" id="efone">
                <i class="fa fa-phone" aria-hidden="true"></i>
            </label>
            <label class="half">
                <input class="fone" type="text" placeholder="Celular (Opcional)" id="fax">
                <i class="fa fa-phone" aria-hidden="true"></i>
            </label>
        </div>
        <div class="full">
            <label class="half">
                <input class="cep ecep" type="text" placeholder="CEP">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span id="msg-ecep" style="display:none">
                    O cep digitado é invalido!
                </span>
            </label>
            <button class="add cep2" type="button" onclick="window.open('http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCep.cfm', '_blank')">
                Não sabe seu CEP?
                <i class="fa fa-search" aria-hidden="true"></i>
            </button>
        </div>
        <label>
            <input type="text" class="elogradouro" placeholder="Logradouro">
            <i class="fa fa-road" aria-hidden="true"></i>
        </label>
        <div class="full">
            <label class="half">
                <input type="text" placeholder="Número">
                <i class="fa fa-home" aria-hidden="true"></i>
            </label>
            <label class="half">
                <input type="text" placeholder="Complemento">
                <i class="fa fa-asterisk" aria-hidden="true"></i>
            </label>
        </div>
        <label>
            <input type="text" class="ebairro" placeholder="Bairro">
            <i class="fa fa-asterisk" aria-hidden="true"></i>
        </label>
        <div class="full">
            <label class="half">
                <select name="estado" class="eestado buscar_cidades">
                    <option disabled selected value="">Estado</option>
                    <?php
                    $result = mysqli_query($con, "SELECT * FROM TB_VV_ESTADOS ORDER BY nome ASC");
                    while ($val = mysqli_fetch_array($result)) {
                        echo "<option value='$val[uf]'>$val[nome]</option>";
                    }

                    ?>
                </select>
                <i class="fa fa-map" aria-hidden="true"></i>
            </label>
            <label class="half">
                <select name="ecidade" class="cidade ecidade">
                    <option disabled selected value="">Cidade</option>
                </select>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </label>
        </div>
        <div class="full">
            <a href="" target="step1" class="link link-js-step back">
                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Voltar
            </a>
            <button type="button" id="epasso3">
                Finalizar cadastro <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
            <input type="submit" id="upload" style="display: none">
        </div>
    </form>
</div>