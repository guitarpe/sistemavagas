<div class="box small" id="msg-sair">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>Sair</h2>
    <form class="active">
        <div class="full msg-alterar">
            <span>Deseja se desconectar do portal?</span>
        </div>
        <div class="full pd-5 text-center">
            <a href="<?php echo PATH_ALL . '/sair.php' ?>" class="btn button">
                Sair
            </a>
            <a href="#" class="btn button cancelar">
                Cancelar
            </a>
        </div>
    </form>
</div>