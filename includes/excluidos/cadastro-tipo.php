<div class="box small" id="cadastro-tipo">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>Cadastre-se</h2>
    <div class="col-sm-12 text-center">
        <a href="<?php echo PATH_ALL . '/cadastrocandidato.php' ?>" class="btn btn-icon button btn-tipo">
            <i class="fa fa-user"></i>
            Cadastre-se como Candidato
        </a>
        <a href="<?php echo PATH_ALL . '/cadastroempresa.php' ?>" class="btn btn-icon button btn-tipo">
            <i class="fa fa-building-o"></i>
            Cadastre-se como Empresa
        </a>
    </div>
</div>

