<div class="box" id="cadastro">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>
        Cadastre seu currículo
    </h2>
    <div class="steps">
        <ul>
            <li class="active step1">
                <div class="op"></div>
                <i class="fa fa-user" aria-hidden="true"></i>
                <span>Dados Pessoais</span>
            </li>
            <li class="step2">
                <div class="op"></div>
                <i class="fa fa-briefcase" aria-hidden="true"></i>
                <span>Perfil Profissional</span>
            </li>
            <li class="step3">
                <div class="op"></div>
                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                <span>Conhecimento</span>
            </li>
        </ul>
    </div>
    <form class="active">
        <div class="full msg">
            <p>
                Olá, vamos ajudar você a preencher seu cadastro, começando pelo básico. Preencha as informações abaixo e clique em próximo.
            </p>
        </div>
        <div class="full">
            <label class="half">
                <input type="text" placeholder="E-mail" id="email">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span id="msg-email" style="display:none"></span>
            </label>
            <label class="half">
                <input type="text" placeholder="Confirmar e-mail" id="cemail">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span id="msg-cemail" style="display:none">
                    Os e-mails não são iguais
                </span>
            </label>
        </div>
        <div class="full">
            <label class="half">
                <input type="password" placeholder="Senha" name="senha" id="senha">
                <i class="fa fa-asterisk" aria-hidden="true"></i>
                <span id="msg-senhaa" style="display:none">
                    A senha deve ter no mínimo 6 caracteres
                </span>
            </label>
            <label class="half">
                <input type="password" placeholder="Confirmar senha" name="confirmar-senha" id="confirmar-senha">
                <i class="fa fa-asterisk" aria-hidden="true"></i>
                <span id="msg-senhac" style="display:none">
                    As senhas não são iguais
                </span>
            </label>
        </div>
        <label>
            <input type="text" placeholder="Nome" name="nome" id="nome">
            <i class="fa fa-user" aria-hidden="true"></i>
        </label>
        <div class="full">
            <label class="half">
                <input class="cpf" type="text" placeholder="CPF" name="cpf" id="cpf">
                <i class="fa fa-id-card" aria-hidden="true"></i>
                <span id="msg-cpf" style="display:none"></span>
            </label>
            <label class="half">
                <input class="data" type="text" placeholder="Data de Nascimento" name="datanasc" id="datanasc">
                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                <span id="msg-data" style="display:none"></span>
            </label>
        </div>
        <div class="full">
            <label class="half">
                <span>Sexo:</span>
                <label>
                    <input type="radio" name="sexo" value="Masculino" checked>
                    Masculino
                </label>
                <label>
                    <input type="radio" name="sexo" value="Feminino">
                    Feminino
                </label>
                <i class="fa fa-venus-mars" aria-hidden="true"></i>
            </label>
            <label class="half">
                <select id="ddd" style="width: 20%; padding: 0 0 0 10px">
                    <option disabled selected value="0">DDD</option>
                    <?php
                    $res_prefixos = mysqli_query($con, "SELECT descricao, estado FROM TB_VV_PREFIXOS");
                    while ($prefixo = mysqli_fetch_array($res_prefixos)) {
                        echo "<option title='$prefixo[estado]'>$prefixo[descricao]</option>";
                    }

                    ?>
                </select>
                <input class="fone-s" type="text" placeholder="Telefone" name="fone" id="fone" style="width:78%; float:right">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span id="msg-fone" style="display:none; margin-left: 22%"></span>
            </label>
        </div>
        <div class="full">

            <label class="half">
                <select name="estado" id="estado1" class="busca_cidades">
                    <option disabled selected value="">Estado</option>
                    <?php
                    $result1 = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                    while ($val = mysqli_fetch_array($result1)) {
                        echo "<option value='$val[uf]'>$val[nome]</option>";
                    }

                    ?>
                </select>
                <i class="fa fa-map" aria-hidden="true"></i>
            </label>
            <label class="half">
                <select name="cidade" class=" cidades" id="cidade">
                    <option disabled selected value="">Cidade</option>
                </select>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </label>
        </div>
        <label id="lbltermos" class="col-md-6">
            <input type="checkbox" id="termos">
            <span>Li e aceito os <a href="#">Termos e Condições</a> para uso de políticas de privacidade </span>
        </label>
        <div class="full">
            <a href="" target="entrar" class="link link-js">
                Já tem seu cadastro? Faça login agora!
            </a>
            <button class="link-js-step" type="button" id="passo1">
                Próximo passo <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </div>

        <a href="" target="cadastro-empresa" class="empresa link-js">
            Você quer cadastrar a sua empresa e as vagas disponíveis? Clique aqui!
        </a>
    </form>
    <form>
        <div class="full msg">
            <p>
                Vamos falar sobre o seu perfil profissional? Estamos especialmente interessados em conhecer suas experiências anteriores e qual Vaga de Emprego você está procurando, assim poderemos lhe ajudar melhor.
                Selecione seu Cargo Profissional e clique no botão INCLUIR.
                Você pode incluir mais de um Cargo Profissional.
            </p>
        </div>
        <div class="full" id="cargos">
            <strong>Cargo profissional</strong>
            <span>
                Esse é o campo de objetivo do seu currículo, muito importante ter foco, por isso selecione de um até três no máximo.
            </span>
            <div class="full" id="objprof">
                <label class="half" id="label-cargo">
                    <select id="cargoprofissional">
                        <option disabled selected value="">Cargo Profissional</option>
                        <?php
                        $result2 = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                        while ($val = mysqli_fetch_array($result2)) {
                            echo "<option>$val[nome]</option>";
                        }

                        ?>
                        <option>Outro</option>
                    </select>
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                </label>
                <label class="third" id="label-ncargo" style="display:none">
                    <input type="text" placeholder="Digite o Cargo Profissional">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </label>
                <button class="add" id="incluircargoprofissional" type="button">
                    <i class="fa fa-plus" aria-hidden="true"></i> Incluir
                </button>
            </div>
        </div>
        <div class="full">
            <strong>
                Cargos profissionais cadastrados
            </strong>
            <ul id="cargosgrid">
                <li>Nenhum cargo profissional cadastrado.</li>
            </ul>
        </div>
        <div class="full border">
            <strong class="mb15">
                <span>Você ja possui experiência profissional?</span>
                <label>
                    <input name="experiencia" type="radio" class="yn" value="Não" checked>
                    <span>Não</span>
                </label>
                <label>
                    <input name="experiencia" type="radio" class="yn" value="Sim">
                    <span>Sim</span>
                </label>
            </strong>
        </div>
        <div class="experiencia off">
            <div class="full msg" id="msg3">
                <p>
                    Legal! Agora, nos descreva brevemente suas últimas experiências profissionais, ou as experiências profissionais mais importantes, que mais agregaram na sua carreira.
                    Preencha as informações sobre sua Experiência Profissional e clique no botão INCLUIR EXPERIÊNCIA.
                    Você pode incluir mais de uma Experiência Profissional.
                </p>
            </div>
            <div class="full" id="experiencias">
                <label class="half">
                    <input type="text" placeholder="Nome da empresa" name="nomeempresa" id="nomeempresa">
                    <i class="fa fa-diamond" aria-hidden="true"></i>
                </label>
                <label class="half">
                    <input class="dinheiro" type="text" placeholder="Salário (opcional)" name="salario" id="salario" style="padding: 0 0 0 10px">
                    <i class="fa fa-dollar" aria-hidden="true"></i>
                </label>
                <div class="full">
                    <label class="full" id="label-cargoexp">
                        <select id="cargoempresa">
                            <option disabled selected value="">Cargo Profissional</option>
                            <?php
                            $result3 = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                            while ($val = mysqli_fetch_array($result3)) {
                                echo "<option>$val[nome]</option>";
                            }

                            ?>
                            <option>Outro</option>
                        </select>
                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                    </label>
                    <label class="half" id="label-ncargoexp" style="display:none">
                        <input type="text" placeholder="Digite o Cargo Profissional" id="novocargoexp" >
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </label>
                </div>
                <label>
                    <textarea placeholder="Atividades Exercidas" style="height:80px"></textarea>
                </label>
                <div class="full">
                    <div class="half">
                        <label class="half">
                            <input class="data-s" type="text" placeholder="Início (mês/ano)" name="datainicio" id="datainicio" style="padding: 0 0 0 10px">
                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            <span id="msg-dataexp" style="display:none"></span>
                        </label>
                        <label class="half">
                            <input class="data-s" type="text" placeholder="Fim (mês/ano)" name="datafim" id="datafim">
                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            <span id="msg-dataexp2" style="display:none"></span>
                        </label>
                    </div>
                    <div class="half">
                        <label class="half">
                            <input type="checkbox" id="emprego-atual">
                            <span>Emprego atual</span>
                        </label>
                        <button class="add" id="incluirexperiencia" type="button">
                            <i class="fa fa-plus" aria-hidden="true"></i> Incluir
                        </button>
                    </div>
                </div>
            </div>
            <div class="full">
                <strong>
                    Experiências cadastradas
                </strong>
                <ul class="edit" id="expgrid">
                    <li>Nenhuma experiência profissional cadastrada.</li>
                </ul>
            </div>
        </div>
        <div class="full">
            <a href="" target="step_cur1" class="link link-js-step back">
                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Voltar
            </a>
            <button id="passo2" target="step_cur3" class="link-js-step" type="button">
                Próximo passo <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </div>
    </form>
    <form id="step_cur3">
        <div class="full msg">
            <p>
                Sabemos que no mercado de trabalho atual, competitivo, é extremamente importante estarmos em constante atualização, inclusive dos nossos conhecimentos técnicos (saiba mais em <a href="https://www.educaretransformar.com.br/" target="_blank">www.educaretransformar.com.br</a>).
            </p>
        </div>
        <div class="full">
            <strong>Preencha abaixo a sua formação acadêmica/técnica:</strong>
            <div class="full">
                <label class="trd">
                    <select name="formacao" id="formacao">
                        <option disabled selected value="">Formação</option>
                        <?php
                        $result4 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                        while ($val = mysqli_fetch_array($result4)) {
                            echo "<option>$val[descricao]</option>";
                        }

                        ?>
                    </select>
                    <i class="fa fa-level-up" aria-hidden="true"></i>
                </label>
                <label class="trd">
                    <input type="text" placeholder="Instituição de ensino" name="instituicao" id="instituicao">
                    <i class="fa fa-university" aria-hidden="true"></i>
                </label>
                <label class="trd">
                    <select name="situacao" id="situacao">
                        <option disabled selected value="">Situação</option>
                        <?php
                        $result5 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                        while ($val = mysqli_fetch_array($result5)) {
                            if ($val["nome"] == "Trancado") {
                                echo "<option id='trancado'>$val[nome]</option>";
                            } else {
                                echo "<option>$val[nome]</option>";
                            }
                        }

                        ?>
                    </select>
                    <i class="fa fa-check" aria-hidden="true"></i>
                </label>
            </div>
            <label id="label-curso" style="display:none">
                <input type="text" placeholder="Nome do curso" id="nomeformacao">
                <i class="fa fa-bookmark" aria-hidden="true"></i>
            </label>
            <div class="full">
                <label class="trd" id="label-prev" style="display:none">
                    <input class="data-ano" type="text" placeholder="Ano de conclusão" name="anofim" id="anofim">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <span id="msg-datacurso3" style="display:none"></span>
                </label>
                <label class="trd" id="label-semestre" style="display:none">
                    <select id="semestre">
                        <option disabled selected value="">Semestre</option>
                        <option value="1">1º semestre</option>
                        <option value="2">2º semestre</option>
                        <option value="3">3º semestre</option>
                        <option value="4">4º semestre</option>
                        <option value="5">5º semestre</option>
                        <option value="6">6º semestre</option>
                        <option value="7">7º semestre</option>
                        <option value="8">8º semestre</option>
                        <option value="9">9º semestre</option>
                        <option value="10">10º semestre</option>
                        <option value="11">11º semestre</option>
                        <option value="12">12º semestre</option>
                        <option value="13">13º semestre</option>
                        <option value="14">14º semestre</option>
                        <option value="15">15º semestre</option>
                        <option value="16">16º semestre</option>
                    </select>
                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                </label>
                <label class="trd" id="label-turno" style="display:none">
                    <select name="turno">
                        <option disabled selected value="">Turno</option>
                        <option>Manhã</option>
                        <option>Tarde</option>
                        <option>Noite</option>
                        <option>EAD</option>
                    </select>
                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                </label>
                <div class="third">
                    <button class="add" id="incluirformacao" type="button">
                        <i class="fa fa-plus" aria-hidden="true"></i> Incluir
                    </button>
                </div>
            </div>
        </div>
        <div class="full">
            <strong>
                Formações cadastradas
            </strong>
            <ul class="edit" id="formacoesgrid">
                <li>Nenhuma formação cadastrada.</li>
            </ul>
        </div>
        <div class="full border">
            <strong>Preencha abaixo os seus cursos extracurriculares:</strong>
            <label>
                <input type="text" placeholder="Nome do curso" name="nomecurso" id="nomecurso">
                <i class="fa fa-bookmark" aria-hidden="true"></i>
            </label>
            <div class="full">
                <label class="half">
                    <input type="text" placeholder="Instituição de ensino" name="instituicao2" id="instituicao2">
                    <i class="fa fa-university" aria-hidden="true"></i>
                </label>
                <label class="half">
                    <select name="situacao2" id="situacao2">
                        <option disabled selected value="">Situação</option>
                        <?php
                        $result6 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                        while ($val = mysqli_fetch_array($result6)) {
                            echo "<option>$val[nome]</option>";
                        }

                        ?>
                    </select>
                    <i class="fa fa-check" aria-hidden="true"></i>
                </label>
            </div>
            <div class="full">
                <div class="quad3">
                    <label class="half" id="label-iniciocurso">
                        <input class="data-s" type="text" placeholder="Início (mês/ano)" name="iniciocurso" id="iniciocurso" style="padding: 0 0 0 10px">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <span id="msg-datacurso" style="display:none"></span>
                    </label>
                    <label class="half" id="label-fimcurso">
                        <input class="data-s" type="text" placeholder="Fim (mês/ano)" name="fimcurso" id="fimcurso">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <span id="msg-datacurso2" style="display:none"></span>
                    </label>
                </div>
                <button class="add" id="incluircursos" type="button">
                    <i class="fa fa-plus" aria-hidden="true"></i> Incluir
                </button>
            </div>
        </div>
        <div class="full">
            <strong>
                Cursos cadastrados
            </strong>
            <ul class="edit" id="cursosgrid">
                <li>Nenhum curso cadastrado.</li>
            </ul>
        </div>
        <div class="full border">
            <strong>
                Idiomas
            </strong>
            <label class="third">
                <select name="idioma" id="idioma">
                    <option disabled selected value="">Idioma</option>
                    <?php
                    $result7 = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                    while ($val = mysqli_fetch_array($result7)) {
                        echo "<option>$val[descricao]</option>";
                    }

                    ?>
                </select>
                <i class="fa fa-flag" aria-hidden="true"></i>
            </label>
            <label class="third">
                <select name="nivelidioma" id="nivelidioma">
                    <option disabled selected value="">Nível</option>
                    <?php
                    $result = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                    while ($val = mysqli_fetch_array($result)) {
                        echo "<option>$val[descricao]</option>";
                    }

                    ?>
                </select>
                <i class="fa fa-flag" aria-hidden="true"></i>
            </label>
            <button class="add" id="incluiridioma" type="button">
                <i class="fa fa-plus" aria-hidden="true"></i> Incluir
            </button>
        </div>
        <div class="full">
            <strong>
                Idiomas cadastrados
            </strong>
            <ul id="idiomasgrid">
                <li>Nenhum idioma cadastrado.</li>
            </ul>
        </div>

        <div class="full">
            <a href="" target="step_cur2" class="link link-js-step back">
                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Voltar
            </a>
            <button id="cadastrarcurriculo" type="button">
                Cadastrar <i class="fa fa-angle-double-right" aria-hidden="true" id="icadastrarcurriculo"></i>
            </button>
        </div>
    </form>
</div>