<?php
require_once 'defines.php';

class Funcoes
{

    function __construct()
    {

    }

    function removeracentos($str)
    {

        $str = strtolower($str);

        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = preg_replace('/_+/', '_', $str);
        return $str;
    }

    function senhamd5($tipo, $senha, $email)
    {
        return strtoupper(md5($tipo . $email . $senha));
    }

    function returnValor($str)
    {
        return str_replace(",", ",", str_replace(".", "", $str));
    }

    function trocarAspas($texto)
    {
        return str_replace('"', "'", $texto);
    }

    function formataData($str, $add = null)
    {
        if (empty($add)) {
            $data = new DateTime("$str");
        } else {
            $data = new DateTime("$str");
            $data->add(new DateInterval("P" . $add . "M"));
        }
        return $data->format('d/m/Y');
    }

    function multiexplode($delimiters, $string)
    {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }

    function dec_enc($action, $string)
    {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '12DF4RGT7U';
        $secret_iv = 'HGTR43ED78';

        $key = hash('sha256', $secret_key, true);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 1) {
            $output = openssl_encrypt($string, $encrypt_method, $key, OPENSSL_RAW_DATA, $iv);
            $output = $this->safe_b64encode($output);
        } else if ($action == 2) {
            $output = openssl_decrypt($this->safe_b64decode($string), $encrypt_method, $key, OPENSSL_RAW_DATA, $iv);
        }

        return $output;
    }

    private function safe_b64encode($string)
    {

        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    private function safe_b64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public static function redir($url = "", $tipo = null)
    {
        if ($tipo == 1) {
            header('location: ' . $url);
        } else {
            header('location: ' . PATH_ALL . '/' . $url);
        }
        exit();
    }

    public static function alert($situacao = '', $acao = 'acao', $usuario = null)
    {
        $_SESSION[$acao] = TRUE;
        $_SESSION["acao-class"] = $situacao;

        if (!empty($usuario)) {
            $_SESSION["acao-user"] = $usuario;
        } else {
            $_SESSION["acao-user"] = null;
        }
    }

    function get_client_ip()
    {

        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }
}
