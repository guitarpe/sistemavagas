<head>
    <meta charset="utf-8">
    <meta name="description" content="<?php echo!isset($description) ? 'Vagas e Vagas - Encontre seu emprego aqui' : $description ?>">
    <meta name="keywords" content="emprego,job,oemprego,vagas,trabalhe,jobs,trabalho,mercado,vaga">
    <meta name="author" content="Foottsdev tecnologia">
    <meta property="og:url" content="<?php echo!isset($url) ? 'https://www.vagasevagas.com.br' : $url ?>" />
    <meta property="og:title" content="<?php echo!isset($title) ? 'Vagas e Vagas - Encontre seu emprego aqui' : 'Vagas e Vagas - Vaga: ' . $title ?>"/>
    <meta property="og:description" content="<?php echo!isset($description) ? 'Vagas e Vagas - Encontre seu emprego aqui' : $description ?>">
    <meta property="og:image" content="<?php echo PATH_ASSETS . '/img/logo-menor.jpg' ?>">
    <meta property="og:type" content="job" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:locale:alternate" content="en_US" />
    <meta property="og:locale:alternate" content="es_ES" />
    <meta name="twitter:card" content="<?php echo!isset($title) ? 'Vagas e Vagas - Encontre seu emprego aqui' : 'Vagas e Vagas - Vaga: ' . $title ?>"/>
    <meta name="twitter:site" content="<?php echo!isset($url) ? 'https://www.vagasevagas.com.br' : $url ?>"/>
    <meta name="twitter:creator" content="Foottsdev tecnologia"/>

    <title><?php echo!isset($title) ? 'Vagas e Vagas - Encontre seu emprego aqui' : 'Vagas e Vagas - Vaga: ' . $title ?></title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?= PATH_FAVICONS ?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= PATH_FAVICONS ?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= PATH_FAVICONS ?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= PATH_FAVICONS ?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= PATH_FAVICONS ?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= PATH_FAVICONS ?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= PATH_FAVICONS ?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= PATH_FAVICONS ?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= PATH_FAVICONS ?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= PATH_FAVICONS ?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= PATH_FAVICONS ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= PATH_FAVICONS ?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= PATH_FAVICONS ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= PATH_FAVICONS ?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= PATH_FAVICONS ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= PATH_ASSETS ?>/css/jquery-ui.css">
    <link href="<?= PATH_ASSETS ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= PATH_ASSETS ?>/bootstrap/css/bootstrap-multiselect.css" />
    <link rel="stylesheet" href="<?= PATH_ASSETS ?>/plugins/Uikit/notify-2.27.2.css">
    <link rel="stylesheet" href="<?= PATH_ASSETS ?>/css/geral/geral.css">
    <link rel="stylesheet" href="<?= PATH_ASSETS ?>/css/geral/geral-mobile.css">
    <link rel="stylesheet" href="<?= PATH_ASSETS ?>/css/geral/wizard.css">
    <script type = "text/javascript" src = "<?= PATH_ASSETS ?>/js/jquery/jquery.min.js" ></script>

    <?php if (false) { ?>
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '{703064670042109}',
                    cookie: true,
                    xfbml: true,
                    version: '{api-version}'
                });

                FB.AppEvents.logPageView();

            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12&appId=122534665025110&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php } ?>

<?php
$res_publi1 = mysqli_query($con, "SELECT imagem, titulo, link, posicao FROM TB_VV_PUBLICIDADE WHERE posicao=1 ORDER BY RAND() LIMIT 1");
$res_publi2 = mysqli_query($con, "SELECT imagem, titulo, link, posicao FROM TB_VV_PUBLICIDADE WHERE posicao=1 ORDER BY RAND() LIMIT 1");
$res_publi3 = mysqli_query($con, "SELECT imagem, titulo, link, posicao FROM TB_VV_PUBLICIDADE WHERE posicao=2 ORDER BY RAND() LIMIT 1");
$res_publi4 = mysqli_query($con, "SELECT imagem, titulo, link, posicao FROM TB_VV_PUBLICIDADE WHERE posicao=2 ORDER BY RAND() LIMIT 1");

$publi1 = mysqli_fetch_array($res_publi1);
$publi2 = mysqli_fetch_array($res_publi2);
$publi3 = mysqli_fetch_array($res_publi3);
$publi4 = mysqli_fetch_array($res_publi4);

?>
</head>