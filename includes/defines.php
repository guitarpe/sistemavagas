<?php
$url = filter_input(INPUT_SERVER, 'HTTP_HOST');

if ($url === "localhost") {
    define('URL', 'http://localhost');
    $diretorio = 'portfolio/sistemavagas';

    define('DIRETORIO', '/portfolio/sistemavagas');
    define('PATH_ALL', '/' . $diretorio);
    define('PATH_ASSETS', '/' . $diretorio . '/assets');
    define('PATH_INCLUDES', '/' . $diretorio . '/includes');
    define('PATH_CANDIDATOS', '/' . $diretorio . '/candidatos');
    define('PATH_EMPRESAS', '/' . $diretorio . '/empresas');
    define('PATH_ADMIN', '/' . $diretorio . '/admin');
    define('PATH_IMAGENS', '/' . $diretorio . '/images');
    define('PATH_FAVICONS', '/' . $diretorio . '/assets/img/favicons');

    define('PASTAASSETS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'assets');

    define('JS_DATATABLE_URL', '/' . $diretorio . '/assets/plugins/dataTable');
    define('URL_CSS_DATATABLE', '/' . $diretorio . '/assets/css/dataTable');
    define('URL_CSS_UIKIT_PATH', '/' . $diretorio . '/assets/css/Uikit');
    define('URL_JS_UIKIT_PATH', '/' . $diretorio . '/assets/js/Uikit');

    define('PATH_EMP_IMAGENS', '/' . $diretorio . '/empresas/images');
    define('PATH_CAN_IMAGENS', '/' . $diretorio . '/candidatos/images');

    define("HOME", "http://localhost");
    define("FACEBOOK", "#");
    define("TWITTER", "#");
    define("PLUS", "#");
} else {
    define('URL', 'https://www.vagasevagas.com.br');
    $diretorio = '';

    define('DIRETORIO', '');
    define('PATH_ALL', '');
    define('PATH_ASSETS', '/assets');
    define('PATH_INCLUDES', '/includes');
    define('PATH_CANDIDATOS', '/candidatos');
    define('PATH_EMPRESAS', '/empresas');
    define('PATH_ADMIN', '/admin');
    define('PATH_IMAGENS', '/images');
    define('PATH_FAVICONS', '/assets/img/favicons');

    define('PASTAASSETS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'assets');

    define('JS_DATATABLE_URL', '/assets/plugins/dataTable');
    define('URL_CSS_DATATABLE', '/assets/css/dataTable');
    define('URL_CSS_UIKIT_PATH', '/assets/css/Uikit');
    define('URL_JS_UIKIT_PATH', '/assets/js/Uikit');

    define('PATH_EMP_IMAGENS', '/empresas/images');
    define('PATH_CAN_IMAGENS', '/candidatos/images');

    define("HOME", "https://www.vagasevagas.com.br");
    define("FACEBOOK", "#");
    define("LINKEDIN", "#");
    define("WHATSAPP", "#");
    define("TWITTER", "#");
    define("PLUS", "#");
}

define('SYSTEM_PATH', substr(realpath(dirname(__FILE__)), 0, -8));

define('HOST', '177.11.50.91');
define('USER', 'oemsagedu');
define('PASS', 'bEp04nAF80d');
define('DB', 'vagasevagas');
define('PORT', 3306);
