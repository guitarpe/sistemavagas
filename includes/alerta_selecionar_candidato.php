<div class="box small" id="alerta-selecionar_candidato">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>Selecionar Candidato</h2>
    <form class="active">
        <div class="full text-center">
            <i class="fa fa-check" style="font-size:42px; color: green; margin-bottom: 10px"></i>
        </div>
        <div class="full" style="margin-bottom: 20px; text-align:center;">
            <span>Candidato selecionado com sucesso!</span>
        </div>
        <div class="full">
            <button type="button" onclick="location.reload()">
                Continuar
                <i class="fa fa-angle-double-right"></i>
            </button>
        </div>
    </form>
</div>