<?php
include "conexao.php";

$estado = filter_input(INPUT_POST, 'estado');

$sql_cidades = "SELECT
                    cid.nome
                FROM TB_VV_CIDADES cid
                    INNER JOIN TB_VV_ESTADOS est ON est.id = cid.id_estado
                WHERE est.uf = '" . $estado . "' ORDER BY cid.nome ASC";

$res = mysqli_query($con, $sql_cidades) or die(mysqli_error($con));

while ($array = mysqli_fetch_array($res)) {
    echo $array["nome"] . ";";
}