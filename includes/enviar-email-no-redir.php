<?php
include_once 'defines.php';

require_once SYSTEM_PATH . '/vendor/src/PHPMailer.php';

date_default_timezone_set('America/Sao_Paulo');

error_reporting(E_ALL);
ini_set('display_errors', 1);

try {

    if (isset($dados['COPY'])) {
        $mails_copias = explode(", ", $dados['TO_MAIL']);
        $text_copias = explode(", ", $dados['TO_TXT']);
    }

    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->Host = $dados['HOST'];
    $mail->SMTPAuth = true;
    $mail->Username = $dados['USER'];
    $mail->Password = $dados['PASS'];
    $mail->SMTPSecure = 'TLS';
    $mail->Port = $dados['PORT'];
    $mail->CharSet = "UTF-8";
    $mail->setFrom($dados['FROM_MAIL'], $dados['FROM_TXT']);

    if (!isset($dados['COPY'])) {
        $mail->addAddress($dados['TO_MAIL'], $dados['TO_TXT']);
    } else {
        foreach ($mails_copias as $key => $value) {
            $mail->addAddress($value, $text_copias[$key]);
        }
    }

    $mail->isHTML(true);
    $mail->Subject = $dados['SUBJECT'];
    $mail->MsgHTML($dados['MENSAGE']);

    $mail->send();

    echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
} catch (Exception $e) {
    echo $e->getMessage();
}

?>
<form class="active">
    <div class="full text-center">
        <i class="fa fa-check" style="font-size:40px; color: green; margin-bottom: 10px"></i>
    </div>
    <div class="full" style="margin-bottom: 20px; text-align:center;">
        <span>Seu currículo foi enviado com sucesso!</span>
    </div>
</form>
<script>
    $(document).ready(function () {
        console.log("teste");
        $(document).find("a.descandidatar").removeClass("hidden");
    });
</script>
