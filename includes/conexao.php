<?php
include_once 'defines.php';
include_once 'Funcoes.php';
include 'Database.php';

$con = mysqli_connect(HOST, USER, PASS, DB);

mysqli_set_charset($con, "utf8");

//pegar as configurações do sistema
$res_confguracoes = mysqli_query($con, "SELECT
                                            MAX(id) AS ID, SMTP_HOST, SMTP_USER, SMTP_PASS,
                                            SMTP_PORT, MAIL_FROM, MAIL_COPY, COPY_TXT
                                        FROM TB_VV_CONFIG") or die(mysqli_error($con));
$configuracoes_sis = mysqli_fetch_array($res_confguracoes);

//conexão com PDO
$conn = new Database(HOST, PORT, DB, USER, PASS);

$tipo_cad = '';
