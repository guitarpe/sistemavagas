<?php
session_start();

include "includes/conexao.php";
$func = new Funcoes();

$email = filter_input(INPUT_POST, 'email');
$tipo = filter_input(INPUT_POST, 'tipo');

if ($tipo == 'empresa') {

    $res = mysqli_query($con, "SELECT us.id, us.email, us.nome, us.recebe_email FROM TB_VV_EMPRESAS emp INNER JOIN TB_VV_USUARIOS us ON us.email=emp.email AND us.tipo='$tipo' WHERE emp.email = '$email'");
    $usuario = mysqli_fetch_array($res);
} elseif ($tipo == 'candidato') {

    $res = mysqli_query($con, "SELECT us.id, us.email, us.nome, us.recebe_email FROM TB_VV_CANDIDATOS can INNER JOIN TB_VV_USUARIOS us ON us.email=can.email AND us.tipo='$tipo' WHERE can.email = '$email'");
    $usuario = mysqli_fetch_array($res);
} elseif ($tipo == 'admin') {

    $res = mysqli_query($con, "SELECT us.id, us.email, us.nome, us.recebe_email FROM TB_VV_USUARIOS us WHERE us.email = '$email' AND us.tipo='$tipo'");
    $usuario = mysqli_fetch_array($res);
}

$to = $usuario['email'];
$to_txt = $usuario['nome'];

$codigo = rand(1000, 9999);
$data = date('Y-m-d');
$hora = date('H:i:s');

$sql_recuperacao = mysqli_query($con, "insert into TB_VV_RECUPERACAO (email, codigo, data, hora) VALUES ('$to', '$codigo', '$data', '$hora')");

$subject = "Recuperação de Senha no Vagas e Vagas";

$message = '
    <html>
    <head>
    </head>
    <body>
        <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
            <img src="' . HOME . '/images/top.png">
            <div class="container" style="padding:20px">
                <table>
                    <tr>
                        <td>
                            <h2><b></b></h2>
                            <br/>Olá ' . $usuario['nome'] . '
                            <br/>
                            <br/>
                            Código de Recuperação é: ' . $codigo . '.
                            <br/>
                            <br/>
                            <a href="' . HOME . '/recuperarsenha.php?codigo=' . $codigo . '&email=' . $to . '&tipo=' . $tipo . '" style="text-decoration:none">
                                Clique aqui para Alterar sua Senha
                            </a>
                            <br/>
                            <br/>
                            Equipe Vagas e Vagas.
                        </td>
                    </tr>
                </table>
                <div class="row" align="right">
                    <a href="' . FACEBOOK . '" style="text-decoration:none">
                        <img src="' . HOME . '/images/facebook.png">
                    </a>
                    <a href="' . TWITTER . '" style="text-decoration:none">
                        <img src="' . HOME . '/images/twitter.png">
                    </a>
                    <a href="' . PLUS . '" style="text-decoration:none">
                        <img src="' . HOME . '/images/googlep.png">
                    </a>
                </div>
            </div>
            <img src="' . HOME . '/images/top.png">
        <div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $usuario['id'] . '&tipo=candidato" target="_blank">Não quero receber mais mensagens</a></div>
            </div>
    </body>
    </html>';

if (intval($usuario['recebe_email']) == 1) {
    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $to_txt,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    include_once 'includes/enviar-email-to-redir.php';
}

?>
<html>
    <?php include "includes/cabecalho.php" ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>

        <section class="miolo-conteudo">
            <div class="area-institucional">

                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi1['link'])) { ?>
                            <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
                <section class="institucional">
                    <h1 class="text-center">Esqueceu sua senha?</h1>
                    <p class="text-center">
                    <div class="box-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <?php
                                    if (mysqli_num_rows($res) == 0) {

                                        echo "<h4>E-mail não encontrado.</h4>";
                                    } else {

                                        echo "<h4>E-mail enviado para efetuar a recuperação de sua senha.</h4>";
                                    }

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </p>
                </section>

                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi2['link'])) { ?>
                            <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
