<?php
session_start();

include "includes/conexao.php";

$res_noticia = mysqli_query($con, "SELECT *  FROM TB_VV_NOTICIAS ORDER BY id DESC LIMIT 3");
$res_uf = mysqli_query($con, "SELECT COUNT(*) numero_vagas, nome FROM VW_VAGAS GROUP BY estado ORDER BY nome asc");
$res_estados = mysqli_query($con, "SELECT nome, uf FROM TB_VV_ESTADOS ORDER BY uf ASC");

$estados = [];

while ($row = mysqli_fetch_array($res_estados)) {
    array_push($estados, $row['nome']);
}

$vagas = [];
$res = mysqli_query($con, "SELECT
                                distinct id, img_empresa, nome_empresa, sum(numero_vagas) as numero_vagas
                            FROM VW_VAGAS
                            WHERE img_empresa!=''
                                GROUP BY id_empresa
                            ORDER BY RAND() limit 12");

while ($row = mysqli_fetch_array($res)) {
    $dados = [
        'id' => $row['id'],
        'imagem' => $row['img_empresa'],
        'nome_empresa' => $row['nome_empresa'],
        'numero_vagas' => $row['numero_vagas']
    ];

    array_push($vagas, $dados);
}

$oportunidades = array_chunk($vagas, 4, true);

$tipo_cad = "index";

?>

<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php"; ?>
        </header>
        <section class="container-banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <h1>
                            SEU FUTURO COMEÇA AGORA!
                        </h1>
                        <form action="<?php echo PATH_ALL . '/buscar-vagas.php?tipo=1' ?>" method="get">
                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                            <input type="text" list="listacargos" placeholder="Digite o cargo de seu interesse" class="vaga cargobusca" name="cargo[0]">
                            <datalist id="listacargos">

                            </datalist>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <select class="estado" name="estado[0]" required>
                                <option disabled selected value="">Estado</option>
                                <?php foreach ($estados as $row) { ?>
                                    <option><?php echo $row ?></option>
                                <?php } ?>
                            </select>
                            <button>
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 sell">
                        <h2>
                            Envie seu currículo<br>
                            <span>
                                para as melhores empresas!
                            </span>
                        </h2>
                        <p>
                            O Vagas&Vagas se importa com você e com
                        </p>
                        <p>os seus sonhos, e nosso maior desejo é</p>
                        <p> fazermos parte deles!</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="miolo">
            <div class="container">
                <div class="row">
                    <?php if (!isset($_GET['activate']) || $_GET['activate'] == true) { ?>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <ul class="news">
                                    <?php while ($noticias = mysqli_fetch_array($res_noticia)) { ?>
                                        <li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <a href="">
                                                <figure>
                                                    <img src="<?php echo PATH_IMAGENS . '/' . $noticias['imagem'] ?>">
                                                </figure>
                                                <h5><?php echo $noticias['titulo'] ?></h5>
                                                <p><?php echo $noticias['descricao'] ?></p>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 por-estado">

                            <h2>
                                <strong>Vagas&Vagas</strong><br>
                                por estado
                            </h2>
                            <form action="<?php echo PATH_ALL . '/' . 'buscar-vagas.php' ?>" method="get" id="vagas_estado_form" class="form-control">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <select name="estado[0]" required>
                                    <option disabled selected value="">Estado</option>
                                    <?php foreach ($estados as $row) { ?>
                                        <option><?php echo $row ?></option>
                                    <?php } ?>
                                </select>
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                            <div class="estados-list" id="list1">
                                <?php while ($vg = mysqli_fetch_array($res_uf)) { ?>
                                    <ul>
                                        <li>
                                            <span class="estados-link vagas_estado" data-uf="<?php echo $vg['nome'] ?>">
                                                <?php
                                                if (intval($vg['numero_vagas']) > 1) {
                                                    echo '<a href="' . DIRETORIO . '/buscar-vagas.php?estado=' . $vg['nome'] . '">' . $vg['numero_vagas'] . " Vagas em " . $vg['nome'] . '</a>';
                                                } else {
                                                    echo '<a href="' . DIRETORIO . '/buscar-vagas.php?estado=' . $vg['nome'] . '">' . $vg['numero_vagas'] . " Vaga em " . $vg['nome'] . '</a>';
                                                }

                                                ?>
                                            </span>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </div>
                            <div class="estados-list" id="list2" style="display:none">
                                <ul id="vagas-list"></ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center us-inatvo">
                            <h2>Você ainda não ativou o seu acesso. É necessário ativá-lo para poder usufruir de todos os recursos do Vagas & Vagas. </h2>
                            <a href="<?php echo PATH_ALL . '/candidatos/ativar-usuario.php ' ?>" class="btn btn-success button dark">ATIVAR MEU ACESSO AGORA</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <section class="publicidade">
            <div class="container">
                <span>Publicidade</span>
                <?php if (!empty($publi1['link'])) { ?>
                    <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                <?php } else { ?>
                    <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                <?php } ?>
            </div>
        </section>
        <section class="empresas">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="1000000">
                            <div class="carousel-inner">
                                <?php $count = 0; ?>
                                <?php foreach ($oportunidades as $ops) { ?>
                                    <div class="col-md-12 col-xs-12 col-sm-12 item <?php echo $count == 0 ? 'active' : '' ?>">
                                        <?php
                                        foreach ($ops as $dados) {
                                            $arquivo = PATH_EMP_IMAGENS . '/' . $dados['imagem'];

                                            ?>
                                            <div class="emp col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <a href="<?php echo PATH_ALL ?>/ver-oportunidade.php?id=<?php echo $dados['id'] ?>">
                                                    <img src="<?php echo PATH_EMP_IMAGENS . '/' . $dados['imagem'] ?>" alt="<?php echo $dados['nome_empresa'] ?>" class="img-responsive">
                                                    <strong>
                                                        <?php echo $dados['numero_vagas'] . " vagas na " . $dados['nome_empresa']; ?>
                                                    </strong>
                                                </a>
                                            </div>
                                            <?php
                                        }

                                        ?>
                                        <?php
                                        $count++;

                                        ?>
                                    </div>
                                    <?php
                                }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <section class="publicidade">
            <div class="container">
                <span>Publicidade</span>
                <?php if (!empty($publi2['link'])) { ?>
                    <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                <?php } else { ?>
                    <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                <?php } ?>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>