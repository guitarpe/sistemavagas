<?php
session_start();

include "includes/conexao.php";
$res = mysqli_query($con, "SELECT * FROM TB_VV_INSTITUCIONAL WHERE id_institucional = 1");
$institucional = mysqli_fetch_array($res);

$tipo_cad = "empresa";

$op = filter_input(INPUT_GET, 'op');

?>

<html>
    <?php include "includes/cabecalho.php" ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="publicidade">
            <div class="container">
                <span>Publicidade</span>
                <?php if (!empty($publi1['link'])) { ?>
                    <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                <?php } else { ?>
                    <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                <?php } ?>
            </div>
        </section>

        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="institucional">
                    <h1 class="text-center">Cadastro de empresa</h1>
                    <div class="box-body">
                        <div class="container">
                            <form action="<?php echo PATH_ALL . '/actions/recebe_empresa.php' ?>" method="post">
                                <p align="center">
                                    <?php if (empty($op)) { ?>
                                        Olá, vamos ajudar você a preencher seu cadastro. Preencha as informações abaixo e clique em Cadastrar.
                                    <?php } else { ?>
                                        Antes de anunciar suas vagas você deve se cadastrar primeiro, mas fique tranquilo, vamos ajudar você a preencher seu cadastro. <br>Preencha as informações abaixo e clique em Cadastrar.
                                    <?php } ?>
                                </p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <input class="form-control" type="text" placeholder="Nome da empresa" name="nome" id="enome" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <input class="cnpj form-control verifica_cnpj_cadastrado" name="cnpj" type="text" placeholder="CNPJ" id="ecnpj" required>
                                            <span id="msg-cpf" style="display:none"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-calendar"></i>
                                            </span>
                                            <input class="form-control" type="text" name="ramoatividade" placeholder="Ramo de atividade" id="eramo">
                                            <span id="msg-data" style="display:none"></span>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Descrição da empresa" name="descricao" id="descricao" class="form-control" required></textarea>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <input class="form-control numbers" name="nfuncionarios" type="text" placeholder="Número de funcionários" id="nfuncionarios" required>
                                            <span id="msg-cpf" style="display:none"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <input class="form-control" type="text" name="site" placeholder="Site" id="site">
                                            <span id="msg-data" style="display:none"></span>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <input class="form-control" type="text" placeholder="Nome completo do usuário" name="nomeusuario" id="unome" required>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-envelope"></i>
                                            </span>
                                            <input class="form-control validemail verifica_email_cadastrado" type="text" placeholder="E-mail do Usuário" name="email" id="eemail" required>
                                            <span id="msg-email" style="display:none"></span>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-envelope"></i>
                                            </span>
                                            <input class="form-control validemail" type="text" placeholder="E-mail de relacionamento da empresa" name="emailrelacionamento" id="eemailr" required>
                                            <span id="msg-email" style="display:none"></span>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-asterisk"></i>
                                            </span>
                                            <input type="password" placeholder="Senha" name="senha" class="form-control txtSenha" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-asterisk"></i>
                                            </span>
                                            <input type="password" placeholder="Confirmar senha" name="confirmar-senha" class="form-control confisenha nopaste" required>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-earphone"></i>
                                            </span>
                                            <input class="form-control fones" type="text" placeholder="Telefone" name="fone" id="telefone" required>
                                            <span id="msg-fone" style="display:none"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-earphone"></i>
                                            </span>
                                            <input class="form-control fones" type="text" placeholder="Celular (Opcional)" name="celular" id="telefone2">
                                            <span id="msg-fone" style="display:none"></span>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <input class="cep form-control ecep" name="cep" type="text" placeholder="CEP">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <input class="form-control elogradouro" type="text" placeholder="Logradouro" id="logradouro" name="logradouro">
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <input class="form-control numbers" type="text" name="numero" placeholder="Número" id="enumero">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <input class="form-control" type="text" name="complemento" placeholder="Complemento" id="ecomplemento">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <input class="form-control ebairro" type="text" name="bairro" placeholder="Bairro" id="ebairro">
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <span id="msg-ecep" style="display:none">
                                    O cep digitado é invalido!
                                </span>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <select name="estado" id="estado" class="form-control eestado busca_cidades" required>
                                                <option disabled selected value="">Estado</option>
                                                <?php
                                                $result = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                                while ($val = mysqli_fetch_array($result)) {
                                                    echo "<option value='$val[uf]'>$val[nome]</option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-globe"></i>
                                            </span>
                                            <select name="cidade" id="cidade" class="form-control ecidade cidades" required>
                                                <option value="">Cidade</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="termos" name="termos" required>
                                            <label class="form-check-label" for="termos">
                                                Li e aceito os <a href="#">Termos e Condições</a> para uso de políticas de privacidade
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="" target="entrar" class="link link-js">
                                            Já tem seu cadastro? Faça login agora!
                                        </a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button class="btn btn-success btn-icon btn-lg" type="submit">
                                            Finalizar Cadastro <span class="glyphicon glyphicon-ok"></span>
                                        </button>
                                    </div>
                                </div>
                                </br>
                                </br>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
            <a href="<?php echo PATH_ALL . '/' . 'cadastrocandidato.php' ?>">
                <button class="btn btn-secondary btn-lg btn-block" type="submit" >Você quer cadastrar como candidato e ver as vagas disponíveis? Clique aqui!</button>
            </a>
        </section>

        <section class="publicidade">
            <div class="container">
                <span>Publicidade</span>
                <?php if (!empty($publi2['link'])) { ?>
                    <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                <?php } else { ?>
                    <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                <?php } ?>
            </div>
        </section>

        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>

    </body>
</html>
