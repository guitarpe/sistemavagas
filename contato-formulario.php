<?php
session_start();

include "includes/conexao.php";

$sql_perguntas = mysqli_query($con, "select * FROM TB_VV_FAQ ORDER by id_pergunta DESC");

$sql_perguntas1 = mysqli_query($con, "select * FROM TB_VV_FAQ ORDER by id_pergunta DESC");

$sql_perguntas2 = mysqli_query($con, "select * FROM TB_VV_FAQ ORDER by id_pergunta DESC");

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "includes/cabecalho.php" ?>
    </head>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <section class="publicidade">
                <div class="container">
                    <span>Publicidade</span>
                    <?php if (!empty($publi1['link'])) { ?>
                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                    <?php } else { ?>
                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                    <?php } ?>
                </div>
            </section>
            <div class="container">
                <div class="col-sm-12">
                    <h2 class="title">Fale Conosco</h2>
                </div>
                <div class="col-md-12" align="center">
                    </br>
                    <h4>Você também pode acessar a nossa página de <a href="<?php echo PATH_ALL . '/contato.php' ?>"><strong>Dúvidas Frequentes</strong></a> e verificar se podemos lhe ajudar, <br/>ou fale conosco através do formulário baixo.</h4>
                    </br>
                </div>
                <div class="col-md-8 col-sm-offset-2">
                    <form class="form" action="<?php echo PATH_ALL . '/actions/envia_fale_conosco.php' ?>" method="post" role="form">
                        <div class="form-group col-sm-12">
                            <input type="text" class="form-control" name="nome" placeholder="Nome" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group col-sm-2">
                            <select class="form-control busca_cidades" name="estado" required>
                                <option value="">Estado</option>
                                <?php
                                $result = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                while ($val = mysqli_fetch_array($result)) {

                                    ?>
                                    <option value="<?php echo $val['uf']; ?>"><?php echo $val['uf'] ?></option>
                                    <?php
                                }

                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select name="cidade" id="cidades" title="Cidade" class="form-control cidades" required>
                                <option value="">Cidade</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <textarea name="mensagem" class="form-control" placeholder="Mensagem..." required></textarea>
                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-success form-control">
                                <i class="fa fa-envelope"></i>
                                BOTÃO ENVIAR A MENSAGEM
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-8 col-sm-offset-2 margin-bottom-50">
                    <?php if (false) { ?>
                        <div class="col-md-4 text-center">
                            <i class="fa fa-map-marker" id="icon-contato"></i>
                            <span id="icon-descricao">Endereço</span>
                            <p>Av. Presidente Vargas,</p>
                            <p>590 - Centro RJ</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <i class="fa fa-phone" id="icon-contato"></i>
                            <span id="icon-descricao">Fone</span>
                            <p>(21) 98219-0017</p>
                        </div>
                    <?php } ?>
                    <div class="col-md-12 text-center">
                        <i class="fa fa-search" id="icon-contato"></i>
                        <span id="icon-descricao">Siga nas redes sociais</span>
                        <div class="row">
                            <a class="link-social" href="<?php echo LINKEDIN ?>" target="_blank" title="Compartilhar no Linkedin">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                            <a class="link-social" href="<?php echo FACEBOOK ?>" target="_blank" title="Compartilhar no Facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a class="link-social" href="<?php echo WHATSAPP ?>" target="_blank" title="Compartilhar no Whattsapp">
                                <i class="fa fa-whatsapp" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <section class="publicidade">
                <div class="container">
                    <span>Publicidade</span>
                    <?php if (!empty($publi2['link'])) { ?>
                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                    <?php } else { ?>
                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                    <?php } ?>
                </div>
            </section>
        </section>

        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
