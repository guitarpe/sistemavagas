<?php
session_start();

include "includes/conexao.php";

$func = new Funcoes();

$tipo_cad = "busca";

$tipo = filter_input(INPUT_GET, "tipo");

//parametros de filtragem
$pagina = filter_input(INPUT_GET, "pagina");

$cargo = filter_input(INPUT_GET, "cargo", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$estado = filter_input(INPUT_GET, "estado", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$cidade = filter_input(INPUT_GET, "cidade", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$nivel = filter_input(INPUT_GET, "nivel", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$faixa = filter_input(INPUT_GET, "faixa", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$contrato = filter_input(INPUT_GET, "contrato", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$turno = filter_input(INPUT_GET, "turno", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$escolaridade = filter_input(INPUT_GET, "escolaridade", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

$sql = "SELECT
            id, estado, nome, cargo, cidade, nivel_hierarquico,
            faixa_salarial, forma_contratacao, turno, ensino_minimo, nome_empresa,
            numero_vagas, data_anuncio, status, data_expira atividades
        FROM VW_VAGAS WHERE 1=1 ";

//paginação
$total_reg = 20;

if (!isset($pagina)) {
    $pag = "1";
} else {
    $pag = $pagina;
}

$ini = $pag - 1;
$inicio = $ini * $total_reg;

$where = "";

if (count($cargo) > 0 && sizeof($cargo) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($cargo); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " cargo LIKE '%$cargo[$i]%' ";
    }
    $where .= ") ";
}

if (count($estado) > 0 && sizeof($estado) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($estado); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " nome LIKE '%$estado[$i]%' ";
    }
    $where .= ") ";
}

if (count($cidade) > 0 && sizeof($cidade) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($cidade); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " cidade LIKE '%$cidade[$i]%' ";
    }
    $where .= ") ";
}

if (count($nivel) > 0 && sizeof($nivel) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($nivel); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " nivel_hierarquico LIKE '%$nivel[$i]%' ";
    }
    $where .= ") ";
}

if (count($faixa) > 0 && sizeof($faixa) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($faixa); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " faixa_salarial LIKE '%$faixa[$i]%' ";
    }
    $where .= ") ";
}

if (count($contrato) > 0 && sizeof($contrato) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($contrato); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " forma_contratacao LIKE '%$contrato[$i]%' ";
    }
    $where .= ") ";
}

if (count($turno) > 0 && sizeof($turno) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($turno); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " turno LIKE '%$turno[$i]%' ";
    }
    $where .= ") ";
}

if (count($escolaridade) > 0 && sizeof($escolaridade) > 0) {

    $where .= " AND (";
    for ($i = 0; $i < count($escolaridade); $i++) {
        $where .= $i != 0 ? ' ||' : '';
        $where .= " ensino_minimo LIKE '%$escolaridade[$i]%' ";
    }
    $where .= ") ";
}

$sql .= $where . " ORDER BY data_anuncio DESC, cargo, faixa_salarial, forma_contratacao, turno ASC LIMIT $inicio, $total_reg";

$sql_todos = "SELECT
                    count(*) as todos
                FROM VW_VAGAS
                WHERE 1=1 " . $where;
$res_todos = mysqli_query($con, $sql_todos) or die(mysqli_error($con));
$todos = mysqli_fetch_array($res_todos);

$res_vagas = mysqli_query($con, $sql) or die(mysqli_error($con));

$tr = $todos['todos'];
$tp = $tr / $total_reg;

$num_paginas = ceil($tr / $total_reg);

?>

<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <?php include "includes/vagas-filtro.php" ?>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <section class="form-sec">
                            <form class="search" action="<?php echo PATH_ALL ?>/buscar-vagas.php?tipo=1" method="get">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <input type="text" placeholder="Digite o cargo de seu interesse" class="vaga cargobusca" name="cargo[0]">
                                <datalist id="listacargos">

                                </datalist>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <select class="estado" name="estado[0]" required>
                                    <option selected disabled value="">Estado</option>
                                    <?php
                                    $res_estados = mysqli_query($con, "SELECT nome, uf FROM TB_VV_ESTADOS ORDER BY uf ASC");
                                    while ($estado = mysqli_fetch_array($res_estados)) {

                                        ?>
                                        <option><?php echo $estado['nome'] ?></option>
                                    <?php } ?>
                                </select>
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </section>
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi1['link'])) { ?>
                                    <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>
                        <?php if (mysqli_num_rows($res_vagas) == 0) { ?>
                            <div class="vagas">
                                <?php if (!empty($tipo) && intval($tipo) == 1 && !empty($cargo)) { ?>
                                    <h3>Nenhuma vaga encontrada no momento com esse nome <?php echo $cargo ?>. Favor tente pesquisar outro cargo</h3>
                                <?php } else if (!empty($tipo) && intval($tipo) == 1 && !empty($estado)) { ?>
                                    <h3>Nenhuma vaga encontrada para o estado de <?php echo $estado ?> no momento com esse nome. Favor tente pesquisar outro estado</h3>
                                <?php } else { ?>
                                    <h3>Nenhuma vaga encontrada no momento com esse nome. Favor tente pesquisar novamente</h3>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="vagas">
                                <ul>
                                    <?php while ($vaga = mysqli_fetch_array($res_vagas)) { ?>
                                        <li>
                                            <div class="ttl">
                                                <div class="rt">
                                                    <?php
                                                    $titulo = $vaga["cargo"];
                                                    if ($vaga["numero_vagas"] == 1) {
                                                        $titulo .= " (1 VAGA) - " . $vaga['cidade'] . "-" . $vaga['estado'];
                                                    } else {
                                                        $titulo .= " (" . $vaga['numero_vagas'] . " VAGAS) - " . $vaga['cidade'] . "-" . $vaga['estado'];
                                                    }

                                                    ?>
                                                    <h6><a href="<?php echo PATH_ALL . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" class="oportunidade"><?php echo $titulo; ?></a></h6>
                                                    <div class="item">
                                                        <i class="fa fa-bookmark" aria-hidden="true"></i>
                                                        <?php echo $vaga['nome_empresa']; ?>
                                                    </div>
                                                </div>
                                                <div class="lt">
                                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                                    <?php echo $func->formataData($vaga['data_anuncio']); ?>
                                                </div>
                                            </div>

                                            <div class="info">
                                                <p><?php echo $vaga['atividades']; ?></p>
                                                <a href="<?php echo PATH_ALL . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" class="oportunidade">
                                                    Visualizar oportunidade <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                </a>
                                            </div>

                                            <div class="foot">
                                                <div class="col-md-6">
                                                    <span>
                                                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                        <?php echo $vaga['forma_contratacao']; ?>
                                                    </span>
                                                    <span>
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        <?php echo $vaga['cidade'] . "/" . $vaga['estado']; ?>
                                                    </span>
                                                </div>
                                                <div class="col-md-6 btns">
                                                    <div class="share fright text-right">
                                                        <a href="<?php echo 'https://www.linkedin.com/shareArticle?mini=true&url=' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] . '&title=' . $titulo . '&summary=' . $vaga['atividades'] . '&source=' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" target="_blank" title="Compartilhar no Linkedin">
                                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="<?php echo 'https://www.facebook.com/sharer/sharer.php?u= ' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] . '&summary=' . $vaga['atividades'] . '&title=Vaga: ' . $titulo . '&description=' . $vaga['atividades'] . '&picture=' . URL . PATH_ASSETS . '/img/logo-menor.jpg' ?>" target="_blank" title="Compartilhar no Facebook">
                                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="https://wa.me/?text=<?php echo 'Vaga: ' . $titulo . '. Segue o link: ' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" data-action="share/whatsapp/share" target="_blank" title="Compartilhar no Whattsapp">
                                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <?php
                                                    if (isset($_SESSION["id"]) && $_SESSION["tipo"]) {
                                                        if ($_SESSION["tipo"] == "candidato") {
                                                            $res = mysqli_query($con, "SELECT data FROM TB_VV_CANDIDATURAS WHERE id_candidato = $_SESSION[id] AND id_vaga = $vaga[id]");
                                                            $isCandidato = mysqli_num_rows($res);
                                                        }
                                                    }
                                                    if (isset($isCandidato)) {
                                                        if ($isCandidato) {

                                                            ?>
                                                            <a href="#" class="right" style="font-size: 25px;margin:5px" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                                <i class="fa fa-times-rectangle-o cor-red"></i>
                                                            </a>
                                                            <a href="#" class="btn-enviado" title="Você já se cadastrou à essa vaga de emprego!">
                                                                <i class="fa fa-check-square-o"></i>
                                                                Currículo Enviado
                                                            </a>
                                                        <?php } else { ?>
                                                            <a href="#" class="right hidden" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                                <i class="fa fa-times-rectangle-o cor-red"></i>
                                                            </a>
                                                            <a href="#" class="btn" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_CANDIDATOS ?>/actions/recebe_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>&tipo=1">
                                                                Quero esta vaga!
                                                            </a>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <a href="#" class="btn-login" data-load-title="Faça seu login" data-vaga="<?php echo $vaga["id"] ?>" data-toggle="modal" data-target="#modal-default-md" data-load-url="<?php echo PATH_ALL ?>/modais/entrar.php">
                                                            Quero esta vaga!
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>

                                <div class="list-flt">
                                    <div class="list-flt" style="text-align: center">
                                        <?php
                                        $anterior = $pag - 1;
                                        $proximo = $pag + 1;

                                        ?>

                                        <div class="pagination">
                                            <?php if (intval($num_paginas) > 1) { ?>
                                                <div class="pagnobord"><a href="&pagina=0">Primeira</a></div>
                                                <?php
                                                if ($pag > 1) {

                                                    ?>
                                                    <div class="pagop"><a href="&pagina=<?php echo $anterior ?>" class="ant"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></div>
                                                    <?php
                                                }

                                                $cont = 0;

                                                ?>

                                                <?php for ($i = 1; $i <= $num_paginas; $i++) { ?>
                                                    <?php if ($i > ($pagina - 3) && $cont < 10) { ?>
                                                        <?php if ($i == $pagina) { ?>
                                                            <div class="pagopac"><?php echo $i <= 9 ? '0' . $i : $i ?></div>
                                                        <?php } else { ?>
                                                            <div class="pagop"><a href="&pagina=<?php echo $i ?>"><?php echo $i <= 9 ? '0' . $i : $i ?></a></div>

                                                            <?php
                                                            $cont++;
                                                        }
                                                    }

                                                    ?>
                                                <?php } ?>
                                                <?php
                                                if ($pag < $tp) {

                                                    ?>
                                                    <div class="pagop"><a href="&pagina=<?php echo $proximo ?>" class="prox"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
                                                    <?php
                                                }

                                                ?>
                                                <div class="pagnobord"><a href="&pagina=<?php echo $num_paginas ?>">Última</a></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi2['link'])) { ?>
                                    <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>