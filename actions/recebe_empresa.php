<?php
session_start();

include "../includes/conexao.php";

$func = new Funcoes();

$nome_empresa = filter_input(INPUT_POST, 'nome');
$cnpj = filter_input(INPUT_POST, 'cnpj');
$ramo = filter_input(INPUT_POST, 'ramoatividade');
$nome_usuario = filter_input(INPUT_POST, 'nomeusuario');
$email = filter_input(INPUT_POST, 'email');
$senha = filter_input(INPUT_POST, 'senha');
$descricao = filter_input(INPUT_POST, 'descricao');
$qtd_funcionarios = filter_input(INPUT_POST, 'nfuncionarios');
$site = filter_input(INPUT_POST, 'site');
$email_empresa = filter_input(INPUT_POST, 'emailrelacionamento');
$fone = filter_input(INPUT_POST, 'fone');
$celular = filter_input(INPUT_POST, 'celular');
$cep = filter_input(INPUT_POST, 'cep');
$logradouro = filter_input(INPUT_POST, 'logradouro');
$numero = filter_input(INPUT_POST, 'numero');
$complemento = filter_input(INPUT_POST, 'complemento');
$bairro = filter_input(INPUT_POST, 'bairro');
$cidade = filter_input(INPUT_POST, 'cidade');
$uf = filter_input(INPUT_POST, 'estado');

$senhausuario = $func->dec_enc(1, 'empresa' . '|' . $senha . '|' . $email);

$query = "INSERT INTO TB_VV_EMPRESAS "
    . "(nome_empresa, cnpj, ramo_atividade, nome_usuario, email, senha, descricao, qtd_funcionarios, site, email_empresa, fone, fax, cep, logradouro, numero, complemento, bairro, cidade, estado) "
    . "VALUES "
    . "('$nome_empresa', '$cnpj', '$ramo', '$nome_usuario', '$email', '$senhausuario', '$descricao', '$qtd_funcionarios', '$site', '$email_empresa', '$fone', '$celular', '$cep', '$logradouro', '$numero', '$complemento', '$bairro', '$cidade', '$uf')";

$sql_empresa = mysqli_query($con, $query) or die(mysqli_error($con));

$id_empresa = mysqli_insert_id($con);

if (!empty($id_empresa)) {

    $query_us = "INSERT INTO TB_VV_USUARIOS "
        . "(id, nome, email, senha, tipo, status) "
        . "VALUES "
        . "('$id_empresa', '$nome_usuario', '$email', '$senhausuario', 'empresa', '0');";

    $sql_us = mysqli_query($con, $query_us) or die(mysqli_error($con));

    $_SESSION['id'] = $id_empresa;
    $_SESSION['nome'] = $nome_usuario;
    $_SESSION['email'] = $email;
    $_SESSION['senha'] = $senhausuario;
    $_SESSION['tipo'] = 'empresa';
    $_SESSION['status'] = 0;

    $to = filter_input(INPUT_POST, 'email');
    $to_txt = filter_input(INPUT_POST, 'empresa');

    $subject = 'Bem vindo ao Vagas e Vagas!';

    $message = '
            <html>
            <head>
            </head>
            <body>
                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                    <img src="' . HOME . '/images/top.png">
                    <div class="container" style="padding:20px">
                        <table>
                            <tr>
                                <td>
                                    <h2><b></b></h2>
                                    <br/>Olá ' . $nome_usuario . '
                                    <br/>
                                    <br/>
                                    É uma alegria termos você conosco em nosso Portal Vagas e Vagas.
                                    <br/>
                                    <br/>
                                    Você cadastrou a empresa ' . $nome_empresa . ' em nosso portal e basta cadastrar suas vagas que já estarão disponíveis para visualização de todos.
                                    <br/>
                                    <br/>
                                    Lembramos que fazemos tudo por pessoas, para pessoas e por pessoas:
                                    <br/>
                                    <br/>
                                    1. Não insira dados errados sobre sua empresa, isso além de não ser legal, é ilegal.
                                    <br/>
                                    <br/>
                                    2. Não crie vagas que não existem. Coloque-se no lugar do candidato e não crie expectativas falsas.
                                    <br/>
                                    <br/>
                                    3. MUITO IMPORTANTE: lembre-se sempre de selecionar o seu candidato e depois fechar sua vaga.
                                </td>
                                <td>
                                    <img src="' . HOME . '/images/confirmado.png">
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                        <hr>
                        <br/>
                        Estas simples regras darão um destaque maior às suas vagas e, acima de tudo, fará deste portal um espaço mais justo e melhor para todos.
                        <br/>
                        <br/>
                        Acesse seu painel <a href="' . HOME . '/empresas/" style="color:blue">' . HOME . '/empresas</a>
                        <br/>
                        <br/>
                        PS. Ajude divulgar este trabalho especial para seus colegas! É muito importante para todos.
                        <br/>
                        <br/>
                        Contamos muito com você!
                        <br/>
                        <br/>
                        Equipe Vagas e Vagas.
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <div class="row" align="right">
                            <a href="' . FACEBOOK . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/facebook.png">
                            </a>
                            <a href="' . TWITTER . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/twitter.png">
                            </a>
                            <a href="' . PLUS . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/googlep.png">
                            </a>
                        </div>
                    </div>
                    <img src="' . HOME . '/images/top.png">
                    <div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $id_empresa . '&tipo=empresa" target="_blank">Não quero receber mais mensagens</a></div>
                </div>
            </body>
            </html>';

    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $to_txt,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    include_once '../includes/enviar-email.php';

    $func->redir('empresas/mail_cadastro_empresa.php?cnpj=' . $cnpj . '&empresa=' . $nome_empresa . '&op=1');
}
