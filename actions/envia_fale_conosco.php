<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

$nome = filter_input(INPUT_POST, 'nome');
$email = filter_input(INPUT_POST, 'email');
$estado = filter_input(INPUT_POST, 'estado');
$cidade = filter_input(INPUT_POST, 'cidade');
$mensagem = filter_input(INPUT_POST, 'mensagem');

$ip = $func->get_client_ip();

$sql_slct_msg = "SELECT * FROM TB_VV_MENSAGEN_ENVIADAS WHERE ip='$ip' AND data=CURDATE()";
$res_slct_msg = mysqli_query($con, $sql_slct_msg) or die(mysqli_error($con));

if (mysqli_num_rows($res_slct_msg) < 3) {
    $sql_msg = "INSERT INTO TB_VV_MENSAGEN_ENVIADAS (ip) VALUES ('$ip')";
    $res_msg = mysqli_query($con, $sql_msg) or die(mysqli_error($con));

    $subject = 'Acabamos de receber do ' . $nome . ', uma mensagem - Fale Conosco!';

    $msg = 'Mensagem de ' . $nome . ' (' . $email . ') ' . $cidade . '-' . $estado . '<br>';

    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas - Formulário de Contato',
        'TO_MAIL' => 'edicao.cv@gmail.com',
        'TO_TXT' => $nome,
        'SUBJECT' => $subject,
        'MENSAGE' => $msg . $mensagem
    ];

    $situacao = 'msg-fale-conosco';
    $func->alert($situacao, 'acao');
    include_once '../includes/enviar-email.php';
} else {
    $situacao = 'msg-fale-conosco-lim';
    $func->alert($situacao, 'acao');
    $func->redir("contato-formulario.php");
}


