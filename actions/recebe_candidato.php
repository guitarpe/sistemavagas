<?php
include "../includes/conexao.php";

$func = new Funcoes();

$dtnasc = filter_input(INPUT_POST, 'datanasc');

$dtn = DateTime::createFromFormat('d/m/Y', $dtnasc);

$email = filter_input(INPUT_POST, "email");
$senha = filter_input(INPUT_POST, "senha");
$nome = filter_input(INPUT_POST, "nome");
$cpf = filter_input(INPUT_POST, "cpf");
$data = $dtn->format("Y-m-d");
$sexo = filter_input(INPUT_POST, "sexo");
$fone = filter_input(INPUT_POST, "fone");
$celular = filter_input(INPUT_POST, "celular");
$estado = filter_input(INPUT_POST, "estado");
$cidade = filter_input(INPUT_POST, "cidade");

$cargos = filter_input(INPUT_POST, 'cargoprofissional', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$nomepresa = filter_input(INPUT_POST, 'nomeempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

$nomeempresa = filter_input(INPUT_POST, 'nomeempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$salario = filter_input(INPUT_POST, 'salario', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$cargoempresa = filter_input(INPUT_POST, 'cargoempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$atividades = filter_input(INPUT_POST, 'atividades', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$datainicio = filter_input(INPUT_POST, 'datainicio', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$datafim = filter_input(INPUT_POST, 'datafim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$empregoatual = filter_input(INPUT_POST, 'empregoatual', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$formacao = filter_input(INPUT_POST, 'formacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$nomecurso = filter_input(INPUT_POST, 'nomecurso', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$idioma = filter_input(INPUT_POST, 'idioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

$senhausuario = $func->dec_enc(1, 'candidato' . '|' . $senha . '|' . $email);

$sql_candidato = mysqli_query($con, "SELECT COUNT(*) as total, id FROM TB_VV_CANDIDATOS WHERE email='$email' AND cpf='$cpf'") or die(mysqli_error($con));
$res_cand = mysqli_fetch_array($sql_candidato);

if (intval($res_cand['total']) == 0) {
    $query = "INSERT INTO TB_VV_CANDIDATOS "
        . "(email, senha, nome, cpf, nascimento, sexo, fone, celular, estado, cidade) "
        . "VALUES "
        . "('$email', '$senhausuario', '$nome', '$cpf', '$data', '$sexo', '$fone', '$celular', '$estado', '$cidade')";

    $sql = mysqli_query($con, $query) or die(mysqli_error($con));

    $id = mysqli_insert_id($con);

    if (!empty($cargos[0])) {
        foreach ($cargos as $key) {
            if (!empty($key)) {
                $sql = "INSERT INTO TB_VV_CARGOS_CAND (id_candidato, cargo) VALUES ($id, '$key')";
                $res = mysqli_query($con, $sql) or die(mysqli_error($con));
            }
        }
    }

    if (!empty($nomepresa[0])) {

        foreach ($nomepresa as $key => $value) {

            $valsalario = $func->returnValor($salario[$key]);
            $empatual = isset($empregoatual[$key]) ? intval($empregoatual[$key]) : 0;

            $sql = "INSERT INTO TB_VV_EXP_PROFISSIONAIS "
                . "(id_candidato, nome_empresa, cargo, atividades, inicio, fim, emprego_atual, salario) "
                . "VALUES "
                . "($id, '$nomeempresa[$key]', '$cargoempresa[$key]', '$atividades[$key]', '$datainicio[$key]', '$datafim[$key]', $empatual, '$valsalario')";

            $res = mysqli_query($con, $sql) or die(mysqli_error($con));
        }
    }

    if (!empty($formacao[0])) {

        $formacao = filter_input(INPUT_POST, 'formacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $instituicao = filter_input(INPUT_POST, 'instituicao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $situacao = filter_input(INPUT_POST, 'situacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $nomeformacao = filter_input(INPUT_POST, 'nomeformacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $anofim = filter_input(INPUT_POST, 'anofim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $semestre = filter_input(INPUT_POST, 'semestre', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $turno = filter_input(INPUT_POST, 'turno', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        foreach ($formacao as $key => $value) {

            $sql_formacao = "INSERT INTO TB_VV_FORMACOES_CAND "
                . "(id_candidato, nivel, instituicao, curso, semestre, situacao, fim, turno) "
                . "VALUES "
                . "($id, '$formacao[$key]', '$instituicao[$key]', '$nomeformacao[$key]', '$semestre[$key]', '$situacao[$key]', '$anofim[$key]', '$turno[$key]')";

            $res_formacao = mysqli_query($con, $sql_formacao) or die(mysqli_error($con));
        }
    }

    if (!empty($nomecurso[0])) {

        $nomecurso = filter_input(INPUT_POST, 'nomecurso', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $instituicao2 = filter_input(INPUT_POST, 'instituicao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $situacao2 = filter_input(INPUT_POST, 'situacao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $turnoextra = filter_input(INPUT_POST, 'turnoextra', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $diassemana = filter_input(INPUT_POST, 'diassemana', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $cargahoraria = filter_input(INPUT_POST, 'cargahoraria', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $anofim = filter_input(INPUT_POST, 'anofim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        foreach ($nomecurso as $key => $value) {
            $sql_curso = "INSERT INTO TB_VV_CURSOS_CAND "
                . "(id_candidato, nome, instituicao, situacao, turnoextra, diassemana, cargahoraria, anofim ) "
                . "VALUES "
                . "($id, '$nomecurso[$key]', '$instituicao2[$key]', '$situacao2[$key]', '$turnoextra[$key]', '$diassemana[$key]', '$cargahoraria[$key]', '$anofim[$key]')";

            $res_curso = mysqli_query($con, $sql_curso) or die(mysqli_error($con));
        }
    }

    if (!empty($idioma[0])) {

        $idioma = filter_input(INPUT_POST, 'idioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $nivelidioma = filter_input(INPUT_POST, 'nivelidioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        foreach ($idioma as $key => $value) {

            $sql_idioma = "INSERT INTO TB_VV_IDIOMAS_CAND "
                . "(id_candidato, idioma, nivel) "
                . "VALUES "
                . "($id, '$idioma[$key]', '$nivelidioma[$key]')";

            $res_idioma = mysqli_query($con, $sql_idioma) or die(mysqli_error($con));
        }
    }

    $sql_usuario = "INSERT INTO TB_VV_USUARIOS "
        . "(id, nome, email, senha, tipo, status) "
        . "VALUES "
        . "('$id', '$nome', '$email', '$senhausuario', 'candidato', 0)";

    $sql_usuario = mysqli_query($con, $sql_usuario) or die(mysqli_error($con));

    $_SESSION['id'] = $id;
    $_SESSION['nome'] = $nome;
    $_SESSION['email'] = $email;
    $_SESSION['senha'] = $senhausuario;
    $_SESSION['tipo'] = 'candidato';
}
$func->redir('candidatos/mail_bem_vindo.php?email=' . $email . '&nome=' . $nome . '&op=1');

