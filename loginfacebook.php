<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['code'])) {

    // Informe o seu App ID abaixo
    $appId = '29048572934875';

    // Digite o App Secret do seu aplicativo abaixo:
    $appSecret = '123456789';

    // Url informada no campo "Site URL"
    $redirectUri = urlencode('http://vagasevagas.com.br/');

    // Obtém o código da query string
    $code = $_GET['code'];

    // Monta a url para obter o token de acesso e assim obter os dados do usuário
    $token_url = "https://graph.facebook.com/oauth/access_token?"
        . "client_id=" . $appId . "&redirect_uri=" . $redirectUri
        . "&client_secret=" . $appSecret . "&code=" . $code;

    //pega os dados
    $response = @file_get_contents($token_url);
    if ($response) {
        $params = null;
        parse_str($response, $params);
        if (isset($params['access_token']) && $params['access_token']) {
            $graph_url = "https://graph.facebook.com/me?access_token="
                . $params['access_token'];
            $user = json_decode(file_get_contents($graph_url));

            // nesse IF verificamos se veio os dados corretamente
            if (isset($user->email) && $user->email) {

                /*
                 * Apartir daqui, você já tem acesso aos dados usuario, podendo armazená-los
                 * em sessão, cookie ou já pode inserir em seu banco de dados para efetuar
                 * autenticação.
                 * No meu caso, solicitei todos os dados abaixo e guardei em sessões.
                 */

                $email = $user->email;

                $sql = "SELECT * FROM TB_VV_USUARIOS WHERE email = '$email'";
                $res = mysqli_query($con, $sql);
                $array = mysqli_fetch_array($res);

                $qregistros = mysqli_num_rows($res);

                if ($qregistros) {

                    $array = mysqli_fetch_array($res);

                    if ($array['senha'] == $senha) {

                        session_start();

                        if ($array['tipo'] == 'candidato' && $array['status'] == '0') {

                            $_SESSION['nome'] = $array['nome'];
                            $_SESSION['email'] = $array['email'];
                            $_SESSION['senha'] = $array['senha'];

                            echo "<script>location.href='index.php?activate=false'</script>";
                        } else {

                            $_SESSION['id'] = $array['id'];
                            $_SESSION['nome'] = $array['nome'];
                            $_SESSION['email'] = $array['email'];
                            $_SESSION['senha'] = $array['senha'];
                            $_SESSION['tipo'] = $array['tipo'];

                            if ($array['tipo'] == 'empresa') {
                                $_SESSION['status'] = $array['status'];
                                if ($_POST['id_vaga'] == "") {
                                    echo "<script>top.location.href='empresas'</script>";
                                } else {
                                    echo "<script>top.location.href='ver-oportunidade.php?id=$_POST[id_vaga]'</script>";
                                }
                            } else {
                                if ($array['tipo'] == 'candidato') {
                                    if ($_POST['id_vaga'] == "") {
                                        echo "<script>top.location.href='candidatos'</script>";
                                    } else {

                                        include "includes/defines.php";

                                        ini_set('display_errors', 1);

                                        error_reporting(E_ALL);

                                        $res = mysqli_query($con, "INSERT INTO TB_VV_CANDIDATURAS (id_candidato, id_vaga, data) VALUES ($_SESSION[id], $_POST[id_vaga], '$data')");

                                        $res = mysqli_query($con, "SELECT id_empresa, cargo FROM TB_VV_VAGAS WHERE id = $_POST[id_vaga]");
                                        $vaga = mysqli_fetch_array($res);

                                        $res = mysqli_query($con, "SELECT email, nome_empresa  FROM TB_VV_EMPRESAS WHERE id = $vaga[id_empresa]");
                                        $empresa = mysqli_fetch_array($res);

                                        $to = $empresa['email'];

                                        $subject = 'Você tem uma nova candidatura em sua vaga no Vagas&Vagas!';

                                        $message = '
							<html>
							<head>
							</head>
							<body>
								<div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2; font-family: "Comic Sans MS", cursive, sans-serif">
									<img src="' . HOME . '/images/top.png">
									<div class="container" style="padding:20px">
										<table>
											<tr>
												<td>
													<h2><b>Olá ' . $empresa['nome_empresa'] . '</b></h2>
													<br/>
													<br/>Queremos dar os parabéns pois, a sua vaga ' . $vaga['cargo'] . ' está chamando a atenção dos candidatos!
													<br/>
													<br/>' . $_SESSION['nome'] . ' acabou de se candidatar ao cargo oferecido e, você poderá visualizar mais informações ou entrar em contato através do seu painel.
													<br/>
													<br/>Para acessar agora o portal Vagas&Vagas, acesse o link a seguir: <a href="' . HOME . '/empresas/" style="color:blue">' . HOME . '/empresas</a>
													<br/>
													<br/>
													<br/>Muito obrigado,
													<br/>Equipe VagaseVagas.
												</td>
												<td>
													<img src="' . HOME . '/images/confirmado.png">
												</td>
											</tr>
										</table>
										<br/>
										<br/>
										<div class="row" align="right">
											<a href="' . FACEBOOK . '" style="text-decoration:none">
												<img src="' . HOME . '/images/facebook.png">
											</a>
											<a href="' . TWITTER . '" style="text-decoration:none">
												<img src="' . HOME . '/images/twitter.png">
											</a>
											<a href="' . PLUS . '" style="text-decoration:none">
												<img src="' . HOME . '/images/googlep.png">
											</a>
										</div>
									</div>
									<img src="' . HOME . '/images/top.png">
								</div>
							</body>
							</html>
						';

                                        $headers = 'MIME-Version: 1.0' . "\r\n";
                                        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                                        $headers .= 'From: Vagas&Vagas <naoresponder@vagasevagas.com.br>' . "\r\n";

                                        mail($to, $subject, $message, $headers);

                                        echo "<script>top.location.href='ver-oportunidade.php?id=$_POST[id_vaga]&candidatura=success'</script>";
                                    }
                                } else {
                                    echo "<script>top.location.href='admin'</script>";
                                }
                            }
                        }
                    } else {
                        if ($_POST['id_vaga'] == "") {
                            echo "<script>top.location.href='buscar-vagas.php?vaga=$_POST[id_vaga]&password=invalid'</script>";
                        } else {
                            echo "<script>top.location.href='index.php?password=invalid'</script>";
                        }
                    }
                } else {
                    if ($_POST['id_vaga'] == "") {
                        echo "<script>top.location.href='index.php?user=invalid'</script>";
                    } else {
                        echo "<script>top.location.href='buscar-vagas.php?vaga=$_POST[id_vaga]&user=invalid'</script>";
                    }
                }
            }
        } else {
            echo "Erro de conexão com Facebook";
            exit(0);
        }
    } else {
        echo "Erro de conexão com Facebook";
        exit(0);
    }
} else if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['error'])) {
    echo 'Permissão não concedida';
}