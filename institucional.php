<?php
session_start();

include "includes/conexao.php";

$res = mysqli_query($con, "SELECT * FROM TB_VV_INSTITUCIONAL WHERE id_institucional = 1");
$institucional = mysqli_fetch_array($res);

?>

<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="publicidade">
                    <div class="container">
                        <div class="col-sm-12">
                            <span>Publicidade</span>
                            <?php if (!empty($publi1['link'])) { ?>
                                <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </section>
                <section class="institucional">
                    <div class="container">
                        <h1 class="text-center">Institucional</h1>
                        <p><?php echo str_replace("</p>", "</p><br/>", $institucional['texto']) ?></p>
                    </div>
                </section>
                <section class="publicidade">
                    <div class="container">
                        <div class="col-sm-12">
                            <span>Publicidade</span>
                            <?php if (!empty($publi2['link'])) { ?>
                                <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
