<?php
include_once '../includes/defines.php';

?>
<form class="active" action="<?php echo PATH_ALL . '/includes/suspender.php' ?>" method="post">
    <h2>Mensagem</h2>
    <input type="hidden" name="id" id="id" value="<?php echo $_SESSION['id'] ?>"/>
    <div class="full msg-alterar">
        <span>Você não receberá mais notificações de vagas e de suas candidaturas no seu E-mail, você deseja realmente suspender essas informações?</span>
    </div>
    <div class="full text-center">
        <a href="#" class="btn button">
            Suspender
        </a>
        <a href="#" class="btn button cancelar">
            Cancelar
        </a>
    </div>
</form>