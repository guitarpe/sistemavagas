<?php
include_once '../includes/defines.php';
include_once "../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');

$res = mysqli_query($con, "SELECT
                                id,
                                id_candidato,
                                idioma,
                                nivel
                           FROM TB_VV_IDIOMAS_CAND
                           WHERE id=$id AND id_candidato=$usuario") or die(mysqli_error($con));

$dados = mysqli_fetch_array($res);

?>
<form class="active" method="post" action="<?php echo PATH_CANDIDATOS . '/actions/editar-info.php?tipo=5' ?>">
    <div class="form-group row">
        <input type="hidden" name="edit_idioma_id" id="edit_idioma_id" value="<?php echo $id ?>"/>
        <input type="hidden" name="edit_idioma_usuario" id="edit_idioma_usuario" value="<?php echo $usuario ?>"/>
    </div>
    <div class="form-group row">
        <div class="col-md-12">
            <div class="input-group">
                <select name="idioma" id="idioma" class="form-control">
                    <option value="">Idioma</option>
                    <?php
                    $result1 = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                    while ($val = mysqli_fetch_array($result1)) {

                        ?>
                        <option value="<?php echo $val['descricao']; ?>" <?php echo $dados['idioma'] == $val['descricao'] ? 'selected="selected"' : '' ?>><?php echo $val['descricao'] ?></option>
                        <?php
                    }

                    ?>
                </select>
                <span class="input-group-addon">
                    <i class="fa fa-plus"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12">
            <div class="input-group">
                <select name="nivelidioma" id="nivelidioma" class="form-control">
                    <option value="">Nível</option>
                    <?php
                    $result2 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                    while ($val = mysqli_fetch_array($result2)) {

                        ?>
                        <option value="<?php echo $val['descricao']; ?>" <?php echo $dados['nivel'] == $val['descricao'] ? 'selected="selected"' : '' ?>><?php echo $val['descricao'] ?></option>
                        <?php
                    }

                    ?>
                </select>
                <span class="input-group-addon">
                    <i class="fa fa-plus"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-12">
            <div class="col-sm-6 col-xs-6 text-center">
                <button type="submit">
                    Atualizar
                </button>
            </div>
            <div class="col-sm-6 col-xs-6 text-center">
                <a href="#" class="btn button cancelar">Cancelar</a>
            </div>
        </div>
    </div>
</form>