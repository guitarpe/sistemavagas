<?php
session_start();

include_once '../includes/defines.php';
include_once '../includes/conexao.php';

$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');

if (intval($id) == 0) {
    $sql = "SELECT id, cargo FROM VW_VAGAS WHERE id_empresa=" . $_SESSION['id'];
    $res_cand = mysqli_query($con, $sql)or die(mysqli_error($con));
}

?>
<form class="active" action="<?php echo PATH_ALL . '/empresas/actions/selecionar-candidato.php' ?>" method="post">
    <div class="full" style="margin-bottom: 20px; text-align:center;">
        <span>Deseja realmente selecionar este candidato?</span>
    </div>
    <?php if (intval($id) == 0) { ?>
        <div class="full" style="margin-bottom: 20px; text-align:center;">
            <select name="vaga" id="desc_id_vaga" class="form-control select2">
                <option value="">Selecione a Vaga</option>
                <?php while ($row = mysqli_fetch_array($res_cand)) { ?>
                    <option value="<?php echo $row['id'] ?>"><?php echo $row['cargo'] ?></option>
                <?php } ?>
            </select>
        </div>
    <?php } else { ?>
        <?php $vagas = explode(',', $id); ?>
        <?php if (count($vagas) > 1) { ?>
            <?php
            $sql = "SELECT id, cargo FROM VW_VAGAS WHERE id in(" . $id . ") AND id_empresa=" . $_SESSION['id'];
            $res_cand = mysqli_query($con, $sql)or die(mysqli_error($con));

            ?>
            <div class="full" style="margin-bottom: 20px; text-align:center;">
                <select name="vaga" id="desc_id_vaga" class="form-control select2">
                    <option value="">Selecione a Vaga</option>
                    <?php while ($row = mysqli_fetch_array($res_cand)) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['cargo'] ?></option>
                    <?php } ?>
                </select>
            </div>
        <?php } else { ?>
            <input type="hidden" id="desc_id_vaga" name="vaga" value="<?php echo $id ?>">
        <?php } ?>
    <?php } ?>
    <div class="full text-center">
        <input type="hidden" id="desc_id_cand" name="usuario" value="<?php echo $usuario ?>">
        <button type="submit">
            Selecionar
        </button>
    </div>
</form>