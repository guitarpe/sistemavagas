<?php
include_once '../includes/defines.php';

?>
<form class="active">
    <div class="full text-center">
        <i class="fa fa-user-times msg-excluir"></i>
    </div>
    <div class="full margin-bottom-20">
        <span>Você tem certeza que deseja iniciar o procedimento para <b>Excuir Cadastro</b> de seu currículo no portal Vagas & Vagas? Se você tem certeza, clique em <b>Continuar</b> para iniciar o processo de cancelamento.</span>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-6 col-xs-6 text-center">
            <a href="<?php echo PATH_ALL . '/candidatos/excluir-cadastro.php' ?>" class="btn button">
                Continuar <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
        </div>
        <div class="col-sm-6 col-xs-6 text-center">
            <a href="#" class="btn button cancelar">Cancelar</a>
        </div>
    </div>
</form>