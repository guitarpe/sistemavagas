<?php
include_once '../includes/defines.php';
include_once "../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');

$res = mysqli_query($con, "SELECT
                                id,
                                id_candidato,
                                nivel,
                                instituicao,
                                curso,
                                semestre,
                                situacao,
                                fim,
                                turno
                           FROM TB_VV_FORMACOES_CAND
                           WHERE id=$id AND id_candidato=$usuario") or die(mysqli_error($con));

$dados = mysqli_fetch_array($res);

?>
<script>
    $(document).ready(function () {
<?php if ($dados['nivel'] === "Curso técnico" || $dados['nivel'] === "Ensino superior" || $dados['nivel'] === "Pós-graduação") { ?>
            $('.div_formacao').find('.campo_nomecurso').removeClass('hidden');
            $('.div_formacao').find('.campo_situacao').removeClass('hidden');
            $('.div_formacao').find('.campo_instituicao').removeClass('hidden');
            $('.div_formacao').find('.campo_ano_conclusao').addClass('hidden');
            $('.div_formacao').find('.campo_turno').addClass('hidden');
            $('.div_formacao').find('.campo_semestre').addClass('hidden');
<?php } else if ($dados['nivel'] === "Ensino fundamental" || $dados['nivel'] === "Ensino médio") { ?>
            $('.div_formacao').find('.campo_nomecurso').addClass('hidden');
            $('.div_formacao').find('.campo_situacao').removeClass('hidden');
            $('.div_formacao').find('.campo_instituicao').removeClass('hidden');
            $('.div_formacao').find('.campo_ano_conclusao').addClass('hidden');
            $('.div_formacao').find('.campo_turno').addClass('hidden');
            $('.div_formacao').find('.campo_semestre').addClass('hidden');
<?php } ?>

<?php if ($dados['nivel'] === "Curso técnico" || $dados['nivel'] === "Ensino superior" || $dados['nivel'] === "Pós-graduação") { ?>


    <?php if ($dados['situacao'] === "Concluído") { ?>
                $('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $('.div_formacao').find('.campo_turno').addClass('hidden');
                $('.div_formacao').find('.campo_semestre').addClass('hidden');
    <?php } else if ($dados['situacao'] === "Em Andamento") { ?>
                $('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsão de Conclusão');
                $('.div_formacao').find('.campo_turno').removeClass('hidden');
                $('.div_formacao').find('.campo_semestre').removeClass('hidden');
    <?php } else if ($dados['situacao'] === "Trancado") { ?>
                $('.div_formacao').find('.campo_ano_conclusao').addClass('hidden');
                $('.div_formacao').find('.campo_turno').addClass('hidden');
                $('.div_formacao').find('.campo_semestre').removeClass('hidden');
    <?php } ?>
<?php } else if ($dados['nivel'] === "Ensino fundamental" || $dados['nivel'] === "Ensino médio") { ?>

    <?php if ($dados['situacao'] === "Concluído") { ?>
                $('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $('.div_formacao').find('.campo_turno').addClass('hidden');
                $('.div_formacao').find('.campo_semestre').addClass('hidden');
    <?php } else if ($dados['situacao'] === "Em Andamento") { ?>
                $('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsao de Conclusão');
                $('.div_formacao').find('.campo_turno').removeClass('hidden');
                $('.div_formacao').find('.campo_semestre').addClass('hidden');
    <?php } else if ($dados['situacao'] === "Trancado") { ?>
                $('.div_formacao').find('.campo_ano_conclusao').addClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $('.div_formacao').find('.campo_turno').addClass('hidden');
                $('.div_formacao').find('.campo_semestre').addClass('hidden');
    <?php } else { ?>
                $('.div_formacao').find('.campo_ano_conclusao').addClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $('.div_formacao').find('.campo_turno').addClass('hidden');
                $('.div_formacao').find('.campo_semestre').addClass('hidden');
    <?php } ?>
<?php } ?>
    });
</script>
<form class="active" method="post" action="<?php echo PATH_CANDIDATOS . '/actions/editar-info.php?tipo=3' ?>">
    <div class="div_formacao">
        <div class="form-group row">
            <input type="hidden" name="edit_form_id" id="edit_form_id" value="<?php echo $id ?>"/>
            <input type="hidden" name="edit_form_usuario" id="edit_form_usuario" value="<?php echo $usuario ?>"/>
            <div class="col-md-4">
                <div class="input-group">
                    <select name="formacaomod" id="formacaomod" class="form-control slct_formacao">
                        <option value="">Formação</option>
                        <?php
                        $result5 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                        while ($val = mysqli_fetch_array($result5)) {

                            ?>
                            <option value="<?php echo $val['descricao'] ?>" <?php echo $dados['nivel'] == $val['descricao'] ? 'selected="selected"' : '' ?>><?php echo $val['descricao'] ?></option>
                            <?php
                        }

                        ?>
                    </select>
                    <span class="input-group-addon">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4 campo_instituicao hidden">
                <div class="input-group">
                    <input type="text" placeholder="Instituição de ensino" name="instituicaomod" id="instituicaomod" class="form-control" value="<?php echo $dados['instituicao'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-university" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4 campo_situacao hidden">
                <div class="input-group">
                    <select name="situacaomod" id="situacaomod" class="form-control slct_situacao">
                        <option value="">Situação</option>
                        <?php
                        $result4 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                        while ($val = mysqli_fetch_array($result4)) {
                            if ($val["nome"] == "Trancado") {

                                ?>
                                <option value="<?php echo $val['nome'] ?>" <?php echo $dados['situacao'] == $val['nome'] ? 'selected="selected"' : '' ?> id="trancado"><?php echo $val['nome'] ?></option>
                                <?php
                            } else {

                                ?>
                                <option value="<?php echo $val['nome'] ?>" <?php echo $dados['situacao'] == $val['nome'] ? 'selected="selected"' : '' ?>><?php echo $val['nome'] ?></option>
                                <?php
                            }
                        }

                        ?>
                    </select>
                    <span class="input-group-addon">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12 campo_nomecurso hidden">
                <div class="input-group">
                    <input type="text" placeholder="Nome do curso" name="nomeformacaomod" id="nomeformacaomod" class="form-control" value="<?php echo $dados['curso'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-bookmark" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 campo_ano_conclusao hidden">
                <div class="input-group">
                    <input class="data-ano form-control info_ano" type="text" placeholder="Ano de conclusão" name="anofimmod" id="anofimmod" value="<?php echo $dados['fim'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4 campo_semestre hidden">
                <div class="input-group">
                    <select name="semestremod" id="semestremod" class="form-control">
                        <option value="" <?php echo dados['semestre'] == "" ? 'selected="selected"' : '' ?>>Semestre</option>
                        <option value="1" <?php echo dados['semestre'] == 1 ? 'selected="selected"' : '' ?>>1º semestre</option>
                        <option value="2" <?php echo dados['semestre'] == 2 ? 'selected="selected"' : '' ?>>2º semestre</option>
                        <option value="3" <?php echo dados['semestre'] == 3 ? 'selected="selected"' : '' ?>>3º semestre</option>
                        <option value="4" <?php echo dados['semestre'] == 4 ? 'selected="selected"' : '' ?>>4º semestre</option>
                        <option value="5" <?php echo dados['semestre'] == 5 ? 'selected="selected"' : '' ?>>5º semestre</option>
                        <option value="6" <?php echo dados['semestre'] == 6 ? 'selected="selected"' : '' ?>>6º semestre</option>
                        <option value="7" <?php echo dados['semestre'] == 7 ? 'selected="selected"' : '' ?>>7º semestre</option>
                        <option value="8" <?php echo dados['semestre'] == 8 ? 'selected="selected"' : '' ?>>8º semestre</option>
                        <option value="9" <?php echo dados['semestre'] == 9 ? 'selected="selected"' : '' ?>>9º semestre</option>
                        <option value="10" <?php echo dados['semestre'] == 10 ? 'selected="selected"' : '' ?>>10º semestre</option>
                        <option value="11" <?php echo dados['semestre'] == 11 ? 'selected="selected"' : '' ?>>11º semestre</option>
                        <option value="12" <?php echo dados['semestre'] == 12 ? 'selected="selected"' : '' ?>>12º semestre</option>
                        <option value="13" <?php echo dados['semestre'] == 13 ? 'selected="selected"' : '' ?>>13º semestre</option>
                        <option value="14" <?php echo dados['semestre'] == 14 ? 'selected="selected"' : '' ?>>14º semestre</option>
                        <option value="15" <?php echo dados['semestre'] == 15 ? 'selected="selected"' : '' ?>>15º semestre</option>
                        <option value="16" <?php echo dados['semestre'] == 16 ? 'selected="selected"' : '' ?>>16º semestre</option>
                    </select>
                    <span class="input-group-addon">
                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4 campo_turno hidden">
                <div class="input-group">
                    <select name="turnomod" id="turnomod" class="form-control">
                        <option value="" <?php echo $dados['turno'] == "" ? '' : '' ?>>Turno</option>
                        <option value="Manhã" <?php echo $dados['turno'] == "Manhã" ? '' : '' ?>>Manhã</option>
                        <option value="Tarde" <?php echo $dados['turno'] == "Tarde" ? '' : '' ?>>Tarde</option>
                        <option value="Noite" <?php echo $dados['turno'] == "Noite" ? '' : '' ?>>Noite</option>
                        <option value="EAD" <?php echo $dados['turno'] == "EAD" ? '' : '' ?>>EAD</option>
                    </select>
                    <span class="input-group-addon">
                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="col-sm-6 col-xs-6 text-center">
                    <button type="submit">
                        Atualizar
                    </button>
                </div>
                <div class="col-sm-6 col-xs-6 text-center">
                    <a href="#" class="btn button cancelar">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
</form>