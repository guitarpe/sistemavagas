<?php
include_once '../includes/defines.php';
include_once "../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');

$res = mysqli_query($con, "SELECT
                                id,
                                id_candidato,
                                nome,
                                instituicao,
                                situacao,
                                turnoextra,
                                diassemana,
                                cargahoraria,
                                anofim
                           FROM TB_VV_CURSOS_CAND
                           WHERE id=$id AND id_candidato=$usuario") or die(mysqli_error($con));

$dados = mysqli_fetch_array($res);

?>
<script>
    $(document).ready(function () {
<?php if ($dados['situacao'] === "Concluído") { ?>

            $('.div_cursosmod').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
            $('.div_cursosmod').find('.campo_turno').addClass('hidden');
            $('.div_cursosmod').find('.dias_semanas').addClass('hidden');

            $('#slct_0mod').multiselect("destroy");

<?php } else if ($dados['situacao'] === "Em Andamento") { ?>

            $('.div_cursosmod').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsao de Conclusão');
            $('.div_cursosmod').find('.campo_turno').removeClass('hidden');
            $('.div_cursosmod').find('.dias_semanas').removeClass('hidden');

            var diass = '<?php echo $dados['diassemana'] ?>';
            var arrayop = diass.split(", ");

            $.map(arrayop, function (key) {
                $("#slct_0mod option[value='" + key + "']").prop("selected", true);
            });

            $('#slct_0mod').multiselect({
                nonSelectedText: 'Selecione os Dias da Semana',
                enableFiltering: false,
                enableCaseInsensitiveFiltering: false,
                buttonWidth: '100%',
                nSelectedText: 'dias',
                allSelectedText: 'Todos',
                delimiter: ', ',
                onChange: function (element, checked) {

                    var brands = $('#slct_0mod option:selected');

                    var selected = [];

                    $(brands).each(function (index, brand) {
                        selected.push($(this).val());
                    });

                    $('#dias_semana_0mod').val(selected.join(', '));
                }
            });
<?php } else if ($dados['situacao'] === "Trancado") { ?>

            $('.div_cursosmod').find('.campo_ano_conclusao').addClass('hidden');
            $('.div_cursosmod').find('.campo_turno').addClass('hidden');
            $('.div_cursosmod').find('.dias_semanas').addClass('hidden');

            $('#slct_0mod').multiselect("destroy");
<?php } ?>
        $('#slct_0mod').multiselect("refresh");
    });

</script>
<form class="active div_cursosmod" method="post" action="<?php echo PATH_CANDIDATOS . '/actions/editar-info.php?tipo=4' ?>">
    <div class="form-group row">
        <input type="hidden" name="edit_cur_id" id="edit_cur_id" value="<?php echo $id ?>"/>
        <input type="hidden" name="edit_cur_usuario" id="edit_cur_usuario" value="<?php echo $usuario ?>"/>
        <div class="col-md-8">
            <div class="input-group">
                <input type="text" placeholder="Nome do curso" id="nomecursomod" name="nomecursomod" class="form-control" value="<?php echo $dados['nome'] ?>">
                <span class="input-group-addon">
                    <i class="fa fa-bookmark" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" placeholder="Carga Horária" id="cargahorariamod" name="cargahorariamod" class="form-control" value="<?php echo $dados['cargahoraria'] ?>">
                <span class="input-group-addon">
                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-8">
            <div class="input-group">
                <input type="text" placeholder="Instituição de ensino" id="instituicao2mod" name="instituicao2mod" class="form-control" value="<?php echo $dados['instituicao'] ?>">
                <span class="input-group-addon">
                    <i class="fa fa-university" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <select id="situacao2mod" name="situacao2mod" class="form-control slct_situacao2mod" data-length="0">
                    <option value="">Situação</option>
                    <?php
                    $result3 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                    while ($val = mysqli_fetch_array($result3)) {

                        ?>
                        <option value="<?php echo $val['nome'] ?>" <?php echo $dados['situacao'] == $val['nome'] ? 'selected="selected"' : '' ?>><?php echo $val['nome'] ?></option>
                        <?php
                    }

                    ?>
                </select>
                <span class="input-group-addon">
                    <i class="fa fa-check" aria-hidden="true"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4 campo_turno">
            <div class="input-group">
                <select name="turnoextramod" id="turnoextramod" class="form-control">
                    <option value="" <?php echo $dados['turnoextra'] == "" ? 'selected="selected"' : '' ?>>Turno</option>
                    <option value="Manhã" <?php echo $dados['turnoextra'] == "Manhã" ? 'selected="selected"' : '' ?>>Manhã</option>
                    <option value="Tarde" <?php echo $dados['turnoextra'] == "Tarde" ? 'selected="selected"' : '' ?>>Tarde</option>
                    <option value="Noite" <?php echo $dados['turnoextra'] == "Noite" ? 'selected="selected"' : '' ?>>Noite</option>
                    <option value="EAD" <?php echo $dados['turnoextra'] == "EAD" ? 'selected="selected"' : '' ?>>EAD</option>
                </select>
                <span class="input-group-addon">
                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        <div class="col-md-4 dias_semanas">
            <div class="input-group">
                <select class="form-control dias_semana" id="slct_0mod" multiple>
                    <option value="Domingo">Domingo</option>
                    <option value="Segunda-feira">Segunda-feira</option>
                    <option value="Terca-feira">Terça-feira</option>
                    <option value="Quarta-feira">Quarta-feira</option>
                    <option value="Quinta-feira">Quinta-feira</option>
                    <option value="Sexta-feira">Sexta-feira</option>
                    <option value="Sábado">Sábado</option>
                </select>
                <input type="hidden" class="dias_semana_hiden" id="dias_semana_0mod" id="diassemanamod" name="diassemanamod" value="<?php echo $dados['diassemana'] ?>"/>
                <span class="input-group-addon">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        <div class="col-md-4 campo_ano_conclusao">
            <div class="input-group">
                <input class="data-ano form-control ano_conclusao" type="text" placeholder="Ano de conclusão" id="anofim2mod" name="anofim2mod" value="<?php echo $dados['anofim'] ?>">
                <span class="input-group-addon">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="col-sm-6 col-xs-6 text-center">
                <button type="submit">
                    Atualizar
                </button>
            </div>
            <div class="col-sm-6 col-xs-6 text-center">
                <a href="#" class="btn button cancelar">Cancelar</a>
            </div>
        </div>
    </div>
</form>