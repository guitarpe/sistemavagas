<?php
include_once '../includes/defines.php';
$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');

?>
<form class="active" action="<?php echo PATH_ALL . '/empresas/actions/recebe_descartacandidato.php' ?>" method="post">
    <div class="full" style="margin-bottom: 20px; text-align:center;">
        <span>Deseja realmente descartar o candidato?</span>
    </div>
    <div class="full text-center">
        <input type="hidden" id="desc_id_vaga" name="desc_id_vaga" value="<?php echo $id ?>">
        <input type="hidden" id="desc_id_cand" name="desc_id_cand" value="<?php echo $usuario ?>">
        <button type="submit">
            Descartar
        </button>
    </div>
</form>