<?php
include_once '../includes/defines.php';

$vaga = filter_input(INPUT_GET, 'vaga');
$usuario = filter_input(INPUT_GET, 'usuario');

?>
<form class="active" action="<?php echo PATH_ALL . '/candidatos/actions/recebe_excluircandidatura.php' ?>" method="post">
    <div class="full text-center">
        <i class="fa fa-trash mgs-excluir-cand"></i>
    </div>
    <div class="full mgs-alterar-check">
        <h4>Deseja excluir sua candidatura para esta vaga ?</h4>
    </div>
    <div class="full text-center">
        <input type="hidden" id="del_id_vaga" name="del_id_vaga" value="<?php echo $vaga ?>">
        <input type="hidden" id="del_id_cand" name="del_id_cand" value="<?php echo $usuario ?>">
        <button type="submit">
            Excluir
            <i class="fa fa-angle-double-right"></i>
        </button>
    </div>
</form>