<?php
include_once '../includes/defines.php';
$id = filter_input(INPUT_GET, 'id');

?>
<form class="active" action="<?php echo PATH_CANDIDATOS . '/actions/recebe_excluirmsg.php' ?>" method="post">
    <div class="full text-center">
        <i class="fa fa-trash-o mgs-excluir-msg"></i>
    </div>
    <div class="full msg-alterar">
        <span>Deseja excluir esta mensagem?</span>
    </div>
    <div class="full text-center">
        <input type="hidden" id="del_id_msg" name="id" value="<?php echo $id ?>">
        <button type="submit">
            Excluir
            <i class="fa fa-angle-double-right"></i>
        </button>
    </div>
</form>