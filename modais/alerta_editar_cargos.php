<?php
include_once '../includes/defines.php';
include_once "../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');

$res = mysqli_query($con, "SELECT id, id_candidato, cargo FROM TB_VV_CARGOS_CAND WHERE id=$id AND id_candidato=$usuario") or die(mysqli_error($con));

$dados = mysqli_fetch_array($res);

?>
<form class="active" method="post" action="<?php echo PATH_CANDIDATOS . '/actions/editar-info.php?tipo=1' ?>">
    <div class="full margin-bottom-20">
        <div class="input-group">
            <input type="hidden" name="edit_cargo_id" class="edit_cargo_id" value="<?php echo $id ?>"/>
            <input type="hidden" name="edit_cargo_usuario" class="edit_cargo_usuario" value="<?php echo $usuario ?>"/>
            <select name="editcargo" id="editcargo" class="form-control select2 cargoop">
                <option value="">Cargo Profissional</option>
                <?php
                $result_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                while ($val = mysqli_fetch_array($result_cargos)) {

                    ?>
                    <option value="<?php echo $val['nome'] ?>" <?php echo $dados['cargo'] == $val['nome'] ? 'selected="selected"' : '' ?>><?php echo $val['nome'] ?></option>
                    <?php
                }

                ?>
            </select>
            <span class="input-group-addon">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </span>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-6 col-xs-6 text-center">
            <button type="submit">
                Atualizar
            </button>
        </div>
        <div class="col-sm-6 col-xs-6 text-center">
            <a href="#" class="btn button cancelar">Cancelar</a>
        </div>
    </div>
</form>