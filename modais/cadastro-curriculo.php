<?php
include_once '../includes/defines.php';
include_once "../includes/conexao.php";

?>
<div id="cadastro">
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="col-sm-4 active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Etapa 1">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-user  "></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="col-sm-4 disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Etapa 2">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-briefcase"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="col-sm-4 disabled">
                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Finalização">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-education"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <form action="<?php echo PATH_ALL . '/actions/recebe_candidato.php' ?>" method="post" role="form">
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Cadastro de candidato - ETAPA 1</h3>
                        </div>
                        <div class="panel-body">
                            <p align="center">
                                Olá, vamos ajudar você a preencher seu cadastro, começando pelo básico. Preencha as informações abaixo e clique em próximo.
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-envelope"></i>
                                        </span>
                                        <input type="text" placeholder="E-mail" name="email" id="txtEmail" class="form-control validemail" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-envelope"></i>
                                        </span>
                                        <input type="text" placeholder="Confirmar e-mail" name="repeteemail" id="repetir_email" class="form-control validemail nopaste" required autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-asterisk"></i>
                                        </span>
                                        <input type="password" placeholder="Senha" name="senha" class="form-control txtSenha" required maxlength="6">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-asterisk"></i>
                                        </span>
                                        <input type="password" placeholder="Confirmar senha" name="confirmar-senha" class="form-control confisenha nopaste" required maxlength="6">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-user"></i>
                                        </span>
                                        <input type="text" placeholder="Nome" name="nome" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-user"></i>
                                        </span>
                                        <input class="cpf form-control verifica_cpf_cadastrado" type="text" placeholder="CPF" id="cpf" name="cpf" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                        <input class="data form-control" type="text" placeholder="Data de Nascimento" name="datanasc" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12 area-radio-form">
                                    <label class="opckb fleft">Sexo</label>
                                    <div class="form-radio form-control-inline fleft">
                                        <input type="radio" id="sexo1" name="sexo" class="form-control-input" title="Sexo" value="Masculino" required>
                                        <label class="opckb form-control-label" for="sexo1">Masculino </label>
                                    </div>
                                    <div class="form-radio form-control-inline fleft">
                                        <input type="radio" id="sexo2" name="sexo" class="form-control-input" title="Sexo" value="Feminino" required>
                                        <label class="opckb form-control-label" for="sexo2">Feminino </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-earphone"></i>
                                        </span>
                                        <input class="form-control fones" type="text" placeholder="Telefone" name="fone" id="telefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-earphone"></i>
                                        </span>
                                        <input class="form-control fones" type="text" placeholder="Celular" name="celular" id="celular" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-globe"></i>
                                        </span>
                                        <select name="estado" id="estado" title="Estado" class="form-control busca_cidades" required>
                                            <option value="">Estado</option>
                                            <?php
                                            $result = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                            while ($val = mysqli_fetch_array($result)) {
                                                echo "<option value='$val[uf]'>$val[nome]</option>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-globe"></i>
                                        </span>
                                        <select name="cidade" id="cidades" title="Cidade" class="form-control cidades" required>
                                            <option value="">Cidade</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="termos" title="Termos e Condições" name="termos" required>
                                        <label class="opckb form-check-label" for="termos">
                                            Li e aceito os <a href="#" class="link_termos">Termos e Condições</a> para uso de políticas de privacidade
                                        </label>
                                    </div>
                                </div>
                            </div>
                            </br>
                            <ul class="list-inline">
                                <li class="pull-left">
                                    <a href="#" target="entrar" class="link link-js pull-left">
                                        Já tem seu cadastro? Faça login agora!
                                    </a>
                                </li>
                                <li class="pull-right">
                                    <button type="button" class="btn btn-lg btn-primary next-step">
                                        Próximo passo <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Cadastro de candidato - ETAPA 2</h3>
                        </div>
                        <div class="panel-body">
                            <p align="center">
                                Vamos falar sobre o seu perfil profissional? Estamos especialmente interessados em conhecer suas experiências anteriores e qual Vaga de Emprego você está procurando, assim poderemos lhe ajudar melhor. Selecione seu Cargo Profissional e clique no botão INCLUIR. Você pode incluir até 3 Cargos Profissionais.
                            </p>
                            <div class="row">
                                <div id="origem">
                                    <div class="div_cargo">
                                        <div class="col-md-12">
                                            <div class="input-group" style="margin-bottom: 10px">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                </span>
                                                <select id="select_cargo" name="cargoprofissional[]" title="Cargo Profissional" class="form-control select2 cargoop" required>
                                                    <option value="">Cargo Profissional</option>
                                                    <?php
                                                    $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                                    while ($val = mysqli_fetch_array($res_cargos)) {
                                                        echo "<option value='$val[nome]'>$val[nome]</option>";
                                                    }

                                                    ?>
                                                    <option>Outro</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="outro_cargo hidden">
                                                <div class="input-group" class="third hidden" id="label-ncargo">
                                                    <input class="form-control" type="text" name="cargoprofissional[]" placeholder="Digite o Cargo Profissional" id="novocargo" >
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-md-2">
                                        <input type="button" value="Adicionar Cargo" id="add_cargo" class="btn btn-warning form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="button" value="Excluir Cargo" id="rem_cargo" class="btn btn-danger form-control" style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 area-radio-form">
                                    <label class="fleft">Você ja possui experiência profissional?</label>
                                    <div class="form-radio form-control-inline fleft">
                                        <input type="radio" id="experiencia1" name="experiencia" class="form-control-input" value="Não" checked="checked">
                                        <label class="form-control-label" for="experiencia1">Não</label>
                                    </div>
                                    <div class="form-radio form-control-inline fleft">
                                        <input type="radio" id="experiencia2" name="experiencia" class="form-control-input" value="Sim">
                                        <label class="form-control-label" for="experiencia2">Sim</label>
                                    </div>
                                </div>
                            </div>
                            <div id="origem_empresa" style="display: none">
                                <p>
                                    Legal! Agora, nos descreva brevemente suas últimas experiências profissionais, ou as experiências profissionais mais importantes, que mais agregaram na sua carreira.
                                    Preencha as informações sobre sua Experiência Profissional e clique no botão INCLUIR EXPERIÊNCIA.
                                    Você pode incluir mais de uma Experiência Profissional.
                                </p>
                                <div class="campos_empresa">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-user"></i>
                                                </span>
                                                <input type="text" placeholder="Nome da empresa" name="nomeempresa[]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-usd"></i>
                                                </span>
                                                <input class="dinheiro form-control salario" type="text" placeholder="Salário (opcional)" name="salario[]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="div_cargo_emp">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-briefcase"></i>
                                                    </span>
                                                    <select name="cargoempresa[]" class="form-control cargo" title="Cargo Profissional">
                                                        <option  value="">Cargo Profissional</option>
                                                        <?php
                                                        $result_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                                        while ($val = mysqli_fetch_array($result_cargos)) {
                                                            echo "<option>$val[nome]</option>";
                                                        }

                                                        ?>
                                                        <option>Outro</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 margin-top-10 outro_cargo hidden" id="label-ncargoexp">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-plus"></i>
                                                    </span>
                                                    <input class="form-control" type="text" placeholder="Digite o Cargo Profissional" id="novocargoexp" name="cargoempresa[]" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <textarea name="atividades[]" placeholder="Atividades Exercidas" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-calendar"></i>
                                                </span>
                                                <input class="data-s form-control data_ini" type="text" placeholder="Início (mês/ano)" name="datainicio[]">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-calendar"></i>
                                                </span>
                                                <input class="data-s form-control data_fim" type="text" placeholder="Fim (mês/ano)" name="datafim[]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-check">
                                                <input class="form-check-input set_empr_atual" type="checkbox" name="empregoatual[]" value="1">
                                                <label class="form-check-label" for="empatual">
                                                    Emprego atual
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <input type="button" value="Adicionar Experiência" id="adicionar_experiencia" class="btn btn-warning form-control">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="button" value="Excluir Experiência" id="remover_experiencia" class="btn btn-danger form-control" style="display: none">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </br>
                            <ul class="list-inline">
                                <li class="pull-left">
                                    <button type="button" class="btn btn-lg btn-default prev-step">
                                        <span class="glyphicon glyphicon-chevron-left"></span> Passo anterior
                                    </button>
                                </li>
                                <li class="pull-right">
                                    <button type="button" class="btn btn-lg btn-primary next-step">
                                        Próximo passo <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" role="tabpanel" id="step3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Finalização de candidato</h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                Sabemos que no mercado de trabalho atual, competitivo, é extremamente importante estarmos em constante atualização, inclusive dos nossos conhecimentos técnicos (saiba mais em <a href="https://www.educaretransformar.com.br/" target="_blank">www.educaretransformar.com.br</a>).
                            </p>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>Preencha abaixo a sua formação acadêmica/técnica:</strong>
                                </div>
                            </div>
                            <div class="div_formacao" style="margin-bottom: 20px">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <select name="formacao[]" class="form-control slct_formacao">
                                                <option value="">Formação</option>
                                                <?php
                                                $result5 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                                                while ($val = mysqli_fetch_array($result5)) {
                                                    echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 campo_instituicao hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <input type="text" placeholder="Instituição de ensino" name="instituicao[]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4 campo_situacao hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <select name="situacao[]" class="form-control slct_situacao">
                                                <option value="">Situação</option>
                                                <?php
                                                $result4 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                                                while ($val = mysqli_fetch_array($result4)) {
                                                    if ($val["nome"] == "Trancado") {
                                                        echo "<option value='$val[nome]' id='trancado'>$val[nome]</option>";
                                                    } else {
                                                        echo "<option value='$val[nome]'>$val[nome]</option>";
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 campo_nomecurso hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <input type="text" placeholder="Nome do curso" name="nomeformacao[]" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 campo_ano_conclusao hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <input class="data-ano form-control info_ano" type="text" placeholder="Ano de conclusão" name="anofim[]">
                                        </div>
                                    </div>
                                    <div class="col-md-4 campo_semestre hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <select name="semestre[]" class="form-control">
                                                <option value="">Semestre</option>
                                                <option value="1">1º semestre</option>
                                                <option value="2">2º semestre</option>
                                                <option value="3">3º semestre</option>
                                                <option value="4">4º semestre</option>
                                                <option value="5">5º semestre</option>
                                                <option value="6">6º semestre</option>
                                                <option value="7">7º semestre</option>
                                                <option value="8">8º semestre</option>
                                                <option value="9">9º semestre</option>
                                                <option value="10">10º semestre</option>
                                                <option value="11">11º semestre</option>
                                                <option value="12">12º semestre</option>
                                                <option value="13">13º semestre</option>
                                                <option value="14">14º semestre</option>
                                                <option value="15">15º semestre</option>
                                                <option value="16">16º semestre</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 campo_turno hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <select name="turno[]" class="form-control">
                                                <option value="">Turno</option>
                                                <option value="Manhã">Manhã</option>
                                                <option value="Tarde">Tarde</option>
                                                <option value="Noite">Noite</option>
                                                <option value="EAD">EAD</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="button" value="Adicionar Formação" id="adicionar_formacao" class="btn btn-warning form-control">
                                </div>
                                <div class="col-md-2">
                                    <input type="button" value="Excluir Formação" id="remover_formacao" class="btn btn-danger form-control" style="display: none">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>Preencha abaixo os seus cursos extracurriculares:</strong>
                                </div>
                            </div>
                            <div class="div_cursos" style="margin-bottom: 20px">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <input type="text" placeholder="Nome do curso" name="nomecurso[]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <input type="text" placeholder="Carga Horária" name="cargahoraria[]" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <input type="text" placeholder="Instituição de ensino" name="instituicao2[]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <select name="situacao2[]" class="form-control slct_situacao2" data-length="0">
                                                <option value="">Situação</option>
                                                <?php
                                                $result3 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                                                while ($val = mysqli_fetch_array($result3)) {
                                                    echo "<option value='$val[nome]'>$val[nome]</option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 campo_turno hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <select name="turnoextra[]" class="form-control">
                                                <option value="">Turno</option>
                                                <option value="Manhã">Manhã</option>
                                                <option value="Tarde">Tarde</option>
                                                <option value="Noite">Noite</option>
                                                <option value="EAD">EAD</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 dias_semanas hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <select class="form-control dias_semana" id="slct_0" multiple>
                                                <option value="Domingo">Domingo</option>
                                                <option value="Segunda-feira">Segunda-feira</option>
                                                <option value="Terca-feira">Terça-feira</option>
                                                <option value="Quarta-feira">Quarta-feira</option>
                                                <option value="Quinta-feira">Quinta-feira</option>
                                                <option value="Sexta-feira">Sexta-feira</option>
                                                <option value="Sábado">Sábado</option>
                                            </select>
                                            <input type="hidden" class="dias_semana_hiden" id="dias_semana_0" name="diassemana[]"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4 campo_ano_conclusao hidden">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-duplicate"></i>
                                            </span>
                                            <input class="data-ano form-control ano_conclusao" type="text" placeholder="Ano de conclusão" name="anofim[]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="button" value="Adicionar Curso" id="adicionar_curso" class="btn btn-warning form-control">
                                </div>
                                <div class="col-md-2">
                                    <input type="button" value="Excluir Curso" id="remover_curso" class="btn btn-danger form-control" style="display: none">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        Idiomas
                                    </strong>
                                </div>
                            </div>
                            <div class="div_idioma" style="margin-bottom: 20px">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <select name="idioma[]" class="form-control">
                                                <option value="">Idioma</option>
                                                <?php
                                                $result1 = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                                                while ($val = mysqli_fetch_array($result1)) {
                                                    echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </span>
                                            <select name="nivelidioma[]" class="form-control">
                                                <option value="">Nível</option>
                                                <?php
                                                $result2 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                                                while ($val = mysqli_fetch_array($result2)) {
                                                    echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="button" value="Adicionar Idioma" id="adicionar_idioma" class="btn btn-warning form-control">
                                </div>
                                <div class="col-md-2">
                                    <input type="button" value="Excluir Idioma" id="remover_idioma" class="btn btn-danger form-control" style="display: none">
                                </div>
                            </div>
                            </br>
                            <ul class="list-inline">
                                <li class="pull-left">
                                    <button type="button" class="btn btn-lg btn-default prev-step">
                                        <span class="glyphicon glyphicon-chevron-left"></span> Passo anterior
                                    </button>
                                </li>
                                <li class="pull-right">
                                    <button type="submit" class="btn btn-lg btn-primary btn-info-full">
                                        Finalizar Cadastro <span class="glyphicon glyphicon-ok"></span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/custom.js"></script>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/cadastrocandidato.js"></script>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/wizard.js"></script>