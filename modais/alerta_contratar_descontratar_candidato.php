<?php
include_once '../includes/defines.php';
$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');
$status = filter_input(INPUT_GET, 'status');

$msg = intval($status) == 1 ? 'Deseja realmente contratar o candidato?' : 'Deseja realmente remover a contratação do candidato?';
$msgbtn = intval($status) == 1 ? 'Contratar' : 'Descartar';

?>
<form class="active" action="<?php echo PATH_ALL . '/empresas/actions/recebe_alterar_status_candidatura.php' ?>" method="post">
    <div class="full" style="margin-bottom: 20px; text-align:center;">
        <span><?php echo $msg ?></span>
    </div>
    <div class="full text-center">
        <input type="hidden" id="desc_id_vaga" name="vaga" value="<?php echo $id ?>">
        <input type="hidden" id="desc_id_cand" name="usuario" value="<?php echo $usuario ?>">
        <input type="hidden" id="desc_id_vaga" name="status" value="<?php echo $status ?>">
        <button type="submit">
            <?php echo $msgbtn ?>
        </button>
    </div>
</form>