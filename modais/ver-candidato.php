<?php
session_start();
include"../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    echo '<h3>Você não tem acesso!</h3>';
}
if ($_SESSION['tipo'] != 'empresa' || !isset($id)) {
    echo '<h3>Você não tem acesso!</h3>';
} else {

    $sql = "SELECT
                can.id,
                can.nome,
                can.email,
                can.sexo,
                can.foto,
                can.cidade,
                can.estado AS uf,
                est.nome AS estado,
                can.fone,
                can.celular,
                can.estado_civil,
                can.obj_profissional,
                can.filhos,
                can.pretensao_salarial,
                can.viajar,
                can.veiculo,
                GROUP_CONCAT(DISTINCT cnh.categoria SEPARATOR ', ') AS categorias,
                ((YEAR(NOW()) - YEAR(can.nascimento)) - (DATE_FORMAT(NOW(), '%m%d') < DATE_FORMAT(can.nascimento, '%m%d'))) AS idade,
                (SELECT GROUP_CONCAT(DISTINCT cdt.id_vaga SEPARATOR ',') FROM TB_VV_CANDIDATURAS cdt LEFT OUTER JOIN TB_VV_VAGAS vg ON vg.id=cdt.id_vaga WHERE cdt.id_candidato=$id AND vg.id_empresa=" . $_SESSION['id'] . ") AS id_vaga
            FROM
                TB_VV_CANDIDATOS can
                LEFT OUTER JOIN TB_VV_USUARIOS us ON us.id = can.id
                LEFT OUTER JOIN TB_VV_CNH_CAND cnh ON cnh.id_candidato = can.id
                LEFT OUTER JOIN TB_VV_IDIOMAS_CAND idi ON idi.id_candidato = can.id
                LEFT OUTER JOIN TB_VV_DEFICIENCIAS_CAND def ON can.id = def.id_candidato
                LEFT OUTER JOIN TB_VV_DEFICIENCIAS df ON df.id = def.id_deficiencia
                LEFT OUTER JOIN TB_VV_FORMACOES_CAND form ON can.id = form.id_candidato
                LEFT OUTER JOIN TB_VV_INFORMATICA_CAND info ON can.id = info.id_candidato
                LEFT OUTER JOIN TB_VV_INFORMATICA inf ON inf.id = info.id_informatica
                LEFT OUTER JOIN TB_VV_ESTADOS est ON est.uf = can.estado
                LEFT OUTER JOIN TB_VV_CARGOS_CAND car ON car.id_candidato = can.id
            WHERE
                us.status = 1 AND us.tipo = 'candidato' AND can.id=$id
            GROUP BY can.id";

    $res_cand = mysqli_query($con, $sql)or die(mysqli_error($con));
    $dados = mysqli_fetch_array($res_cand);

    ?>
    <!DOCTYPE html>
    <html>
        <body>
            <section class="miolo-conteudo" style="border-top: 0px; padding-top: 0px">
                <div class="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="vagas">
                                <ul>
                                    <li style="padding: 0px; margin: 0px;">
                                        <div class="ttl">
                                            <figure>
                                                <div class="fotoperfil">
                                                    <?php if ($dados['foto']) { ?>
                                                        <img src="<?php echo PATH_CAN_IMAGENS . '/' . $dados['foto'] ?>">;
                                                        <?php
                                                    } else {

                                                        ?>
                                                        <img src="<?php echo PATH_IMAGENS . '/usuario.png' ?>">;
                                                        <?php
                                                    }

                                                    ?>
                                                </div>
                                            </figure>
                                            <div class="rt">
                                                <h6><?php echo $dados['nome'] ?></h6>
                                                <div class="item">
                                                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                    <?php
                                                    $key = 1;
                                                    $res_cargos = mysqli_query($con, "SELECT cargo FROM TB_VV_CARGOS_CAND WHERE id_candidato=" . $dados['id']);
                                                    while ($cargos = mysqli_fetch_array($res_cargos)) {
                                                        if ($key < mysqli_num_rows($res_cargos)) {
                                                            echo $cargos['cargo'] . " / ";
                                                        } else {
                                                            echo $cargos['cargo'];
                                                        }
                                                        $key++;
                                                    }

                                                    ?>
                                                </div>
                                                <div class="item">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <?php echo $dados['cidade'] . "/" . $dados['uf'] ?>
                                                </div>
                                                <div class="item">
                                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                                    <?php echo $dados['idade'] . " anos" ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info">
                                            <div class="caracteristicas">
                                                <div class="col">
                                                    <strong>
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        E-mail: <?php echo $dados['email'] ?>
                                                    </strong>
                                                    <?php if ($dados['sexo'] == "Masculino") { ?>
                                                        <strong>
                                                            <i class="fa fa-male" aria-hidden="true"></i>
                                                            Sexo: <?php echo $dados['sexo'] ?>
                                                        </strong>
                                                    <?php } else { ?>
                                                        <strong>
                                                            <i class="fa fa-female" aria-hidden="true"></i>
                                                            Sexo: <?php echo $dados['sexo'] ?>
                                                        </strong>
                                                    <?php } ?>
                                                    <strong>
                                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                                        Telefone:
                                                        <?php
                                                        if ($dados['fone'] != "" && $dados['fone'] != null) {
                                                            echo $dados['fone'];
                                                            if ($dados['celular'] != "" && $dados['celular'] != null) {
                                                                echo " / " . $dados['celular'];
                                                            }
                                                        } else if ($dados['celular'] != "" && $dados['celular'] != null) {
                                                            echo $dados['celular'];
                                                        }

                                                        ?>
                                                    </strong>
                                                    <?php if ($dados['estado_civil'] != "" && $dados['estado_civil'] != null) { ?>
                                                        <strong>
                                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                                            Estado civil: <?php echo $dados['estado_civil']; ?>
                                                        </strong>
                                                        <?php
                                                    }
                                                    if ($dados['filhos'] != "" && $dados['filhos'] != null) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-users" aria-hidden="true"></i>
                                                            Filhos:
                                                            <?php
                                                            if ($dados['filhos'] == 1) {
                                                                echo $dados['filhos'] . " filho";
                                                            } else if ($dados['filhos'] > 1) {
                                                                echo $dados['filhos'] . " filhos";
                                                            } else {
                                                                echo "Não tem filhos";
                                                            }

                                                            ?>
                                                        </strong>
                                                    <?php }

                                                    ?>
                                                </div>
                                                <div class="col">
                                                    <?php if ($dados['pretensao_salarial'] != "" && $dados['pretensao_salarial'] != null) { ?>
                                                        <strong>
                                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                                            Pretensão salarial: <?php echo $dados['pretensao_salarial']; ?>
                                                        </strong>
                                                        <?php
                                                    }
                                                    if ($dados['viajar'] == 1) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-plane" aria-hidden="true"></i>
                                                            Tem disponibilidade para viajar
                                                        </strong>
                                                        <?php
                                                    }
                                                    if ($dados['veiculo'] == 1) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-car" aria-hidden="true"></i>
                                                            Possui veiculo Próprio
                                                        </strong>
                                                        <?php
                                                    }
                                                    $res_cnh = mysqli_query($con, "SELECT categoria FROM TB_VV_CNH_CAND WHERE id_candidato=" . $dados['id'] . " ORDER BY categoria ASC");
                                                    if (mysqli_num_rows($res_cnh)) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                                            Possui CNH categoria
                                                            <?php
                                                            $key = 1;
                                                            while ($cat = mysqli_fetch_array($res_cnh)) {
                                                                if ($key < mysqli_num_rows($res_cnh)) {
                                                                    echo $cat['categoria'] . ", ";
                                                                } else {
                                                                    echo $cat['categoria'];
                                                                }
                                                                $key++;
                                                            }

                                                            ?>

                                                        </strong>
                                                    <?php } ?>
                                                </div>
                                                <?php
                                                $res_idiomas = mysqli_query($con, "SELECT * FROM TB_VV_IDIOMAS_CAND WHERE id_candidato=" . $dados['id']);
                                                if (mysqli_num_rows($res_idiomas)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                                                        Idiomas
                                                    </strong>
                                                    <?php
                                                }
                                                while ($idioma = mysqli_fetch_array($res_idiomas)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Idioma: <?php echo $idioma['idioma'] ?>
                                                            </span>
                                                            <span>
                                                                Nível: <?php echo $idioma['nivel'] ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                $res_formacoes = mysqli_query($con, "SELECT nivel, instituicao, curso, semestre, situacao, fim, turno FROM TB_VV_FORMACOES_CAND WHERE id_candidato=" . $dados['id']);
                                                if (mysqli_num_rows($res_formacoes)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                        Formações
                                                    </strong>
                                                    <?php
                                                }
                                                while ($formacao = mysqli_fetch_array($res_formacoes)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Nível de Ensino: <?php echo $formacao['nivel']; ?>
                                                            </span>
                                                            <?php if ($formacao['curso'] != "" && $formacao['curso'] != NULL) { ?>
                                                                <span>
                                                                    Curso : <?php echo $formacao['curso']; ?>
                                                                </span>
                                                            <?php } ?>
                                                            <span>
                                                                Instituição: <?php echo $formacao['instituicao']; ?>
                                                            </span>
                                                            <span>
                                                                Situação: <?php echo $formacao['situacao']; ?>
                                                            </span>
                                                            <?php if ($formacao['semestre'] != "" && $formacao['semestre'] != null) { ?>
                                                                <span>
                                                                    Semestre: <?php echo $formacao['semestre'] . "º"; ?>
                                                                </span>
                                                            <?php } ?>
                                                            <?php if ($formacao['turno'] != "" && $formacao['turno'] != null) { ?>
                                                                <span>
                                                                    Turno: <?php echo $formacao['turno']; ?>
                                                                </span>
                                                            <?php } ?>
                                                            <?php if ($formacao['fim'] != "" && $formacao['fim'] != null) { ?>
                                                                <span>
                                                                    Ano de conclusão: <?php echo $formacao['fim']; ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php
                                                $res_cursos = mysqli_query($con, "SELECT situacao, nome, instituicao, anofim FROM TB_VV_CURSOS_CAND WHERE id_candidato=" . $dados['id']);
                                                if (mysqli_num_rows($res_cursos)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                        Cursos
                                                    </strong>
                                                    <?php
                                                }
                                                while ($cursos = mysqli_fetch_array($res_cursos)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Nome do curso: <?php echo $cursos['nome'] ?>
                                                            </span>
                                                            <span>
                                                                Instituição: <?php echo $cursos['instituicao'] ?>
                                                            </span>
                                                            <span>
                                                                Situação: <?php echo $cursos['situacao'] ?>
                                                            </span>
                                                            <?php if ($cursos['anofim'] != "") { ?>
                                                                <span>
                                                                    Conclusão: <?php echo $cursos['anofim'] ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                $res_exp = mysqli_query($con, "SELECT * FROM TB_VV_EXP_PROFISSIONAIS WHERE id_candidato=" . $dados['id']);
                                                if (mysqli_num_rows($res_exp)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-history" aria-hidden="true"></i>
                                                        Histórico de experiência profissional
                                                    </strong>
                                                    <?php
                                                }
                                                while ($exp = mysqli_fetch_array($res_exp)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Nome da empresa: <?php echo $exp['nome_empresa'] ?>
                                                            </span>
                                                            <span>
                                                                Cargo: <?php echo $exp['cargo'] ?>
                                                            </span>
                                                            <?php if ($exp['atividades'] != "" && $exp['atividades'] != null) { ?>
                                                                <span>
                                                                    Atividades: <?php echo $exp['atividades'] ?>
                                                                </span>
                                                            <?php } ?>
                                                            <span>
                                                                Início: <?php
                                                                echo $exp['inicio'];
                                                                if ($exp['fim'] != "" && $exp['fim'] != null) {
                                                                    echo " - Fim: " . $exp['fim'];
                                                                }

                                                                ?>
                                                            </span>
                                                            <?php if ($exp['salario'] != "" && $exp['salario'] != "0.00" && $exp['salario'] != null) { ?>
                                                                <span>
                                                                    Salário: <?php echo "R$ " . str_replace(".", ",", $exp['salario']); ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?php if (!empty($dados['id_vaga'])) { ?>
                                <div class="col-sm-12">
                                    <a href="#" data-load-title="Selecionar Candidato" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL . '/modais/alerta_selecionar_candidato.php?id=' . $dados['id_vaga'] . '&usuario=' . $dados['id'] ?>" class="btn button">
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i> Selecionar Candidato
                                    </a>
                                </div>
                            <?php } else { ?>
                                <a href="#" data-load-title="Selecionar Candidato" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL . '/modais/alerta_selecionar_candidato.php?id=0&usuario=' . $dados['id'] ?>" class="btn button">
                                    <i class="fa fa-handshake-o" aria-hidden="true"></i> Selecionar Candidato
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </html>
<?php } ?>
