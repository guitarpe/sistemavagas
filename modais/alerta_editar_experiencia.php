<?php
include_once '../includes/defines.php';
include_once "../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');
$usuario = filter_input(INPUT_GET, 'usuario');
$func = new Funcoes();

$res = mysqli_query($con, "SELECT
                                id,
                                id_candidato,
                                nome_empresa,
                                salario,
                                atividades,
                                cargo,
                                emprego_atual,
                                fim,
                                inicio
                           FROM TB_VV_EXP_PROFISSIONAIS
                           WHERE id=$id AND id_candidato=$usuario") or die(mysqli_error($con));

$dados = mysqli_fetch_array($res);

?>
<script>
    $(document).ready(function () {
<?php if ($dados['cargo'] == 'Outro') { ?>
            $("#cargoempresamod").val('Outros');
            $("#cargoempresaoutro").val(<?php echo $dados['cargo'] ?>);
            $(".outro_cargo").removeClass('hidden');
<?php } else { ?>
            $("#cargoempresamod").val(<?php echo $dados['cargo'] ?>);
            $(".outro_cargo").addClass('hidden');
            $("#cargoempresaoutro").val("");
<?php } ?>

<?php if ($dados['emprego_atual'] == '1') { ?>
            $("#empregoatualmod").prop("checked", true);
<?php } else { ?>
            $("#empregoatualmod").prop("checked", false);
<?php } ?>
    })
</script>
<div class="container-fluid">
    <form class="active" method="post" action="<?php echo PATH_CANDIDATOS . '/actions/editar-info.php?tipo=2' ?>">
        <div class="row form-group">
            <input type="hidden" name="edit_exp_id" id="edit_exp_id" value="<?php echo $id ?>"/>
            <input type="hidden" name="edit_exp_usuario" id="edit_exp_usuario" value="<?php echo $usuario ?>"/>
            <div class="col-sm-8">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Nome da empresa" name="nomeempresamod" id="nomeempresamod" value="<?php echo $dados['nome_empresa'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-building-o" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group">
                    <input class="form-control dinheiro" class="dinheiro" type="text" placeholder="Salário (opcional)" name="salariomod" id="salariomod" value="<?php echo $dados['salario'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-dollar" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="div_cargo_emp">
                <div class="col-sm-12" id="label-cargoexp">
                    <div class="input-group">
                        <select class="form-control cargo" name="cargoempresamod" id="cargoempresamod">
                            <option selected value="">Cargo Profissional</option>
                            <?php
                            $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                            while ($row = mysqli_fetch_array($res_cargos)) {

                                ?>
                                <option <?php echo $dados['cargo'] == $row['nome'] ? 'selected="selected"' : '' ?>><?php echo $row['nome'] ?></option>
                                <?php
                            }

                            ?>
                            <option <?php echo $dados['cargo'] == 'Outro' ? 'selected="selected"' : '' ?>>Outro</option>
                        </select>
                        <span class="input-group-addon">
                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
                <div class="col-sm-12 outro_cargo hidden" id="label-ncargoexp">
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="Digite o Cargo Profissional" id="cargoempresaoutro" name="cargoempresaoutro" >
                        <span class="input-group-addon">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <textarea name="atividadesmod" id="atividadesmod" placeholder="Atividades Exercidas"><?php echo $dados['atividades'] ?></textarea>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <div class="input-group">
                    <input class="form-control data_ini data-s" type="text" placeholder="Início (mês/ano)" name="datainiciomod" id="datainiciomod" value="<?php echo $dados['inicio'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group">
                    <input class="form-control data_fim data-s" type="text" placeholder="Fim (mês/ano)" name="datafimmod" id="datafimmod" value="<?php echo $dados['fim'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input set_empr_atual" value="1" type="checkbox" name="empregoatualmod" id="empregoatualmod">
                    <label class="form-check-label">
                        Emprego atual
                    </label>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="col-sm-6 col-xs-6 text-center">
                    <button type="submit">
                        Atualizar
                    </button>
                </div>
                <div class="col-sm-6 col-xs-6 text-center">
                    <a href="#" class="btn button cancelar">Cancelar</a>
                </div>
            </div>
        </div>
    </form>
</div>