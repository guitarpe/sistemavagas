<?php
session_start();
include_once '../includes/defines.php';
include "../includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');
$res = mysqli_query($con, "SELECT * FROM TB_VV_MENSAGENS WHERE id=" . $id) or die(mysqli_error($con));

$array = mysqli_fetch_array($res);

//seta a mensagem como visualizada
$res_up = mysqli_query($con, "UPDATE TB_VV_MENSAGENS SET status=1 WHERE id=" . $id . "") or die(mysqli_error($con));

?>

<ul>
    <li>
        <div id="head-msg">
            <table>
                <tr>
                    <td>
                        <strong><?php echo $array["nome"] ?></strong>
                        <span style='font-weight:normal'> < </span><?php echo $array["email"] ?>>
                    </td>
                    <td style='text-align:right'>
                        <?php echo $array["data"] . ' ' . $array["hora"] ?>
                    </td>
                </tr>
            </table>
        </div>
    </li>
    <li class="margin-30">
        <strong class="font-20">
            Olá <?php echo $_SESSION["nome"] ?>
        </strong>
        <p class="margin-top-10">
        <div id="body-msg">
            <?php echo $array["mensagem"] ?>
        </div>
        </p>
    </li>
</ul>
