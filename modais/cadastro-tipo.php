<?php
include_once '../includes/defines.php';

?>
<form class="active">
    <div class="col-sm-12 text-center">
        <a href="<?php echo PATH_ALL . '/cadastrocandidato.php' ?>" class="btn btn-icon button btn-tipo">
            <i class="fa fa-user"></i>
            Cadastre-se como Candidato
        </a>
        <a href="<?php echo PATH_ALL . '/cadastroempresa.php' ?>" class="btn btn-icon button btn-tipo">
            <i class="fa fa-building-o"></i>
            Cadastre-se como Empresa
        </a>
    </div>
</form>
