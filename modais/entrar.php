<?php
include_once '../includes/defines.php';

?>
<form class="active" action="<?php echo PATH_ALL . '/logando.php' ?>" method="post" id="login">
    <label>
        <input type="text" placeholder="E-mail" name="user" id="user">
        <i class="fa fa-envelope" aria-hidden="true"></i>
        <span id="msg-email"></span>
    </label>

    <label>
        <input type="password" placeholder="Senha" name="password" id="password">
        <i class="fa fa-asterisk" aria-hidden="true"></i>
        <span id="msg-senha"></span>
    </label>
    <label>
        <select name="tipo">
            <option value="candidato">Candidato</option>
            <option value="empresa">Empresa</option>
            <option value="admin">Administrativo</option>
        </select>
        <i class="fa fa-users" aria-hidden="true"></i>
        <span id="msg-senha"></span>
    </label>
    <div class="row">
        <div class="col-sm-6 col-xs-6 text-right">
            <a href="<?php echo PATH_ALL . '/esqueceuasenha.php' ?>" class="link">
                Esqueceu sua senha?
            </a>
        </div>
        <div class="col-sm-6 col-xs-6 text-left">
            <button type="submit" id="button">
                Entrar <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <input type="hidden" name="id_vaga" id="id_vaga">
</form>