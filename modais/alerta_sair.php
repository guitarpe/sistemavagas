<?php
include_once '../includes/defines.php';

?>
<form class="active">
    <div class="full msg-alterar">
        <span>Deseja se desconectar do portal?</span>
    </div>
    <div class="full pd-5 text-center">
        <a href="<?php echo PATH_ALL . '/sair.php' ?>" class="btn button">
            Sair
        </a>
    </div>
</form>