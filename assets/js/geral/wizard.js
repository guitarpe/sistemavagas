$(document).ready(function () {

    $('.nav-tabs > li a[title]').tooltip();

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');

        if (validar(this)) {
            $active.next().removeClass('disabled');
            nextTab($active);
        }
    });

    $(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function validar(obj) {

    var count = 0;
    var msg = "";
    var title = "";
    var nameobj = "";

    $(obj).closest('.tab-pane').find('input,textarea,select').filter('[required]:visible').each(function (i, requiredField) {

        if ($(requiredField).attr('type') == 'radio') {

            if ($(requiredField).attr("name") != nameobj) {
                var chk = 0;

                if ($("input:radio[name='" + $(requiredField).attr("name") + "']:checked").length > 0) {
                    chk++;
                } else {
                    title = $(this).attr('title');
                }

                if (chk == 0) {
                    msg += 'Verifique o campo: ' + title + '<br>';
                    count++;
                }
            }

        } else if ($(requiredField).attr('type') == 'checkbox') {

            if (!$(requiredField).is(":checked")) {
                title = $(requiredField).attr('title');

                msg += 'Verifique o campo: ' + title + '<br>';
                count++;
            }
        } else {

            if ($(requiredField).val() === '' || $(requiredField).val() === null) {

                if (typeof $(requiredField).attr('placeholder') === 'undefined') {
                    title = $(requiredField).attr('title');
                } else {
                    title = $(requiredField).attr('placeholder');
                }

                msg += 'Verifique o campo: ' + title + '<br>';

                count++;
            }
        }

        nameobj = $(requiredField).attr('name');
    });

    if (count > 0) {
        $dispararAlerta(msg, 'warning');

        return false;
    } else {
        return true;
    }
}

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}