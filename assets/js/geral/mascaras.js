$(document).ready(function () {
    $('.cpf').mask('000.000.000-00');
    $('.cep').mask('00000-000');
    $('.fone').mask('(00) 00000-0000');
    $('.tel').mask('(00) 0000-0000');
    $('.fone-s').mask('00000-0000');
    $('.fax').mask('(00) 0000-0000');
    $('.data').mask('00/00/0000');
    $('.data-s').mask('00/0000');
    $('.data-ano').mask('0000');
    $('.cnpj').mask('00.000.000/0000-00');
    $('.dinheiro').mask('#.##0,00', {reverse: true});
    $('.numero').mask('#');
    $('.fones').mask('(00) 0000-00009');
});