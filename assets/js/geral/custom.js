$(document).ready(function () {

    if ($('.table-pages').length) {
        $(".table-pages").DataTable({
            "language": {
                url: caminho + '/assets/plugins/dataTable/lang/Portuguese-Brasil.json'
            }
        });

        $(".dataTables_wrapper select:not(.cert)").select2({
            minimunResultsForSearch: -1
        });
    }

    if ($('.select2').lenght > 0) {
        $('.select2').select2();
    }

    $dispararAlerta = function (msg, tipo) {

        $.UIkit.notify({
            message: msg,
            status: tipo,
            timeout: 5000,
            pos: 'top-right'
        });

        return false;
    };

    $anoInvalido = function (ano) {

        var limitemaior = ((new Date).getFullYear() + 10);
        var anoinfo = parseInt(ano);

        if (ano !== "") {
            if (anoinfo > limitemaior || anoinfo < 1900) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    };

    $('#modal-default-md').on('show.bs.modal', function (e) {
        $(this).find('.modal-body').html("");

        var loadurl = $(e.relatedTarget).data('load-url');
        var title = $(e.relatedTarget).data('load-title');

        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').load(loadurl);

    });

    $('#modal-default-mini').on('show.bs.modal', function (e) {
        $(this).find('.modal-body').html("");

        var loadurl = $(e.relatedTarget).data('load-url');
        var title = $(e.relatedTarget).data('load-title');

        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').load(loadurl);
    });

    $('#modal-default').on('show.bs.modal', function (e) {
        $(this).find('.modal-body').html("");

        var loadurl = $(e.relatedTarget).data('load-url');
        var title = $(e.relatedTarget).data('load-title');

        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').load(loadurl);
    });

    $(document).on("change", ".ver_imagem", function () {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(this.files[0]);
        $('#foto-perfil').css("border-color", "#ccc");
        oFReader.onload = function (oFREvent) {
            $("#img-icone").remove();
            $("#foto-perfil").css("background-image", "url(" + oFREvent.target.result + ")");
            $("#foto-perfil").css("background-repeat", "no-repeat");
            $("#foto-perfil").css("background-position", "center");
            $("#foto-perfil").css("background-color", "#fff");
            $("#foto-perfil").css("background-size", "contain");
        };
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#foto-perfil").on("click", function () {
        $('#imagem').click();
    });

    $('.numbers').keyup(function () {
        this.value = this.value.replace(/[^0-9\/]/g, '');
    });

    $(".validemail").on("blur", function () {

        var email = $(this).val();
        if (email != "") {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $dispararAlerta('E-mail Inválido', 'warning');
            }
        }
    });

    $(document).on("click", ".habilita", function () {
        var valor = $(this).val();
        var area = $(this).data('area');

        if (valor == 'Sim' || valor == 1 || valor == '1' || valor == 'Salário Fixo') {
            $(area).removeClass("hidden");
        } else {
            $(area).addClass("hidden");
        }
    });

    $("#tipobusca").on("change", function () {
        var valor = $(this).val();

        if (valor == "Salário Fixo") {
            $("#palco1").css("display", "block");
        } else {
            $("#palco1").css("display", "none");
        }
    });

    $(document).on("change", ".cargo", function () {

        var outro_cargo = $(this).closest('.div_cargo_emp').find('.outro_cargo');

        if ($(this).val() === 'Outro') {
            outro_cargo.removeClass("hidden");
            outro_cargo.find('input').prop('required', true);
        } else {
            outro_cargo.addClass("hidden");
            outro_cargo.find('input').prop('required', false);
        }
    });

    $(".data").on('blur', function () {

        var data = $(this).val();

        if (data != "") {
            var dtArray = data.split("/");

            if (dtArray == null) {

            } else {

                var dtDay = dtArray[0];
                var dtMonth = dtArray[1];
                var dtYear = dtArray[2];

                if (dtMonth < 1 || dtMonth > 12) {

                    $dispararAlerta("Data Inválida", 'warning');
                    $(this).val("");
                    $(this).focus();

                } else if (dtDay < 1 || dtDay > 31) {

                    $dispararAlerta("Data Inválida", 'warning');
                    $(this).val("");
                    $(this).focus();

                } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {

                    $dispararAlerta("Data Inválida", 'warning');
                    $(this).val("");
                    $(this).focus();

                } else if (dtMonth == 2) {

                    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

                    if (dtDay > 29 || (dtDay == 29 && !isleap)) {

                        $dispararAlerta("Data Inválida", 'warning');
                        $(this).val("");
                        $(this).focus();

                    }
                }

                if (!$anoInvalido(dtYear)) {

                    $dispararAlerta("Ano inválido", 'warning');
                    $(this).val("");
                    $(this).focus();
                }
            }
        }
    });

    $('.nopaste').bind('copy paste', function (e) {
        e.preventDefault();
        $dispararAlerta("Não é possível realizar cópia deste campo", 'danger');
    });

    //valida a senha
    $(".confisenha").on('change', function () {
        var senha = $(this).val();
        var campo = $(".txtSenha").val();
        if (campo !== senha) {
            $dispararAlerta("Repita a senha corretamente", 'danger');
            $(this).val("");
            $(this).focus();
        }
    });


    $(".tamcampo").on('change', function () {
        var campo = $(this).val();
        var tam = $(this).attr('maxlength');

        if (campo.length < tam) {
            $dispararAlerta("Este campo deve ter " + tam + " caracteres", 'danger');
            $(this).val("");
            $(this).focus();
        }
    });

    $(".cancelar").on("click", function (e) {
        e.preventDefault();
        $('.close').click();
    });

    $(document).on("change", '.busca_cidades', function () {
        var estado = $(this).val();
        if ($('.ecep').length) {

            var cep = $(".ecep").val();

            if (estado !== "" && cep === "") {

                $.ajax({
                    type: "POST",
                    url: caminho + "/includes/buscar_cidades.php",
                    dataType: "html",
                    data: {
                        estado: estado
                    },
                    success: function (result) {
                        var data = result.split(";");

                        $(".cidades").html("");
                        $(".cidades").append('<option disabled selected value="">Cidade</option>');
                        for (var i = 0; i < data.length - 1; i++) {
                            $(".cidades").append("<option>" + data[i] + "</option>");
                        }
                    }
                });
            }
        } else {

            if (estado !== "") {
                $.ajax({
                    type: "POST",
                    url: caminho + "/includes/buscar_cidades.php",
                    dataType: "html",
                    data: {
                        estado: estado
                    },
                    success: function (result) {
                        var data = result.split(";");
                        $(".cidades").html("");
                        $(".cidades").append('<option disabled selected value="">Selecione</option>');
                        for (var i = 0; i < data.length - 1; i++) {
                            $(".cidades").append("<option>" + data[i] + "</option>");
                        }
                    }
                });
            }
        }
    });

    $(".ecep").on("blur", function () {

        var cep = $(this).val();

        $(".elogradouro").val("");
        $(".ebairro").val("");
        $(".eestado").val("");
        $(".ecidade").val("");

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        $.get(url, function (data) {
            if (!("erro" in data)) {
                if (Object.prototype.toString.call(data) === '[object Array]') {
                    var data = data[0];
                }
                $(".elogradouro").val(data.logradouro);
                $(".ebairro").val(data.bairro);
                $(".eestado").val(data.uf.toUpperCase()).change();

                var cidade = data.localidade.toUpperCase();

                if (data.uf.toUpperCase() !== "") {
                    $.ajax({
                        type: "POST",
                        url: caminho + "/includes/buscar_cidades.php",
                        dataType: "html",
                        data: {
                            estado: data.uf.toUpperCase()
                        },
                        success: function (result) {
                            var data = result.split(";");
                            for (var i = 0; i < data.length - 1; i++) {
                                $(".cidades").append("<option>" + data[i] + "</option>");
                            }

                            $(".cidades").val(cidade);
                        }
                    });
                }
            }
        });
    });

    $('.cnpj').on('blur', function () {

        var numdoc = $(this);

        if (numdoc.val() != "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/validacao.php",
                dataType: "html",
                data: {
                    tipo: 2,
                    numdoc: numdoc.val()
                },
                success: function (result) {
                    if (result == 0) {
                        $dispararAlerta("CNPJ Inválido", 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $('.cpf').on('blur', function () {

        var numdoc = $(this);

        if (numdoc.val() != "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/validacao.php",
                dataType: "html",
                data: {
                    tipo: 1,
                    numdoc: numdoc.val()
                },
                success: function (result) {

                    if (result == false) {
                        $dispararAlerta("CPF Inválido", 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $(".vagas_estado").on("click", function () {
        var estado = $(this).data('uf');

        $('#vagas_estado').val(estado);
        $('#vagas_estado_form').submit();
    });

    $(".reenviar").on("click", function () {
        $("#check").hide();
        $("#spinner").show();

        var us = $(this).data('id');

        $.ajax({
            type: "GET",
            url: caminho + "/candidatos/mail_ativacao_cadastro.php?user=" + us,
            dataType: "html",
            success: function (result) {
                $('.modal').addClass('active');
                $('.modal .box').removeClass('active');
                $('.modal .box#ativacao-cadastro').addClass('active');
            }
        });
        setTimeout(function () {
            $("#spinner").hide();
            $("#check").show();
        }, 3000);
    });

//    $(".descandidatar").on("click", function (e) {
//        e.preventDefault();
//        var candidato = $(this).data("candidato");
//        var vaga = $(this).data("vaga");
//        $('#del_id_vaga').val(vaga);
//        $('#del_id_cand').val(candidato);
//    });

    $('.link-js').on('click', function (e) {
        e.preventDefault();

        var tab = $(this).attr('target');

        $('.modal').addClass('active');
        $('.modal .box').removeClass('active');
        $('.modal .box#' + tab).addClass('active');
    });

    $('.modal .box button.close').on('click', function (e) {
        e.preventDefault();

        $('.modal').removeClass('active');
        $(this).parent('.box').removeClass('active');
    });

    var cargo = [];
    var estado = [];
    var cidadebusca = [];
    var nivel = [];
    var faixa = [];
    var contrato = [];
    var turno = [];
    var escolaridade = [];
    var informatica = [];
    var sexo = [];
    var cidade = [];
    var deficiencia = [];

    $('.filtrar input').on('change', function () {

        var campo = $(this).data('id');
        var tipo = $(this).data('tipo');

        if ($(this).is(':checked')) {

            switch (tipo) {
                case 1:
                    if ($.inArray($(this).val(), cargo) < 0) {
                        cargo.push($(this).val());
                    }
                    $(campo).val(cargo.join('|'));
                case 2:
                    if ($.inArray($(this).val(), estado) < 0) {
                        estado.push($(this).val());
                    }
                    $(campo).val(estado.join('|'));
                case 3:
                    if ($.inArray($(this).val(), cidadebusca) < 0) {
                        cidadebusca.push($(this).val());
                    }
                    $(campo).val(cidadebusca.join('|'));
                case 4:
                    if ($.inArray($(this).val(), nivel) < 0) {
                        nivel.push($(this).val());
                    }
                    $(campo).val(nivel.join('|'));
                case 5:
                    if ($.inArray($(this).val(), faixa) < 0) {
                        faixa.push($(this).val());
                    }
                    $(campo).val(faixa.join('|'));
                case 6:
                    if ($.inArray($(this).val(), contrato) < 0) {
                        contrato.push($(this).val());
                    }
                    $(campo).val(contrato.join('|'));
                case 7:
                    if ($.inArray($(this).val(), turno) < 0) {
                        turno.push($(this).val());
                    }
                    $(campo).val(turno.join('|'));
                case 8:
                    if ($.inArray($(this).val(), escolaridade) < 0) {
                        escolaridade.push($(this).val());
                    }
                    $(campo).val(escolaridade.join('|'));
                case 9:
                    if ($.inArray($(this).val(), informatica) < 0) {
                        informatica.push($(this).val());
                    }
                    $(campo).val(informatica.join('|'));
                case 10:
                    if ($.inArray($(this).val(), sexo) < 0) {
                        sexo.push($(this).val());
                    }
                    $(campo).val(sexo.join('|'));
                case 11:
                    if ($.inArray($(this).val(), cidade) < 0) {
                        cidade.push($(this).val());
                    }
                    $(campo).val(cidade.join('|'));
                case 12:
                    if ($.inArray($(this).val(), deficiencia) < 0) {
                        deficiencia.push($(this).val());
                    }
                    $(campo).val(deficiencia.join('|'));
            }
        } else {

            switch (tipo) {
                case 1:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 2:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 3:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 4:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 5:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 6:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 7:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 8:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 9:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 10:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 11:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
                case 12:
                    $(campo).val($(campo).val().replace($(this).val(), ''));
            }
        }
    });

    $('.cargobusca').on('keypress change', function () {

        var termo = $(this).val();

        if (termo.length >= 3) {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/buscar_cargos.php",
                async: false,
                data: {
                    termo: termo
                },
                success: function (result) {
                    $("#listacargos").html(result);
                }
            });
        }
    });

//    $(".candidatarLogin").on('click', function (e) {
//        e.preventDefault(e);
//
//        var vaga = $(this).data("vaga");
//
//        $('.modal').addClass('active');
//        $('.modal .box').removeClass('active');
//        $('.modal .box#entrar').addClass('active');
//        $('#id_vaga').val(vaga);
//
//    });

    $(".descandidatar").on('click', function (e) {
        e.preventDefault();

        var candidato = $(this).data("candidato");
        var vaga = $(this).data("vaga");

        $('#del_id_vaga').val(vaga);
        $('#del_id_cand').val(candidato);
    });

//    $(".candidatar").on('click', function (e) {
//        e.preventDefault();
//
//        var candidato = $(this).data("candidato");
//        var vaga = $(this).data("id");
//
//        var obj = $(this);
//
//        $.ajax({
//            type: "POST",
//            url: caminho + "/candidatos/actions/recebe_candidatura.php",
//            dataType: 'html',
//            data: {
//                id_candidato: candidato,
//                id_vaga: vaga
//            },
//            success: function (result) {
//                obj.parent().find("a.descandidatar").removeClass("hidden");
//            }
//        });
//    });

    $('.link-js').on('click').on(function (ev) {
        ev.preventDefault();
        var tab = $(this).attr('target');
        $('.modal').addClass('active');
        $('.modal .box').removeClass('active');
        $('.modal .box#' + tab).addClass('active');

        $('body').addClass('scroll-no');


    });

    $('.modal .box button.close').on('click', function (ev) {
        ev.preventDefault();
        $('.modal').removeClass('active');
        $(this).parent('.box').removeClass('active');


        $('body').removeClass('scroll-no');

    });

    $(document).on('click', '.filter.close h5', function (ev) {
        ev.preventDefault();
        $(this).parent().removeClass('close');
        $(this).parent().addClass('open');
    });

    $(document).on('click', '.filter.open h5', function (ev) {
        ev.preventDefault();
        $(this).parent().removeClass('open');
        $(this).parent().addClass('close');
    });

    $(document).on('click', '.vagas ul li .foot a.btn', function (ev) {
        ev.preventDefault();
        $(this).addClass('active');
        $(this).html('Currículo Enviado');

    });

    $(document).on('click', 'header nav.off button', function (ev) {
        ev.preventDefault();
        $("header nav").addClass("on");
        $("header nav").removeClass("off");
    });

    $(document).on('click', 'header nav.on button', function (ev) {
        ev.preventDefault();
        $("header nav").removeClass("on");
        $("header nav").addClass("off");
    });

    $(document).on('click', '.filtro-mobile.off h4', function (ev) {
        ev.preventDefault();
        $(".filtro-mobile").removeClass("off");
        $(".filtro-mobile").addClass("on");
    });

    $(document).on('click', '.filtro-mobile.on h4', function (ev) {
        ev.preventDefault();
        $(".filtro-mobile").removeClass("on");
        $(".filtro-mobile").addClass("off");
    });

    $(document).on('click', '.miolo-conteudo nav.off h4', function (ev) {
        ev.preventDefault();
        $(".miolo-conteudo nav").removeClass("off");
        $(".miolo-conteudo nav").addClass("on");
    });

    $(document).on('click', '.miolo-conteudo nav.on h4', function (ev) {
        ev.preventDefault();
        $(".miolo-conteudo nav").removeClass("on");
        $(".miolo-conteudo nav").addClass("off");
    });

    $(".slctpage").on("change", function () {
        var page = $("#pag1").val();
        var categoria = $("#categoria").val();
        var ordenacao = $("#ordenacao").val();
        location.href = "list-blogs.php?categoria=" + categoria + "&ordenacao=" + ordenacao + "&pag=" + page;
    });

    $(".pg").change(function () {
        var _value = $(this).val();
        var select_group = $(this).attr("select-group");
        $('select[select-group="' + select_group + '"]').not(this).val(_value);
    });

    $("#tipo-empresa").on("click", function () {
        $('.modal').addClass('active');
        $('.modal .box').removeClass('active');
        $('.modal .box#cadastro-empresa').addClass('active');
    });

    $("#tipo-candidato").on("click", function () {
        $('.modal').addClass('active');
        $('.modal .box').removeClass('active');
        $('.modal .box#cadastro').addClass('active');
    });

    $(".btn-enviado").on('mouseover', function () {
        $(this).html("<i class='fa fa-times'></i> Excluir Candidatura");
    });

    $(".btn-enviado").on('mouseover', function () {
        $(this).html("<i class='fa fa-check-square-o'></i> Currículo Enviado");
    });

    $('.yn').on('change', function () {

        var yn = $(this).val();
        var name = $(this).attr('name');

        if (yn == 'Sim') {
            $(this).parent().parent().parent().parent().find('.' + name).removeClass('off');
        } else {
            $(this).parent().parent().parent().parent().find('.' + name).addClass('off');

        }
    });

    $validaForm = function (obj) {

        var objData = obj.find('#datanasc');
        var data = obj.find('#datanasc').val();

        if (data != "") {
            var dtArray = data.split("/");

            if (dtArray == null) {

            } else {

                var dtDay = dtArray[0];
                var dtMonth = dtArray[1];
                var dtYear = dtArray[2];

                if (dtMonth < 1 || dtMonth > 12) {

                    $dispararAlerta("Data Inválida", 'warning');
                    objData.val("");
                    objData.focus();
                    return false;

                } else if (dtDay < 1 || dtDay > 31) {

                    $dispararAlerta("Data Inválida", 'warning');
                    objData.val("");
                    objData.focus();
                    return false;

                } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {

                    $dispararAlerta("Data Inválida", 'warning');
                    objData.val("");
                    objData.focus();
                    return false;

                } else if (dtMonth == 2) {

                    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

                    if (dtDay > 29 || (dtDay == 29 && !isleap)) {

                        $dispararAlerta("Data Inválida", 'warning');
                        objData.val("");
                        objData.focus();
                        return false;

                    }
                }

                if (!$anoInvalido(dtYear)) {

                    $dispararAlerta("Ano inválido", 'warning');
                    objData.val("");
                    objData.focus();
                    return false;
                }
            }
        }
    }

    if ($('#tb-admin-vagas').length) {
        var table = $('#tb-admin-vagas').DataTable({
            "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7 nopad_r"i><"col-sm-5 nopad_l"p>>',
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "language": {
                url: caminho + '/assets/plugins/dataTable/lang/Portuguese-Brasil.json'
            },
            "ajax": {
                "url": caminho + "/admin/includes/getListaVagas.php?tipo=0",
                "type": "POST"
            },
            columns: [
                {searchable: true},
                null,
                null,
                null,
                null,
                null,
                {searchable: false, orderable: false}
            ],
            "columnDefs": [
                {
                    "targets": [6],
                    "class": "acoes",
                    "orderable": false,
                    "render": function (data, type, row) {
                        if (row[5] === 'Ativo') {
                            return '<a href="edit-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-warning" title="Editar"><i class="fa fa-edit"></i></a> <a href="actions/delete-vaga.php?id=' + row[0] + '" class="btn btn-xs btn-danger" title="Excluir"><i class="fa fa-trash"></i></a> <a href="actions/desativar-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-primary" title="Inativar"><i class="fa fa-close"></i></a>';
                        } else {
                            return '<a href="edit-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-warning" title="Editar"><i class="fa fa-edit"></i></a> <a href="actions/delete-vaga.php?id=' + row[0] + '" class="btn btn-xs btn-danger" title="Excluir"><i class="fa fa-trash"></i></a> <a href="actions/ativar-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-success" title="Ativar"><i class="fa fa-check"></i></a>';
                        }
                    }
                }
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });

        $(".dataTables_wrapper select").select2({
            minimunResultsForSearch: -1
        });


        $('#btnbuscar').click(function () {

            table.column(0).search($('#codigo-vaga').val().trim());
            table.column(1).search($('#nome-cargo').val().trim());
            table.column(2).search($('#nome-empresa').val().trim());
            table.column(3).search($('#estados').val());
            table.column(4).search($('#data').val());

            table.draw();
        });
    }

    if ($('#tb-admin-vagas-inativas').length) {
        var table = $('#tb-admin-vagas-inativas').DataTable({
            "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7 nopad_r"i><"col-sm-5 nopad_l"p>>',
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "language": {
                url: caminho + '/assets/plugins/dataTable/lang/Portuguese-Brasil.json'
            },
            "ajax": {
                "url": caminho + "/admin/includes/getListaVagas.php?tipo=2",
                "type": "POST"
            },
            columns: [
                {searchable: true},
                null,
                null,
                null,
                null,
                null,
                {searchable: false, orderable: false}
            ],
            "columnDefs": [
                {
                    "targets": [6],
                    "class": "acoes",
                    "orderable": false,
                    "render": function (data, type, row) {
                        if (row[5] === 'Ativo') {
                            return '<a href="edit-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-warning" title="Editar"><i class="fa fa-edit"></i></a> <a href="actions/delete-vaga.php?id=' + row[0] + '" class="btn btn-xs btn-danger" title="Excluir"><i class="fa fa-trash"></i></a> <a href="actions/desativar-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-primary" title="Inativar"><i class="fa fa-close"></i></a>';
                        } else {
                            return '<a href="edit-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-warning" title="Editar"><i class="fa fa-edit"></i></a> <a href="actions/delete-vaga.php?id=' + row[0] + '" class="btn btn-xs btn-danger" title="Excluir"><i class="fa fa-trash"></i></a> <a href="actions/ativar-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-success" title="Ativar"><i class="fa fa-check"></i></a>';
                        }
                    }
                }
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });

        $(".dataTables_wrapper select").select2({
            minimunResultsForSearch: -1
        });


        $('#btnbuscar').click(function () {

            table.column(0).search($('#codigo-vaga').val().trim());
            table.column(1).search($('#nome-cargo').val().trim());
            table.column(2).search($('#nome-empresa').val().trim());
            table.column(3).search($('#estados').val());
            table.column(4).search($('#data').val());

            table.draw();
        });
    }

    if ($('#tb-admin-vagas-expiradas').length) {

        $("#reativarbusca").on("click", function () {

            var cargo = $('#nome-cargo').val();
            var empresa = $('#nome-empresa').val();
            var estados = $('#estados').val();
            var dt_ini = $('#data_ini').val();
            var dt_fim = $('#data_fim').val();

            if (cargo != "" || empresa != "" || estados != "" || dt_ini != "" || dt_fim != "") {
                $.ajax({
                    type: "POST",
                    url: caminho + "/admin/actions/reativar_vagas_busca.php",
                    dataType: "html",
                    data: {
                        cargo: cargo,
                        empresa: empresa,
                        estados: estados,
                        dt_ini: dt_ini,
                        dt_fim: dt_fim
                    },
                    success: function (result) {
                        $dispararAlerta("Realizado com sucesso", "success");
                    }
                });
            } else {
                $dispararAlerta("É necessário adicionar algum parâmetro de busca", "danger");
            }
        });

        var table = $('#tb-admin-vagas-expiradas').DataTable({
            "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7 nopad_r"i><"col-sm-5 nopad_l"p>>',
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "language": {
                url: caminho + '/assets/plugins/dataTable/lang/Portuguese-Brasil.json'
            },
            "ajax": {
                "url": caminho + "/admin/includes/getListaVagasExpiradas.php",
                "type": "POST"
            },
            columns: [
                {searchable: true},
                null,
                null,
                null,
                null,
                null,
                {searchable: false, orderable: false}
            ],
            "columnDefs": [
                {
                    "targets": [6],
                    "class": "acoes",
                    "orderable": false,
                    "render": function (data, type, row) {
                        return '<a href="edit-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-warning" title="Editar"><i class="fa fa-edit"></i></a> <a href="actions/delete-vaga.php?id=' + row[0] + '" class="btn btn-xs btn-danger" title="Excluir"><i class="fa fa-trash"></i></a> <a href="actions/reativar-vaga.php?id_vaga=' + row[0] + '" class="btn btn-xs btn-primary" title="Reativar"><i class="fa fa-refresh"></i></a>';
                    }
                }
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });

        $(".dataTables_wrapper select").select2({
            minimunResultsForSearch: -1
        });


        $('#btnbuscar').click(function () {

            table.column(0).search($('#codigo-vaga').val().trim());
            table.column(1).search($('#nome-cargo').val().trim());
            table.column(2).search($('#nome-empresa').val().trim());
            table.column(3).search($('#estados').val());
            table.column(4).search($('#data_ini').val());
            table.column(5).search($('#data_fim').val());
            table.draw();
        });
    }

    if ($('#tb-admin-candidatos').length) {

        var table = $('#tb-admin-candidatos').DataTable({
            "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7 nopad_r"i><"col-sm-5 nopad_l"p>>',
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "language": {
                url: caminho + '/assets/plugins/dataTable/lang/Portuguese-Brasil.json'
            },
            "ajax": {
                "url": caminho + "/admin/includes/getListaCandidatos.php",
                "type": "POST"
            },
            columns: [
                {searchable: true},
                null,
                null,
                null,
                null,
                null,
                null,
                {searchable: false, orderable: false}
            ],
            "columnDefs": [
                {
                    "targets": [7],
                    "class": "acoes",
                    "orderable": false,
                    "render": function (data, type, row) {
                        return '<a href="edit-candidato.php?id=' + row[0] + '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> <a href="actions/delete-candidato.php?id=' + row[0] + '" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';
                    }
                }
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });

        $(".dataTables_wrapper select").select2({
            minimunResultsForSearch: -1
        });


        $('#btnbuscar').click(function () {

            table.column(1).search($('#nome').val().trim());
            table.column(2).search($('#cpf').val().trim());
            table.column(3).search($('#email').val());
            table.column(4).search($('#telefone').val());
            table.column(5).search($('#estados').val());
            table.column(6).search($('#cidades').val());
            table.draw();
        });
    }

    if ($('#tb-admin-empresas').length) {

        var table = $('#tb-admin-empresas').DataTable({
            "dom": '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7 nopad_r"i><"col-sm-5 nopad_l"p>>',
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "language": {
                url: caminho + '/assets/plugins/dataTable/lang/Portuguese-Brasil.json'
            },
            "ajax": {
                "url": caminho + "/admin/includes/getListaEmpresas.php",
                "type": "POST"
            },
            columns: [
                {searchable: true},
                null,
                null,
                null,
                null,
                null,
                null,
                {searchable: false, orderable: false}
            ],
            "columnDefs": [
                {
                    "targets": [7],
                    "class": "acoes",
                    "orderable": false,
                    "render": function (data, type, row) {
                        return '<a href="edit-empresa.php?id=' + row[0] + '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> <a href="actions/delete-empresa.php?id=' + row[0] + '" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';
                    }
                }
            ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });

        $(".dataTables_wrapper select").select2({
            minimunResultsForSearch: -1
        });


        $('#btnbuscar').click(function () {

            table.column(1).search($('#nome_empresa').val().trim());
            table.column(2).search($('#cnpj').val().trim());
            table.column(3).search($('#email').val());
            table.column(4).search($('#telefone').val());
            table.column(5).search($('#estados').val());
            table.column(6).search($('#cidades').val());
            table.draw();
        });
    }

    $("#fontplus").on("click", function () {

        var $elemento = $("#text-blog p");
        var tam = parseFloat($elemento.css('font-size'));

        $elemento.css('font-size', tam + 1);
    });

    $("#fontminus").on("click", function () {
        var $elemento = $("#text-blog p");
        var tam = parseFloat($elemento.css('font-size'));

        $elemento.css('font-size', tam - 1);
    });
});

//function showDialogCadSuccess()
//{
//    $('.modal').addClass('active');
//    $('.modal .box').removeClass('active');
//    $('.modal .box#msg-cadastro-sucesso').addClass('active');
//}

//function showDialogNoAccess()
//{
//    $('.modal').addClass('active');
//    $('.modal .box').removeClass('active');
//    $('.modal .box#msg-sem-acesso').addClass('active');
//}

//function showDialogUsSenhaInv()
//{
//    $('.modal').addClass('active');
//    $('.modal .box').removeClass('active');
//    $('.modal .box#msg-usuario-senha-inv').addClass('active');
//}

/*------AQUI------*/
$(window).scroll(function () {
    $("header nav").removeClass("on");
    $("header nav").addClass("off");
});