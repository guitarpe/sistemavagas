$(document).ready(function () {
    $(document).on("blur", "#eemail", function () {

        var email = $(this).val();

        if (email !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/buscar_email.php",
                dataType: "html",
                data: {
                    email: email,
                    tipo: 'empresa'
                },
                success: function (result) {
                    if (result == true) {
                        $("#eemail").val("");
                        $("#eemail").focus();
                        $dispararAlerta("Já existe um e-mail igual a este cadastrado", 'warning');
                    }
                }
            });
        }
    });

    $("#ver").mousedown(function () {
        $('#senha').attr('type', 'text');
    });

    $("#ver").mouseup(function () {
        $('#senha').attr('type', 'password');
    });
});
