$(document).ready(function () {

    var idiomas_vaga = new Array();
    var idiomas_vaga_id = new Array();

//    $('input[name="idiomas"]').click(function () {
//
//        $(this).attr('checked', true);
//
//        $("input[name='idiomas']").each(function () {
//            if ($(this).is(':checked') === false) {
//                $(this).attr('checked', false);
//            } else {
//                $(this).attr('checked', true);
//            }
//        });
//    });

    $('input[name="outroestadocidade"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='outroestadocidade']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="viajar"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='viajar']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="veiculo"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='veiculo']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="cnh"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='cnh']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="tipocnh"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='tipocnh']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="deficiencia"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='deficiencia']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="informatica"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='informatica']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $('input[name="visibilidade"]').click(function () {

        $(this).attr('checked', true);

        $("input[name='visibilidade']").each(function () {
            if ($(this).is(':checked') === false) {
                $(this).attr('checked', false);
            } else {
                $(this).attr('checked', true);
            }
        });
    });

    $(document).on('click', '#adicionar_idioma', function () {
        var quantidade = 3;

        if ($("div.div_idioma").length < quantidade) {
            var $row = $("div.div_idioma:last").clone();
            $row.insertAfter("div.div_idioma:last");

            $row.find("input:text").val("").end();
            $row.find("select").prop("selectedIndex", 0);
            $row.find("textarea").val("").end();
        }

        if ($("div.div_idioma").length > 1) {
            $("#remover_idioma").css('display', 'block');
        } else {
            $("#remover_idioma").css('display', 'none');
        }
    });

    $(document).on('click', '#remover_idioma', function () {
        $("div.div_idioma:last").remove();

        if ($("div.div_idioma").length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $(".remove_idioma").on("click", function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var obj = $(this);

        $.ajax({
            type: "POST",
            url: caminho + "/empresas/actions/recebe_removeridiomavaga.php",
            dataType: "html",
            data: {
                id: id
            },
            success: function (result) {
                if (result) {
                    obj.closest('tr').remove();
                    $dispararAlerta("Removido com sucesso", 'success');
                } else {
                    $dispararAlerta("Erro ao remover o cargo", 'error');
                }

                if ($("#idiomasarea tr").length === 0) {
                    $("#idiomasarea").html("<tr id='uexp'><td>Nenhum idioma cadastrado!</td></tr>");
                }
            }
        });
    });

    /**/
    $("#addidiomavagamod").on("click", function (e) {
        e.preventDefault();

        var key = true;

        if ($("#idiomavagamod").val() == "") {
            $("#idiomavagamod").css("border-color", "red");
            key = false;
        } else {
            $("#idiomavagamod").css("border-color", "#ccc");
        }

        if ($("#nivelidiomavagamod").val() == "") {
            $("#nivelidiomavagamod").css("border-color", "red");
            key = false;
        } else {
            $("#nivelidiomavagamod").css("border-color", "#ccc");
        }

        if (key) {

            var adicionados = $("#validiomasmod").val();
            if (adicionados !== null) {

                var valData = adicionados.split(',');

                $.each(valData, function (key, value) {
                    idiomas_vaga_id.push(value);
                });
            }

            var idioma = $("#idiomavagamod").val();
            var nivel = $("#nivelidiomavagamod").val();

            var obj = {
                idioma: $("#idiomavagamod").val(),
                nivel: $("#nivelidiomavagamod").val()
            };

            var obj2 = idioma + ':' + nivel;
            idiomas_vaga_id.push(obj2);

            idiomas_vaga.push(obj);

            $("#validiomasmod").val(idiomas_vaga_id.join(","));

            $montarIdiomasGridmod();
        }
    });

    $montarIdiomasGridmod = function () {
        //$('#idiomasgridmod').html("");
        if (idiomas_vaga.length) {
            for (var i = 0; i < idiomas_vaga.length; i++) {
                $('#idiomasgridmod').append(
                        '<div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 td-border">' +
                        '<span>' + idiomas_vaga[i].idioma + '</span>' +
                        '</div>' +
                        '<div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 td-border">' +
                        '<span>' + idiomas_vaga[i].nivel + '</span>' +
                        '</div>' +
                        '<div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 text-center td-border">' +
                        '<a href="#" class="btn buttonmini btn-sm" onclick="$removeIdiomamod(' + i + ')">' +
                        '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                        '</a>' +
                        '</div>');
            }
        } else {
            $('#idiomasgridmod').append('<li>Nenhum idioma cadastrado.</li>');
        }
    };

    $removeIdiomamod = function (i) {
        var aux = new Array();
        for (var j = 0; j < idiomas_vaga.length; j++) {
            if (i != j) {

                var idioma = $("#idiomavagamod").val();
                var nivel = $("#nivelidiomavagamod").val();

                var obj2 = idioma + ':' + nivel;
                idiomas_vaga_id.push(obj2);

                aux.push({
                    idioma: idiomas_vaga[j].idioma,
                    nivel: idiomas_vaga[j].nivel
                });
            }
        }
        idiomas_vaga = aux;

        $("#validiomasmod").val(idiomas_vaga_id.join(","));
        $montarIdiomasGrid();
    };
});

//var beneficios = new Array();
//var idiomas_vaga = new Array();
var cnh = new Array();
var deficiencias = new Array();
var conhecimentos_informatica = new Array();

function otherLocal(key)
{
    if (key) {
        $("#local").show();
    } else {
        $("#cidadevaga").val("");
        $("#estadovaga").val("");
        $("#local").hide();
    }
}

function recebeDadosVaga()
{
    var forma = $("#formacontratacao").val();
    var cargo = $("#cargo").val();
    var nivel = $("#nivelhierarquico").val();
    var nvagas = $("#nvagas").val();
    var turno = $("#turno").val();
    var atividades = $("#atividades").val();
    var faixasalarial = $("#faixasalarial").val();
    var sexo = document.getElementsByName("sexo")[0].checked ? document.getElementsByName("sexo")[0].value : document.getElementsByName("sexo")[1].checked ? document.getElementsByName("sexo")[1].value : document.getElementsByName("sexo")[2].checked ? document.getElementsByName("sexo")[2].value : null;
    var experienciamin = $("#experienciamin").val();
    var ensinomin = $("#ensinomin").val();
    var estado = $("#estadovaga").val();
    var cidade = $("#cidadevaga").val();
    var viajar = document.getElementsByName("viajar")[0].checked ? "Não" : "Sim";
    var veiculo = document.getElementsByName("veiculo")[0].checked ? "Não" : "Sim";
    var visibilidade = document.getElementsByName("visibilidade")[0].checked ? "Não" : "Sim";

    if (cargo == "Outro") {
        cargo = $("#novocargo").val();
    }

    if (faixasalarial.toUpperCase() == ("Salário Fixo").toUpperCase()) {
        var salario = $("#vsalario").val();
    } else {
        var salario = null;
    }

    $.ajax({
        type: "POST",
        url: caminho + "/empresas/actions/recebe_anunciovaga.php",
        dataType: "html",
        data: {
            forma: forma,
            cargo: cargo,
            nivel: nivel,
            nvagas: nvagas,
            turno: turno,
            atividades: atividades,
            faixasalarial: faixasalarial,
            salario: salario,
            beneficios: beneficios,
            sexo: sexo,
            experienciamin: experienciamin,
            ensinomin: ensinomin,
            idiomas: idiomas_vaga,
            cidade: cidade,
            estado: estado,
            viajar: viajar,
            veiculo: veiculo,
            cnh: cnh,
            deficiencias: deficiencias,
            informatica: conhecimentos_informatica,
            visibilidade: visibilidade
        },
        success: function () {
            window.location.href = "/vagasevagas/empresas/gerenciar-vagas.php";
        }
    });
}

