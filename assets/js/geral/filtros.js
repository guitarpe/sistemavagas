

function filtrar(id, filtro)
{
    var values = location.search.slice(0);
    var key = values == "" ? true : false;
    var url = caminho + "buscar-vagas.php" + values;

    if (id == 1) {
        if (key) {
            location.href = url + "?cargo=" + filtro;
        } else {
            location.href = url + "&cargo=" + filtro;
        }
    } else if (id == 2) {
        if (key) {
            location.href = url + "?estado=" + filtro;
        } else {
            location.href = url + "&estado=" + filtro;
        }
    } else if (id == 3) {
        if (key) {
            location.href = url + "?cidade=" + filtro;
        } else {
            location.href = url + "&cidade=" + filtro;
        }
    } else if (id == 4) {
        if (key) {
            location.href = url + "?nivel=" + filtro;
        } else {
            location.href = url + "&nivel=" + filtro;
        }
    } else if (id == 5) {
        if (key) {
            location.href = url + "?faixa=" + filtro;
        } else {
            location.href = url + "&faixa=" + filtro;
        }
    } else if (id == 6) {
        if (key) {
            location.href = url + "?contrato=" + filtro;
        } else {
            location.href = url + "&contrato=" + filtro;
        }
    } else if (id == 7) {
        if (key) {
            location.href = url + "?informatica=" + filtro;
        } else {
            location.href = url + "&informatica=" + filtro;
        }
    } else if (id == 8) {
        if (key) {
            location.href = url + "?deficiencia=" + filtro;
        } else {
            location.href = url + "&deficiencia=" + filtro;
        }
    } else if (id == 9) {
        if (key) {
            location.href = url + "?turno=" + filtro;
        } else {
            location.href = url + "&turno=" + filtro;
        }
    } else if (id == 10) {
        if (key) {
            location.href = url + "?beneficio=" + filtro;
        } else {
            location.href = url + "&beneficio=" + filtro;
        }
    } else if (id == 11) {
        if (key) {
            location.href = url + "?escolaridade=" + filtro;
        } else {
            location.href = url + "&escolaridade=" + filtro;
        }
    }
}

function removeFiltroCandidato(id)
{
    var values = location.search.slice(1);
    var datas = values.split('&');
    var url = "buscar-profissional.php";
    var key = true;
    if (id == 1) {
        datas.forEach(function (field) {
            data = field.split('=');
            if (data[0] != "cargo" && key) {
                url += "?" + data[0] + "=" + data[1];
                key = false;
            } else if (data[0] != "cargo" && !key) {
                url += "&" + data[0] + "=" + data[1];
            }
        });
        location.href = url;
    } else if (id == 2) {
        datas.forEach(function (field) {
            data = field.split('=');
            if (data[0] != "estado" && key) {
                url += "?" + data[0] + "=" + data[1];
                key = false;
            } else if (data[0] != "estado" && !key) {
                url += "&" + data[0] + "=" + data[1];
            }
        });
        location.href = url;
    } else if (id == 3) {
        datas.forEach(function (field) {
            data = field.split('=');
            if (data[0] != "cidade" && key) {
                url += "?" + data[0] + "=" + data[1];
                key = false;
            } else if (data[0] != "cidade" && !key) {
                url += "&" + data[0] + "=" + data[1];
            }
        });
        location.href = url;
    } else if (id == 4) {
        datas.forEach(function (field) {
            data = field.split('=');
            if (data[0] != "sexo" && key) {
                url += "?" + data[0] + "=" + data[1];
                key = false;
            } else if (data[0] != "sexo" && !key) {
                url += "&" + data[0] + "=" + data[1];
            }
        });
        location.href = url;
    } else if (id == 5) {
        datas.forEach(function (field) {
            data = field.split('=');
            if (data[0] != "informatica" && key) {
                url += "?" + data[0] + "=" + data[1];
                key = false;
            } else if (data[0] != "informatica" && !key) {
                url += "&" + data[0] + "=" + data[1];
            }
        });
        location.href = url;
    } else if (id == 6) {
        datas.forEach(function (field) {
            data = field.split('=');
            if (data[0] != "deficiencia" && key) {
                url += "?" + data[0] + "=" + data[1];
                key = false;
            } else if (data[0] != "deficiencia" && !key) {
                url += "&" + data[0] + "=" + data[1];
            }
        });
        location.href = url;
    }
}

function filtrarCandidato(id, filtro)
{
    var values = location.search.slice(0);
    var key = values == "" ? true : false;
    var url = "buscar-profissional.php" + values;

    if (id == 1) {
        if (key) {
            location.href = url + "?cargo=" + filtro;
        } else {
            location.href = url + "&cargo=" + filtro;
        }
    } else if (id == 2) {
        if (key) {
            location.href = url + "?estado=" + filtro;
        } else {
            location.href = url + "&estado=" + filtro;
        }
    } else if (id == 3) {
        if (key) {
            location.href = url + "?cidade=" + filtro;
        } else {
            location.href = url + "&cidade=" + filtro;
        }
    } else if (id == 4) {
        if (key) {
            location.href = url + "?sexo=" + filtro;
        } else {
            location.href = url + "&sexo=" + filtro;
        }
    } else if (id == 5) {
        if (key) {
            location.href = url + "?informatica=" + filtro;
        } else {
            location.href = url + "&informatica=" + filtro;
        }
    } else if (id == 6) {
        if (key) {
            location.href = url + "?escolaridade=" + filtro;
        } else {
            location.href = url + "&escolaridade=" + filtro;
        }
    } else if (id == 7) {
        if (key) {
            location.href = url + "?deficiencia=" + filtro;
        } else {
            location.href = url + "&deficiencia=" + filtro;
        }
    }
}