$(document).ready(function () {

    $("#ativar").on("click", function (e) {
        e.preventDefault();
        $("#ativar").addClass('hidden');
        $("#suspender").removeClass('hidden');
    });

    $(".remove_cargos").on("click", function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var obj = $(this);

        if ($("#cargosgrid tr").length > 1) {
            $.ajax({
                type: "POST",
                url: caminho + "/candidatos/actions/recebe_removercargosprofissionais.php",
                dataType: "html",
                data: {
                    id: id
                },
                success: function (result) {
                    if (result) {
                        obj.closest('tr').remove();
                        $dispararAlerta("Removido com sucesso", 'success');
                    } else {
                        $dispararAlerta("Erro ao remover o cargo", 'error');
                    }
                }
            });
        } else {
            $('#modal-default-aviso').on('show.bs.modal', function (e) {
                var modal = $(this);
                modal.find('.modal-body').html("");
                modal.find('.modal-title').text('Atenção');
                modal.find('.modal-body').html('<h3>O Currículo deve possuir no mínimo um Cargo Profissional cadastrado!</h3>');
            });

            $('#modal-default-aviso').modal('show');
        }
    });

    $(".remove_experiencia").on("click", function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var obj = $(this);

        $.ajax({
            type: "POST",
            url: caminho + "/candidatos/actions/recebe_removerexperiencia.php",
            dataType: "html",
            data: {
                id: id
            },
            success: function (result) {
                if (result) {
                    obj.closest('tr').remove();
                    $dispararAlerta("Removido com sucesso", 'success');
                } else {
                    $dispararAlerta("Erro ao remover o experiência", 'error');
                }

                if ($("#exparea tr").length === 0) {
                    $("#exparea").html("<tr id='uexp'><td>Nenhuma experiência cadastrada!</td></tr>");
                }
            }
        });
    });

    $(".remove_formacao").on("click", function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var obj = $(this);

        $.ajax({
            type: "POST",
            url: caminho + "/candidatos/actions/recebe_removerformacao.php",
            dataType: "html",
            data: {
                id: id
            },
            success: function (result) {
                if (result) {
                    obj.closest('tr').remove();
                    $dispararAlerta("Removido com sucesso", 'success');
                } else {
                    $dispararAlerta("Erro ao remover o cargo", 'error');
                }

                if ($("#formacoesarea tr").length == 0) {
                    $("#formacoesarea").html("<tr id='uexp'><td>Nenhuma formação cadastrada!</td></tr>");
                }
            }
        });
    });

    $(".remove_curso").on("click", function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var obj = $(this);

        $.ajax({
            type: "POST",
            url: caminho + "/candidatos/actions/recebe_removercurso.php",
            dataType: "html",
            data: {
                id: id
            },
            success: function (result) {
                if (result) {
                    obj.closest('tr').remove();
                    $dispararAlerta("Removido com sucesso", 'success');
                } else {
                    $dispararAlerta("Erro ao remover o cargo", 'error');
                }

                if ($("#cursosarea tr").length === 0) {
                    $("#cursosarea").html("<tr id='uexp'><td>Nenhum curso cadastrado!</td></tr>");
                }
            }
        });
    });

    $(".remove_idioma").on("click", function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var obj = $(this);

        $.ajax({
            type: "POST",
            url: caminho + "/candidatos/actions/recebe_removeridioma.php",
            dataType: "html",
            data: {
                id: id
            },
            success: function (result) {
                if (result) {
                    obj.closest('tr').remove();
                    $dispararAlerta("Removido com sucesso", 'success');
                } else {
                    $dispararAlerta("Erro ao remover o cargo", 'error');
                }

                if ($("#idiomasarea tr").length === 0) {
                    $("#idiomasarea").html("<tr id='uexp'><td>Nenhum idioma cadastrado!</td></tr>");
                }
            }
        });
    });

    $(".fotoperfil").on("click", function () {
        $("#foto").click();
    });

    $("#incluircargo").on("click", function () {
        if (document.getElementById("cargo").value != "") {

            var key = true;
            $("#novocargo").css("border-color", "#ccc");
            $("#cargo").css("border-color", "#ccc");

            if (document.getElementById("cargo").value == "Outro") {
                if (document.getElementById("novocargo").value != "") {
                    var cargo = document.getElementById("novocargo").value;
                    $("#novocargo").val("");
                    $("#label-ncargo").hide();
                    $("#label-cargo").removeClass("third").addClass("quad3");
                } else {
                    $("#novocargo").css("border-color", "red");
                    key = false;
                }
            } else {
                var cargo = document.getElementById("cargo").value;
            }

            if (key) {
                $.ajax({
                    type: "POST",
                    url: caminho + "/actions/recebe_cadastrocargoprofissional.php",
                    dataType: 'html',
                    data: {
                        cargo: cargo
                    },
                    success: function (id) {
                        if (id == -1) {
                            $('#modal-default-aviso').modal('show');

                            $('#modal-default-aviso').on('show.bs.modal', function (e) {
                                $(this).find('.modal-title').text('Atenção');
                                $(this).find('.modal-body').load('<p>O número máximo de Cargos Profissionais é três!</p>');
                            });
                        } else {

                            $('#modal-default-aviso').modal('show');

                            $('#modal-default-aviso').on('show.bs.modal', function (e) {
                                $(this).find('.modal-title').text('Atenção');
                                $(this).find('.modal-body').load('<p>Cargo Profissional incluído com sucesso!</p>');
                            });
                            setCargosGrid(id, cargo);
                        }
                    }
                });
                $("#cargo").val("");
            }

        } else {
            if (document.getElementById("cargo").value == "") {
                $("#cargo").css("border-color", "red");
            }
        }
    });

    $("#incluirexperiencia").on("click", function () {

        $("#experiencias").css("border", "");
        $("#experiencias").css("padding", "");
        $("#nomeempresa").css("border-color", "#ccc");
        $("#cargoempresa").css("border-color", "#ccc");
        $("#datainicio").css("border-color", "#ccc");
        $("#datafim").css("border-color", "#ccc");
        $("#passo2").attr("target", "step3");
        var key = true;

        if (document.getElementById("nomeempresa").value == "") {
            $("#nomeempresa").css("border-color", "red");
            key = false;
        }
        if (document.getElementById("cargoempresa").value == "") {
            $("#cargoempresa").css("border-color", "red");
            key = false;
        }
        if (document.getElementById("cargoempresa").value == "Outro" &&
                document.getElementById("novocargoexp").value == "") {
            $("#novocargoexp").css("border-color", "red");
            key = false;
        }
        if (document.getElementById("datainicio").value == "") {
            $("#datainicio").css("border-color", "red");
            key = false;
        }
        if (document.getElementById("datafim").value == "" && document.getElementById("emprego-atual").checked == false) {
            $("#datafim").css("border-color", "red");
            key = false;
        }
        if (document.getElementById("datainicio").value != "" &&
                document.getElementById("datafim").value != "") {
            var inicio = document.getElementById("datainicio").value.split("/");
            var fim = document.getElementById("datafim").value.split("/");

            if (inicio[1] > fim[1]) {
                $("#datainicio").css("border-color", "red");
                $("#datafim").css("border-color", "red");
                $("#msg-dataexp").show();
                key = false;
            } else {
                if (inicio[1] == fim[1]) {
                    if (inicio[0] > fim[0]) {
                        $("#datainicio").css("border-color", "red");
                        $("#datafim").css("border-color", "red");
                        $("#msg-dataexp").show();
                        key = false;
                    } else {
                        $("#datainicio").css("border-color", "#ccc");
                        $("#datafim").css("border-color", "#ccc");
                        $("#msg-dataexp").hide();
                    }
                } else {
                    $("#datainicio").css("border-color", "#ccc");
                    $("#datafim").css("border-color", "#ccc");
                    $("#msg-dataexp").hide();
                }
            }
        }
        if (key) {
            if (document.getElementById("cargoempresa").value == "Outro") {
                var empresa = document.getElementById("nomeempresa").value;
                var cargo = document.getElementById("novocargoexp").value
                $.ajax({
                    type: "POST",
                    url: caminho + "/actions/recebe_cadastroexperiencia.php",
                    dataType: 'html',
                    data: {
                        nomeempresa: document.getElementById("nomeempresa").value,
                        cargoempresa: document.getElementById("novocargoexp").value,
                        atividades: document.getElementById("atividades").value,
                        inicio: document.getElementById("datainicio").value,
                        fim: document.getElementById("datafim").value,
                        empregoatual: document.getElementById("emprego-atual").checked ? 1 : 0,
                        salario: document.getElementById("salario").value
                    },
                    success: function (id) {
                        $('#modal-default-aviso').modal('show');

                        $('#modal-default-aviso').on('show.bs.modal', function (e) {
                            $(this).find('.modal-title').text('Atenção');
                            $(this).find('.modal-body').load('<p>Experiência incluída com sucesso!</p>');
                        });
                        setExpGrid(id, empresa, cargo);
                    }
                });

            } else {
                var empresa = document.getElementById("nomeempresa").value;
                var cargo = document.getElementById("cargoempresa").value;
                $.ajax({
                    type: "POST",
                    url: caminho + "/actions/recebe_cadastroexperiencia.php",
                    dataType: 'html',
                    data: {
                        nomeempresa: document.getElementById("nomeempresa").value,
                        cargoempresa: document.getElementById("cargoempresa").value,
                        atividades: document.getElementById("atividades").value,
                        inicio: document.getElementById("datainicio").value,
                        fim: document.getElementById("datafim").value,
                        empregoatual: document.getElementById("emprego-atual").checked ? 1 : 0,
                        salario: document.getElementById("salario").value
                    },
                    success: function (id) {

                        $('#modal-default-aviso').modal('show');

                        $('#modal-default-aviso').on('show.bs.modal', function (e) {
                            $(this).find('.modal-title').text('Atenção');
                            $(this).find('.modal-body').load('<p>Experiência incluída com sucesso!</p>');
                        });
                        setExpGrid(id, empresa, cargo);
                    }
                });
            }


            $("#nomeempresa").val("");
            $("#cargoempresa").val("");
            $("#novocargoexp").val("");
            $("#label-ncargoexp").hide();
            $("#atividades").val("");
            $("#datainicio").val("");
            $("#datafim").val("");
            $("#salario").val("");
            if (document.getElementById("emprego-atual").checked) {
                document.getElementById("emprego-atual").click();
            }
        }
    });

    $("#instituicao").blur(function () {
        if (document.getElementById("instituicao").value != "") {
            $("#instituicao").css("border-color", "#ccc");
        }
    });

    $("#nivelensino").change(function () {
        var nivel = document.getElementById("nivelensino").value;
        if (nivel == "Ensino fundamental" ||
                nivel == "Ensino médio") {
            $(".optcursos").hide();
            $(".noptcursos").show();
        } else {
            $(".optcursos").show();
            $(".noptcursos").hide();
        }
    });
});

function setCargosGrid(id, cargo) {
    $("#cargosgrid").append(
            "<li>" +
            "<span>" + cargo + "</span>" +
            "<button type='button' onclick='removeCargosProfissionais(" + id + ", this)'>" +
            "<i class='fa fa-times-circle' aria-hidden='true'></i> " +
            "Remover" +
            "</button>" +
            "</li>"
            );
}

function setExpGrid(id, empresa, cargo) {
    if ($("#exparea li").length == 1) {
        $("#uexp").remove();
    }

    $("#exparea").append(
            "<li>" +
            "<span>" + empresa + "</span>" +
            "<span>" + cargo + "</span>" +
            "<button type='button' onclick='removeExperienciaProfissional(" + id + ", this)'>" +
            "<i class='fa fa-times-circle' aria-hidden='true'></i> " +
            "Remover" +
            "</button>" +
            "</li>"
            );
}