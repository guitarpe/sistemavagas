$(document).ready(function () {

    $(".verifica_cpf_cadastrado").on("blur", function () {

        var numdoc = $(this).val();

        var idobj = $(this).attr("id");
        var ob = "#" + idobj;

        if (numdoc !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/buscar_doc.php",
                dataType: "html",
                data: {
                    doc: numdoc,
                    tipo: 'candidato'
                },
                success: function (result) {
                    if (result == true) {
                        $(ob).val("");
                        $(ob).focus();

                        $dispararAlerta("Este número de documento já está cadastrado", 'warning');
                    }
                }
            });
        }
    });

    $("#txtEmail").on("blur", function () {

        var email = $(this).val();

        if (email !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/buscar_email.php",
                dataType: "html",
                data: {
                    email: email,
                    tipo: 'candidato'
                },
                success: function (result) {
                    if (result == true) {
                        $("#txtEmail").val("");
                        $("#txtEmail").focus();
                        $dispararAlerta("Já existe um e-mail igual a este cadastrado", 'warning');
                    }
                }
            });
        }
    });

    $("#add_cargo").on('click', function () {

        var cloneCount = 1;
        var quantidade = 3;

        if ($("div.div_cargo").length < quantidade) {
            var $row = $("div.div_cargo:last").clone();
            $row.find('select').attr('id', 'select_cargo' + cloneCount++);

            $row.find('.outro_cargo').addClass("hidden");
            $row.insertAfter("div.div_cargo:last");
            $row.find("select").prop("selectedIndex", 0);
        } else {
            $('#modalAviso').modal();
        }

        if ($("div.div_cargo").length > 1) {
            $("#rem_cargo").css('display', 'block');
        } else {
            $("#rem_cargo").css('display', 'none');
        }

    });

    $("#rem_cargo").on('click', function () {
        $("div.div_cargo:last").remove();

        if ($("div.div_cargo").length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $(document).on("change", ".cargoop", function () {

        var outro_cargo = $(this).closest('.div_cargo').find('.outro_cargo');

        if ($(this).val() === 'Outro') {
            outro_cargo.removeClass("hidden");
            outro_cargo.find('input').prop('required', true);
        } else {
            outro_cargo.addClass("hidden");
            outro_cargo.find('input').prop('required', false);
        }
    });

    $("input[name='experiencia']").on('click', function () {
        var valor = $(this).val();

        if (valor == 'Sim') {
            $("#origem_empresa").css('display', 'block');

            $("#origem_empresa").find("input[name='nomeempresa[]']").prop('required', true);
            $("#origem_empresa").find("input[name='cargoempresa[]']").prop('required', true);
            $("#origem_empresa").find("input[name='datainicio[]']").prop('required', true);
            $("#origem_empresa").find("input[name='datafim[]']").prop('required', true);

        } else {
            $("#origem_empresa").css('display', 'none');

            $("#origem_empresa").find("input[name='nomeempresa[]']").prop('required', false);
            $("#origem_empresa").find("input[name='cargoempresa[]']").prop('required', false);
            $("#origem_empresa").find("input[name='datainicio[]']").prop('required', false);
            $("#origem_empresa").find("input[name='datafim[]']").prop('required', false);
        }

        $("div.campos_empresa:not(:first)").each(function () {
            $(this).remove();
        });

        $("#remover_experiencia").css('display', 'none');
    });

    $("#adicionar_experiencia").on('click', function () {
        var $row = $("div.campos_empresa:last").clone();
        $row.insertAfter("div.campos_empresa:last");

        $row.find("input:text").val("").end();
        $row.find("select").prop("selectedIndex", 0);
        $row.find("textarea").val("");
        $row.find("input:checkbox").prop("checked", false);
        $row.find(".salario").removeAttr('required');

        if ($("div.campos_empresa").length > 1) {
            $("#remover_experiencia").css('display', 'block');
        } else {
            $("#remover_experiencia").css('display', 'none');
        }
    });

    $("#remover_experiencia").on('click', function () {
        $("div.campos_empresa:last").remove();

        if ($("div.campos_empresa").length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $("#adicionar_curso").on('click', function () {
        var quantidade = 3;

        if ($("div.div_cursos").length < quantidade) {
            var $row = $("div.div_cursos:last").clone();
            $row.insertAfter("div.div_cursos:last");

            var lg = $("div.div_cursos").length;
            var id_dias = 'dias_semana_' + (lg - 1);
            var hash_id_dias = '#dias_semana_' + (lg - 1);
            var slct_dias = 'slct_' + (lg - 1);
            var hash_dias = '#slct_' + (lg - 1);

            $row.find("select.dias_semana").prop("id", slct_dias);
            $row.find("select.slct_situacao2").attr("data-length", (lg - 1));
            $row.find("input.dias_semana_hiden").prop("id", id_dias);
            $row.find(hash_id_dias).val("");

            $row.find("input:text").val("").end();
            $row.find("select:not(.dias_semana)").prop("selectedIndex", 0);
            $row.find("textarea").val("").end();

            $row.find('.campo_turno').addClass('hidden');
            $row.find('.dias_semanas').addClass('hidden');
            $row.find('.campo_ano_conclusao').addClass('hidden');


            $row.find('.dias_semanas').find("div.btn-group:last").remove();

            $row.find(hash_dias).multiselect("destroy");
        }

        if ($("div.div_cursos").length > 1) {
            $("#remover_curso").css('display', 'block');
        } else {
            $("#remover_curso").css('display', 'none');
        }
    });

    $("#remover_curso").on('click', function () {
        $("div.div_cursos:last").remove();

        if ($("div.div_cursos").length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $("#adicionar_formacao").on('click', function () {
        var quantidade = 3;

        if ($("div.div_formacao").length < quantidade) {
            var $row = $("div.div_formacao:last").clone();
            $row.insertAfter("div.div_formacao:last");

            $row.find("input:text").val("").end();
            $row.find("select").prop("selectedIndex", 0);
            $row.find("textarea").val("").end();
        }

        if ($("div.div_formacao").length > 1) {
            $("#remover_formacao").css('display', 'block');
        } else {
            $("#remover_formacao").css('display', 'none');
        }
    });

    $("#remover_formacao").on('click', function () {
        $("div.div_formacao:last").remove();

        if ($("div.div_formacao").length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $("#adicionar_idioma").on('click', function () {
        var quantidade = 3;

        if ($("div.div_idioma").length < quantidade) {
            var $row = $("div.div_idioma:last").clone();
            $row.insertAfter("div.div_idioma:last");

            $row.find("input:text").val("").end();
            $row.find("select").prop("selectedIndex", 0);
            $row.find("textarea").val("").end();
        }

        if ($("div.div_idioma").length > 1) {
            $("#remover_idioma").css('display', 'block');
        } else {
            $("#remover_idioma").css('display', 'none');
        }
    });

    $("#remover_idioma").on('click', function () {
        $("div.div_idioma:last").remove();

        if ($("div.div_idioma").length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $("#repetir_email").on('blur', function () {
        var email = $(this).val();
        var campo = $("#txtEmail").val();

        if (email !== "") {
            if (campo !== email) {
                $dispararAlerta("Repita o e-mail corretamente", 'warning');
                $(this).val("");
                $(this).focus();
            }
        }
    });

    $(document).on('change', '.slct_formacao', function () {

        var valor = $(this).val();

        if (valor === "Curso técnico" || valor === "Ensino superior" || valor === "Pós-graduação") {

            $(this).closest('.div_formacao').find('.campo_nomecurso').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_situacao').find('select').val('');
            $(this).closest('.div_formacao').find('.campo_instituicao').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
            $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            $(this).closest('.div_formacao').find('.campo_nomecurso').removeClass('hidden');
            $(this).closest('.div_formacao').find('.campo_situacao').removeClass('hidden');
            $(this).closest('.div_formacao').find('.campo_instituicao').removeClass('hidden');

            $(this).closest('.div_formacao').find('.campo_ano_conclusao').addClass('hidden');
            $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
            $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');

        } else if (valor === "Ensino fundamental" || valor === "Ensino médio") {

            $(this).closest('.div_formacao').find('.campo_nomecurso').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_situacao').find('select').val('');
            $(this).closest('.div_formacao').find('.campo_instituicao').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
            $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            $(this).closest('.div_formacao').find('.campo_nomecurso').addClass('hidden');
            $(this).closest('.div_formacao').find('.campo_situacao').removeClass('hidden');
            $(this).closest('.div_formacao').find('.campo_instituicao').removeClass('hidden');

            $(this).closest('.div_formacao').find('.campo_ano_conclusao').addClass('hidden');
            $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
            $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');
        }
    });

    $(document).on('change', '.slct_situacao', function () {

        var valor = $(this).val();
        var formacao = $(this).closest('.div_formacao').find('.slct_formacao').val();

        if (formacao === "Curso técnico" || formacao === "Ensino superior" || formacao === "Pós-graduação") {

            $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
            $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            if (valor === "Concluído") {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            } else if (valor === "Em Andamento") {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsão de Conclusão');
                $(this).closest('.div_formacao').find('.campo_turno').removeClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').removeClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            } else if (valor === "Trancado") {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').addClass('hidden');
                $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').removeClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            }
        } else if (formacao === "Ensino fundamental" || formacao === "Ensino médio") {

            $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
            $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
            $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            if (valor === "Concluído") {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            } else if (valor === "Em Andamento") {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsao de Conclusão');
                $(this).closest('.div_formacao').find('.campo_turno').removeClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            } else if (valor === "Trancado") {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').addClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            } else {

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').addClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
                $(this).closest('.div_formacao').find('.campo_turno').addClass('hidden');
                $(this).closest('.div_formacao').find('.campo_semestre').addClass('hidden');

                $(this).closest('.div_formacao').find('.campo_ano_conclusao').find('input').val('');
                $(this).closest('.div_formacao').find('.campo_turno').find('select').val('');
                $(this).closest('.div_formacao').find('.campo_semestre').find('select').val('');

            }

        }
    });

    $(document).on('change', '.slct_situacao2mod', function () {

        var valor = $(this).val();

        var hash_id_dias = '#dias_semana_0';
        var slct_dias = '#slct_0';

        if (valor === "Concluído") {

            $('.div_cursosmod').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
            $('.div_cursosmod').find('.campo_turno').addClass('hidden');
            $('.div_cursosmod').find('.dias_semanas').addClass('hidden');

            $(slct_dias).multiselect("destroy");

        } else if (valor === "Em Andamento") {

            $('.div_cursosmod').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsao de Conclusão');
            $('.div_cursosmod').find('.campo_turno').removeClass('hidden');
            $('.div_cursosmod').find('.dias_semanas').removeClass('hidden');

            $(slct_dias).multiselect({
                nonSelectedText: 'Selecione os Dias da Semana',
                enableFiltering: false,
                enableCaseInsensitiveFiltering: false,
                buttonWidth: '100%',
                nSelectedText: 'dias',
                allSelectedText: 'Todos',
                delimiter: ', ',
                onChange: function (element, checked) {

                    var brands = $(slct_dias + ' option:selected');

                    var selected = [];

                    $(brands).each(function (index, brand) {
                        selected.push($(this).val());
                    });

                    $(hash_id_dias).val(selected.join(', '));
                }
            });

        } else if (valor === "Trancado") {

            $('.div_cursosmod').find('.campo_ano_conclusao').addClass('hidden');
            $('.div_cursosmod').find('.campo_turno').addClass('hidden');
            $('.div_cursosmod').find('.dias_semanas').addClass('hidden');

            $(slct_dias).multiselect("destroy");
        }
    });

    $(document).on('change', '.slct_situacao2', function () {

        var valor = $(this).val();

        var num = $(this).data('length');
        var hash_id_dias = '#dias_semana_' + num;
        var slct_dias = '#slct_' + num;

        if (valor === "Concluído") {

            $(this).closest('.div_cursos').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Ano de Conclusão');
            $(this).closest('.div_cursos').find('.campo_turno').addClass('hidden');
            $(this).closest('.div_cursos').find('.dias_semanas').addClass('hidden');

            $(slct_dias).multiselect("destroy");

        } else if (valor === "Em Andamento") {

            $(this).closest('.div_cursos').find('.campo_ano_conclusao').removeClass('hidden').find('input').attr('placeholder', 'Previsao de Conclusão');
            $(this).closest('.div_cursos').find('.campo_turno').removeClass('hidden');
            $(this).closest('.div_cursos').find('.dias_semanas').removeClass('hidden');

            $(slct_dias).multiselect({
                nonSelectedText: 'Selecione os Dias da Semana',
                enableFiltering: false,
                enableCaseInsensitiveFiltering: false,
                buttonWidth: '100%',
                nSelectedText: 'dias',
                allSelectedText: 'Todos',
                delimiter: ', ',
                onChange: function (element, checked) {

                    var brands = $(slct_dias + ' option:selected');

                    var selected = [];

                    $(brands).each(function (index, brand) {
                        selected.push($(this).val());
                    });

                    $(hash_id_dias).val(selected.join(', '));
                }
            });

        } else if (valor === "Trancado") {

            $(this).closest('.div_cursos').find('.campo_ano_conclusao').addClass('hidden');
            $(this).closest('.div_cursos').find('.campo_turno').addClass('hidden');
            $(this).closest('.div_cursos').find('.dias_semanas').addClass('hidden');

            $(slct_dias).multiselect("destroy");
        }
    });

    $(document).on("change", ".data_fim", function () {
        var dtini = $(this).closest('.campos_empresa').find('.data_ini').val();
        var objdtini = $(this).closest('.campos_empresa').find('.data_ini');

        var dtfim = $(this).val();

        if (dtfim !== "") {
            var inicio = dtini.split("/");
            var final = dtfim.split("/");

            var dataini = new Date(inicio[1], inicio[0] - 1, 1);
            var datafim = new Date(final[1], final[0] - 1, 1);

            if (!$anoInvalido(inicio[1])) {

                var nomecampo = objdtini.attr('placeholder');

                $dispararAlerta("Informe " + nomecampo + " válida", 'warning');
                objdtini.val("");
                objdtini.focus();

            } else if (!$anoInvalido(final[1])) {

                var nomecampo = $(this).attr('placeholder');

                $dispararAlerta("Informe " + nomecampo + " válida", 'warning');
                $(this).val("");
                $(this).focus();
            } else {

                if (datafim < dataini) {
                    $dispararAlerta("A data Início não pode ser maior que a data final!", 'warning');
                    $(this).val("");
                    $(this).focus();
                }
            }
        }

    });

    $(document).on("change", ".data_ini", function () {
        var dtini = $(this).val();
        var dtfim = $(this).closest('.campos_empresa').find('.data_fim').val();
        var objdtini = $(this).closest('.campos_empresa').find('.data_fim');

        if (dtini !== "") {
            var inicio = dtini.split("/");
            var final = dtfim.split("/");

            var dataini = new Date(inicio[1], inicio[0] - 1, 1);
            var datafim = new Date(final[1], final[0] - 1, 1);

            if (!$anoInvalido(final[1])) {

                var nomecampo = objdtini.attr('placeholder');

                $dispararAlerta("Informe " + nomecampo + " válida", 'warning');
                objdtini.val("");
                objdtini.focus();

            } else if (!$anoInvalido(inicio[1])) {

                var nomecampo = $(this).attr('placeholder');

                $dispararAlerta("Informe " + nomecampo + " válida", 'warning');
                $(this).val("");
                $(this).focus();

            } else {

                if (datafim < dataini) {
                    $dispararAlerta("A data Início não pode ser maior que a data final!", 'warning');
                    $(this).val("");
                    $(this).focus();
                }
            }
        }

    });

    $(document).on('change', '.set_empr_atual', function () {
        var data_saida = $(this).closest('.campos_empresa').find('.data_fim');

        if (this.checked) {
            data_saida.removeAttr('required');
            data_saida.val("");
        } else {
            data_saida.attr('required', 'required');
            data_saida.val("");
        }
    });

    $(document).on('blur', '.info_ano', function () {

        var anoinfo = $(this).val();

        if (anoinfo !== "") {
            var anoatual = (new Date).getFullYear();

            var valor = $(this).closest('.div_formacao').find('.slct_situacao').val();

            var nomecampo = $(this).attr('placeholder');

            if (!$anoInvalido(anoinfo)) {

                var nomecampo = $(this).attr('placeholder');

                $dispararAlerta("Informe " + nomecampo + " válida", 'warning');
                $(this).val("");
                $(this).focus();

            } else {
                if (valor === "Em Andamento") {
                    if (anoinfo < anoatual) {
                        $dispararAlerta("O campo " + nomecampo + " não pode ser anterior ao ano atual", 'warning');
                        $(this).val("");
                        $(this).focus();
                    }
                } else if (valor === "Concluído" || valor === "Trancado") {
                    if (anoinfo > anoatual) {
                        $dispararAlerta("O campo " + nomecampo + " não pode ser superior ao ano atual", 'warning');
                        $(this).val("");
                        $(this).focus();
                    }
                }
            }
        }
    });

    $(document).on('blur', '.ano_conclusao', function () {

        var anoinfo = $(this).val();

        if (anoinfo !== "") {
            var anoatual = (new Date).getFullYear();

            var valor = $(this).closest('.div_cursos').find('.slct_situacao2').val();

            var nomecampo = $(this).attr('placeholder');

            if (!$anoInvalido(anoinfo)) {

                var nomecampo = $(this).attr('placeholder');

                $dispararAlerta("Informe " + nomecampo + " válida", 'warning');
                $(this).val("");
                $(this).focus();

            } else {
                if (valor === "Em Andamento") {
                    if (anoinfo < anoatual) {
                        $dispararAlerta("O campo " + nomecampo + " não pode ser anterior ao ano atual", 'warning');
                        $(this).val("");
                        $(this).focus();
                    }
                } else if (valor === "Concluído" || valor === "Trancado") {
                    if (anoinfo > anoatual) {
                        $dispararAlerta("O campo " + nomecampo + " não pode ser superior ao ano atual", 'warning');
                        $(this).val("");
                        $(this).focus();
                    }
                }
            }
        }
    });

    $("#cargoprofissional").on("change", function () {
        $("#cargos").css("border", "");
        $("#cargos").css("padding", "");
        $("#cargoprofissional").css("border-color", "#ccc");
        if (document.getElementById("cargoprofissional").value == "Outro") {
            $("#label-cargo").removeClass("half").addClass("third");
            $("#label-ncargo").show();
        } else {
            $("#label-cargo").removeClass("third").addClass("half");
            $("#label-ncargo").hide();
        }
    });

    $("#novocargo").blur(function () {
        if (document.getElementById("novocargo").value != "") {
            $("#novocargo").css("border-color", "#ccc");
        }
    });

    $("#passo2").on("click", function () {
        var exp = document.getElementsByName("experiencia")[0].checked ? document.getElementsByName("experiencia")[0].value : document.getElementsByName("experiencia")[1].value;
        $("#cargos").css("border", "");
        $("#cargos").css("padding", "");
        $("#experiencias").css("border", "");
        $("#experiencias").css("padding", "");
        $("#passo2").attr("target", "step3");

        if (cargosprof.length == 0) {
            $("#cargos").css("border", "1px solid red");
            $("#cargos").css("padding", "20px");
            $("#passo2").attr("target", "step2");
        }

        if (exp == "Sim" && experiencias.length == 0) {
            $("#experiencias").css("border", "1px solid red");
            $("#experiencias").css("padding", "20px");
            $("#passo2").attr("target", "step2");
        }
    });

    $("#cadastrarcurriculo").on("click", function () {

        $("#icadastrarcurriculo").removeClass("fa fa-angle-double-right").addClass("fa fa-spinner fa-spin");
        recebeDadosCandidato();

    });

    $("#nomeempresa").blur(function () {
        $("#nomeempresa").css("border-color", "#ccc");
        $("#experiencias").css("border", "");
        $("#experiencias").css("padding", "");
    });

    $("#novocargoexp").blur(function () {
        if (document.getElementById("novocargoexp").value != "") {
            $("#novocargoexp").css("border-color", "#ccc");
        }
    });

    $("#formacao").blur(function () {
        if (document.getElementById("formacao").value != "") {
            $("#formacao").css("border-color", "#ccc");
        }
    });

    $("#situacao").blur(function () {
        if (document.getElementById("situacao").value != "") {
            $("#situacao").css("border-color", "#ccc");
        }
    });

    $("#situacao2").blur(function () {
        if (document.getElementById("situacao2").value != "") {
            $("#situacao2").css("border-color", "#ccc");
        }
    });

    $("#nomecurso").blur(function () {
        if (document.getElementById("nomecurso").value != "") {
            $("#nomecurso").css("border-color", "#ccc");
        }
    });

    $("#instituicao").blur(function () {
        if (document.getElementById("instituicao").value != "") {
            $("#instituicao").css("border-color", "#ccc");
        }
    });

    $("#iniciocurso").blur(function () {
        $("#iniciocurso").css("border-color", "#ccc");
        $("#msg-datacurso").hide();
        var data = new Date();
        var ano = data.getFullYear();

        if (document.getElementById("iniciocurso").value.length == 7 &&
                document.getElementById("fimcurso").value.length == 7) {
            var inicio = document.getElementById("iniciocurso").value.split("/");
            var fim = document.getElementById("fimcurso").value.split("/");

            if (inicio[1] > fim[1]) {
                $("#iniciocurso").css("border-color", "red");
                $("#fimcurso").css("border-color", "red");
                $("#msg-datacurso").text("Data de início é superior a data do fim!");
                $("#msg-datacurso").show();
            } else {
                if (inicio[1] == fim[1]) {
                    if (inicio[0] > fim[0]) {
                        $("#iniciocurso").css("border-color", "red");
                        $("#fimcurso").css("border-color", "red");
                        $("#msg-datacurso").text("Data de início é superior a data do fim!");
                        $("#msg-datacurso").show();
                    } else {
                        $("#iniciocurso").css("border-color", "#ccc");
                        $("#fimcurso").css("border-color", "#ccc");
                        $("#msg-datacurso").hide();
                        $("#msg-datacurso2").hide();
                    }
                } else {
                    $("#iniciocurso").css("border-color", "#ccc");
                    $("#fimcurso").css("border-color", "#ccc");
                    $("#msg-datacurso").hide();
                    $("#msg-datacurso2").hide();
                }
            }

            if (inicio[1] < 1900 || inicio[1] > ano || inicio[0] < 1 || inicio[0] > 12) {
                $("#iniciocurso").css("border-color", "red");
                $("#msg-datacurso").text("Data inválida!");
                $("#msg-datacurso").show();
            }
            if (fim[1] < 1900 || fim[1] > ano || fim[0] < 1 || fim[0] > 12) {
                $("#fimcurso").css("border-color", "red");
                $("#msg-datacurso2").text("Data inválida!");
                $("#msg-datacurso2").show();
            }
        } else {
            if (document.getElementById("iniciocurso").value.length == 7) {
                var inicio = document.getElementById("iniciocurso").value.split("/");
                if (inicio[1] < 1900 || inicio[1] > ano || inicio[0] < 1 || inicio[0] > 12) {
                    $("#iniciocurso").css("border-color", "red");
                    $("#msg-datacurso").text("Data inválida!");
                    $("#msg-datacurso").show();
                }
            } else {
                $("#iniciocurso").css("border-color", "red");
                $("#msg-datacurso").text("Data inválida!");
                $("#msg-datacurso").show();
            }
        }
    });

    $("#fimcurso").blur(function () {
        if (document.getElementById("fimcurso").value != "") {
            $("#fimcurso").css("border-color", "#ccc");
            $("#msg-datacurso2").hide();
            var data = new Date();
            var ano = data.getFullYear();

            if (document.getElementById("iniciocurso").value.length == 7 &&
                    document.getElementById("fimcurso").value.length == 7) {
                var inicio = document.getElementById("iniciocurso").value.split("/");
                var fim = document.getElementById("fimcurso").value.split("/");

                if (inicio[1] > fim[1]) {
                    $("#iniciocurso").css("border-color", "red");
                    $("#fimcurso").css("border-color", "red");
                    $("#msg-datacurso").text("Data de início é superior a data do fim!");
                    $("#msg-datacurso").show();
                } else {
                    if (inicio[1] == fim[1]) {
                        if (inicio[0] > fim[0]) {
                            $("#iniciocurso").css("border-color", "red");
                            $("#fimcurso").css("border-color", "red");
                            $("#msg-datacurso").text("Data de início é superior a data do fim!");
                            $("#msg-datacurso").show();
                        } else {
                            $("#iniciocurso").css("border-color", "#ccc");
                            $("#fimcurso").css("border-color", "#ccc");
                            $("#msg-datacurso").hide();
                            $("#msg-datacurso2").hide();
                        }
                    } else {
                        $("#iniciocurso").css("border-color", "#ccc");
                        $("#fimcurso").css("border-color", "#ccc");
                        $("#msg-datacurso").hide();
                        $("#msg-datacurso2").hide();
                    }
                }

                if (inicio[1] < 1900 || inicio[1] > ano || inicio[0] < 1 || inicio[0] > 12) {
                    $("#iniciocurso").css("border-color", "red");
                    $("#msg-datacurso").text("Data inválida!");
                    $("#msg-datacurso").show();
                }
                if (fim[1] < 1900 || fim[1] > ano || fim[0] < 1 || fim[0] > 12) {
                    $("#fimcurso").css("border-color", "red");
                    $("#msg-datacurso2").text("Data inválida!");
                    $("#msg-datacurso2").show();
                }
            } else {
                if (document.getElementById("fimcurso").value.length == 7) {
                    var fim = document.getElementById("fimcurso").value.split("/");
                    if (fim[1] < 1900 || fim[1] > ano || fim[0] < 1 || fim[0] > 12) {
                        $("#fimcurso").css("border-color", "red");
                        $("#msg-datacurso2").text("Data inválida!");
                        $("#msg-datacurso2").show();
                    }
                } else {
                    $("#fimcurso").css("border-color", "red");
                    $("#msg-datacurso2").text("Data inválida!");
                    $("#msg-datacurso2").show();
                }
            }
        }
    });

    $("#instituicao2").blur(function () {
        if (document.getElementById("instituicao2").value != "") {
            $("#instituicao2").css("border-color", "#ccc");
        }
    });

    $("#nomeformacao").blur(function () {
        if (document.getElementById("nomeformacao").value != "") {
            $("#nomeformacao").css("border-color", "#ccc");
        }
    });

    $("#semestre").blur(function () {
        if (document.getElementById("semestre").value != "") {
            $("#semestre").css("border-color", "#ccc");
        }
    });

    $("#turno").blur(function () {
        if (document.getElementById("turno").value != "") {
            $("#turno").css("border-color", "#ccc");
        }
    });

    $("#anofim").blur(function () {
        if (document.getElementById("anofim").value != "") {
            $("#anofim").css("border-color", "#ccc");
            $("#msg-datacurso3").hide();
            var data = new Date();
            var ano = data.getFullYear();
            var fim = document.getElementById("anofim").value;
            if (fim < 1900 || fim > ano + 10) {
                $("#anofim").css("border-color", "red");
                $("#msg-datacurso3").text("Ano inválido!");
                $("#msg-datacurso3").show();
            }
        }
    });

    $("#formacao").change(function () {

        if ($("#formacao").val().toUpperCase() == ("Ensino fundamental").toUpperCase() ||
                $("#formacao").val().toUpperCase() == ("Ensino médio").toUpperCase()) {
            $("#trancado").hide();
            $("#situacao").val("");
        } else {
            $("#trancado").show();
        }

        if ($("#situacao").val().toUpperCase() == ("Concluído").toUpperCase()) {

            if ($("#formacao").val().toUpperCase() == ("Curso técnico").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Ensino superior").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $("#label-curso").show();
                $("#label-prev").show();
                $("#label-semestre").hide();
                $("#label-turno").hide();
                $("#turno").val("");
                $("#semestre").val("");
            } else {
                $("#label-curso").hide();
                $("#label-prev").show();
                $("#label-semestre").hide();
                $("#label-turno").hide();
                $("#turno").val("");
                $("#semestre").val("");
                $("#nomeformacao").val("");
            }
        } else if ($("#situacao").val().toUpperCase() == ("Em Andamento").toUpperCase()) {

            if ($("#formacao").val().toUpperCase() == ("Curso técnico").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Ensino superior").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $("#label-curso").show();
                $("#label-prev").show();
                $("#label-semestre").show();
                $("#label-turno").show();
            } else {
                $("#label-curso").hide();
                $("#label-prev").show();
                $("#label-semestre").hide();
                $("#label-turno").show();
                $("#nomeformacao").val("");
                $("#semestre").val("");
            }

        } else {
            if ($("#formacao").val().toUpperCase() == ("Curso técnico").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Ensino superior").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $("#label-curso").show();
                $("#label-prev").hide();
                $("#label-semestre").show();
                $("#label-turno").hide();
                $("#anofim").val("");
                $("#turno").val("");
            } else {
                $("#label-curso").hide();
                $("#label-prev").hide();
                $("#label-semestre").hide();
                $("#label-turno").hide();
                $("#nomeformacao").val("");
                $("#anofim").val("");
                $("#turno").val("");
                $("#semestre").val("");
            }
        }
    });

    $("#situacao").change(function () {
        if ($("#situacao").val().toUpperCase() == ("Concluído").toUpperCase()) {

            if ($("#formacao").val().toUpperCase() == ("Curso técnico").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Ensino superior").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $("#label-curso").show();
                $("#label-prev").show();
                $("#label-semestre").hide();
                $("#label-turno").hide();
                $("#turno").val("");
                $("#semestre").val("");
            } else {
                $("#label-curso").hide();
                $("#label-prev").show();
                $("#label-semestre").hide();
                $("#label-turno").hide();
                $("#turno").val("");
                $("#semestre").val("");
                $("#nomeformacao").val("");
            }
        } else if ($("#situacao").val().toUpperCase() == ("Em Andamento").toUpperCase()) {

            if ($("#formacao").val().toUpperCase() == ("Curso técnico").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Ensino superior").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $("#label-curso").show();
                $("#label-prev").show();
                $("#label-semestre").show();
                $("#label-turno").show();
            } else {
                $("#label-curso").hide();
                $("#label-prev").show();
                $("#label-semestre").hide();
                $("#label-turno").show();
                $("#nomeformacao").val("");
                $("#semestre").val("");
            }

        } else {
            if ($("#formacao").val().toUpperCase() == ("Curso técnico").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Ensino superior").toUpperCase() ||
                    $("#formacao").val().toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $("#label-curso").show();
                $("#label-prev").hide();
                $("#label-semestre").show();
                $("#label-turno").hide();
                $("#anofim").val("");
                $("#turno").val("");
            } else {
                $("#label-curso").hide();
                $("#label-prev").hide();
                $("#label-semestre").hide();
                $("#label-turno").hide();
                $("#nomeformacao").val("");
                $("#anofim").val("");
                $("#turno").val("");
                $("#semestre").val("");
            }
        }
    });

    $("#situacao2").change(function () {
        if ($("#situacao2").val().toUpperCase() == ("Trancado").toUpperCase() ||
                $("#situacao2").val().toUpperCase() == ("Não finalizado").toUpperCase()) {
            $("#label-fimcurso").hide();
            $("#iniciocurso").val("");
            $("#fimcurso").val("");
        } else {
            $("#label-fimcurso").show();
        }
    });

    $('#cemail').bind('paste', function (event) {
        event.preventDefault();
    });
});


/* ------------------ AQUI --------------------- */
var cargosprof = new Array();
var experiencias = new Array();
var formacoes = new Array();
var idiomas = new Array();
var cursos = new Array();

function enviarEmailCandidato(email, nome, senha) {

    const url = caminho + "/candidatos/mail_ativacao_cadastro.php?email=" + email + "&nome=" + nome + "&senha=" + senha;

    $.get(url, function () {
        showDialogActivate();
    });
}

function recebeDadosCandidato() {
    var email = document.getElementById("email").value;
    var senha = document.getElementById("senha").value;
    var nome = document.getElementById("nome").value;
    var cpf = document.getElementById("cpf").value;
    var data = formataData(document.getElementById("datanasc").value);
    var sexo = document.getElementsByName("sexo")[0].checked ? document.getElementsByName("sexo")[0].value : document.getElementsByName("sexo")[1].value;
    var fone = document.getElementById("ddd").value + " " + document.getElementById("fone").value;
    var estado = document.getElementById("estado").value;
    var cidade = document.getElementById("cidade").value;

    $.ajax({
        type: "POST",
        url: caminho + "/candidatos/actions/recebe_alteracadastracandidato.php",
        dataType: 'html',
        data: {
            email: email,
            senha: senha,
            nome: nome,
            cpf: cpf,
            data: data,
            sexo: sexo,
            fone: fone,
            estado: estado,
            cidade: cidade,
            cargosprof: cargosprof,
            experiencias: experiencias,
            formacoes: formacoes,
            idiomas: idiomas,
            cursos: cursos
        },
        success: function () {
            enviarEmailCandidato(email, nome, senha);
        }
    });

}

function montarCargosGrid() {
    $('#cargosgrid').html("");
    if (cargosprof.length) {
        for (var i = 0; i < cargosprof.length; i++) {
            $('#cargosgrid').append(
                    '<li>' +
                    '<span>' + cargosprof[i] + '</span>' +
                    '<button type="button" onclick="removeCargo(' + i + ')">' +
                    '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                    '</button>' +
                    '</li>'
                    );
        }
    } else {
        $('#cargosgrid').append('<li>Nenhum cargo profissional cadastrado.</li>');
    }
}

function montarExpGrid() {
    $('#expgrid').html("");
    if (experiencias.length) {
        for (var i = 0; i < experiencias.length; i++) {
            $('#expgrid').append(
                    '<li>' +
                    '<span>' + experiencias[i].nome_empresa + '</span>' +
                    '<span>' + experiencias[i].cargo_empresa + '</span>' +
                    '<button type="button" onclick="removeExp(' + i + ')">' +
                    '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                    '</button>' +
                    '</li>'
                    );
        }
    } else {
        $('#expgrid').append('<li>Nenhuma experiência profissional cadastrada.</li>');
    }
}

function montarFormacoesGrid() {
    $('#formacoesgrid').html("");
    if (formacoes.length) {
        for (var i = 0; i < formacoes.length; i++) {
            if (formacoes[i].nivel.toUpperCase() == ("Curso técnico").toUpperCase() ||
                    formacoes[i].nivel.toUpperCase() == ("Curso superior").toUpperCase() ||
                    formacoes[i].nivel.toUpperCase() == ("Pós-graduação").toUpperCase()) {

                $('#formacoesgrid').append(
                        '<li>' +
                        '<span>' + formacoes[i].nivel + '</span>' +
                        '<span>' + formacoes[i].curso + '</span>' +
                        '<button type="button" onclick="removeFormacao(' + i + ')">' +
                        '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                        '</button>' +
                        '</li>'
                        );
            } else {
                $('#formacoesgrid').append(
                        '<li>' +
                        '<span>' + formacoes[i].nivel + '</span>' +
                        '<span>' + formacoes[i].situacao + '</span>' +
                        '<button type="button" onclick="removeFormacao(' + i + ')">' +
                        '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                        '</button>' +
                        '</li>'
                        );
            }
        }
    } else {
        $('#formacoesgrid').append('<li>Nenhuma formação cadastrada.</li>');
    }
}

function montarCursosGrid() {
    $('#cursosgrid').html("");
    if (cursos.length) {
        for (var i = 0; i < cursos.length; i++) {
            $('#cursosgrid').append(
                    '<li>' +
                    '<span>' + cursos[i].curso + '</span>' +
                    '<span>' + cursos[i].situacao + '</span>' +
                    '<button type="button" onclick="removeCurso(' + i + ')">' +
                    '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                    '</button>' +
                    '</li>'
                    );
        }
    } else {
        $('#cursosgrid').append('<li>Nenhum curso cadastrado.</li>');
    }
}

function montarIdiomasGrid2() {
    $('#idiomasgrid').html("");
    if (idiomas.length) {
        for (var i = 0; i < idiomas.length; i++) {
            $('#idiomasgrid').append(
                    '<li>' +
                    '<span>' + idiomas[i].idioma + '</span>' +
                    '<span>' + idiomas[i].nivel + '</span>' +
                    '<button type="button" onclick="removeIdiomaCurriculo(' + i + ')">' +
                    '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                    '</button>' +
                    '</li>'
                    );
        }
    } else {
        $('#idiomasgrid').append('<li>Nenhum idioma cadastrado.</li>');
    }
}

function removeCargo(i) {
    var aux = new Array();
    for (var j = 0; j < cargosprof.length; j++) {
        if (i != j) {
            aux.push(cargosprof[j]);
        }
    }
    cargosprof = aux;
    montarCargosGrid();
    $("#objprof").show();
}

function removeExp(i) {
    var aux = new Array();
    for (var j = 0; j < experiencias.length; j++) {
        if (i != j) {
            aux.push({
                nome_empresa: experiencias[j].nome_empresa,
                cargo_empresa: experiencias[j].cargo_empresa,
                atividades: experiencias[j].atividades,
                inicio: experiencias[j].inicio,
                fim: experiencias[j].fim,
                salario: experiencias[j].salario
            });
        }
    }
    experiencias = aux;
    montarExpGrid();
}

function removeFormacao(i) {
    var aux = new Array();
    for (var j = 0; j < formacoes.length; j++) {
        if (i != j) {
            aux.push({
                nivel: formacoes[j].nivel,
                instituicao: formacoes[j].instituicao,
                curso: formacoes[j].curso,
                semestre: formacoes[j].semestre,
                situacao: formacoes[j].situacao,
                fim: formacoes[j].fim,
                turno: formacoes[j].turno
            });
        }
    }
    formacoes = aux;
    montarFormacoesGrid();
}

function removeCurso(i) {
    var aux = new Array();
    for (var j = 0; j < cursos.length; j++) {
        if (i != j) {
            aux.push({
                curso: cursos[j].curso,
                situacao: cursos[j].situacao,
                instituicao: cursos[j].instituicao,
                inicio: cursos[j].inicio,
                fim: cursos[j].fim
            });
        }
    }
    cursos = aux;
    montarCursosGrid();
}

function removeIdiomaCurriculo(i) {
    var aux = new Array();
    for (var j = 0; j < idiomas.length; j++) {
        if (i != j) {
            aux.push({
                idioma: idiomas[j].idioma,
                nivel: idiomas[j].nivel
            });
        }
    }
    idiomas = aux;
    montarIdiomasGrid2();
}

