$(document).ready(function () {

    $("#enome").blur(function () {
        if (document.getElementById("enome").value != "") {
            $('#enome').css("border-color", "#ccc");
        }
    });

    $("#ecnpj").blur(function () {

        var cnpj = document.getElementById("ecnpj").value;

        if (cnpj != "") {

            $('#ecnpj').css("border-color", "#ccc");

            $.ajax({
                type: "POST",
                url: caminho + "/includes/validacao.php",
                dataType: 'html',
                data: {cnpj: cnpj},
                success: function (result) {
                    verificarCNPJ(result);
                }
            });
        }
    });

    $("#eemail").blur(function () {

        var email = document.getElementById("eemail").value;
        var emailFilter = /^.+@.+\..{2,}$/;
        var illegalChars = /[\(\)\<\>\,\;\:\\\/\"\[\]]/;

        if (email != "") {
            if (!(emailFilter.test(email)) || email.match(illegalChars)) {
                $("#eemail").css("border-color", "red");
                $("#msg-eemail").text('Informe um email válido!');
                $("#msg-eemail").show();
            } else {

                $("#eemail").css("border-color", "#ccc");
                $("#msg-eemail").hide();

                $.ajax({
                    type: "POST",
                    url: caminho + "/includes/validacao.php",
                    dataType: 'html',
                    data: {email: email},
                    success: function (result) {
                        verificarEmail(result);
                    }
                });
            }
        }
    });

    $(".verifica_email_cadastrado").on("blur", function () {

        var email = $(this).val();

        if (email !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/buscar_email.php",
                dataType: "html",
                data: {
                    email: email,
                    tipo: 'empresa'
                },
                success: function (result) {
                    if (result == true) {
                        $(".verifica_email_cadastrado").val("");
                        $(".verifica_email_cadastrado").focus();
                        $dispararAlerta("Já existe um e-mail igual a este cadastrado", 'warning');
                    }
                }
            });
        }
    });

    $(".verifica_cnpj_cadastrado").on("blur", function () {

        var numdoc = $(this).val();

        var idobj = $(this).attr("id");
        var ob = "#" + idobj;

        if (numdoc !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/includes/buscar_doc.php",
                dataType: "html",
                data: {
                    doc: numdoc,
                    tipo: 'empresa'
                },
                success: function (result) {
                    if (result == true) {
                        $(ob).val("");
                        $(ob).focus();
                        $dispararAlerta("Este número de documento já está cadastrado", 'warning');
                    }
                }
            });
        }
    });

    $("#eramo").blur(function () {
        if (document.getElementById("eramo").value != "") {
            $('#eramo').css("border-color", "#ccc");
        }
    });

    $("#unome").blur(function () {
        if (document.getElementById("unome").value != "") {
            $('#unome').css("border-color", "#ccc");
        }
    });

    $("#esenha").blur(function () {
        if (document.getElementById("esenha").value != "") {
            if (document.getElementById("esenha").value.length < 6) {
                $('#esenha').css("border-color", "red");
                $('#msg-senha').show();
            } else {
                $('#msg-senha').hide();
                $('#esenha').css("border-color", "#ccc");
            }
        }
        if (document.getElementById("csenha").value != "") {
            if (document.getElementById("csenha").value != document.getElementById("esenha").value) {
                $('#csenha').css("border-color", "red");
                $('#msg-csenha').show();
            } else {
                $('#msg-csenha').hide();
                $('#csenha').css("border-color", "#ccc");
            }
        }
    });

    $("#csenha").blur(function () {
        if (document.getElementById("csenha").value != "") {
            if (document.getElementById("csenha").value != document.getElementById("esenha").value) {
                $('#csenha').css("border-color", "red");
                $('#msg-csenha').show();
            } else {
                $('#msg-csenha').hide();
                $('#csenha').css("border-color", "#ccc");
            }
        }
    });

    $("#etermos").blur(function () {
        if (document.getElementById('etermos').checked != false) {
            $('#labeltermos').css("border-color", "white");
        }
    });

    $("#emaile").blur(function () {
        var email = document.getElementById("emaile").value;
        var emailFilter = /^.+@.+\..{2,}$/;
        var illegalChars = /[\(\)\<\>\,\;\:\\\/\"\[\]]/;

        if (email != "") {
            if (!(emailFilter.test(email)) || email.match(illegalChars)) {
                $("#emaile").css("border-color", "red");
                $("#msg-emaile").text('Endereço de e-mail inválido');
                $("#msg-emaile").show();
                $('#epasso3').attr("target", "step2");

            } else {
                $('#emaile').css("border-color", "#ccc");
                $("#msg-emaile").hide();
            }
        }
    });

    $("#efone").blur(function () {
        if (document.getElementById("efone").value != "") {
            $('#efone').css("border-color", "#ccc");
        }
    });

    $(document).on("blur", "#ecep", function () {

        var cep = document.getElementById("ecep").value;

        $("#logradouro").val("");
        $("#ebairro").val("");
        $("#eestado").val("");
        $("#ecidade").val("");

        if (cep != "") {

            $('#ecep').css("border-color", "#ccc");
            $("#msg-ecep").hide();

            if (cep.length < 9) {

                $("#ecep").css("border-color", "red");
                $("#msg-ecep").show();

            } else {
                get('https://viacep.com.br/ws/' + cep + '/json/');
            }
        }
    });

    $("#logradouro").blur(function () {
        if (document.getElementById("logradouro").value != "") {
            $('#logradouro').css("border-color", "#ccc");
        }
    });

    $("#enumero").blur(function () {
        if (document.getElementById("enumero").value != "") {
            $('#enumero').css("border-color", "#ccc");
        }
    });

    $("#ebairro").blur(function () {
        if (document.getElementById("ebairro").value != "") {
            $('#ebairro').css("border-color", "#ccc");
        }
    });

    $("#ecidade").blur(function () {
        if (document.getElementById("ecidade").value != "") {
            $('#ecidade').css("border-color", "#ccc");
        }
    });

    $("#eestado").blur(function () {
        if (document.getElementById("eestado").value != "") {
            $('#eestado').css("border-color", "#ccc");
        }
    });

    $('#epasso2').on('click', function () {

        var key = true;
        $('#epasso2').attr("target", "step1");

        if (document.getElementById('enome').value == "") {
            $('#enome').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('ecnpj').value == "") {
            $('#ecnpj').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('eramo').value == "") {
            $('#eramo').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('unome').value == "") {
            $('#unome').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('eemail').value == "") {
            $('#eemail').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('esenha').value == "") {
            $('#esenha').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('csenha').value == "") {
            $('#csenha').css("border-color", "red");
            key = false;
        }

        if (document.getElementById('etermos').checked == false) {
            $('#labeltermos').css("border", "1px solid red");
            $('#labeltermos').css("border-radius", "5px");
            key = false;
        }

        if ($("#msg-ecnpj").css("display") == "none" &&
                $("#msg-eemail").css("display") == "none" &&
                $("#msg-senha").css("display") == "none" &&
                $("#msg-csenha").css("display") == "none" &&
                key) {
            $('#epasso2').attr("target", "step2");
        }

    });

    $('#epasso3').on('click', function () {

        var key = true;

        if (document.getElementById('emaile').value == "") {
            $('#emaile').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('efone').value == "") {
            $('#efone').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('ecep').value == "") {
            $('#ecep').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('logradouro').value == "") {
            $('#logradouro').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('enumero').value == "") {
            $('#enumero').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('ebairro').value == "") {
            $('#ebairro').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('eestado').value == "") {
            $('#eestado').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (document.getElementById('ecidade').value == "") {
            $('#ecidade').css("border-color", "red");
            $('#epasso3').attr("target", "step2");
            key = false;
        }

        if (key
                && $("#msg-emaile").css("display") == "none"
                && $("#msg-ecep").css("display") == "none") {
            recebeDadosEmpresa();
        }
    });
});

function buscarCidades2()
{
    if (document.getElementById("eestado").value != "") {
        $.ajax({
            type: "POST",
            url: caminho + "/includes/buscar_cidades.php",
            dataType: "html",
            data: {
                estado: document.getElementById("eestado").value
            },
            success: function (result) {
                var data = result.split(";");
                $("#ecidade").html("<option disabled selected value=''>Cidade</option>");
                for (var i = 0; i < data.length - 1; i++) {
                    $("#ecidade").append("<option>" + data[i] + "</option>");
                }
            }
        });
    }
}

function enviarEmail(usuario, empresa, email, cnpj) {
    $.ajax({
        type: "POST",
        url: caminho + "/empresas/mail_cadastro_empresa.php",
        dataType: "html",
        data: {
            empresa: empresa,
            cnpj: cnpj
        }
    });

    $.ajax({
        type: "POST",
        url: caminho + "/empresas/mail_boas_vindas.php",
        dataType: "html",
        data: {
            usuario: usuario,
            email: email,
            empresa: empresa
        }
    });
}

function recebeDadosEmpresa() {
    var nome_empresa = document.getElementById('enome').value;
    var cnpj = document.getElementById("ecnpj").value;
    var ramo = document.getElementById("eramo").value;
    var nome_usuario = document.getElementById("unome").value;
    var email = document.getElementById("eemail").value;
    var senha = document.getElementById("esenha").value;
    var descricao = document.getElementById('descricao').value;
    var qtd_funcionarios = document.getElementById("nfuncionarios").value;
    var site = document.getElementById("site").value;
    var email_empresa = document.getElementById("emaile").value;
    var fone = document.getElementById("efone").value;
    var fax = document.getElementById("fax").value;
    var cep = document.getElementById("ecep").value;
    var logradouro = document.getElementById("logradouro").value;
    var numero = document.getElementById("enumero").value;
    var complemento = document.getElementById("ecomplemento").value;
    var bairro = document.getElementById('ebairro').value;
    var cidade = document.getElementById("ecidade").value;
    var estado = document.getElementById("eestado").value;

    $.ajax({
        type: "POST",
        url: caminho + "/empresas/actions/recebe_cadastroempresa.php",
        dataType: 'html',
        data: {
            nome_empresa: nome_empresa,
            cnpj: cnpj,
            ramo: ramo,
            nome_usuario: nome_usuario,
            email: email,
            senha: senha,
            descricao: descricao,
            qtd_funcionarios: qtd_funcionarios,
            site: site,
            email_empresa: email_empresa,
            fone: fone,
            fax: fax,
            cep: cep,
            logradouro: logradouro,
            numero: numero,
            complemento: complemento,
            bairro: bairro,
            cidade: cidade,
            estado: estado
        },
        success: function () {
            enviarEmail(nome_usuario, nome_empresa, email, cnpj);
            $("#upload").click();
        }
    });
}

function verificarEmail(key) {
    if (!key) {
        $("#eemail").css("border-color", "red");
        $("#msg-eemail").text('Esse e-mail já está cadastrado!');
        $("#msg-eemail").show();
    } else {
        $("#eemail").css("border-color", "#ccc");
        $("#msg-eemail").hide();
    }
}

function get(url) {

    $.get(url, function (data) {

        if (!("erro" in data)) {

            if (Object.prototype.toString.call(data) === '[object Array]') {
                var data = data[0];

            }

            $("#logradouro").val(data.logradouro);
            $("#ebairro").val(data.bairro);
            $("#eestado").val(data.uf.toUpperCase());
            buscarCidades2();
            setTimeout(function () {
                $("#ecidade").val(data.localidade.toUpperCase());
            }, 500);
        }
    });
}