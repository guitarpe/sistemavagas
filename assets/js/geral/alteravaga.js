var beneficiosEdit = new Array();
var cnhEdit = new Array();
var deficienciasEdit = new Array();
var informaticaEdit = new Array();
var idiomasEdit = new Array();




function listarIdiomas() {
    $('#idiomaslist').html("");
    if (idiomasEdit.length) {
        for (var i = 0; i < idiomasEdit.length; i++) {
            $('#idiomaslist').append(
                    '<li>' +
                    '<span>' + idiomasEdit[i].idioma + '</span>' +
                    '<span>' + idiomasEdit[i].nivel + '</span>' +
                    '<button type="button" onclick="delIdioma(' + i + ')">' +
                    '<i class="fa fa-times-circle" aria-hidden="true"></i> Remover' +
                    '</button>' +
                    '</li>'
                    );
        }
    } else {
        $('#idiomaslist').append('<li>Nenhum Idioma foi cadastrado!</li>');
    }
}

function delIdioma(i) {
    var aux = new Array();
    for (var j = 0; j < idiomasEdit.length; j++) {
        if (i != j) {
            aux.push({
                idioma: idiomasEdit[j].idioma,
                nivel: idiomasEdit[j].nivel
            });
        }
    }
    idiomasEdit = aux;
    listarIdiomas();
}

function showDialogUpdateVaga()
{
    $('.modal').addClass('active');
    $('.modal .box').removeClass('active');
    $('.modal .box#msg-up-vaga').addClass('active');
}

$(function () {
    $("#cargo_profissional").change(function () {
        if ($("#cargo_profissional").val() == "Outro") {
            $("#outrocargo").removeClass("off");
            $("#lbcargo").removeClass("half").addClass("trd");
            $("#lbforma").removeClass("half").addClass("trd");
            $("#outro").attr("required", "true");
        } else {
            $("#outrocargo").addClass("off");
            $("#lbcargo").removeClass("trd").addClass("half");
            $("#lbforma").removeClass("trd").addClass("half");
            $("#outro").removeAttr("required");
        }
    });
});

$(function () {
    $("#faixa_salarial").change(function () {
        if ($("#faixa_salarial").val().toUpperCase() == ("Salário Fixo").toUpperCase()) {
            $("#label-salario").removeClass("off");
            $("#valor_salario").attr("required", "true");
        } else {
            $("#label-salario").addClass("off");
            $("#valor_salario").removeAttr("required");
        }
    });
});

$(function () {
    $("#simoutrolocal").on("click", function () {
        $("#outrolocalvaga").removeClass("off");
        $("#outroestado").attr("required", "true");
        $("#outracidade").attr("required", "true");
    });
});

$(function () {
    $("#naooutrolocal").on("click", function () {
        $("#outrolocalvaga").addClass("off");
        $("#outroestado").removeAttr("required");
        $("#outracidade").removeAttr("required");
    });
});

$(function () {
    $("#outroestado").change(function () {
        getCidades();
    });
});

$(function () {
    $("#idiomasim").on("click", function () {
        $(".idiomas").removeClass("off");
        $("#idioma_vaga").attr("required", "true");
        $("#nivelidioma_vaga").attr("required", "true");
    });
});

$(function () {
    $("#idiomanao").on("click", function () {
        $(".idiomas").addClass("off");
        $("#idioma_vaga").removeAttr("required");
        $("#nivelidioma_vaga").removeAttr("required");
    });
});

$(function () {
    $("#addidioma_vaga").on("click", function () {
        if ($("#idioma_vaga").val() != null && $("#nivelidioma_vaga").val() != null) {
            $("#addidioma_vaga").attr("type", "button");
            idiomasEdit.push({idioma: $("#idioma_vaga").val(), nivel: $("#nivelidioma_vaga").val()});
            $("#idioma_vaga").val("");
            $("#nivelidioma_vaga").val("");
            listarIdiomas();
        } else {
            $("#addidioma_vaga").removeAttr("type");
        }
    });
});

$(function () {
    $("#atualizarvaga").on("click", function () {

        var forma = document.getElementById("forma_contratacao").value;
        var cargo = document.getElementById("cargo_profissional").value;
        var nivel = document.getElementById("nivel_hierarquico").value;
        var nvagas = document.getElementById("numero_vagas").value;
        var turno = document.getElementById("turnos_vaga").value;
        var atividades = document.getElementById("atividades_vaga").value;
        var faixasalarial = document.getElementById("faixa_salarial").value;
        var sexo = document.getElementsByName("sexo_vaga")[0].checked ? document.getElementsByName("sexo_vaga")[0].value : document.getElementsByName("sexo_vaga")[1].checked ? document.getElementsByName("sexo_vaga")[1].value : document.getElementsByName("sexo_vaga")[2].checked ? document.getElementsByName("sexo_vaga")[2].value : null;
        var experienciamin = document.getElementById("experiencia_min").value;
        var ensinomin = document.getElementById("ensino_min").value;
        var estado = document.getElementById("outroestado").value;
        var cidade = document.getElementById("outracidade").value;
        var viajar = document.getElementsByName("disp_viajar")[0].checked ? "Não" : "Sim";
        var veiculo = document.getElementsByName("possuir_veiculo")[0].checked ? "Não" : "Sim";
        var visibilidade = document.getElementsByName("visidados")[0].checked ? "Não" : "Sim";

        if (cargo == "Outro") {
            var cargo = document.getElementById("outro").value;
        }

        if (faixasalarial.toUpperCase() == ("Salário Fixo").toUpperCase()) {
            var salario = document.getElementById("valor_salario").value;
        } else {
            var salario = null;
        }

        var checks = document.getElementsByName("beneficios_vaga");
        var checkDeficiencia = document.getElementsByName("vaga_def");
        var checkInformatica = document.getElementsByName("informatica_vaga");

        for (var i = 0; i < checks.length; i++) {
            if (checks[i].checked) {
                beneficiosEdit.push(checks[i].value);
            }
        }

        for (var i = 0; i < checkDeficiencia.length; i++) {
            if (checkDeficiencia[i].checked) {
                deficienciasEdit.push(checkDeficiencia[i].value);
            }
        }

        for (var i = 0; i < checkInformatica.length; i++) {
            if (checkInformatica[i].checked) {
                informaticaEdit.push(checkInformatica[i].value);
            }
        }

        if (document.getElementById("catA").checked) {
            cnhEdit.push("A");
        }
        if (document.getElementById("catB").checked) {
            cnhEdit.push("B");
        } else if (document.getElementById("catC").checked) {
            cnhEdit.push("C");
        } else if (document.getElementById("catD").checked) {
            cnhEdit.push("D");
        } else if (document.getElementById("catE").checked) {
            cnhEdit.push("E");
        }

        $.ajax({
            type: "POST",
            url: caminho + "/actions/recebe_alteravaga.php",
            dataType: "html",
            data: {
                forma: forma,
                cargo: cargo,
                nivel: nivel,
                nvagas: nvagas,
                turno: turno,
                atividades: atividades,
                faixasalarial: faixasalarial,
                salario: salario,
                beneficios: beneficiosEdit,
                sexo: sexo,
                experienciamin: experienciamin,
                ensinomin: ensinomin,
                idiomas: idiomasEdit,
                cidade: cidade,
                estado: estado,
                viajar: viajar,
                veiculo: veiculo,
                cnh: cnhEdit,
                deficiencias: deficienciasEdit,
                informatica: informaticaEdit,
                visibilidade: visibilidade
            }
        });

    });
});
