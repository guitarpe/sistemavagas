<?php
include "includes/conexao.php";

$tipo = filter_input(INPUT_GET, "tipo");
$id = filter_input(INPUT_GET, "id");

?>

<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="publicidade">
                    <div class="container">
                        <div class="col-sm-12">
                            <span>Publicidade</span>
                            <?php if (!empty($publi1['link'])) { ?>
                                <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </section>
                <section class="institucional">
                    <div class="box-body">
                        <form action="<?php echo PATH_ALL . '/actions/recebe_registrosenha.php' ?>" method="post" role="form">
                            <input type="hidden" name="tipo" value="<?php echo $tipo ?>"/>
                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <h3>Realize o registro de uma nova senha</h3>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-asterisk"></i>
                                        </span>
                                        <input type="password" placeholder="Nova Senha" name="senha" class="form-control txtSenha" required autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-asterisk"></i>
                                        </span>
                                        <input type="password" placeholder="Confirmar Senha" name="confisenha" class="form-control confisenha nopaste" required autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4 col-md-offset-4 text-center">
                                    <button class="btn btn-success" type="submit">
                                        Registrar Nova Senha
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <section class="publicidade">
                    <div class="container">
                        <div class="col-sm-12">
                            <span>Publicidade</span>
                            <?php if (!empty($publi2['link'])) { ?>
                                <a href="<?php echo $publi12['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>

