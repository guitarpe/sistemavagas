<?php
session_start();
include "../../includes/conexao.php";

$func = new Funcoes();

$diretorio = "../images/";
$nomeimagem = $_FILES['imagem']['name'];
$tmp = $_FILES['imagem']['tmp_name'];
$ext = substr($nomeimagem, -4, 4);
$newnome = strtoupper(date("Ymdhis") . md5($nomeimagem));
$nomefinalfoto = $newnome . $ext;
$upload = $diretorio . $newnome . $ext;

$senha = filter_input(INPUT_POST, 'senha');
$email = filter_input(INPUT_POST, 'email');

$nomeempresa = filter_input(INPUT_POST, 'nomeempresa');
$cnpj = filter_input(INPUT_POST, 'cnpj');
$ramoatividade = filter_input(INPUT_POST, 'ramoatividade');
$nomeusuario = filter_input(INPUT_POST, 'nomeusuario');
$descricaoempresa = filter_input(INPUT_POST, 'descricaoempresa');
$nfuncionarios = filter_input(INPUT_POST, 'nfuncionarios');
$siteempresa = filter_input(INPUT_POST, 'siteempresa');
$emailempresa = filter_input(INPUT_POST, 'emailempresa');
$foneempresa = filter_input(INPUT_POST, 'foneempresa');
$celular = filter_input(INPUT_POST, 'celular');
$cepempresa = filter_input(INPUT_POST, 'cepempresa');
$logradouroempresa = filter_input(INPUT_POST, 'logradouroempresa');
$numero = filter_input(INPUT_POST, 'numero');
$complemento = filter_input(INPUT_POST, 'complemento');
$bairro = filter_input(INPUT_POST, 'bairro');
$cidade = filter_input(INPUT_POST, 'cidade');
$estado = filter_input(INPUT_POST, 'estado');
$id = filter_input(INPUT_GET, 'id');

$recebe_curriculo = filter_input(INPUT_POST, 'recebe_curriculo');

$senhausuario = $func->dec_enc(1, 'empresa' . '|' . $senha . '|' . $email);

if ($tmp != NULL) {

    if (move_uploaded_file($tmp, $upload)) {

        $query_emp = "UPDATE TB_VV_EMPRESAS SET "
            . "nome_empresa = '$nomeempresa', "
            . "cnpj = '$cnpj', "
            . "ramo_atividade = '$ramoatividade', "
            . "nome_usuario = '$nomeusuario', "
            . "email = '$email', "
            . "senha = '$senhausuario', "
            . "imagem = '$nomefinalfoto', "
            . "descricao = '$descricaoempresa', "
            . "qtd_funcionarios = '$nfuncionarios', "
            . "site = '$siteempresa', "
            . "email_empresa = '$emailempresa', "
            . "fone = '$foneempresa', "
            . "fax = '$celular', "
            . "cep = '$cepempresa', "
            . "logradouro = '$logradouroempresa', "
            . "numero = '$numero', "
            . "complemento = '$complemento', "
            . "bairro = '$bairro', "
            . "cidade = '$cidade', "
            . "estado = '$estado', "
            . "candidaturas_curriculos = '$recebe_curriculo' "
            . "WHERE id = '$id'";
    }
} else {
    $query_emp = "UPDATE TB_VV_EMPRESAS SET "
        . "nome_empresa = '$nomeempresa', "
        . "cnpj = '$cnpj', "
        . "ramo_atividade = '$ramoatividade', "
        . "nome_usuario = '$nomeusuario', "
        . "email = '$email', "
        . "senha = '$senhausuario', "
        . "descricao = '$descricaoempresa', "
        . "qtd_funcionarios = '$nfuncionarios', "
        . "site = '$siteempresa', "
        . "email_empresa = '$emailempresa', "
        . "fone = '$foneempresa', "
        . "fax = '$celular', "
        . "cep = '$cepempresa', "
        . "logradouro = '$logradouroempresa', "
        . "numero = '$numero', "
        . "complemento = '$complemento', "
        . "bairro = '$bairro', "
        . "cidade = '$cidade', "
        . "estado = '$estado', "
        . "candidaturas_curriculos = '$recebe_curriculo' "
        . "WHERE id = '$id'";
}

$sql_empresa = mysqli_query($con, $query_emp) or die(mysqli_error($con));

$query_us = "UPDATE TB_VV_USUARIOS SET nome = '$nomeusuario', email = '$email', senha = '$senhausuario' WHERE id = '$id' AND tipo = 'empresa'";

$sql_us = mysqli_query($con, $query_us) or die(mysqli_error($con));

$situacao = "msg-atualizacao";
$func->alert($situacao, 'acao');
$func->redir('empresas/dados-empresa.php');
