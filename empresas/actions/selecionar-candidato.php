<?php
session_start();
include "../../includes/conexao.php";

$func = new Funcoes();

$cand = filter_input(INPUT_POST, 'usuario');
$vag = filter_input(INPUT_POST, 'vaga');

$res_slct = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATURAS WHERE id_candidato=$cand AND id_vaga=$vag")or die(mysqli_error($con));

if (mysqli_num_rows($res_slct) == 0) {
    $data = date('Y-m-d');
    $sql = "INSERT INTO TB_VV_CANDIDATURAS(id_candidato, id_vaga, data, status) VALUES ($cand, $vag, '$data', 0)";
    $res = mysqli_query($con, $sql)or die(mysqli_error($con));
}

$res_slct_vaga = mysqli_query($con, "SELECT cargo, numero_vagas, cidade, estado FROM TB_VV_VAGAS WHERE id = $vag");
$vagas = mysqli_fetch_array($res_slct_vaga);

if (intval($vagas['numero_vagas']) > 1) {
    $titulo = strtoupper($vagas['cargo'] . " (" . $vagas['numero_vagas'] . " VAGAS) - " . $vagas['cidade'] . " " . $vagas['estado']);
} else {
    $titulo = strtoupper($vagas['cargo'] . " (" . $vagas['numero_vagas'] . " VAGA) - " . $vagas['cidade'] . " " . $vagas['estado']);
}

$msg = "Temos a alegria de dizer que o seu currículo despertou interesse em uma empresa e está sendo analisado para a vaga $titulo.
	Agora depende apenas da análise da empresa para ser chamado para entrevista ou não.
	De qualquer forma desejamos todo sucesso do mundo para você e que dê tudo certo.
	Equipe Vagas&Vagas.";

date_default_timezone_set('America/Sao_Paulo');
$data = date("d/m/Y");
$hora = date('H:i');

$res = mysqli_query($con, "INSERT INTO TB_VV_MENSAGENS (id_candidato, titulo, mensagem, nome, email, data, hora, status) VALUES ($cand, 'Candidatura', '$msg', 'Vagas&Vagas', 'vagasevagas.com.br', '$data', '$hora', 0)");

$res_candidato = mysqli_query($con, "SELECT nome, email FROM TB_VV_CANDIDATOS WHERE id = " . $cand);
$candidato = mysqli_fetch_array($res_candidato);

$res_usu = mysqli_query($con, "SELECT id, nome, email, recebe_email FROM TB_VV_USUARIOS WHERE id=$cand AND tipo='candidato'");
$usuario = mysqli_fetch_array($res_usu);

$res_vagas = mysqli_query($con, "SELECT cargo, numero_vagas, cidade, estado FROM TB_VV_VAGAS WHERE id = " . $vag);
$vaga = mysqli_fetch_array($res_vagas);

if ($vaga['numero_vagas'] > 1) {
    $titulo = strtoupper($vaga['cargo'] . " (" . $vaga['numero_vagas'] . " vagas) - " . $vaga['cidade'] . " " . $vaga['estado']);
} else if ($vaga['numero_vagas'] == 1) {
    $titulo = strtoupper($vaga['cargo'] . " (" . $vaga['numero_vagas'] . " vaga) - " . $vaga['cidade'] . " " . $vaga['estado']);
}

$to = $candidato['email'];
$to_txt = $candidato['nome'];

$subject = "Uma empresa se interessou no seu curriculo no Vagas e Vagas";

$message = '
	<html>
	<head>
	</head>
	<body>
		<div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
			<img src="' . HOME . '/images/top.png">
			<div class="container" style="padding:20px">
				<table>
					<tr>
						<td>
							<h2><b></b></h2>
							<br/>Olá ' . $candidato['nome'] . '
							<br/>
							<br/>
							Temos a alegria de dizer que o seu currículo despertou interesse em uma empresa e está sendo analisado para a vaga ' . $titulo . '.
							<br/>
							<br/>
							Agora depende apenas da análise da empresa para ser chamado para entrevista ou não.
							<br/>
							<br/>
							De qualquer forma desejamos todo sucesso do mundo para você e que dê tudo certo.
							<br/>
							<br/>
							Equipe Vagas e Vagas.
						</td>
					</tr>
				</table>
				<div class="row" align="right">
					<a href="' . FACEBOOK . '" style="text-decoration:none">
						<img src="' . HOME . '/images/facebook.png">
					</a>
					<a href="' . TWITTER . '" style="text-decoration:none">
						<img src="' . HOME . '/images/twitter.png">
					</a>
					<a href="' . PLUS . '" style="text-decoration:none">
						<img src="' . HOME . '/images/googlep.png">
					</a>
				</div>
			</div>
			<img src="' . HOME . '/images/top.png">
		<div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $usuario['id'] . '&tipo=candidato" target="_blank">Não quero receber mais mensagens</a></div>
            </div>
	</body>
	</html>';

if (intval($usuario['recebe_email']) == 1) {
    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $to_txt,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    $situacao = 'msg-selecionar-candidato';
    $func->alert($situacao, 'acao');
    include_once '../../includes/enviar-email.php';
}
