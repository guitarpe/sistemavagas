<?php
session_start();

include "../../includes/conexao.php";

$func = new Funcoes();

$cnh = filter_input(INPUT_POST, "cnh");
$id_vaga = filter_input(INPUT_GET, "id");
$sexo = filter_input(INPUT_POST, "sexo");
$cargo = filter_input(INPUT_POST, "cargo");
$turno = filter_input(INPUT_POST, "turno");
$viajar = filter_input(INPUT_POST, "viajar");
$nvagas = filter_input(INPUT_POST, "nvagas");
$idiomas = filter_input(INPUT_POST, "idiomas");
$veiculo = filter_input(INPUT_POST, "veiculo");
$tipocnh = filter_input(INPUT_POST, "tipocnh");
$ensinomin = filter_input(INPUT_POST, "ensinomin");
$cargooutro = filter_input(INPUT_POST, "cargooutro");
$estadovaga = filter_input(INPUT_POST, "estadovaga");
$cidadevaga = filter_input(INPUT_POST, "cidadevaga");
$atividades = filter_input(INPUT_POST, "atividades");
$forma = filter_input(INPUT_POST, "formacontratacao");
$nivel = filter_input(INPUT_POST, "nivelhierarquico");
$deficiencia = filter_input(INPUT_POST, "deficiencia");
$informatica = filter_input(INPUT_POST, "informatica");
$visibilidade = filter_input(INPUT_POST, "visibilidade");
$faixasalarial = filter_input(INPUT_POST, "faixasalarial");
$experienciamin = filter_input(INPUT_POST, "experienciamin");
$outroestadocidade = filter_input(INPUT_POST, "outroestadocidade");
$beneficios = filter_input(INPUT_POST, "beneficios", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$deficiencias = filter_input(INPUT_POST, "deficiencias", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$informaticas = filter_input(INPUT_POST, "informaticas", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$idiomavaga = filter_input(INPUT_POST, "idiomavaga", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$nivelidiomavaga = filter_input(INPUT_POST, "nivelidiomavaga", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

$vsalario = empty(filter_input(INPUT_POST, "vsalario")) ? 0 : filter_input(INPUT_POST, "vsalario");
$data = date("Y-m-d");
$id_empresa = $_SESSION["id"];

//vaga para outro estado
if (intval($outroestadocidade) === 1) {
    $cidade = $cidadevaga;
    $estado = $estadovaga;
} else if (intval($outroestadocidade) === 0) {
    $res_local = mysqli_query($con, "SELECT cidade, estado  FROM TB_VV_EMPRESAS WHERE id=" . $id_empresa) or die(mysqli_error($con));
    $local = mysqli_fetch_array($res_local);
    $cidade = $local["cidade"];
    $estado = $local["estado"];
}

//faixasalarial
if ($faixasalarial == "Salário Fixo") {
    $salario = $func->returnValor($vsalario);
} else {
    $salario = null;
}

//cargo
if ($cargo == "Outro") {
    $cargovaga = $cargooutro;
} else {
    $cargovaga = $cargo;
}


$sql = "UPDATE TB_VV_VAGAS SET
        forma_contratacao = '$forma',
        cargo = '$cargo',
        numero_vagas = '$nvagas',
        nivel_hierarquico = '$nivel',
        turno = '$turno',
        atividades = '$atividades',
        faixa_salarial = '$faixasalarial',
        salario = '$salario',
        sexo = '$sexo',
        experiencia_minima = '$experienciamin',
        ensino_minimo = '$ensinomin',
        vaga_outrolocal='$outroestadocidade',
        cidade = '$cidade',
        estado = '$estado',
        viajar = '$viajar',
        veiculo_proprio = '$veiculo',
        visibilidade_dados = '$visibilidade'
        WHERE id_empresa = $id_empresa AND id=$id_vaga";

$res = mysqli_query($con, $sql);

if (!empty($beneficios[0])) {

    $res_delete = mysqli_query($con, "DELETE FROM TB_VV_BENEFICIOS_VAGA WHERE id_vaga=$id_vaga") or die(mysqli_error($con));

    foreach ($beneficios as $value) {
        mysqli_query($con, "INSERT INTO TB_VV_BENEFICIOS_VAGA (id_vaga, descricao) VALUES ($id_vaga, '$value')") or die(mysqli_error($con));
    }
}

if ($idiomas == "Sim") {

    if (!empty($idiomavaga[0])) {
        $ct = 0;
        foreach ($idiomavaga as $value) {

            mysqli_query($con, "INSERT INTO TB_VV_IDIOMAS_VAGA (id_vaga, idioma, nivel) VALUES ($id_vaga, '$value', '$nivelidiomavaga[$ct]')") or die(mysqli_error($con));

            $ct++;
        }
    }
}

if ($cnh == "Sim") {
    $res_delete = mysqli_query($con, "DELETE FROM TB_VV_CNH WHERE id_vaga=$id_vaga") or die(mysqli_error($con));

    mysqli_query($con, "INSERT INTO TB_VV_CNH (id_vaga, categoria) VALUES ($id_vaga, '$tipocnh') ON DUPLICATE KEY UPDATE id_vaga=$id_vaga, categoria='$tipocnh'") or die(mysqli_error($con));
}

if ($deficiencia == "Sim") {
    if (!empty($deficiencias[0])) {
        foreach ($deficiencias as $value) {
            mysqli_query($con, "INSERT INTO TB_VV_DEFICIENCIAS_VAGA (id_vaga, deficiencia) VALUES ($id_vaga, '$value') ON DUPLICATE KEY UPDATE id_vaga=$id_vaga, deficiencia='$value'") or die(mysqli_error($con));
        }
    }
}

if ($informatica == "Sim") {
    if (!empty($informaticas[0])) {
        foreach ($informaticas as $value) {
            mysqli_query($con, "INSERT INTO TB_VV_INFORMATICA_VAGA (id_vaga, nome) VALUES ($id_vaga, '$value') ON DUPLICATE KEY UPDATE id_vaga=$id_vaga, nome='$value'") or die(mysqli_error($con));
        }
    }
}

$situacao = 'msg-up-vaga';
$func->alert($situacao, 'acao');
$func->redir('empresas/gerenciar-vagas.php');
