<?php
session_start();

include "../../includes/conexao.php";

$func = new Funcoes();

$cnh = filter_input(INPUT_POST, "cnh");
$sexo = filter_input(INPUT_POST, "sexo");
$turno = filter_input(INPUT_POST, "turno");
$cargo = filter_input(INPUT_POST, "cargo");
$viajar = filter_input(INPUT_POST, "viajar");
$nvagas = filter_input(INPUT_POST, "nvagas");
$idiomas = filter_input(INPUT_POST, "idiomas");
$veiculo = filter_input(INPUT_POST, "veiculo");
$tipocnh = filter_input(INPUT_POST, "tipocnh");
$ensinomin = filter_input(INPUT_POST, "ensinomin");
$validiomas = filter_input(INPUT_POST, "validiomas");
$cargooutro = filter_input(INPUT_POST, "cargooutro");
$atividades = filter_input(INPUT_POST, "atividades");
$estadovaga = filter_input(INPUT_POST, "estadovaga");
$cidadevaga = filter_input(INPUT_POST, "cidadevaga");
$forma = filter_input(INPUT_POST, "formacontratacao");
$nivel = filter_input(INPUT_POST, "nivelhierarquico");
$deficiencia = filter_input(INPUT_POST, "deficiencia");
$informatica = filter_input(INPUT_POST, "informatica");
$visibilidade = filter_input(INPUT_POST, "visibilidade");
$faixasalarial = filter_input(INPUT_POST, "faixasalarial");
$experienciamin = filter_input(INPUT_POST, "experienciamin");
$outroestadocidade = filter_input(INPUT_POST, "outroestadocidade");
$beneficios = filter_input(INPUT_POST, "beneficios", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$deficiencias = filter_input(INPUT_POST, "deficiencias", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$informaticas = filter_input(INPUT_POST, "informaticas", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$idiomavaga = filter_input(INPUT_POST, "idiomavaga", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$nivelidiomavaga = filter_input(INPUT_POST, "nivelidiomavaga", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

$vsalario = empty(filter_input(INPUT_POST, "vsalario")) ? 0 : filter_input(INPUT_POST, "vsalario");

$data = date("Y-m-d");
$expira = date('Y-m-d', strtotime($data . ' + 90 days'));
$id_empresa = $_SESSION["id"];

//vaga para outro estado
if (intval($outroestadocidade) === 1) {
    $cidade = $cidadevaga;
    $estado = $estadovaga;
} else if (intval($outroestadocidade) === 0) {
    $res_local = mysqli_query($con, "SELECT cidade, estado  FROM TB_VV_EMPRESAS WHERE id=" . $id_empresa) or die(mysqli_error($con));
    $local = mysqli_fetch_array($res_local);
    $cidade = $local["cidade"];
    $estado = $local["estado"];
}

//faixasalarial
if ($faixasalarial == "Salário Fixo") {
    $salario = $func->returnValor($vsalario);
} else {
    $salario = null;
}

//cargo
if ($cargo == "Outro") {
    $cargovaga = $cargooutro;
} else {
    $cargovaga = $cargo;
}

$sql_vaga = "INSERT INTO TB_VV_VAGAS "
    . "(id_empresa, forma_contratacao, cargo, numero_vagas, nivel_hierarquico, turno, atividades, faixa_salarial, salario, sexo, "
    . "experiencia_minima, ensino_minimo, vaga_outrolocal, cidade, estado, viajar, veiculo_proprio, visibilidade_dados, data_anuncio, data_expira, status, numero_candidatos) "
    . "VALUES "
    . "($id_empresa, '$forma', '$cargovaga', '$nvagas', '$nivel', '$turno', '$atividades', '$faixasalarial', '$salario', '$sexo', "
    . "'$experienciamin', '$ensinomin', '$outroestadocidade', '$cidade', '$estado', '$viajar', '$veiculo', '$visibilidade', '$data', '$expira', 1, 0)";

$res_vagaa = mysqli_query($con, $sql_vaga) or die(mysqli_error($con));

$id_vaga = $con->insert_id;

if ($id_vaga != null && $id_vaga > 0) {

    if (!empty($beneficios[0])) {
        foreach ($beneficios as $value) {
            mysqli_query($con, "INSERT INTO TB_VV_BENEFICIOS_VAGA (id_vaga, descricao) VALUES ($id_vaga, '$value')") or die(mysqli_error($con));
        }
    }

    if ($idiomas == "Sim") {
        if (!empty($idiomavaga[0])) {
            $ct = 0;
            foreach ($idiomavaga as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_IDIOMAS_VAGA (id_vaga, idioma, nivel) VALUES ($id_vaga, '$value', '$nivelidiomavaga[$ct]')") or die(mysqli_error($con));

                $ct++;
            }
        }
    }

    if ($cnh == "Sim") {
        mysqli_query($con, "INSERT INTO TB_VV_CNH (id_vaga, categoria) VALUES ($id_vaga, '$tipocnh')") or die(mysqli_error($con));
    }

    if ($deficiencia == "Sim") {
        if (!empty($deficiencias[0])) {
            foreach ($deficiencias as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_DEFICIENCIAS_VAGA (id_vaga, deficiencia) VALUES ($id_vaga, '$value')") or die(mysqli_error($con));
            }
        }
    }

    if ($informatica == "Sim") {
        if (!empty($informaticas[0])) {
            foreach ($informaticas as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_INFORMATICA_VAGA (id_vaga, nome) VALUES ($id_vaga, '$value')") or die(mysqli_error($con));
            }
        }
    }
}

$situacao = 'msg-add-vaga-succ';
$func->alert($situacao, 'acao');
$func->redir('empresas/index.php');
