<?php
session_start();
include "../../includes/conexao.php";
$func = new Funcoes();

$status = filter_input(INPUT_POST, 'status');
$id_candidato = filter_input(INPUT_POST, 'usuario');
$vaga = filter_input(INPUT_POST, 'vaga');

$sql = "UPDATE TB_VV_CANDIDATURAS SET status=" . $status . " WHERE id_candidato=" . $id_candidato . " AND id_vaga=" . $vaga;

$res = mysqli_query($con, $sql) or die(mysqli_error($con));

$situacao = intval($status) == 1 ? 'msg-contratar-candidato' : 'msg-descontratar-candidato';
$func->alert($situacao, 'acao');
$func->redir('empresas/gerenciar-candidatos.php?id=' . $vaga);

