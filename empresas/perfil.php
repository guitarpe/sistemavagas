<?php
session_start();
include"../includes/conexao.php";

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}
if ($_SESSION['tipo'] != 'empresa' || !isset($_GET['id'])) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATOS WHERE id = $_GET[id]");
    $dados = mysqli_fetch_array($res);

    $res = mysqli_query($con, "SELECT data FROM TB_VV_CANDIDATURAS WHERE id_candidato = $_GET[id]");
    $isCandidato = mysqli_num_rows($res);

    $data_nasc = explode('-', $dados['nascimento']);
    $data = date('d/m/Y');
    $data = explode('/', $data);
    $idade = $data[2] - $data_nasc[0];

    if ($data_nasc[1] > $data[1]) {
        $idade--;
    } else {
        if ($data_nasc[1] == $data[1] && $data_nasc[2] > $data[0]) {
            $idade--;
        }
    }

    ?>

    <!DOCTYPE html>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="vagas">
                                <ul>
                                    <li>
                                        <div class="ttl">
                                            <figure>
                                                <div class="fotoperfil">
                                                    <?php if ($dados['foto']) { ?>
                                                        <img src="<?php echo PATH_CAN_IMAGENS . '/' . $dados['foto'] ?>">;
                                                        <?php
                                                    } else {

                                                        ?>
                                                        <img src="<?php echo PATH_IMAGENS . '/usuario.png' ?>">;
                                                        <?php
                                                    }

                                                    ?>
                                                </div>
                                            </figure>

                                            <div class="rt">
                                                <h6><?php echo $dados['nome'] ?></h6>
                                                <div class="item">
                                                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                    <?php
                                                    $key = 1;
                                                    $res_cargos = mysqli_query($con, "SELECT cargo FROM TB_VV_CARGOS_CAND WHERE id_candidato = $_GET[id]");
                                                    while ($cargos = mysqli_fetch_array($res_cargos)) {
                                                        if ($key < mysqli_num_rows($res_cargos)) {
                                                            echo $cargos['cargo'] . " / ";
                                                        } else {
                                                            echo $cargos['cargo'];
                                                        }
                                                        $key++;
                                                    }

                                                    ?>
                                                </div>
                                                <div class="item">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <?php echo $dados['cidade'] . "/" . $dados['estado'] ?>
                                                </div>
                                                <div class="item">
                                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                                    <?php echo $idade . " anos" ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="info">
                                            <div class="caracteristicas">
                                                <div class="col">
                                                    <strong>
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        E-mail: <?php echo $dados['email'] ?>
                                                    </strong>
                                                    <?php if ($dados['sexo'] == "Masculino") { ?>
                                                        <strong>
                                                            <i class="fa fa-male" aria-hidden="true"></i>
                                                            Sexo: <?php echo $dados['sexo'] ?>
                                                        </strong>
                                                    <?php } else { ?>
                                                        <strong>
                                                            <i class="fa fa-female" aria-hidden="true"></i>
                                                            Sexo: <?php echo $dados['sexo'] ?>
                                                        </strong>
                                                    <?php } ?>
                                                    <strong>
                                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                                        Telefone:
                                                        <?php
                                                        if ($dados['fone'] != "" && $dados['fone'] != null) {
                                                            echo $dados['fone'];
                                                            if ($dados['celular'] != "" && $dados['celular'] != null) {
                                                                echo " / " . $dados['celular'];
                                                            }
                                                        } else if ($dados['celular'] != "" && $dados['celular'] != null) {
                                                            echo $dados['celular'];
                                                        }

                                                        ?>
                                                    </strong>
                                                    <?php if ($dados['estado_civil'] != "" && $dados['estado_civil'] != null) { ?>
                                                        <strong>
                                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                                            Estado civil: <?php echo $dados['estado_civil']; ?>
                                                        </strong>
                                                        <?php
                                                    }
                                                    if ($dados['filhos'] != "" && $dados['filhos'] != null) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-users" aria-hidden="true"></i>
                                                            Filhos:
                                                            <?php
                                                            if ($dados['filhos'] == 1) {
                                                                echo $dados['filhos'] . " filho";
                                                            } else if ($dados['filhos'] > 1) {
                                                                echo $dados['filhos'] . " filhos";
                                                            } else {
                                                                echo "Não tem filhos";
                                                            }

                                                            ?>
                                                        </strong>
                                                    <?php }

                                                    ?>
                                                </div>
                                                <div class="col">
                                                    <?php if ($dados['pretensao_salarial'] != "" && $dados['pretensao_salarial'] != null) { ?>
                                                        <strong>
                                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                                            Pretensão salarial: <?php echo $dados['pretensao_salarial']; ?>
                                                        </strong>
                                                        <?php
                                                    }
                                                    if ($dados['viajar'] == 1) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-plane" aria-hidden="true"></i>
                                                            Tem disponibilidade para viajar
                                                        </strong>
                                                        <?php
                                                    }
                                                    if ($dados['veiculo'] == 1) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-car" aria-hidden="true"></i>
                                                            Possui veiculo Próprio
                                                        </strong>
                                                        <?php
                                                    }
                                                    $res_cnh = mysqli_query($con, "SELECT categoria FROM TB_VV_CNH_CAND WHERE id_candidato = $_GET[id] ORDER BY categoria ASC");
                                                    if (mysqli_num_rows($res_cnh)) {

                                                        ?>
                                                        <strong>
                                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                                            Possui CNH categoria
                                                            <?php
                                                            $key = 1;
                                                            while ($cat = mysqli_fetch_array($res_cnh)) {
                                                                if ($key < mysqli_num_rows($res_cnh)) {
                                                                    echo $cat['categoria'] . ", ";
                                                                } else {
                                                                    echo $cat['categoria'];
                                                                }
                                                                $key++;
                                                            }

                                                            ?>

                                                        </strong>
                                                    <?php } ?>
                                                </div>
                                                <?php
                                                $res_idiomas = mysqli_query($con, "SELECT * FROM TB_VV_IDIOMAS_CAND WHERE id_candidato = $_GET[id]");
                                                if (mysqli_num_rows($res_idiomas)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                                                        Idiomas
                                                    </strong>
                                                    <?php
                                                }
                                                while ($idioma = mysqli_fetch_array($res_idiomas)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Idioma: <?php echo $idioma['idioma'] ?>
                                                            </span>
                                                            <span>
                                                                Nível: <?php echo $idioma['nivel'] ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                $res_formacoes = mysqli_query($con, "SELECT nivel, instituicao, curso, semestre, situacao, fim, turno FROM TB_VV_FORMACOES_CAND WHERE id_candidato = $_GET[id]");
                                                if (mysqli_num_rows($res_formacoes)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                        Formações
                                                    </strong>
                                                    <?php
                                                }
                                                while ($formacao = mysqli_fetch_array($res_formacoes)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Nível de Ensino: <?php echo $formacao['nivel']; ?>
                                                            </span>
                                                            <?php if ($formacao['curso'] != "" && $formacao['curso'] != NULL) { ?>
                                                                <span>
                                                                    Curso : <?php echo $formacao['curso']; ?>
                                                                </span>
                                                            <?php } ?>
                                                            <span>
                                                                Instituição: <?php echo $formacao['instituicao']; ?>
                                                            </span>
                                                            <span>
                                                                Situação: <?php echo $formacao['situacao']; ?>
                                                            </span>
                                                            <?php if ($formacao['semestre'] != "" && $formacao['semestre'] != null) { ?>
                                                                <span>
                                                                    Semestre: <?php echo $formacao['semestre'] . "º"; ?>
                                                                </span>
                                                            <?php } ?>
                                                            <?php if ($formacao['turno'] != "" && $formacao['turno'] != null) { ?>
                                                                <span>
                                                                    Turno: <?php echo $formacao['turno']; ?>
                                                                </span>
                                                            <?php } ?>
                                                            <?php if ($formacao['fim'] != "" && $formacao['fim'] != null) { ?>
                                                                <span>
                                                                    Ano de conclusão: <?php echo $formacao['fim']; ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php
                                                $res_cursos = mysqli_query($con, "SELECT situacao, nome, instituicao, anofim FROM TB_VV_CURSOS_CAND WHERE id_candidato = $_GET[id]");
                                                if (mysqli_num_rows($res_cursos)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                        Cursos
                                                    </strong>
                                                    <?php
                                                }
                                                while ($cursos = mysqli_fetch_array($res_cursos)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Nome do curso: <?php echo $cursos['nome'] ?>
                                                            </span>
                                                            <span>
                                                                Instituição: <?php echo $cursos['instituicao'] ?>
                                                            </span>
                                                            <span>
                                                                Situação: <?php echo $cursos['situacao'] ?>
                                                            </span>
                                                            <?php if ($cursos['anofim'] != "") { ?>
                                                                <span>
                                                                    Conclusão: <?php echo $cursos['anofim'] ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                $res_exp = mysqli_query($con, "SELECT * FROM TB_VV_EXP_PROFISSIONAIS WHERE id_candidato = $_GET[id]");
                                                if (mysqli_num_rows($res_exp)) {

                                                    ?>
                                                    <strong>
                                                        <i class="fa fa-history" aria-hidden="true"></i>
                                                        Histórico de experiência profissional
                                                    </strong>
                                                    <?php
                                                }
                                                while ($exp = mysqli_fetch_array($res_exp)) {

                                                    ?>
                                                    <div class="carac-item">
                                                        <div class="full">
                                                            <span>
                                                                Nome da empresa: <?php echo $exp['nome_empresa'] ?>
                                                            </span>
                                                            <span>
                                                                Cargo: <?php echo $exp['cargo'] ?>
                                                            </span>
                                                            <?php if ($exp['atividades'] != "" && $exp['atividades'] != null) { ?>
                                                                <span>
                                                                    Atividades: <?php echo $exp['atividades'] ?>
                                                                </span>
                                                            <?php } ?>
                                                            <span>
                                                                Início: <?php
                                                                echo $exp['inicio'];
                                                                if ($exp['fim'] != "" && $exp['fim'] != null) {
                                                                    echo " - Fim: " . $exp['fim'];
                                                                }

                                                                ?>
                                                            </span>
                                                            <?php if ($exp['salario'] != "" && $exp['salario'] != "0.00" && $exp['salario'] != null) { ?>
                                                                <span>
                                                                    Salário: <?php echo "R$ " . str_replace(".", ",", $exp['salario']); ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php" ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
<?php } ?>
