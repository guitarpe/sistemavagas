<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    header("Location:index.php?acesso=false");
}

if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $id = filter_input(INPUT_GET, "id");

    $res_vaga = mysqli_query($con, "SELECT cargo FROM TB_VV_VAGAS WHERE id = $id");
    $vaga = mysqli_fetch_array($res_vaga);

    $sql_candidatos = "SELECT
                            cdt.id_vaga,
                            cdt.data,
                            cdt.status,
                            vg.cargo,
                            cdt.id_candidato,
                            can.nome as nome_candidato
                        FROM TB_VV_CANDIDATURAS cdt
                            INNER JOIN TB_VV_CANDIDATOS can ON can.id=cdt.id_candidato
                            INNER JOIN TB_VV_VAGAS vg ON vg.id=cdt.id_vaga
                        WHERE cdt.id_vaga=$id AND cdt.status != -1";

    if (isset($busca)) {

        $sexo = filter_input(INPUT_POST, "sexo");
        $status = filter_input(INPUT_POST, "status");
        $carg_filtro = filter_input(INPUT_POST, "carg_filtro");

        if (isset($sexo) && $sexo != "") {
            if ($pcd == 1) {
                $sql_candidatos .= " AND can.sexo='Masculino'";
            } else {
                $sql_candidatos .= " AND can.sexo='Feminino'";
            }
        }

        if (isset($status) && $status != "") {
            if ($status == 1) {
                $sql_candidatos .= " AND cdt.status = 1";
            } else {
                $sql_candidatos .= " AND cdt.status = 0";
            }
        }

        if (isset($carg_filtro) && !empty($carg_filtro)) {
            $sql_candidatos .= " AND can.nome LIKE '%" . $carg_filtro . "%'";
        }
    }

    $res_candidaturas = mysqli_query($con, $sql_candidatos);

    ?>

    <!DOCTYPE html>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-empresa.php" ?>
                            <script>$("#gerenciar-candidatos").addClass("active");</script>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <form class="search vagas" action="<?php echo PATH_EMPRESAS . '/gerenciar-candidatos.php?busca=filtros' ?>" method="post">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <select name="sexo">
                                    <option value="">- Sexo -</option>
                                    <option value="1">Masculino</option>
                                    <option value="0">Feminino</option>
                                </select>
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                <select name="status">
                                    <option value="">- Status -</option>
                                    <option value="1">Contratado</option>
                                    <option value="0">Em análise</option>
                                </select>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <input type="text" name="carg_filtro" placeholder="Candidato" class="vaga">
                                <button class="fleft">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                                <button class="btn fleft clean" type="button">Limpar Busca</button>
                            </form>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="form-area">
                                <div class="nome">
                                    <strong>
                                        <?php echo $vaga["cargo"] ?>
                                    </strong>
                                </div>
                                <div class="responsive-table">
                                    <?php if (mysqli_num_rows($res_candidaturas) > 0) { ?>
                                        <table class="vagas-list" border="0">
                                            <thead>
                                                <tr>
                                                    <th>Candidato</th>
                                                    <th width="60">Status</th>
                                                    <th width="150">Candidatura em</th>
                                                    <th width="330"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php while ($candidatos = mysqli_fetch_array($res_candidaturas)) { ?>
                                                    <tr>
                                                        <td>
                                                            <a href="perfil.php?id=<?php echo $candidatos['id_candidato'] ?>"><?php echo $candidatos["nome_candidato"] ?></a>
                                                        </td>
                                                        <td>
                                                            <?php if ($candidatos["status"] == 0) { ?>
                                                                Em análise
                                                            <?php } else { ?>
                                                                Contratado
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $func->formataData($candidatos["data"]) ?>
                                                        </td>
                                                        <td>
                                                            <div class="btns-edit">
                                                                <a href="#" data-load-title="Visualizar Candidato" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_ALL . '/modais/ver-candidato.php?id=' . $candidatos['id_candidato'] ?>" class="btn buttone">
                                                                    <i class="fa fa-eye" aria-hidden="true"></i> Visualizar
                                                                </a>
                                                                <a href="#" data-load-title="Descartar Candidato" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL . '/modais/alerta_descarta_candidato.php?id=' . $candidatos['id_vaga'] . '&usuario=' . $candidatos['id_candidato'] ?>" class="btn buttone">
                                                                    <i class="fa fa-trash" aria-hidden="true"></i> Descartar
                                                                </a>
                                                                <?php if (intval($candidatos["status"]) == 0) { ?>
                                                                    <a href="#" data-load-title="Contratar Candidato" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL . '/modais/alerta_contratar_descontratar_candidato.php?status=1&id=' . $candidatos['id_vaga'] . '&usuario=' . $candidatos['id_candidato'] ?>" class="btn buttone">
                                                                        <i class="fa fa-handshake-o" aria-hidden="true"></i> Contratar
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <a href="#" data-load-title="Remover Contratação" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL . '/modais/alerta_contratar_descontratar_candidato.php?status=0&id=' . $candidatos['id_vaga'] . '&usuario=' . $candidatos['id_candidato'] ?>" class="btn buttone">
                                                                        <i class="fa fa-user-times" aria-hidden="true"></i> Descontratar
                                                                    </a>
                                                                <?php } ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    <?php } else { ?>
                                        <h3>Não há candidatos para esta vaga</h3>
                                    <?php } ?>
                                </div>
                            </div>

                            <section class="publicidade clear">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "../includes/footer.php" ?>

            <section class="modal">
                <?php
                include "../includes/alerta_sair.php";
                include "../includes/alerta_descarta_candidato.php";

                ?>
            </section>

            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
<?php } ?>

