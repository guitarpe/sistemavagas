<?php
session_start();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    header("Location:index.php?acesso=false");
}

if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['estado'] != 1) {
    echo"<script>location.href='.'</script>";
} else {

    include "../includes/conexao.php";

    $res_vagas = mysqli_query($con, "SELECT * FROM TB_VV_VAGAS WHERE id_empresa = $_SESSION[id]");

    ?>

    <!DOCTYPE html>
    <html>
        <head>

            <meta charset="utf-8">
            <meta name="description" content="Descrição">
            <meta name="keywords" content="palavras chave, separadas por virgula">
            <meta name="author" content="JuCamillo Web Co.">
            <meta name="viewport" content="width=device-width">
            <meta name="revisit-after" content="1 days">
            <title>Vagas & Vagas</title>

            <!--CSS geral-->
            <link rel="stylesheet" href="../stylesheets/geral.css"/>

            <!--CSS por seção ou plugin-->

            <!--CSS / END-->

            <!-- favicon -->
            <link rel="shortcut icon" href="../images/favicon.ico">

            <!-- JS bibliotecas -->
            <script src="../javascripts/jquery-1.10.1.min.js"></script>
            <script src="https://use.fontawesome.com/6c10f0500f.js"></script>

            <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

            <script type="text/javascript" src="../javascripts/cadastrovagas.js"></script>

            <style type="text/css">
                .btn-table {
                    float: left;
                    width: auto;
                    padding: 0 5px 30px 5px;
                    height: 20px;
                    line-height: 36px;
                    background: #f0f0f0;
                    color: #7d0091;
                    margin: 0 0 0 0;
                    border-radius: 5px;
                    border: 2px solid #7d0091;
                    font-size: 14px;
                    text-transform: uppercase;
                    font-weight: 600;
                    cursor: pointer;
                }

                .btn-table:hover {
                    background: #7d0091;
                    color: #fff;
                }
            </style>
        </head>

        <body>
            <!--CONTEUDO -->
            <!-- HEADER -->
            <header>
                <?php include "../includes/navbar.php" ?>
            </header>
            <!--HEADER / END -->

            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-empresa.php"; ?>
                            <script>$("#item4").addClass("active");</script>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <form class="search vagas" action="gerenciar-vagas.php?busca=filtros" method="post">
                                <i class="fa fa-wheelchair" aria-hidden="true"></i>
                                <select name="pcd">
                                    <option disabled selected>- PCD -</option>
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                <select name="status">
                                    <option disabled selected>- Status -</option>
                                    <option value="1">Ativado</option>
                                    <option value="0">Desativado</option>
                                </select>
                                <i class="fa fa-cog" aria-hidden="true"></i>
                                <select name="etapa">
                                    <option disabled selected>- Etapa -</option>
                                </select>
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <input type="text" placeholder="Cargo" class="vaga">
                                <button>
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                                <button class="clean" type="button">Limpar Busca</button>
                            </form>


                            <?php
                            $res_publi = mysqli_query($con, "SELECT imagem FROM TB_VV_PUBLICIDADE ORDER BY RAND() LIMIT 1");
                            $publi = mysqli_fetch_array($res_publi);

                            ?>
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <img src="../images/<?php echo $publi['imagem'] ?>">
                                </div>
                            </section>

                            <div class="form-area">
                                <div class="responsive-table">
                                    <table class="vagas-list" border="0">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center">Cargo</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($vaga = mysqli_fetch_array($res_vagas)) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="editar-vaga.php<?php echo $vaga['id'] ?>">
                                                            <?php echo $vaga['cargo'] ?>
                                                        </a>
                                                    </td>
                                                    <td  width="160">
                                                        <button class="btn-table" onclick="window.location.href = 'actions/recebe_duplicar.php?id=<?php echo $vaga['id'] ?>'">
                                                            <i class="fa fa-files-o" aria-hidden="true"></i> Duplicar Vaga
                                                        </button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>
            </section>

            <!--FOOTER -->
            <?php include "../includes/footer.php" ?>
            <!--FOOTER / END -->

            <section class="modal">
                <?php include "includes/cadastro-vagas.php" ?>
            </section>





            <!-- JS PLUGINS -->

            <script type="text/javascript" src="../javascripts/custom.js">
            </script>



            <!-- SCRIPTS / END -->


            <!-- GOOGLE ANALYTICS -->


            <!--GOOGLE ANALYTICS / END-->


            <!--CONTEUDO / END -->

        </body>

    </html>

    <?php
}

if (isset($_GET['busca'])) {

    $sql = "SELECT * FROM TB_VV_VAGAS ";

    if (isset($_POST['pcd'])) {
        $sql = "SELECT * FROM TB_VV_DEFICIENCIAS_VAGA";
    }
    if (isset($_POST['pcd'])) {
        if ($_POST['pcd'] == 1) {
            $sql = "SELECT * FROM TB_VV_VAGAS WHERE status = 1";
        } else {
            $sql = "SELECT * FROM TB_VV_VAGAS WHERE status = 0";
        }
    }
}

?>