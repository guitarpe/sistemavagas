<?php
session_start();
include "../includes/conexao.php";
include '../assets/plugins/mpdf/mpdf.php';

$cod = filter_input(INPUT_GET, 'cod');
$id = $func->dec_enc(2, $cod);

$func = new Funcoes();

$res = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATOS WHERE id=" . $id) or die(mysqli_error($con));
$candidato = mysqli_fetch_array($res);

$res_cargos = mysqli_query($con, "SELECT * FROM TB_VV_CARGOS_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));
$res_form = mysqli_query($con, "SELECT * FROM TB_VV_FORMACOES_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));
$res_curso = mysqli_query($con, "SELECT * FROM TB_VV_CURSOS_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));
$res_exp = mysqli_query($con, "SELECT * FROM TB_VV_EXP_PROFISSIONAIS WHERE id_candidato=" . $id) or die(mysqli_error($con));
$res_cat = mysqli_query($con, "SELECT * FROM TB_VV_INFO_CATEGORIAS ORDER BY nome ASC") or die(mysqli_error($con));
$res_idiomas = mysqli_query($con, "SELECT * FROM TB_VV_IDIOMAS_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));
$res_cnh = mysqli_query($con, "SELECT * FROM TB_VV_CNH_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));
$res_def = mysqli_query($con, "SELECT * FROM TB_VV_TIPOS_DEFICIENCIA ORDER BY nome ASC") or die(mysqli_error($con));


$data_nasc = explode('-', $candidato['nascimento']);
$datainfo = date('d/m/Y');
$data = explode('/', $datainfo);
$idade = $data[2] - $data_nasc[0];

if ($data_nasc[1] > $data[1]) {
    $idade--;
} else {
    if ($data_nasc[1] == $data[1] && $data_nasc[2] > $data[0]) {
        $idade--;
    }
}

$html_fones = "";
if ($candidato['fone'] != "") {
    $html_fones .= "<i class='fa fa-phone' aria-hidden='true'></i> $candidato[fone] ";
}
if ($candidato['celular'] != "") {
    $html_fones .= " - <i class='fa fa-mobile' aria-hidden='true'></i>  $candidato[celular]";
}

$html_info = "<b style='margin-left:27px'>Sexo:</b> $candidato[sexo] ";
if ($candidato['estado_civil'] != NULL) {
    $html_info .= "<b style='margin-left:20px'>Estado Civil:</b> $candidato[estado_civil] <br/> ";
} else {
    $html_info .= "<br/> ";
}
$html_info .= "<b style='margin-left:27px'>Idade:</b> $idade anos ";
if ($candidato['filhos'] != NULL) {
    if ($candidato['filhos'] > 0) {
        if ($candidato['filhos'] == 1) {
            //$html_info .= "<b style='margin-left:31px'>Filhos:</b> $candidato[filhos] filho ";
        } else {
            // $html_info .= "<b style='margin-left:31px'>Filhos:</b> $candidato[filhos] filhos ";
            if ($candidato['filhos'] == 4) {
                //     $html_info .= "ou mais";
            }
        }
    } else {
        //$html_info .= "<b style='margin-left:31px'>Filhos:</b> Não tem filhos ";
    }
} else {
    //$html_info .= "<br/> ";
}

$info_def = 0;
$html_defs = "";

while ($cat = mysqli_fetch_array($res_def)) {

    $res = mysqli_query($con, "SELECT DISTINCT i.nome FROM TB_VV_DEFICIENCIAS AS i LEFT JOIN TB_VV_DEFICIENCIAS_CAND AS c ON i.id = c.id_deficiencia WHERE c.id_candidato=" . $id . " AND i.id_tipo=" . $cat['id']) or die(mysqli_error($con));
    $qtd_rows = mysqli_num_rows($res);

    if ($qtd_rows > 0) {
        $i = 1;

        $html_defs .= $cat['nome'] . ': ';

        while ($info = mysqli_fetch_array($res)['nome']) {
            if ($i < $qtd_rows) {
                $html_defs .= "$info, ";
            } else {
                $html_defs .= "$info";
            }
            $i++;
        }
        $html_defs .= "<br/>";

        $info_def++;
    }
}

if ($info_def > 0) {
    $html_info .= "<br/><br/><b style='margin-left:27px'>Portador de deficiência:</b><br/>";
    $html_info .= $html_defs;
}


$endereco = "";
if (!empty($candidato['logradouro'])) {
    $endereco .= $candidato['logradouro'];
}
if (!empty($candidato['numero'])) {
    $endereco .= ", " . $candidato['numero'];
}
if (!empty($candidato['complemento'])) {
    $endereco .= " - " . $candidato['complemento'];
}
if (!empty($candidato['bairro'])) {
    $endereco .= ", " . $candidato['bairro'] . "<br/>";
}
if (!empty($candidato['cidade'])) {
    $endereco .= $candidato['cidade'];
}
if (!empty($candidato['estado'])) {
    $endereco .= "-" . $candidato['estado'];
}
if (!empty($candidato['cep'])) {
    $endereco .= " - CEP: " . $candidato['cep'];
}

if ($candidato['foto'] != "" && $candidato['foto'] != NULL) {
    $html_dados = "<table style='width:100%; '>
        <tr>
            <td style='width:40%'>
                <img src='images/$candidato[foto]' width='200'>
            </td>
            <td style='width:60%;'>
                <table style='width:100%; '>
                    <tr>
                        <td style='font-size:18px; padding-bottom: 5px'>
                            <b>$candidato[nome]</b>
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;'>
                            $endereco
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;padding-bottom: 5px; padding-top:5px'>
                            $candidato[email]
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;'>
                            $html_fones
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;padding-bottom: 5px; padding-top:5px'>
                            Outras informações pessoais
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;'>
                        $html_info
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>";
} else {
    $html_dados = "<table style='width:100%; '>
        <tr>
            <td style='width:40%'>
            </td>
            <td style='width:60%;'>
                <table style='width:100%; '>
                    <tr>
                        <td style='font-size:18px; padding-bottom: 5px'>
                            <b>$candidato[nome]</b>
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;'>
                            $endereco
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;padding-bottom: 5px; padding-top:5px'>
                            $candidato[email]
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;'>
                            $html_fones
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;padding-bottom: 5px; padding-top:5px'>
                            <strong>Outras informações pessoais</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px;'>
                        $html_info
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>";
}

$html_cargos = "";

while ($cargo = mysqli_fetch_array($res_cargos)['cargo']) {
    $html_cargos .= "<tr><td style='font-size: 11px; padding-top: 5px'>$cargo</td></tr>";
}

$html_form = "<table style='width:100%; font-size:11px; padding-bottom:5px; '>";

while ($form = mysqli_fetch_array($res_form)) {
    if ($form['nivel'] == "Ensino superior" || $form['nivel'] == "Curso técnico" || $form['nivel'] == "Pós-graduação") {

        if ($form['situacao'] == "Em Andamento") {

            $html_form .= "<tr> 
                        <td style='width:70%'>
                            <b>Curso:</b> " . $form['curso'] . "
                        </td>
                        <td style='width:30%'>
                            <b>Tipo:</b> " . $form['nivel'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Instituição:</b> " . $form['instituicao'] . "
                        </td>
                        <td>
                            <b>Situação:</b> " . $form['situacao'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Semestre:</b> " . $form['semestre'] . "º semestre - <b>Turno:</b> " . $form['turno'] . "
                        </td>
                        <td>
                            <b>Previsão de conclusão:</b> " . $form['fim'] . "
                        </td>
                    </tr><tr><td style='height:10px'></td></tr>";
        } else if ($form['situacao'] == "Concluído") {

            $html_form .= "<tr> 
                        <td style='width:70%'>
                            <b>Curso:</b> " . $form['curso'] . "
                        </td>
                        <td style='width:30%'>
                            <b>Tipo:</b> " . $form['nivel'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Instituição:</b> " . $form['instituicao'] . "
                        </td>
                        <td>
                            <b>Situação:</b> " . $form['situacao'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Ano de conclusão:</b> " . $form['fim'] . "
                        </td>
                        <td></td>
                    </tr><tr><td style='height:10px'></td></tr>";
        } else {
            $html_form .= "<tr> 
                        <td style='width:70%'>
                            <b>Curso:</b> " . $form['curso'] . "
                        </td>
                        <td style='width:30%'>
                            <b>Tipo:</b> " . $form['nivel'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Instituição:</b> " . $form['instituicao'] . "
                        </td>
                        <td>
                            <b>Situação:</b> " . $form['situacao'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Semestre:</b> " . $form['semestre'] . "º semestre
                        </td>
                        <td>
                        </td>
                    </tr><tr><td style='height:10px'></td></tr>";
        }
    } else {
        if ($form['situacao'] == "Em Andamento") {

            $html_form .= "<tr> 
                        <td style='width:70%'>
                            <b>Instituição:</b> " . $form['instituicao'] . "
                        </td>
                        <td style='width:30%'>
                            <b>Tipo:</b> " . $form['nivel'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Situação:</b> " . $form['situacao'] . " - <b>Turno:</b> " . $form['turno'] . "
                        </td>
                        <td>
                            <b>Previsão de conclusão:</b> " . $form['fim'] . "
                        </td>
                    </tr><tr><td style='height:10px'></td></tr>";
        } else if ($form['situacao'] == "Concluído") {

            $html_form .= "<tr> 
                        <td style='width:70%'>
                            <b>Instituição:</b> " . $form['instituicao'] . "
                        </td>
                        <td style='width:30%'>
                            <b>Tipo:</b> " . $form['nivel'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Situação:</b> " . $form['situacao'] . "
                        </td>
                        <td>
                            <b>Ano de conclusão:</b> " . $form['fim'] . "
                        </td>
                    </tr><tr><td style='height:10px'></td></tr>";
        } else {

            $html_form .= "<tr> 
                        <td style='width:70%'>
                            <b>Instituição:</b> " . $form['instituicao'] . "
                        </td>
                        <td style='width:30%'>
                            <b>Tipo:</b> " . $form['nivel'] . "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Situação:</b> " . $form['situacao'] . "
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr><td style='height:10px'></td></tr>";
        }
    }
}
$html_form .= "</table>";

$html_curso = "<table style='width:100%; font-size:11px; padding-bottom:5px; '>";
while ($curso = mysqli_fetch_array($res_curso)) {

    if ($curso['situacao'] == "Em Andamento") {

        $html_curso .= "<tr> 
                    <td style='width:40%'>
                        <b>Curso:</b> " . $curso['nome'] . "
                    </td>
                    <td style='width:30%'>
                        <b>Instituição:</b> " . $curso['instituicao'] . "
                    </td>
                    <td style='width:30%'>
                        <b>Carga Horária:</b> " . $curso['cargahoraria'] . "
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Situação:</b> " . $curso['situacao'] . "
                    </td>
                    <td>
                        <b>Turno:</b> " . $curso['turnoextra'] . "
                    </td>
                    <td>
                        <b>Previsão de Conclusão:</b> " . $curso['anofim'] . "
                    </td>
                </tr>
                <tr>
                    <td colspan='3'>
                        <b>Dias da Semana:</b> " . $curso['diassemana'] . "
                    </td>
                </tr>
                <tr><td style='height:10px'></td><td></td><td></td></tr>";
    } else if ($curso['situacao'] == "Concluído") {

        $html_curso .= "<tr> 
                    <td style='width:40%'>
                        <b>Curso:</b> " . $curso['nome'] . "
                    </td>
                    <td style='width:30%'>
                        <b>Instituição:</b> " . $curso['instituicao'] . "
                    </td>
                    <td style='width:30%'>
                        <b>Carga Horária:</b> " . $curso['cargahoraria'] . "
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Situação:</b> " . $curso['situacao'] . "
                    </td>
                    <td>
                        <b>Ano de Conclusão:</b> " . $curso['anofim'] . "
                    </td>
                    <td>
                    </td>
                </tr>
                <tr><td style='height:10px'></td><td></td><td></td></tr>";
    } else {

        $html_curso .= "<tr> 
                    <td style='width:40%'>
                        <b>Curso:</b> " . $curso['nome'] . "
                    </td>
                    <td style='width:30%'>
                        <b>Instituição:</b> " . $curso['instituicao'] . "
                    </td>
                    <td style='width:30%'>
                        <b>Carga Horária:</b> " . $curso['cargahoraria'] . "
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Situação:</b> " . $curso['situacao'] . "
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr><td style='height:10px'></td><td></td><td></td></tr>";
    }
}
$html_curso .= "</table>";

$html_exp = "<table autosize='1' style='width:100%; font-size:11px; padding-bottom:5px; '>";
while ($exp = mysqli_fetch_array($res_exp)) {

    $aux1 = "<b>Inicio:</b> $exp[inicio] ";
    if ($exp['fim'] != "") {
        $aux1 .= "- <b>Fim:</b> $exp[fim]";
    }

    if (intval($exp['emprego_atual']) == 1) {
        $aux1 .= "- <b>Emprego Atual";
    }

    $aux2 = "";
    if ($exp['salario'] > 0) {
        $aux2 .= "<td><b>Salário:</b> R$ $exp[salario]</td>";
    }

    $aux3 = "";
    if ($exp['atividades'] != "") {
        $aux3 .= "<tr><td colspan='2'><b>Atividades:</b> $exp[atividades]</td></tr>";
    }

    $html_exp .= "<tr> 
                <td style='width:50%'>
                    <b>Empresa:</b> $exp[nome_empresa]
                </td>
                <td style='width:50%'>
                    <b>Cargo:</b> $exp[cargo]
                </td>
            </tr>
            <tr>
                <td>
                    $aux1
                </td>
                $aux2
            </tr>
            $aux3
            <tr><td style='height:10px'></td><td></td></tr>";
}
$html_exp .= "</table>";

$info_cand = 0;

$html_informatica = "<table autosize='1' style='width:100%; font-size:11px; padding-bottom:5px;'>";
while ($cat = mysqli_fetch_array($res_cat)) {
    $res = mysqli_query($con, "SELECT DISTINCT i.nome FROM TB_VV_INFORMATICA AS i LEFT JOIN TB_VV_INFORMATICA_CAND AS c ON i.id = c.id_informatica WHERE c.id_candidato=" . $id . " AND i.id_categoria=" . $cat['id']) or die(mysqli_error($con));
    $qtd_rows = mysqli_num_rows($res);
    if ($qtd_rows > 0) {
        $i = 1;
        $html_informatica .= "<tr><td>$cat[nome]: ";
        while ($info = mysqli_fetch_array($res)['nome']) {
            if ($i < $qtd_rows) {
                $html_informatica .= "$info, ";
            } else {
                $html_informatica .= "$info";
            }
            $i++;
        }
        $html_informatica .= "</td></tr>";

        $info_cand++;
    }
}
$html_informatica .= "</table>";

$html_idiomas = "<table autosize='1' style='width:100%; font-size:11px; padding-bottom:5px; '>";
while ($idioma = mysqli_fetch_array($res_idiomas)) {
    $html_idiomas .= "<tr><td>$idioma[idioma]: $idioma[nivel]</td></tr>";
}
$html_idiomas .= "</table>";

$html_outros = "<table autosize='1' style='width:100%; font-size:11px; padding-bottom:5px; '>";
if ($candidato['viajar'] != NULL && $candidato['viajar'] != 0) {
    $html_outros .= "<tr><td>Tem disponibilidade para viajar</td></tr>";
}

if ($candidato['veiculo'] != NULL && $candidato['veiculo'] != 0) {
    $html_outros .= "<tr><td>Possui veiculo Próprio</td></tr>";
}

$qtd_rows = mysqli_num_rows($res_cnh);

if ($qtd_rows > 0) {
    if ($qtd_rows > 1) {
        $html_outros .= "<tr><td>Possui CNH categoria ";
        $i = 0;
        while ($cnh = mysqli_fetch_array($res_cnh)) {
            if ($i == 0) {
                $html_outros .= "$cnh[categoria]";
            } else {
                $html_outros .= "$cnh[categoria]";
            }
            $i++;
        }
        $html_outros .= "</td></tr>";
    } else {
        $html_outros .= "<tr><td>Possui CNH categoria " . mysqli_fetch_array($res_cnh)['categoria'] . "</td></tr>";
    }
}

$html_outros .= "</table>";

$html = "
    <table autosize='1' style='width:100%; border-bottom: 1px solid #eaeaea; '>
        <tr>
            <td style='width:40%'>
                <img src='../images/logo.png'>
            </td>
            <td style='width:60%;padding-top:30px; text-align:right;font-size:25px'>
                CURRICULUM VITAE
            </td>
        </tr>
    </table>";

$html .= $html_dados;

if (mysqli_num_rows($res_cargos) || !empty($candidato['obj_profissional'])) {
    $html .= "<table autosize='1' style='width:100%; '>";
    if (!empty($candidato['obj_profissional'])) {
        $html .= "<tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 20px'>
                <span style='font-size:18px; font-weight:bold'>Objetivo Profissional</span>
            </td>
        </tr>";
    }
    if (!empty($candidato['obj_profissional'])) {
        $html .= "<tr>
            <td>" . $candidato['obj_profissional'] . "</td>
        </tr>";
    }

    if (!empty($html_cargos)) {
        $html .= "<tr><td><strong>Cargos</strong></td></tr>" . $html_cargos;
    }
    $html .= "</table>";
}
if (mysqli_num_rows($res_form)) {
    $html .= "<table autosize='1' style='width:100%; '>
        <tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 20px'>
                <span style='font-size:18px;font-weight:bold'>Formação Acadêmica</span>
            </td>
        </tr>
        </table>
        $html_form";
}
if (mysqli_num_rows($res_curso)) {
    $html .= "<table autosize='1' style='width:100%; '>
        <tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 5px'>
                <span style='font-size:18px;font-weight:bold'>Cursos Extracurriculares</span>
            </td>
        </tr>
        </table>
        $html_curso";
}
if (mysqli_num_rows($res_exp)) {
    $html .= "<table autosize='1' style='width:100%; '>
        <tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 5px'>
                <span style='font-size:18px;font-weight:bold'>Experiências Profissionais</span>
            </td>
        </tr>
        </table>
        $html_exp";
}
if ($info_cand > 0) {
    $html .= "<table autosize='1' style='width:100%; '>
        <tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 5px'>
                <span style='font-size:18px;font-weight:bold'>Conhecimentos em Informática</span>
            </td>
        </tr>
        </table>
        $html_informatica";
}
if (mysqli_num_rows($res_idiomas)) {
    $html .= "<table autosize='1' style='width:100%; '>
        <tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 5px'>
                <span style='font-size:18px;font-weight:bold'>Idiomas</span>
            </td>
        </tr>
        </table>
        $html_idiomas";
}
if (($candidato['viajar'] != NULL && $candidato['viajar'] != 0) || $qtd_rows > 0) {
    $html .= "<table autosize='1' style='width:100%; '>
        <tr>
            <td style='border-bottom: 1px solid #eaeaea; padding-top: 5px'>
                <span style='font-size:18px;font-weight:bold'>Outras Informações</span>
            </td>
        </tr>
        </table>
        $html_outros";
}

$saida = '<html>
    <body>
    <head>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="' . PATH_ASSETS . '/css/geral/pdf.css' . '" rel="stylesheet"/>
    </head>
    <div class="book">
        <div class="page" style="page-break-after: auto;">' . $html . '</div>
    </div>
</body>
</html>';

$pdfFilePath = "CURRICULO-VITAE.pdf";
$pdf = new mPDF();

$PDFContent = mb_convert_encoding(utf8_decode($saida), 'UTF-8', 'ISO-8859-1');

//$pdf->use_kwt = false;
//$pdf->autoPageBreak = true;
//$pdf->shrink_tables_to_fit = 1;
$pdf->WriteHTML($PDFContent, 2);
$pdf->Output($pdfFilePath, "I");
