<?php
include "../includes/conexao.php";
$func = new Funcoes();

$to = filter_input(INPUT_POST, 'email');
$usuario = filter_input(INPUT_POST, 'usuario');
$empresa = filter_input(INPUT_POST, 'empresa');

$sql_emp = "SELECT * FROM TB_VV_USUARIOS WHERE nome=$usuario AND email=$to AND tipo='empresa'";
$res_emp = mysqli_query($con, $sql_emp) or die(mysqli_error($con));
$emp = mysqli_fetch_array($res_emp);

$subject = 'Bem vindo ao Vagas e Vagas!';

$message = '
	<html>
	<head>
	</head>
	<body>
		<div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
			<img src="' . HOME . '/images/top.png">
			<div class="container" style="padding:20px">
				<table>
					<tr>
						<td>
							<h2><b></b></h2>
							<br/>Olá ' . $usuario . '
							<br/>
							<br/>
							É uma alegria termos você conosco em nosso Portal Vagas e Vagas.
							<br/>
							<br/>
							Você cadastrou a empresa ' . $empresa . ' em nosso portal e basta cadastrar suas vagas que já estarão disponíveis para visualização de todos.
							<br/>
							<br/>
							Lembramos que fazemos tudo por pessoas, para pessoas e por pessoas:
							<br/>
							<br/>
							1. Não insira dados errados sobre sua empresa, isso além de não ser legal, é ilegal.
							<br/>
							<br/>
							2. Não crie vagas que não existem. Coloque-se no lugar do candidato e não crie expectativas falsas.
							<br/>
							<br/>
							3. MUITO IMPORTANTE: lembre-se sempre de selecionar o seu candidato e depois fechar sua vaga.
						</td>
						<td>
							<img src="' . HOME . '/images/confirmado.png">
						</td>
					</tr>
				</table>
				<br/>
				<br/>
				<hr>
				<br/>
				Estas simples regras darão um destaque maior às suas vagas e, acima de tudo, fará deste portal um espaço mais justo e melhor para todos.
				<br/>
				<br/>
				Acesse seu painel <a href="' . HOME . '/empresas/" style="color:blue">' . HOME . '/empresas</a>
				<br/>
				<br/>
				PS. Ajude divulgar este trabalho especial para seus colegas! É muito importante para todos.
				<br/>
				<br/>
				Contamos muito com você!
				<br/>
				<br/>
				Equipe Vagas e Vagas.
				<br/>
				<br/>
				<br/>
				<br/>
				<div class="row" align="right">
					<a href="' . FACEBOOK . '" style="text-decoration:none">
						<img src="' . HOME . '/images/facebook.png">
					</a>
					<a href="' . TWITTER . '" style="text-decoration:none">
						<img src="' . HOME . '/images/twitter.png">
					</a>
					<a href="' . PLUS . '" style="text-decoration:none">
						<img src="' . HOME . '/images/googlep.png">
					</a>
				</div>
			</div>
			<img src="' . HOME . '/images/top.png">
		<div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $emp['id'] . '&tipo=empresa" target="_blank">Não quero receber mais mensagens</a></div>
            </div>
	</body>
	</html>';

$dados = [
    'HOST' => $configuracoes_sis['SMTP_HOST'],
    'USER' => $configuracoes_sis['SMTP_USER'],
    'PASS' => $configuracoes_sis['SMTP_PASS'],
    'PORT' => $configuracoes_sis['SMTP_PORT'],
    'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
    'FROM_TXT' => 'Vagas e Vagas',
    'TO_MAIL' => $to,
    'TO_TXT' => $empresa,
    'SUBJECT' => $subject,
    'MENSAGE' => $message
];

include_once '../includes/enviar-email.php';
