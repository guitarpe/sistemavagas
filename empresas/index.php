<?php
session_start();

include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $tipo_cad = "empresa";

    ?>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi1['link'])) { ?>
                                    <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>

                        <div class="dashboard">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-md-6 col-xs-12">
                                        <button class="item" type="button" onclick="location.href = 'dados-empresa.php'">
                                            <i class="fa fa-gear"></i>
                                            Dados da Empresa
                                        </button>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <button class="item" data-load-title="Anunciar vagas de emprego" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_EMPRESAS ?>/includes/cadastro-vagas.php">
                                            <i class="fa fa-bullhorn"></i>
                                            Anunciar Vaga
                                        </button>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <button class="item" type="button" onclick="location.href = 'gerenciar-vagas.php'">
                                            <i class="fa fa-cogs"></i>
                                            Gerenciar Vagas
                                        </button>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <button class="item" type="button" onclick="location.href = 'buscar-profissional.php'">
                                            <i class="fa fa-search"></i>
                                            Buscar Profissional
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section class="publicidade clear">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi2['link'])) { ?>
                                    <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
    <?php
}