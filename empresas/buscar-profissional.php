<?php
session_start();

include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}
if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $cargo = filter_input(INPUT_GET, 'cargo');
    $escolaridade = filter_input(INPUT_GET, 'escolaridade');
    $deficiencia = filter_input(INPUT_GET, 'deficiencia');
    $informatica = filter_input(INPUT_GET, 'informatica');
    $estado = filter_input(INPUT_GET, 'estado');
    $cidade = filter_input(INPUT_GET, 'cidade');
    $sexo = filter_input(INPUT_GET, 'sexo');

    $pagina = filter_input(INPUT_GET, "pagina");

    //paginação
    $total_reg = 20;

    if (!isset($pagina)) {
        $pag = "1";
    } else {
        $pag = $pagina;
    }

    $ini = $pag - 1;
    $inicio = $ini * $total_reg;

    $sql = "SELECT
                id, nome, sexo, foto, cidade, uf, estado, obj_profissional,
                cargos, escolaridade, deficiencias, informatica, idade
            FROM VW_CANDIDATOS WHERE 1=1 ";

    $where = "";
    $urlparam = "?";

    if (!empty($cargo)) {

        $ct1 = 0;
        $arr_cargos = explode('|', $cargo);

        $where .= " AND (";
        foreach ($arr_cargos as $key => $value) {
            if (!empty($value)) {
                $where .= $ct1 != 0 ? ' ||' : '';
                $where .= " cargos LIKE '%$value%' ";
            }
            $ct1++;
        }
        $where .= ") ";

        $urlparam .= 'cargo=' . $cargo . '&';
    }

    if (!empty($escolaridade)) {

        $ct2 = 0;
        $arr_esc = explode('|', $escolaridade);

        $where .= " AND (";
        foreach ($arr_esc as $key => $value) {
            if (!empty($value)) {
                $where .= $ct2 != 0 ? ' ||' : '';
                $where .= " escolaridade LIKE '%$value%' ";
            }
            $ct2++;
        }
        $where .= ") ";

        $urlparam .= 'escolaridade=' . $cargo . '&';
    }

    if (!empty($deficiencia)) {

        $ct3 = 0;
        $arr_defs = explode('|', $deficiencia);

        $where .= " AND (";
        foreach ($arr_defs as $key => $value) {
            if (!empty($value)) {
                $where .= $ct3 != 0 ? ' ||' : '';
                $where .= " deficiencias LIKE '%$value%' ";
            }
            $ct3++;
        }
        $where .= ") ";

        $urlparam .= 'deficiencias=' . $deficiencia . '&';
    }

    if (!empty($informatica)) {

        $ct4 = 0;
        $arr_info = explode('|', $informatica);

        $where .= " AND (";
        foreach ($arr_info as $key => $value) {
            if (!empty($value)) {
                $where .= $ct4 != 0 ? ' ||' : '';
                $where .= " informatica LIKE '%$value%' ";
            }
            $ct4++;
        }
        $where .= ") ";

        $urlparam .= 'informatica=' . $informatica . '&';
    }

    if (!empty($estado)) {

        $ct5 = 0;
        $arr_estados = explode('|', $estado);

        $where .= " AND (";
        foreach ($arr_estados as $key => $value) {
            if (!empty($value)) {
                $where .= $ct5 != 0 ? ' ||' : '';
                $where .= " estado LIKE '%$value%'";
            }
            $ct5++;
        }
        $where .= ") ";

        $urlparam .= 'estado=' . $estado . '&';
    }

    if (!empty($cidade)) {

        $ct6 = 0;
        $arr_cidades = explode('|', $cidade);

        $where .= " AND (";
        foreach ($arr_cidades as $key => $value) {
            if (!empty($value)) {
                $where .= $ct6 != 0 ? ' ||' : '';
                $where .= " cidade LIKE '%$value%' ";
            }
            $ct6++;
        }
        $where .= ") ";

        $urlparam .= 'cidade=' . $cidade . '&';
    }

    if (!empty($sexo)) {

        $ct7 = 0;
        $arr_sexo = explode('|', $sexo);

        $where .= " AND (";
        foreach ($arr_sexo as $key => $value) {
            if (!empty($value)) {
                $where .= $ct7 != 0 ? ' ||' : '';
                $where .= " sexo LIKE '%$value%' ";
            }
            $ct7++;
        }
        $where .= ") ";

        $urlparam .= 'sexo=' . $sexo . '&';
    }

    $sql .= $where . " ORDER BY id DESC LIMIT $inicio,$total_reg";

    $res_candidatos = mysqli_query($con, $sql) or die(mysqli_error($con));

    $sql_todos = "SELECT count(*) todos
                  FROM VW_CANDIDATOS WHERE 1=1 " . $where;


    $res_todos = mysqli_query($con, $sql_todos) or die(mysqli_error($con));
    $todos = mysqli_fetch_array($res_todos);

    $tr = $todos['todos'];
    $tp = $tr / $total_reg;

    $num_paginas = ceil($tr / $total_reg);

    ?>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-filtros-candidato.php"; ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <form class="search" action="<?php echo PATH_EMPRESAS ?>/buscar-profissional.php" method="get">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <input type="text" placeholder="Digite a área profissional, ou o cargo" class="vaga" id="cargo" name="cargo">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <select class="estado" name="estado" id="estadobusca">
                                    <option selected disabled value="">Selecione o estado</option>
                                    <?php
                                    $res_estado = mysqli_query($con, "SELECT nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                    while ($estado = mysqli_fetch_array($res_estado)) {

                                        ?>
                                        <option><?php echo $estado['nome'] ?></option>
                                    <?php } ?>
                                </select>
                                <button>
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="vagas">
                                <?php if (mysqli_num_rows($res_candidatos) == 0) { ?>
                                    <div class="form-area">
                                        <h3>Não há cadidatos cadastrados</h3>
                                    </div>
                                <?php } else { ?>
                                    <ul>
                                        <?php while ($row = mysqli_fetch_array($res_candidatos)) { ?>
                                            <li>
                                                <div class="ttl">
                                                    <figure>
                                                        <div class="fotoperfil">
                                                            <?php if ($row['foto']) { ?>
                                                                <img src="<?php echo PATH_CAN_IMAGENS . '/' . $row['foto'] ?>">
                                                                <?php
                                                            } else {

                                                                ?>
                                                                <img src="<?php echo PATH_IMAGENS . '/usuario.png' ?>">
                                                                <?php
                                                            }

                                                            ?>
                                                        </div>
                                                    </figure>

                                                    <div class="rt">
                                                        <h6><?php echo $row['nome'] ?></h6>
                                                        <div class="item">
                                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                            <?php echo $row['cargos']; ?>
                                                        </div>
                                                        <div class="item">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                            <?php echo $row['cidade'] . "/" . $row['uf'] ?>
                                                        </div>
                                                        <div class="item">
                                                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                                            <?php echo $row['idade'] . " anos" ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="info">
                                                    <p style="text-align: justify;">
                                                        <?php
                                                        echo str_replace("\n", "<br />", $row['obj_profissional']);

                                                        ?>
                                                    </p>
                                                    <a href="#" data-load-title="Visualizar Profissional" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_ALL . '/modais/ver-candidato.php?id=' . $row['id'] ?>" class="oportunidade">
                                                        Visualizar perfil completo <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                                <div class="list-flt">
                                    <div class="list-flt" style="text-align: center">
                                        <?php
                                        $anterior = $pag - 1;
                                        $proximo = $pag + 1;

                                        ?>

                                        <div class="pagination">
                                            <?php if (intval($num_paginas) > 1) { ?>
                                                <div class="pagnobord"><a href="<?php echo $urlparam . "pagina=0" ?>">Primeira</a></div>
                                                <?php
                                                if ($pag > 1) {

                                                    ?>
                                                    <div class="pagop"><a href="<?php echo $urlparam . "pagina=$anterior" ?>" class="ant"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></div>
                                                    <?php
                                                }

                                                $cont = 0;

                                                ?>

                                                <?php for ($i = 1; $i <= $num_paginas; $i++) { ?>
                                                    <?php if ($i > ($pagina - 3) && $cont < 10) { ?>
                                                        <?php if ($i == $pagina) { ?>
                                                            <div class="pagopac"><?php echo $i <= 9 ? '0' . $i : $i ?></div>
                                                        <?php } else { ?>
                                                            <div class="pagop"><a href="<?php echo $urlparam . "pagina=$i" ?>"><?php echo $i <= 9 ? '0' . $i : $i ?></a></div>

                                                            <?php
                                                            $cont++;
                                                        }
                                                    }

                                                    ?>
                                                <?php } ?>
                                                <?php
                                                if ($pag < $tp) {

                                                    ?>
                                                    <div class="pagop"><a href="<?php echo $urlparam . "pagina=$proximo" ?>" class="prox"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
                                                    <?php
                                                }

                                                ?>
                                                <div class="pagnobord"><a href="<?php echo $urlparam . "pagina=$num_paginas" ?>">Última</a></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php" ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
    <?php
}