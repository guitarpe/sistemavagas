<?php
include "../includes/conexao.php";
$func = new Funcoes();

$empresa = filter_input(INPUT_GET, 'empresa');
$cnpj = filter_input(INPUT_GET, 'cnpj');
$op = filter_input(INPUT_GET, 'op');

$subject = "Novo cadastro de empresa no Vagas e Vagas";

$message = 'Olá Leonardo,<br/><br/>
            Uma nova empresa se cadastrou no Portal de Vagas de Emprego Vagas e Vagas e aguarda sua verificação para que possa ser destravada no sistema e assim ter suas Vagas de Emprego divulgadas.​<br/><br/>
            Razão Social: ' . $empresa . '<br/>
            CNPJ: ' . $cnpj . '<br/><br/>
            Para destravar esta empresa, você deve acessar a área administrativa do Portal Vagas e Vagas.<br/><br/>
            Acesse <a href="' . HOME . '/admin/notificacoes-empresas.php"><b>aqui</b></a> a área administrativa!';

$tocopy = explode(", ", $configuracoes_sis['MAIL_COPY']);

$dados = [
    'HOST' => $configuracoes_sis['SMTP_HOST'],
    'USER' => $configuracoes_sis['SMTP_USER'],
    'PASS' => $configuracoes_sis['SMTP_PASS'],
    'PORT' => $configuracoes_sis['SMTP_PORT'],
    'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
    'FROM_TXT' => 'Vagas e Vagas',
    'TO_MAIL' => $configuracoes_sis['MAIL_COPY'],
    'TO_TXT' => $configuracoes_sis['COPY_TXT'],
    'SUBJECT' => $subject,
    'MENSAGE' => $message,
    'COPY' => 1
];

include_once '../includes/enviar-email.php';

if (isset($op) && intval($op) == 1) {
    header("Location:../index.php?cadastrado=true");
} else {
    header("Location:index.php");
}


