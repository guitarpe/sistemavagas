<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}
if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {
    $sql_vagas = "SELECT vg.id, vg.cargo, vg.status FROM TB_VV_VAGAS vg WHERE vg.id_empresa=" . $_SESSION['id'];

    $where = "";
    //paginação
    $total_reg = 20;

    $pagina = filter_input(INPUT_GET, "pagina");

    if (!isset($pagina)) {
        $pc = "1";
    } else {
        $pc = $pagina;
    }

    $ini = $pc - 1;
    $inicio = $ini * $total_reg;

    $busca = filter_input(INPUT_GET, "busca");

    if (isset($busca)) {

        $pcd = filter_input(INPUT_POST, "pcd");
        $status = filter_input(INPUT_POST, "status");
        $etapa = filter_input(INPUT_POST, "etapa");
        $carg_filtro = filter_input(INPUT_POST, "carg_filtro");

        if (isset($pcd) && $pcd != "") {
            if ($pcd == 1) {
                $where .= " AND (SELECT COUNT(*) FROM TB_VV_DEFICIENCIAS_VAGA dfv Where dfv.id_vaga=vg.id) > 0";
            } else {
                $where .= " AND (SELECT COUNT(*) FROM TB_VV_DEFICIENCIAS_VAGA dfv Where dfv.id_vaga=vg.id) = 0";
            }
        }

        if (isset($status) && $status != "") {
            if ($status == 1) {
                $where .= " AND vg.status = 1";
            } else {
                $where .= " AND vg.status = 0";
            }
        }

        if (isset($carg_filtro) && !empty($carg_filtro)) {
            $where .= " AND vg.cargo LIKE '%" . $carg_filtro . "%'";
        }
    }

    $res_vagas = mysqli_query($con, $sql_vagas . $where);
    $count_vagas = mysqli_num_rows($res_vagas);

    $sql_todos = "SELECT count(*) as todos FROM TB_VV_VAGAS vg WHERE vg.id_empresa=" . $_SESSION['id'];

    $res_todos = mysqli_query($con, $sql_todos . $where) or die(mysqli_error($con));
    $todos = mysqli_fetch_array($res_todos);

    $tr = $todos['todos'];
    $tp = $tr / $total_reg;

    $num_paginas = ceil($tr / $total_reg);

    ?>
    <!DOCTYPE html>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-empresa.php"; ?>
                            <script>$("#item3").addClass("active");</script>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <form class="search vagas" action="<?php echo PATH_EMPRESAS . '/gerenciar-vagas.php?busca=filtros' ?>" method="post">
                                <i class="fa fa-wheelchair" aria-hidden="true"></i>
                                <select name="pcd">
                                    <option value="">- PCD -</option>
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                <select name="status">
                                    <option value="">- Status -</option>
                                    <option value="1">Ativado</option>
                                    <option value="0">Desativado</option>
                                </select>
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <input type="text" name="carg_filtro" placeholder="Cargo" class="vaga">
                                <button class="fleft">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                                <button class="btn fleft clean" type="button">Limpar Busca</button>
                            </form>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                            <?php if ($count_vagas == 0) { ?>
                                <div class="form-area">
                                    <h3>Não há vagas cadastradas</h3>
                                </div>
                            <?php } else { ?>
                                <div class="form-area">
                                    <div class="responsive-table">
                                        <table class="vagas-list" border="0">
                                            <thead>
                                                <tr>
                                                    <th>Cargo</th>
                                                    <th width="60">Status</th>
                                                    <th width="100">Candidatos</th>
                                                    <th width="360"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vaga = mysqli_fetch_array($res_vagas)) {
                                                    $res_candidaturas = mysqli_query($con, "SELECT COUNT(data) FROM TB_VV_CANDIDATURAS "
                                                        . "WHERE id_vaga = $vaga[id] AND status <> -1");
                                                    $candidatos = mysqli_fetch_array($res_candidaturas, MYSQLI_NUM)[0];

                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="<?php echo PATH_ALL . '/ver-oportunidade.php?id=' . $vaga['id'] ?>">
                                                                <?php echo $vaga['cargo'] ?>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <?php if (intval($vaga["status"]) == 1) { ?>
                                                                <span class="btn btn-success btn-xs">
                                                                    Publicado
                                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                                </span>
                                                            <?php } else { ?>
                                                                <span class="btn btn-danger btn-xs">
                                                                    Não Publicado
                                                                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                                                </span>
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $candidatos ?>
                                                        </td>
                                                        <td>
                                                            <div class="btns-edit">
                                                                <a href="<?php echo PATH_EMPRESAS . '/editar-vaga.php?id=' . $vaga['id'] ?>" class=" btn buttone">
                                                                    <i class="fa fa-pencil" aria-hidden="true"></i> Editar
                                                                </a>
                                                                <?php if ($vaga["status"] == 0) { ?>
                                                                    <a href="<?php echo PATH_EMPRESAS ?>/actions/recebe_alterar_status_vaga.php?id=<?php echo $vaga['id'] ?>&status=1" class="btn buttone">
                                                                        <i class="fa fa-check-square-o" aria-hidden="true"></i> Publicar
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <a href="<?php echo PATH_EMPRESAS ?>/actions/recebe_alterar_status_vaga.php?id=<?php echo $vaga['id'] ?>&status=0" class="btn buttone">
                                                                        <i class="fa fa-times" aria-hidden="true"></i> Despublicar
                                                                    </a>
                                                                <?php } ?>
                                                                <a href="<?php echo PATH_EMPRESAS . '/gerenciar-candidatos.php?id=' . $vaga['id'] ?>" class=" btn buttone">
                                                                    <i class="fa fa-users" aria-hidden="true"></i> Gerenciar Candidatos
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="list-flt">
                                        <div class="list-flt" style="text-align: center">
                                            <?php
                                            $anterior = $pc - 1;
                                            $proximo = $pc + 1;
                                            $cont = 0;

                                            ?>

                                            <div class="pagination">
                                                <div class="pagnobord"><a href="<?php echo "?pagina=0" ?>">Primeira</a></div>
                                                <?php
                                                if ($pc > 1) {

                                                    ?>
                                                    <div class="pagop"><a href="<?php echo "?pagina=$anterior" ?>" class="ant"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></div>
                                                    <?php
                                                }

                                                ?>

                                                <?php for ($i = 1; $i <= $num_paginas; $i++) { ?>
                                                    <?php if ($i > ($pagina - 3) && $cont < 10) { ?>
                                                        <?php if ($i == $pagina) { ?>
                                                            <div class="pagopac"><?php echo $i <= 9 ? '0' . $i : $i ?></div>
                                                        <?php } else { ?>
                                                            <div class="pagop"><a href="<?php echo "?pagina=$i" ?>"><?php echo $i <= 9 ? '0' . $i : $i ?></a></div>

                                                            <?php
                                                            $cont++;
                                                        }
                                                    }

                                                    ?>
                                                <?php } ?>
                                                <?php
                                                if ($pc < $tp) {

                                                    ?>
                                                    <div class="pagop"><a href="<?php echo "?pagina=$proximo" ?>" class="prox"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
                                                    <?php
                                                }

                                                ?>
                                                <div class="pagnobord"><a href="<?php echo "?pagina=$num_paginas" ?>">Última</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <section class="publicidade clear">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php" ?>
            <section class="modal">
                <?php
                include "../includes/alerta_sair.php";

                ?>
            </section>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>

    <?php
}