<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

$tipo_cad = "empresa";

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $id = filter_input(INPUT_GET, 'id');

    $res_vaga = mysqli_query($con, "SELECT * FROM TB_VV_VAGAS WHERE id = $id") or die(mysqli_error($con));
    $vaga = mysqli_fetch_array($res_vaga);

    $res_empresa = mysqli_query($con, "SELECT cidade, estado  FROM TB_VV_EMPRESAS WHERE id = $_SESSION[id]") or die(mysqli_error($con));
    $empresa = mysqli_fetch_array($res_empresa);

    //benefícios
    $beneficios = array();
    $res_benef = mysqli_query($con, "SELECT descricao FROM TB_VV_BENEFICIOS_VAGA WHERE id_vaga=$id") or die(mysqli_error($con));
    while ($row = mysqli_fetch_array($res_benef)) {
        $beneficios[] = $row['descricao'];
    }

    //deficiencias
    $deficiencias = array();
    $res_defs_vaga_op = mysqli_query($con, "SELECT deficiencia FROM TB_VV_DEFICIENCIAS_VAGA WHERE id_vaga=$id") or die(mysqli_error($con));
    while ($row = mysqli_fetch_array($res_defs_vaga_op)) {
        $deficiencias[] = $row['deficiencia'];
    }

    //deficiencias
    $informaticas = array();
    $res_info_vaga_in = mysqli_query($con, "SELECT nome FROM TB_VV_INFORMATICA_VAGA WHERE id_vaga=$id") or die(mysqli_error($con));
    while ($row = mysqli_fetch_array($res_info_vaga_in)) {
        $informaticas[] = $row['nome'];
    }

    //idiomas vagas
    $res_idiomas_vagas = mysqli_query($con, "SELECT id, idioma, nivel FROM TB_VV_IDIOMAS_VAGA WHERE id_vaga=$id") or die(mysqli_error($con));

    ?>

    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-empresa.php"; ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                            <div class="form-area">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <h2>
                                            <strong>
                                                <?php
                                                if ($vaga["numero_vagas"] > 1) {
                                                    echo "$vaga[cargo] ($vaga[numero_vagas] Vagas) - $vaga[cidade] $vaga[estado]";
                                                } else {
                                                    echo "$vaga[cargo] ($vaga[numero_vagas] Vaga) - $vaga[cidade] $vaga[estado]";
                                                }

                                                ?>
                                            </strong>
                                        </h2>
                                    </div>
                                </div>

                                <form id="formcadvagas" class="cadvagas" action="<?php echo PATH_EMPRESAS . '/actions/recebe_alteravaga.php?id=' . $id ?>" method="post">
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-6 col-xs-12">
                                            <label><strong>Forma de Contratação</strong></label>
                                            <div class="form-group input-group">
                                                <select class="form-control" data-verify="1" id="formacontratacao" name="formacontratacao">
                                                    <option value="0">Forma de Contratação</option>
                                                    <?php
                                                    $res_formas = mysqli_query($con, "SELECT descricao FROM TB_VV_FORMAS_CONTRAT ORDER BY descricao ASC");
                                                    while ($row = mysqli_fetch_array($res_formas)) {
                                                        if ($vaga['forma_contratacao'] == $row['descricao']) {
                                                            echo "<option selected>$row[descricao]</option>";
                                                        } else {
                                                            echo "<option>$row[descricao]</option>";
                                                        }
                                                    }

                                                    ?>
                                                    <option>Outros</option>
                                                </select>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-file-text" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-xs-12">
                                            <label><strong>Nível Hierárquico</strong></label>
                                            <div class="form-group input-group">
                                                <select class="form-control" data-verify="1" id="nivelhierarquico" name="nivelhierarquico">
                                                    <option value="0">Nível Hierárquico</option>
                                                    <?php
                                                    $res_n_hierarq = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_HIERARQ ORDER BY descricao ASC");
                                                    while ($row = mysqli_fetch_array($res_n_hierarq)) {
                                                        if ($vaga['nivel_hierarquico'] == $row['descricao']) {
                                                            echo "<option selected>$row[descricao]</option>";
                                                        } else {
                                                            echo "<option>$row[descricao]</option>";
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="div_cargo_emp">
                                            <div class="col-sm-12 col-md-12 col-xs-12">
                                                <label><strong>Cargo</strong></label>
                                                <div class="form-group input-group">
                                                    <select class="form-control cargo" data-verify="1" id="cargo" name="cargo">
                                                        <option value="">Cargo Profissional</option>
                                                        <?php
                                                        $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                                        while ($row = mysqli_fetch_array($res_cargos)) {
                                                            if ($vaga['cargo'] == $row['nome']) {
                                                                echo "<option selected>$row[nome]</option>";
                                                            } else {
                                                                echo "<option>$row[nome]</option>";
                                                            }
                                                        }

                                                        ?>
                                                        <option>Outro</option>
                                                    </select>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <?php
                                            $res_outros_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS WHERE nome='" . $vaga['cargo'] . "' ORDER BY nome ASC");

                                            ?>
                                            <div class="col-sm-12 col-md-12 col-xs-12 outro_cargo <?php echo mysqli_num_rows($res_outros_cargos) == 0 ? '' : 'hidden' ?>">
                                                <label><strong>Outro Cargo</strong></label>
                                                <div class="form-group input-group">
                                                    <input type="text" class="form-control" data-verify="1" placeholder="Cadastre um novo cargo" name="cargooutro" id="novocargo" value="<?php echo $vaga['cargo'] ?>">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-xs-12">
                                            <label><strong>Número de Vagas</strong></label>
                                            <div class="form-group input-group">
                                                <input class="form-control numero" data-verify="1" type="text" placeholder="Número de Vagas" name="nvagas" id="nvagas" value="<?php echo $vaga['numero_vagas'] ?>">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-xs-12">
                                            <label><strong>Turno</strong></label>
                                            <div class="form-group input-group">
                                                <select class="form-control" data-verify="1" id="turno" name="turno">
                                                    <option value="0">Turno</option>
                                                    <?php
                                                    $res_turnos = mysqli_query($con, "SELECT descricao FROM TB_VV_TURNOS ORDER BY id ASC");
                                                    while ($row = mysqli_fetch_array($res_turnos)) {
                                                        if ($vaga['turno'] == $row['descricao']) {
                                                            echo "<option selected>$row[descricao]</option>";
                                                        } else {
                                                            echo "<option>$row[descricao]</option>";
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                            <label><strong>Atividades</strong></label>
                                            <textarea class="form-control" data-verify="1" placeholder="Atividades a serem desenvolvidas" name="atividades" id="atividades"><?php echo $vaga['atividades'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 msg">
                                            <strong>Remuneração e Benefícios</strong>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                            <label><strong>Remuneração</strong></label>
                                            <div class="form-group input-group">
                                                <select data-area="#area-faixa" data-verify="1" class="habilita form-control" name="faixasalarial" id="faixasalarial">
                                                    <option value="">Faixa Salarial</option>
                                                    <?php
                                                    $res_sal = mysqli_query($con, "SELECT descricao FROM TB_VV_FAIXAS_SALARIAL ORDER BY id ASC");
                                                    while ($row = mysqli_fetch_array($res_sal)) {
                                                        if ($vaga['faixa_salarial'] == $row['descricao']) {
                                                            echo "<option selected>$row[descricao]</option>";
                                                        } else {
                                                            echo "<option>$row[descricao]</option>";
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 <?php echo $vaga['faixa_salarial'] == "Salário Fixo" ? "" : "hidden"; ?>" id="area-faixa">
                                            <div class="form-group input-group">
                                                <input class="form-control dinheiro" data-verify="1" type="text" placeholder="Salário" name="vsalario" id="vsalario" value="<?php echo $vaga['salario'] ?>">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                            <strong>Benefícios</strong> - <span>Selecione abaixo os benefícios inclusos nesta Vaga de Emprego.</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 checks">
                                            <?php
                                            $res_beneficios = mysqli_query($con, "SELECT * FROM TB_VV_BENEFICIOS ORDER BY descricao ASC");
                                            while ($row = mysqli_fetch_array($res_beneficios)) {

                                                ?>
                                                <div class="form-check fleft width-32-p">
                                                    <input class="form-check-input" value="<?php echo $row['descricao'] ?>" type="checkbox" name="beneficios[]" <?php echo in_array($row['descricao'], $beneficios) ? 'checked="checked"' : '' ?>>
                                                    <label class="form-check-label" title="<?php echo $row['descricao'] ?>"><?php echo $row["descricao"] ?></label>
                                                </div>
                                                <?php
                                            }

                                            ?>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks">
                                            <strong>Selecione ao menos um</strong>
                                        </div>
                                        <input type="hidden" id="valbeneficios" name="valbeneficios"/>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                            <strong>Pré-requisitos da Vaga</strong>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12  area-radio-form">
                                            <label class="fleft">Sexo:</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" class="form-control-input" id="sexo0" name="sexo" value="Indiferente" <?php echo $vaga['sexo'] == "Indiferente" ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="sexo0">Indiferente</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" class="form-control-input" id="sexo1" name="sexo" value="Masculino" <?php echo $vaga['sexo'] == "Masculino" ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="sexo1">Masculino</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" class="form-control-input" id="sexo2" name="sexo" value="Feminino" <?php echo $vaga['sexo'] == "Feminino" ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="sexo2">Feminino</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                                            <label><strong>Experiência</strong></label>
                                            <div class="form-group input-group">
                                                <select class="form-control" data-verify="1" name="experienciamin" id="experienciamin">
                                                    <option value="">Experiência Mínima</option>
                                                    <?php
                                                    $res_exp = mysqli_query($con, "SELECT descricao FROM TB_VV_EXP_MINIMAS ORDER BY descricao ASC");
                                                    while ($row = mysqli_fetch_array($res_exp)) {
                                                        if ($vaga['experiencia_minima'] == $row['descricao']) {
                                                            echo "<option selected>$row[descricao]</option>";
                                                        } else {
                                                            echo "<option>$row[descricao]</option>";
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 col-md-7 col-lg-7 col-xs-12">
                                            <label><strong>Nível de Ensino</strong></label>
                                            <div class="form-group input-group">
                                                <select class="form-control" data-verify="1" name="ensinomin" id="ensinomin">
                                                    <option value="">Ensino Mínimo</option>
                                                    <?php
                                                    $res_nivel = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                                                    while ($row = mysqli_fetch_array($res_nivel)) {
                                                        if ($vaga['ensino_minimo'] == $row['descricao']) {
                                                            echo "<option selected>$row[descricao]</option>";
                                                        } else {
                                                            echo "<option>$row[descricao]</option>";
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $res_idiomavaga = mysqli_query($con, "SELECT idioma, nivel FROM TB_VV_IDIOMAS_VAGA WHERE id_vaga = $id");

                                    ?>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                            <label class="fleft" style="width: 330px">Será desejável conhecimento em língua estrangeira?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" data-area="#area-idiomas" class="habilita form-control-input" id="idiomanao" name="idiomas" value="Não" <?php echo mysqli_num_rows($res_idiomavaga) == 0 ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="idiomanao">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" data-area="#area-idiomas" class="habilita form-control-input" id="idiomasim" name="idiomas" value="Sim" <?php echo mysqli_num_rows($res_idiomavaga) > 0 ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="idiomasim">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="<?php echo mysqli_num_rows($res_idiomavaga) == 0 ? 'hidden' : '' ?>" id="area-idiomas">
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                                <strong>Idiomas</strong>
                                            </div>
                                            <div class="div_idioma">
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                                    <div class="form-group input-group">
                                                        <select class="form-control" name="idiomavaga[]">
                                                            <option value="">Idioma</option>
                                                            <?php
                                                            $res_idiomas = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                                                            while ($idioma = mysqli_fetch_array($res_idiomas)) {
                                                                echo "<option>$idioma[descricao]</option>";
                                                            }

                                                            ?>
                                                        </select>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-flag" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                                    <div class="form-group input-group">
                                                        <select class="form-control" name="nivelidiomavaga[]">
                                                            <option value="">Nível</option>
                                                            <?php
                                                            $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                                                            while ($nivel = mysqli_fetch_array($res_niveis)) {
                                                                echo "<option>$nivel[descricao]</option>";
                                                            }

                                                            ?>
                                                        </select>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-flag" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <button class="btn btn-success form-control btn-icon" id="adicionar_idioma" type="button">
                                                    Incluir Idioma
                                                </button>
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-danger form-control btn-icon" id="remover_idioma" type="button" style="display: none">
                                                    Remover Idioma
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" id="idiomasgrid">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                                    <strong>Idiomas cadastrados</strong>
                                                </div>
                                                <div class="col-sm-12 full">
                                                    <table class="edit" id="idiomasarea">
                                                        <?php
                                                        while ($row = mysqli_fetch_array($res_idiomas_vagas)) {

                                                            ?>

                                                            <tr>
                                                                <td><span><?php echo $row["idioma"] ?></span></td>
                                                                <td><span><?php echo $row["nivel"] ?></span></td>
                                                                <td class="width-80">
                                                                    <a href="#" class="btn btn-xs btn-danger remove_idioma" data-id="<?php echo $row['id'] ?>">
                                                                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                                        Remover
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }

                                                        ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">Vaga de Emprego para outro Estado ou Cidade?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" data-area="#area-cidades" class="habilita form-control-input" id="outroestnao" name="outroestadocidade" value="0" <?php echo $vaga['vaga_outrolocal'] == 0 ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="outroestnao">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" data-area="#area-cidades" class="habilita form-control-input" id="outroestsim" name="outroestadocidade" value="1" <?php echo $vaga['vaga_outrolocal'] == 1 ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="outroestsim">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="<?php echo $vaga['vaga_outrolocal'] == 1 ? '' : 'hidden' ?>" id="area-cidades">
                                        <div class="form-group row">
                                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                                <div class="form-group input-group">
                                                    <select class="form-control busca_cidades" name="estadovaga" id="estadovaga">
                                                        <option value="">Estado</option>
                                                        <?php
                                                        $res_estados = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                                        while ($estado = mysqli_fetch_array($res_estados)) {
                                                            if ($vaga['estado'] == $estado[uf]) {
                                                                echo "<option value='$estado[uf]' selected>$estado[nome]</option>";
                                                            } else {
                                                                echo "<option value='$estado[uf]'>$estado[nome]</option>";
                                                            }
                                                        }

                                                        ?>
                                                    </select>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                                <div class="form-group input-group">
                                                    <select class="form-control cidades" name="cidadevaga" id="cidadevaga">
                                                        <option value="">Cidade</option>
                                                        <?php
                                                        $sql_cidades = "SELECT cid.nome FROM TB_VV_CIDADES cid "
                                                            . "INNER JOIN TB_VV_ESTADOS est ON est.id = cid.id_estado "
                                                            . "WHERE est.uf = '" . $vaga['estado'] . "' ORDER BY cid.nome ASC";

                                                        $res = mysqli_query($con, $sql_cidades) or die(mysqli_error($con));

                                                        while ($row = mysqli_fetch_array($res)) {
                                                            if ($vaga['cidade'] == $row[nome]) {
                                                                echo "<option value='$row[nome]' selected>$row[nome]</option>";
                                                            } else {
                                                                echo "<option value='$row[nome]'>$row[nome]</option>";
                                                            }
                                                        }

                                                        ?>
                                                    </select>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">Tem disponibilidade para viajar?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="viajar1" name="viajar" value="0" class="form-control-input" <?php echo intval($vaga['viajar']) == 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="viajar1">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="viajar2" name="viajar" value="1" class="form-control-input" <?php echo intval($vaga['viajar']) == 1 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="viajar2">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">Possui veículo próprio?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="veiculo1" name="veiculo" value="0" class="form-control-input" checked="checked" <?php echo intval($vaga['veiculo_proprio']) == 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="veiculo1">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="veiculo2" name="veiculo" value="1" class="form-control-input" <?php echo intval($vaga['veiculo_proprio']) == 1 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="veiculo2">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $res_cnh = mysqli_query($con, "SELECT categoria FROM TB_VV_CNH WHERE id_vaga=$id") or die(mysqli_error($con));

                                    ?>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">Possui CNH?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="cnh1" name="cnh" value="Não" data-area="#area-cnh" class="habilita form-control-input" <?php echo mysqli_num_rows($res_cnh) == 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="cnh1">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="cnh2" name="cnh" value="Sim" data-area="#area-cnh" class="habilita form-control-input" <?php echo mysqli_num_rows($res_cnh) > 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="cnh2">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $res_cnh_cat = mysqli_query($con, "SELECT categoria FROM TB_VV_CNH WHERE id_vaga=$id") or die(mysqli_error($con));
                                    $arrcategoria = mysqli_fetch_array($res_cnh_cat);

                                    ?>
                                    <div id="area-cnh" class="form-group row <?php echo mysqli_num_rows($res_cnh) == 0 ? 'hidden' : '' ?>">
                                        <div class="col-sm-12 cnh area-radio-form">
                                            <label class="fleft width-300">Qual categoria deve ser a CNH:</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input class="form-control-input" name="tipocnh" type="radio" id="a" value="A" <?php echo $arrcategoria['categoria'] == 'A' ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="a">Categoria A</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input class="form-control-input" name="tipocnh" type="radio" id="b" value="B" <?php echo $arrcategoria['categoria'] == 'B' ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="b">Categoria B</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input class="form-control-input" name="tipocnh" type="radio" id="c" value="C" <?php echo $arrcategoria['categoria'] == 'C' ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="c">Categoria C</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input class="form-control-input" name="tipocnh" type="radio" id="d" value="D" <?php echo $arrcategoria['categoria'] == 'D' ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="d">Categoria D</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input class="form-control-input" name="tipocnh" type="radio" id="e" value="E" <?php echo $arrcategoria['categoria'] == 'E' ? 'checked="checked"' : '' ?>/>
                                                <label class="custom-control-label" for="e">Categoria E</label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $res_defs_vaga = mysqli_query($con, "SELECT deficiencia FROM TB_VV_DEFICIENCIAS_VAGA WHERE id_vaga=$id") or die(mysqli_error($con));

                                    ?>
                                    <div class="form-group row">
                                        <div class="col-sm-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">É uma Pessoa com Deficiência?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="deficiencia1" name="deficiencia" value="Não" data-area="#area-deficiencia" class="habilita form-control-input" <?php echo mysqli_num_rows($res_defs_vaga) == 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="deficiencia1">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="deficiencia2" name="deficiencia" value="Sim" data-area="#area-deficiencia" class="habilita form-control-input" <?php echo mysqli_num_rows($res_defs_vaga) > 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="deficiencia2">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="area-deficiencia" class="form-group row <?php echo mysqli_num_rows($res_defs_vaga) == 0 ? 'hidden' : '' ?>">
                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                            <label class="fleft">Tipos de Deficiência</label>
                                        </div>
                                        <?php
                                        $res_tipos = mysqli_query($con, "SELECT * FROM TB_VV_TIPOS_DEFICIENCIA");
                                        while ($row = mysqli_fetch_array($res_tipos)) {

                                            ?>
                                            <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                                                <label><?php echo $row["nome"] ?> </label>
                                            </div>
                                            <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                                                <?php
                                                $res_deficiencias = mysqli_query($con, "SELECT id, nome, descricao FROM TB_VV_DEFICIENCIAS WHERE id_tipo = $row[id]");

                                                while ($row2 = mysqli_fetch_array($res_deficiencias)) {

                                                    ?>
                                                    <div class="form-check fleft width-32-p">
                                                        <input class="form-check-input" id="def-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>" type="checkbox" name="deficiencias[]" <?php echo in_array($row2['id'], $deficiencias) ? 'checked="checked"' : '' ?>>
                                                        <label class="form-check-label" title="<?php echo $row2['descricao'] ?>"><?php echo $row2["nome"] ?></label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks2">
                                            <strong>Selecione ao menos um</strong>
                                        </div>
                                        <input type="hidden" id="valdeficiencias" name="valdeficiencias"/>
                                    </div>
                                    <?php
                                    $res_info_vaga = mysqli_query($con, "SELECT nome FROM TB_VV_INFORMATICA_VAGA WHERE id_vaga=$id") or die(mysqli_error($con));

                                    ?>
                                    <div class="form-group row">
                                        <div class="col-sm-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">Possui conhecimentos em Informática?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="informatica1" name="informatica" value="Não" data-area="#area-informatica" class="habilita form-control-input" <?php echo mysqli_num_rows($res_info_vaga) == 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="informatica1">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="informatica2" name="informatica" value="Sim" data-area="#area-informatica" class="habilita form-control-input" <?php echo mysqli_num_rows($res_info_vaga) > 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="informatica2">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="area-informatica" class="form-group row <?php echo mysqli_num_rows($res_info_vaga) == 0 ? 'hidden' : '' ?>">
                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                            <label class="fleft">Conhecimentos</label>
                                        </div>
                                        <?php
                                        $res_categorias = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFO_CATEGORIAS");
                                        while ($row = mysqli_fetch_array($res_categorias)) {

                                            ?>
                                            <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                                                <label><?php echo $row["nome"] ?> </label>
                                            </div>
                                            <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                                                <?php
                                                $res_informaticas = mysqli_query($con, "SELECT id,nome FROM TB_VV_INFORMATICA WHERE id_categoria = $row[id]");

                                                while ($row2 = mysqli_fetch_array($res_informaticas)) {

                                                    ?>
                                                    <div class="form-check fleft width-32-p">
                                                        <input class="form-check-input" type="checkbox" name="informaticas[]" id="info-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>"  <?php echo in_array($row2['id'], $informaticas) ? 'checked="checked"' : '' ?>>
                                                        <label class="form-check-label"><?php echo $row2["nome"] ?> </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks3">
                                            <strong>Selecione ao menos um</strong>
                                        </div>
                                        <input type="hidden" id="valconhecimentos" name="valconhecimentos"/>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                            <label class="fleft width-300" style="width: 330px">Visibilidade restrita para os dados da empresa?</label>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="deficiencia1" name="visibilidade" value="0" class="form-control-input" <?php echo intval($vaga['visibilidade_dados']) == 0 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="visibilidade1">Não</label>
                                            </div>
                                            <div class="form-radio form-control-inline fleft">
                                                <input type="radio" id="deficiencia2" name="visibilidade" value="1" class="form-control-input" <?php echo intval($vaga['visibilidade_dados']) == 1 ? 'checked="checked"' : '' ?>>
                                                <label class="custom-control-label" for="visibilidade2">Sim</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-6 text-right">
                                            <button class="btn btn-lg btn-success" id="publicar" type="submit">
                                                Atualizar Vaga <i class="fa fa-check" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
    <?php
}