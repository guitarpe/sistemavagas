<?php
session_start();

if ($_SESSION['tipo'] == 'empresa') {

    include "../includes/conexao.php";

    $res = mysqli_query($con, "SELECT data FROM TB_VV_CANDIDATURAS WHERE id_candidato = $_POST[id_candidato] AND id_vaga = $_POST[id_vaga]");

    if (mysqli_num_rows($res)) {
        echo false;
    } else {
        $data = date('Y-m-d');
        $sql = "INSERT INTO TB_VV_CANDIDATURAS(id_candidato, id_vaga, data, status) VALUES ($_POST[id_candidato], $_POST[id_vaga], '$data', 0)";
        $res = mysqli_query($con, $sql);
        echo true;
    }
}

$res = mysqli_query($con, "SELECT cargo, numero_vagas, cidade, estado FROM TB_VV_VAGAS WHERE id = $_POST[id_vaga]");
$vaga = mysqli_fetch_array($res);

if ($vaga['numero_vagas'] > 1) {
    $titulo = strtoupper("$vaga[cargo] ($vaga[numero_vagas] VAGAS) - $vaga[cidade] $vaga[estado]");
} else {
    $titulo = strtoupper("$vaga[cargo] ($vaga[numero_vagas] VAGA) - $vaga[cidade] $vaga[estado]");
}

$msg = "Temos a alegria de dizer que o seu currículo despertou interesse em uma empresa e está sendo analisado para a vaga $titulo.
	Agora depende apenas da análise da empresa para ser chamado para entrevista ou não.
	De qualquer forma desejamos todo sucesso do mundo para você e que dê tudo certo.
	Equipe Vagas&Vagas.";

date_default_timezone_set('America/Sao_Paulo');
$data = date("d/m/Y");
$hora = date('H:i');

$res = mysqli_query($con, "INSERT INTO TB_VV_MENSAGENS (id_candidato, titulo, mensagem, nome, email, data, hora, status) VALUES ($_POST[id_candidato], 'Candidatura', '$msg', 'Vagas&Vagas', 'vagasevagas.com.br', '$data', '$hora', 0)");

echo "$empresa[email];$empresa[nome_empresa];$vaga[cargo];$candidato[nome]";

?>