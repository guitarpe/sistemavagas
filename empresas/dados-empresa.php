<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}
if ($_SESSION['tipo'] != 'empresa') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {
    $res = mysqli_query($con, "SELECT * FROM TB_VV_EMPRESAS WHERE id = " . $_SESSION['id']);
    $dados = mysqli_fetch_array($res);

    $senhabanco = $func->dec_enc(2, $dados['senha']);

    $senhadec = explode('|', $senhabanco);
    $dados['senha'] = $senhadec[1];

    $tipo_cad = "empresa";

    ?>
    <html>
        <?php include "../includes/cabecalho.php"; ?>
        <body>
            <header>
                <?php include "../includes/navbar.php"; ?>
            </header>
            <section class="miolo-conteudo">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <?php include "includes/menu-empresa.php"; ?>
                            <script>$("#item1").addClass("active");</script>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi1['link'])) { ?>
                                        <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>

                            <div class="col-sm-12 form-area margin-top-10">
                                <div class="row">
                                    <form action="<?php echo PATH_ALL . '/empresas/actions/recebe_alterarempresa.php?id=' . $dados['id'] ?>" method="post" enctype="multipart/form-data" id="dados">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Nome da empresa" id="nomeempresa" name="nomeempresa" value="<?php echo $dados['nome_empresa'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-building" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input class="form-control cnpj" type="text" placeholder="CNPJ" id="cnpj" name="cnpj" value="<?php echo $dados['cnpj'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-id-card" aria-hidden="true"></i>
                                                    </span>
                                                    <span class="msg-ecnpj hidden"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Ramo de atividade" id="ramoatividade" name="ramoatividade" value="<?php echo $dados['ramo_atividade'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-gear" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Nome completo do usuário" id="nomeusuario" name="nomeusuario" value="<?php echo $dados['nome_usuario'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-user" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control validemail" placeholder="E-mail/login" id="email" name="email" value="<?php echo $dados['email'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    </span>
                                                    <span class="msg-eemail hidden"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input class="form-control" type="password" placeholder="Senha" id="senha" name="senha" value="<?php echo $dados['senha'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-asterisk" aria-hidden="true" title="ver" id="ver"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                                <label id="foto-perfil" title="Alterar Imagem" <?php echo!empty($dados['imagem']) ? 'style="border-color: rgb(204, 204, 204); background-image: url(' . PATH_EMP_IMAGENS . '/' . $dados['imagem'] . '); background-repeat: no-repeat; background-position: center center; background-color: rgb(255, 255, 255); background-size: contain;"' : '' ?>>
                                                    <div class="image">
                                                        <div class="preview preview-empresa">
                                                            <?php if (empty($dados['imagem'])) { ?>
                                                                <i class="fa fa-image" aria-hidden="true" id="img-icone"></i>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </label>
                                                <label class="quad3 file hidden">
                                                    <input type="file" value="Selecionar Imagem" id="imagem" name="imagem" class="ver_imagem">
                                                </label>
                                            </div>
                                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                                <textarea class="form-control" placeholder="Descrição da empresa" id="descricaoempresa" name="descricaoempresa" required><?php echo $dados['descricao'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Número de funcionários" id="nfuncionarios" name="nfuncionarios" value="<?php echo $dados['qtd_funcionarios'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-group" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Site" id="siteempresa" name="siteempresa" value="<?php echo $dados['site'] ?>">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-laptop" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control validemail" placeholder="E-mail de relacionamento da empresa" id="emailempresa" name="emailempresa" value="<?php echo $dados['email_empresa'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input class="form-control fones" type="text" placeholder="Telefone" id="foneempresa" name="foneempresa" value="<?php echo $dados['fone'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input class="form-control fones" type="text" placeholder="Celular (Opcional)" id="celular" name="celular" value="<?php echo $dados['fax'] ?>">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input class="form-control cep" type="text" placeholder="CEP" id="cepempresa" name="cepempresa" value="<?php echo $dados['cep'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCep.cfm" class="btn btn-warning btn-icon" type="button" target="_blank">
                                                    Não sabe seu CEP?
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Logradouro" id="logradouroempresa" name="logradouroempresa" value="<?php echo $dados['logradouro'] ?>">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-road" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Número" id="numero" name="numero" value="<?php echo $dados['numero'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-home" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Complemento" id="complemento" name="complemento" value="<?php echo $dados['complemento'] ?>">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Bairro" id="bairro" name="bairro" value="<?php echo $dados['bairro'] ?>" required>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <select name="estado" id="estado" class="form-control busca_cidades">
                                                        <option disabled value="">Estado</option>
                                                        <?php
                                                        $res_estados = mysqli_query($con, "SELECT nome, uf FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                                        while ($val = mysqli_fetch_array($res_estados)) {
                                                            if ($dados['estado'] == $val['uf']) {
                                                                echo "<option selected value='$val[uf]'>$val[nome]</option>";
                                                            } else {
                                                                echo "<option value='$val[uf]'>$val[nome]</option>";
                                                            }
                                                        }

                                                        ?>
                                                    </select>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <select name="cidade" class="form-control cidades" id="cidade" required>
                                                        <option disabled value="">Cidade</option>
                                                        <option><?php echo $dados['cidade'] ?></option>
                                                    </select>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <label>Receber notificações de candidaturas com o currículo do candidato</label>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <select name="recebe_curriculo" class="form-control" id="recebe_curriculo" required>
                                                        <option value="1" <?php echo $dados['candidaturas_curriculos'] == '1' ? 'selected' : '' ?>>Sim</option>
                                                        <option value="0" <?php echo $dados['candidaturas_curriculos'] == '0' ? 'selected' : '' ?>>Não</option>
                                                    </select>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 text-right">
                                                <button type="submit" class="btn btn-success btn-icon btn-lg">
                                                    Alterar cadastro <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <section class="publicidade">
                                <div class="container">
                                    <span>Publicidade</span>
                                    <?php if (!empty($publi2['link'])) { ?>
                                        <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                    <?php } else { ?>
                                        <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                    <?php } ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "../includes/footer.php"; ?>
            <?php include "../includes/rodape.php" ?>
        </body>
    </html>
    <?php
}