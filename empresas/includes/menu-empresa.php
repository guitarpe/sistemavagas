<nav class="off">
    <h4>
        <i class="fa fa-bars" aria-hidden="true"></i> Visualizar Menu da Empresa
    </h4>
    <ul>
        <li>
            <a href="." id="item0">
                <h5>
                    <i class="fa fa-home" aria-hidden="true"></i> Painel da Empresa
                </h5>
            </a>
        </li>
        <li>
            <a href="<?php echo PATH_ALL . '/empresas/dados-empresa.php' ?>" id="item1">
                <h5>
                    <i class="fa fa-gear" aria-hidden="true"></i> Dados da Empresa
                </h5>
            </a>
        </li>
        <li>
            <a href="#" id="item2" data-load-title="Anunciar vagas de emprego" data-toggle="modal" data-target="#modal-default" data-load-url="<?php echo PATH_EMPRESAS ?>/includes/cadastro-vagas.php">
                <h5>
                    <i class="fa fa-bullhorn" aria-hidden="true"></i> Anunciar Vaga
                </h5>
            </a>
        </li>
        <li>
            <a href="<?php echo PATH_ALL . '/empresas/gerenciar-vagas.php' ?>" id="item3">
                <h5>
                    <i class="fa fa-cogs" aria-hidden="true"></i> Gerenciar Vagas
                </h5>
            </a>
        </li>
        <li>
            <a href="<?php echo PATH_ALL . '/empresas/buscar-profissional.php' ?>" id="item5">
                <h5>
                    <i class="fa fa-search" aria-hidden="true"></i> Buscar Profissional
                </h5>
            </a>
        </li>
        <li>
            <a href="#" class="deactive">
                <h5>
                    <i class="fa fa-group" aria-hidden="true"></i> Profissionais Selecionados
                </h5>
            </a>
        </li>
        <li>
            <a href="#" class="deactive">
                <h5>
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Recomendar Profissional
                </h5>
            </a>
        </li>
        <li>
            <a href="#" class="deactive">
                <h5>
                    <i class="fa fa-line-chart" aria-hidden="true"></i> Estatísticas
                </h5>
            </a>
        </li>
    </ul>
</nav>
