<?php
$sql_filtro = "";
$arrcargos = [];
$arrestados = [];
$arrcidades = [];
$arrsexo = [];
$arrinformatica = [];
$arrescolaridade = [];
$arrdeficiencias = [];

$cargo = filter_input(INPUT_GET, 'cargo');
$escolaridade = filter_input(INPUT_GET, 'escolaridade');
$deficiencia = filter_input(INPUT_GET, 'deficiencia');
$informatica = filter_input(INPUT_GET, 'informatica');
$estado = filter_input(INPUT_GET, 'estado');
$cidade = filter_input(INPUT_GET, 'cidade');
$sexo = filter_input(INPUT_GET, 'sexo');

if (!empty($cargo)) {
    $arrcargos = explode('|', $cargo);
}

if (!empty($escolaridade)) {
    $arrescolaridade = explode('|', $escolaridade);
}

if (!empty($deficiencia)) {
    $arrdeficiencias = explode('|', $deficiencia);
}

if (!empty($informatica)) {
    $arrinformatica = explode('|', $informatica);
}

if (!empty($estado)) {
    $arrestados = explode('|', $estado);
}

if (!empty($cidade)) {
    $arrcidades = explode('|', $cidade);
}

if (!empty($sexo)) {
    $arrsexo = explode('|', $sexo);
}

$cargos = array_filter(array_unique($arrcargos), function($value) {
    return $value !== '';
});

$escolaridades = array_filter(array_unique($arrescolaridade), function($value) {
    return $value !== '';
});

$deficiencias = array_filter(array_unique($arrdeficiencias), function($value) {
    return $value !== '';
});

$informaticas = array_filter(array_unique($arrinformatica), function($value) {
    return $value !== '';
});

$estados = array_filter(array_unique($arrestados), function($value) {
    return $value !== '';
});

$cidades = array_filter(array_unique($arrcidades), function($value) {
    return $value !== '';
});

$sexos = array_filter(array_unique($arrsexo), function($value) {
    return $value !== '';
});

if (intval($todos['todos']) == 0) {
    $sql_filtro = "";
} else {
    $sql_filtro .= $where;
}

?>
<div class="filtro-mobile off">
    <form action="<?php echo PATH_EMPRESAS ?>/buscar-profissional.php" method="get">
        <h4>
            <i class="fa fa-filter" aria-hidden="true"></i> Filtro de Pesquisa
        </h4>
        <div class=" pesquisa-mobile">
            <div class="filter" id="filtros" style="display:none">
                <h5>
                    <i class="fa fa-filter" aria-hidden="true"></i> Filtros
                </h5>
            </div>
            <div class="filter close" id="filtro1">
                <h5>
                    <i class="fa fa-briefcase" aria-hidden="true"></i> Cargo
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_cargos = mysqli_query($con, "SELECT
                                                cargos, sum(ct_cargos) as total
                                            FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by cargos");
                            if (mysqli_num_rows($res_cargos) > 0) {
                                while ($row = mysqli_fetch_array($res_cargos)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#cargo" data-tipo="1" class="cbk" value="<?php echo $row['cargos'] ?>" <?= in_array($row['cargos'], $cargos) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['cargos'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="cargo" id="cargo" value="<?php echo $cargo ?>"/>
                    </div>
                </div>
            </div>
            <div class="filter close" id="filtro2">
                <h5>
                    <i class="fa fa-map" aria-hidden="true"></i> Estado
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_estado = mysqli_query($con, "SELECT
                                                estado, sum(ct_estado) as total
                                            FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by estado");
                            if (mysqli_num_rows($res_estado) > 0) {
                                while ($row = mysqli_fetch_array($res_estado)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#estado" data-tipo="2" class="cbk" value="<?php echo $row['estado'] ?>" <?= in_array($row['estado'], $estados) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['estado'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="estado" id="estado" value="<?php echo $estado ?>"/>
                    </div>
                </div>
            </div>
            <div class="filter close" id="filtro3">
                <h5>
                    <i class="fa fa-map-marker" aria-hidden="true"></i> Cidade
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_cidade = mysqli_query($con, "SELECT
                                                    cidade, sum(ct_cidade) as total
                                                FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by cidade");
                            if (mysqli_num_rows($res_cidade) > 0) {
                                while ($row = mysqli_fetch_array($res_cidade)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#cidade" data-tipo="11" class="cbk" value="<?php echo $row['cidade'] ?>" <?= in_array($row['cidade'], $cidades) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['cidade'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="cidade" id="cidade" value="<?php echo $cidade ?>"/>
                    </div>
                </div>
            </div>
            <div class="filter close" id="filtro4">
                <h5>
                    <i class="fa fa-venus-mars" aria-hidden="true"></i> Sexo
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_sexo = mysqli_query($con, "SELECT
                                                sexo, sum(ct_sexo) as total
                                            FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by sexo");
                            if (mysqli_num_rows($res_sexo) > 0) {
                                while ($row = mysqli_fetch_array($res_sexo)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#sexo" data-tipo="10" class="cbk" value="<?php echo $row['sexo'] ?>" <?= in_array($row['sexo'], $sexos) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['sexo'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="sexo" id="sexo" value="<?php echo $sexo ?>"/>
                    </div>
                </div>
            </div>
            <div class="filter close" id="filtro5">
                <h5>
                    <i class="fa fa-laptop" aria-hidden="true"></i> Informática
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_info = mysqli_query($con, "SELECT
                                                informatica, sum(ct_informatica) as total
                                            FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by informatica");
                            if (mysqli_num_rows($res_info) > 0) {
                                while ($row = mysqli_fetch_array($res_info)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#informatica" data-tipo="9" class="cbk" value="<?php echo $row['informatica'] ?>" <?= in_array($row['informatica'], $informaticas) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['informatica'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="informatica" id="informatica" value="<?php echo $informatica ?>"/>
                    </div>
                </div>
            </div>
            <div class="filter close" id="filtro6">
                <h5>
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i> Escolaridade
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_niveis = mysqli_query($con, "SELECT
                                                escolaridade, sum(ct_escolaridade) as total
                                            FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by escolaridade");
                            if (mysqli_num_rows($res_niveis) > 0) {
                                while ($row = mysqli_fetch_array($res_niveis)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#escolaridade" data-tipo="8" class="cbk" value="<?php echo $row['escolaridade'] ?>" <?= in_array($row['escolaridade'], $escolaridades) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['escolaridade'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="escolaridade" id="escolaridade" value="<?php echo $escolaridade ?>"/>
                    </div>
                </div>
            </div>
            <div class="filter close" id="filtro7">
                <h5>
                    <i class="fa fa-wheelchair" aria-hidden="true"></i> PCD
                </h5>
                <div class="labels">
                    <div class="area_filtro">
                        <ul>
                            <?php
                            $res_pcd = mysqli_query($con, "SELECT
                                                deficiencias, sum(ct_deficiencias) as total
                                            FROM VW_CANDIDATOS_FILTRO WHERE 1=1 " . $sql_filtro . " group by deficiencias");

                            if (mysqli_num_rows($res_pcd) > 0) {
                                while ($row = mysqli_fetch_array($res_pcd)) {
                                    if (intval($row['total']) > 0) {

                                        ?>
                                        <li><label class="filtrar"><input type="checkbox" data-id="#deficiencias" data-tipo="12" class="cbk" value="<?php echo $row['deficiencias'] ?>" <?= in_array($row['deficiencias'], $deficiencias) ? 'checked="checked"' : '' ?>/> <span><?php echo $row['deficiencias'] . " (" . $row['total'] . ")" ?></span></label></li>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                        <input type="hidden" name="deficiencia" id="deficiencia" value="<?php echo $deficiencia ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="filter" id="filtro13">
            <button class="btn btn-filtro" type="submit">
                Filtrar
            </button>
        </div>
    </form>
</div>
