<?php
$res_vagas = mysqli_query($con, "SELECT * FROM TB_VV_VAGAS WHERE id_empresa = $_SESSION[id]");

?>

<style type="text/css">
    .modal .box button.cancelar {
        margin-right:10px;
        background-color: white;
        color: #710082;
    }

    .modal .box button.cancelar:hover {
        margin-right:10px;
        background-color: #710082;
        color: white;
    }

    .vagas-candidato {
        max-height: 150px;
        overflow: auto;
        margin-bottom: 5px;
    }

    .aviso input {
        margin-left: 10px;
    }

    .opaco {
        opacity: 0.8;
    }
</style>

<div class="box small" id="selecionar-vaga" style="width: 600px">
    <button class="close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <h2>
        Selecione a Vaga
    </h2>

    <form class="active">
        <div class="full vagas-candidato">
            <?php
            $key = false;
            while ($vaga = mysqli_fetch_array($res_vagas)) {
                $res_sele = mysqli_query($con, "SELECT data FROM TB_VV_CANDIDATURAS WHERE id_vaga = $vaga[id] AND id_candidato = $_GET[id]");
                $num = mysqli_num_rows($res_sele);

                ?>
                <div>
                    <?php if ($num > 0) { ?>
                        <input type="radio" name="vaga" value="<?php echo $vaga['id'] ?>" disabled>
                        <span class="opaco"><?php echo $vaga["cargo"] ?> - Currículo já enviado</span>
                        <?php
                    } else {
                        if ($key) {

                            ?>
                            <input type="radio" name="vaga" value="<?php echo $vaga['id'] ?>">
                            <span><?php echo $vaga["cargo"] ?></span>
                        <?php } else { ?>
                            <input type="radio" name="vaga" value="<?php echo $vaga['id'] ?>" checked>
                            <span><?php echo $vaga["cargo"] ?></span>
                            <?php
                        } $key = true;
                    }

                    ?>
                </div>
            <?php } ?>
        </div>

        <div class="aviso text-center">
            Deseja que o candidato receba um aviso que seu currículo está sendo analisado para esta vaga?<br/>
            <input type="radio" name="avisar" value="sim" checked>
            <span>Sim</span>
            <input type="radio" name="avisar" value="nao">
            <span>Não</span>
        </div>

        <div class="full text-center" style="margin-top:20px">
            <button type="button" onclick="selecionarCandidato(<?php echo $_GET['id'] ?>)" >
                Continuar <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
            <button class="cancelar" type="button" onclick="$('.close').click()">Cancelar</button>
        </div>
    </form>
</div>