<?php
include_once '../../includes/defines.php';
include_once "../../includes/conexao.php";

?>
<div id="cadastro-vagas">
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="col-sm-2 active mod">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Dados Básico">
                        <span class="round-tab">
                            <i class="fa fa-address-card"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="col-sm-2 disabled mod">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Benefícios">
                        <span class="round-tab">
                            <i class="fa fa-dollar"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="col-sm-2 disabled mod">
                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Pré Requisitos">
                        <span class="round-tab">
                            <i class="fa fa-list"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="col-sm-2 disabled mod">
                    <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Dados Adicionais">
                        <span class="round-tab">
                            <i class="fa fa-star"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <form id="formcadvagas" class="cadvagas" method="POST" action="<?php echo PATH_EMPRESAS . '/actions/recebe_anunciovaga.php' ?>">
            <div class="tab-content">
                <div class="tab-pane tbp-mod active" role="tabpanel" id="step1">
                    <div class="row top-modal">
                        <div class="col-sm-12 col-md-12 col-xs-12">
                            <p>Olá! Vamos cadastrar sua Vaga de Emprego, a começar pelos dados básicos. Se não encontrar o cargo de sua vaga em nossa lista de cargos selecione Outro e solicite um novo cargo.</p>
                        </div>
                    </div>
                    <div class="corpo-modal">
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control" data-verify="1" id="formacontratacao" name="formacontratacao">
                                        <option value="0">Forma de Contratação</option>
                                        <?php
                                        $res_formas = mysqli_query($con, "SELECT descricao FROM TB_VV_FORMAS_CONTRAT ORDER BY descricao ASC");
                                        while ($formas = mysqli_fetch_array($res_formas)) {
                                            echo "<option>$formas[descricao]</option>";
                                        }

                                        ?>
                                        <option>Outros</option>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-file-text" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control" data-verify="1" id="nivelhierarquico" name="nivelhierarquico">
                                        <option value="0">Nível Hierárquico</option>
                                        <?php
                                        $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_HIERARQ ORDER BY descricao ASC");
                                        while ($niveis = mysqli_fetch_array($res_niveis)) {
                                            echo "<option>$niveis[descricao]</option>";
                                        }

                                        ?>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="div_cargo_emp">
                                <div class="col-sm-12 col-md-12 col-xs-12">
                                    <div class="form-group input-group">
                                        <select class="form-control cargo" data-verify="1" id="cargo" name="cargo">
                                            <option value="">Cargo Profissional</option>
                                            <?php
                                            $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                            while ($cargo = mysqli_fetch_array($res_cargos)) {
                                                echo "<option>$cargo[nome]</option>";
                                            }

                                            ?>
                                            <option>Outro</option>
                                        </select>
                                        <span class="input-group-addon">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-xs-12 outro_cargo hidden">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" data-verify="1" placeholder="Cadastre um novo cargo" name="cargooutro" id="novocargo">
                                        <span class="input-group-addon">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <div class="form-group input-group">
                                    <input class="form-control numero" data-verify="1" type="text" placeholder="Número de Vagas" name="nvagas" id="nvagas">
                                    <span class="input-group-addon">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control" data-verify="1" id="turno" name="turno">
                                        <option value="0">Turno</option>
                                        <?php
                                        $res_turnos = mysqli_query($con, "SELECT descricao FROM TB_VV_TURNOS ORDER BY id ASC");
                                        while ($turnos = mysqli_fetch_array($res_turnos)) {
                                            echo "<option>$turnos[descricao]</option>";
                                        }

                                        ?>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-xs-12">
                                <textarea class="form-control" data-verify="1" placeholder="Atividades a serem desenvolvidas" name="atividades" id="atividades"></textarea>
                            </div>
                        </div>
                    </div>
                    </br>
                    <ul class="list-inline">
                        <li class="pull-right">
                            <button type="button" class="btn btn-lg btn-primary next-step">
                                Próximo passo <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane tbp-mod" role="tabpanel" id="step2">
                    <div class="row top-modal">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 msg">
                            <p>Cadastrada a Vaga de Emprego, precisamos saber a remuneração e se terá benefícios também.</p>
                        </div>
                    </div>
                    <div class="corpo-modal">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <strong>Remuneração</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                <div class="form-group input-group">
                                    <select data-area="#area-faixamod" data-verify="1" class="habilita form-control" name="faixasalarial" id="faixasalarial">
                                        <option value="">Faixa Salarial</option>
                                        <?php
                                        $res_sal = mysqli_query($con, "SELECT descricao FROM TB_VV_FAIXAS_SALARIAL ORDER BY id ASC");
                                        while ($sal = mysqli_fetch_array($res_sal)) {
                                            echo '<option value="' . $sal['descricao'] . '">' . $sal['descricao'] . '</option>';
                                        }

                                        ?>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-dollar" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 hidden" id="area-faixamod">
                                <div class="form-group input-group">
                                    <input class="form-control dinheiro" data-verify="1" type="text" placeholder="Salário" name="vsalario" id="vsalario">
                                    <span class="input-group-addon">
                                        <i class="fa fa-dollar" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <strong>Benefícios</strong> - <span>Selecione abaixo os benefícios inclusos nesta Vaga de Emprego.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 checks">
                                <?php
                                $res_beneficios = mysqli_query($con, "SELECT * FROM TB_VV_BENEFICIOS ORDER BY descricao ASC");
                                while ($row = mysqli_fetch_array($res_beneficios)) {

                                    ?>
                                    <div class="form-check fleft width-32-p">
                                        <input class="form-check-input" value="<?php echo $row['descricao'] ?>" type="checkbox" name="beneficios[]">
                                        <label class="form-check-label" title="<?php echo $row['descricao'] ?>"><?php echo $row["descricao"] ?></label>
                                    </div>
                                    <?php
                                }

                                ?>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks">
                                <strong>Selecione ao menos um</strong>
                            </div>
                            <input type="hidden" id="valbeneficios" name="valbeneficios"/>
                        </div>
                    </div>
                    </br>
                    <ul class="list-inline">
                        <li class="pull-left">
                            <button type="button" class="btn btn-lg btn-default prev-step">
                                <span class="glyphicon glyphicon-chevron-left"></span> Passo anterior
                            </button>
                        </li>
                        <li class="pull-right">
                            <button type="button" class="btn btn-lg btn-primary next-step">
                                Próximo passo <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane tbp-mod" role="tabpanel" id="step3">
                    <div class="row top-modal">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 msg">
                            <p>Ok! Agora precisamos saber os pré-requisitos da Vaga de Emprego.</p>
                        </div>
                    </div>
                    <div class="corpo-modal">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft">Sexo:</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" class="form-control-input" id="sexo0" name="sexo" value="Indiferente" checked="checked"/>
                                    <label class="custom-control-label" for="sexo0">Indiferente</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" class="form-control-input" id="sexo1" name="sexo" value="Masculino"/>
                                    <label class="custom-control-label" for="sexo1">Masculino</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" class="form-control-input" id="sexo2" name="sexo" value="Feminino"/>
                                    <label class="custom-control-label" for="sexo2">Feminino</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control" data-verify="1" name="experienciamin" id="experienciamin">
                                        <option value="">Experiência Mínima</option>
                                        <?php
                                        $res_exp = mysqli_query($con, "SELECT descricao FROM TB_VV_EXP_MINIMAS ORDER BY descricao ASC");
                                        while ($exp = mysqli_fetch_array($res_exp)) {
                                            echo '<option value="' . $exp['descricao'] . '">' . $exp['descricao'] . '</option>';
                                        }

                                        ?>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-7 col-md-7 col-lg-7 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control" data-verify="1" name="ensinomin" id="ensinomin">
                                        <option value="">Ensino Mínimo</option>
                                        <?php
                                        $res_nivel = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                                        while ($nivel = mysqli_fetch_array($res_nivel)) {
                                            echo '<option value="' . $nivel['descricao'] . '">' . $nivel['descricao'] . '</option>';
                                        }

                                        ?>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft">Será desejável conhecimento em língua estrangeira?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" data-area="#area-idiomasmod" class="habilita form-control-input" id="idiomanao" name="idiomas" value="Não" checked="checked"/>
                                    <label class="custom-control-label" for="idiomanao">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" data-area="#area-idiomasmod" class="habilita form-control-input" id="idiomasim" name="idiomas" value="Sim"/>
                                    <label class="custom-control-label" for="idiomasim">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="hidden" id="area-idiomasmod" >
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                    <strong>Idiomas</strong>
                                </div>
                                <div class="div_idioma">
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                        <div class="form-group input-group">
                                            <select class="form-control" name="idiomavaga[]">
                                                <option value="">Idioma</option>
                                                <?php
                                                $res_idiomas = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                                                while ($idioma = mysqli_fetch_array($res_idiomas)) {
                                                    echo "<option>$idioma[descricao]</option>";
                                                }

                                                ?>
                                            </select>
                                            <span class="input-group-addon">
                                                <i class="fa fa-flag" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                        <div class="form-group input-group">
                                            <select class="form-control" name="nivelidiomavaga[]">
                                                <option value="">Nível</option>
                                                <?php
                                                $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                                                while ($nivel = mysqli_fetch_array($res_niveis)) {
                                                    echo "<option>$nivel[descricao]</option>";
                                                }

                                                ?>
                                            </select>
                                            <span class="input-group-addon">
                                                <i class="fa fa-flag" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                    <div class="col-sm-2">
                                        <button class="btn btn-xs btn-success form-control btn-icon mini" id="adicionar_idioma" type="button">
                                            Incluir Idioma
                                        </button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-xs btn-danger form-control btn-icon mini" id="remover_idioma" type="button" style="display: none">
                                            Remover Idioma
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </br>
                    <ul class="list-inline">
                        <li class="pull-left">
                            <button type="button" class="btn btn-lg btn-default prev-step">
                                <span class="glyphicon glyphicon-chevron-left"></span> Passo anterior
                            </button>
                        </li>
                        <li class="pull-right">
                            <button type="button" class="btn btn-lg btn-primary next-step">
                                Próximo passo <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane tbp-mod" role="tabpanel" id="step4">
                    <div class="row top-modal">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 msg">
                            <p>Mais alguns dados complementares e finalizamos.</p>
                        </div>
                    </div>
                    <div class="corpo-modal">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">Vaga de Emprego para outro Estado ou Cidade?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" data-area="#area-cidadesmod" class="habilita form-control-input" id="outroestnao" name="outroestadocidade" value="0" checked="checked"/>
                                    <label class="custom-control-label" for="outroestnao">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" data-area="#area-cidadesmod" class="habilita form-control-input" id="outroestsim" name="outroestadocidade" value="1"/>
                                    <label class="custom-control-label" for="outroestsim">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div class="row hidden" id="area-cidadesmod">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control busca_cidades" name="estadovaga" id="estadovaga">
                                        <option value="">Estado</option>
                                        <?php
                                        $res_estados = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                        while ($estado = mysqli_fetch_array($res_estados)) {
                                            echo "<option value='$estado[uf]'>$estado[nome]</option>";
                                        }

                                        ?>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-map" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                <div class="form-group input-group">
                                    <select class="form-control cidades" name="cidadevaga" id="cidadevaga">
                                        <option value="">Cidade</option>
                                    </select>
                                    <span class="input-group-addon">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">Tem disponibilidade para viajar?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="viajar1" name="viajar" value="0" class="form-control-input" checked="checked">
                                    <label class="custom-control-label" for="viajar1">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="viajar2" name="viajar" value="1" class="form-control-input">
                                    <label class="custom-control-label" for="viajar2">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">Possui veículo próprio?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="veiculo1" name="veiculo" value="0" class="form-control-input" checked="checked">
                                    <label class="custom-control-label" for="veiculo1">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="veiculo2" name="veiculo" value="1" class="form-control-input">
                                    <label class="custom-control-label" for="veiculo2">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">Possui CNH?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="cnh1" name="cnh" value="Não" data-area="#area-cnhmod" class="habilita form-control-input" checked="checked">
                                    <label class="custom-control-label" for="cnh1">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="cnh2" name="cnh" value="Sim" data-area="#area-cnhmod" class="habilita form-control-input">
                                    <label class="custom-control-label" for="cnh2">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div id="area-cnhmod" class="form-group row hidden">
                            <div class="col-sm-12 cnh area-radio-form">
                                <label class="fleft width-300">Qual categoria deve ser a CNH:</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input class="form-control-input" name="tipocnh" type="radio" id="a" value="A" checked="checked"/>
                                    <label class="custom-control-label" for="a">Categoria A</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input class="form-control-input" name="tipocnh" type="radio" id="b" value="B"/>
                                    <label class="custom-control-label" for="b">Categoria B</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input class="form-control-input" name="tipocnh" type="radio" id="c" value="C"/>
                                    <label class="custom-control-label" for="c">Categoria C</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input class="form-control-input" name="tipocnh" type="radio" id="d" value="D"/>
                                    <label class="custom-control-label" for="d">Categoria D</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input class="form-control-input" name="tipocnh" type="radio" id="e" value="E"/>
                                    <label class="custom-control-label" for="e">Categoria E</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">É uma Pessoa com Deficiência?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="deficiencia1" name="deficiencia" value="Não" data-area="#area-deficienciamod" class="habilita form-control-input" checked="checked">
                                    <label class="custom-control-label" for="deficiencia1">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="deficiencia2" name="deficiencia" value="Sim" data-area="#area-deficienciamod" class="habilita form-control-input">
                                    <label class="custom-control-label" for="deficiencia2">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div id="area-deficienciamod" class="form-group row hidden">
                            <div class="col-sm-12 col-md-12 col-xs-12">
                                <label class="fleft">Tipos de Deficiência</label>
                            </div>
                            <?php
                            $res_tipos = mysqli_query($con, "SELECT * FROM TB_VV_TIPOS_DEFICIENCIA");
                            while ($row = mysqli_fetch_array($res_tipos)) {

                                ?>
                                <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                                    <label><?php echo $row["nome"] ?> </label>
                                </div>
                                <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                                    <?php
                                    if (!empty($row['id'])) {
                                        $res_deficiencias = mysqli_query($con, "SELECT id, nome, descricao FROM TB_VV_DEFICIENCIAS WHERE id_tipo=" . $row['id']);

                                        while ($row2 = mysqli_fetch_array($res_deficiencias)) {

                                            ?>
                                            <div class="form-check fleft width-32-p">
                                                <input class="form-check-input" id="def-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>" type="checkbox" name="deficiencias[]">
                                                <label class="form-check-label" title="<?php echo $row2['descricao'] ?>"><?php echo $row2["nome"] ?></label>
                                            </div>
                                            <?php
                                        }
                                    }

                                    ?>
                                </div>
                            <?php } ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks2">
                                <strong>Selecione ao menos um</strong>
                            </div>
                            <input type="hidden" id="valdeficiencias" name="valdeficiencias"/>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">Possui conhecimentos em Informática?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="informatica1" name="informatica" value="Não" data-area="#area-informaticamod" class="habilita form-control-input" checked="checked">
                                    <label class="custom-control-label" for="informatica1">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="informatica2" name="informatica" value="Sim" data-area="#area-informaticamod" class="habilita form-control-input">
                                    <label class="custom-control-label" for="informatica2">Sim</label>
                                </div>
                            </div>
                        </div>
                        <div id="area-informaticamod" class="form-group row hidden">
                            <div class="col-sm-12 col-md-12 col-xs-12">
                                <label class="fleft">Conhecimentos</label>
                            </div>
                            <?php
                            $res_categorias = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFO_CATEGORIAS");
                            while ($row = mysqli_fetch_array($res_categorias)) {

                                ?>
                                <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                                    <label><?php echo $row["nome"] ?> </label>
                                </div>
                                <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                                    <?php
                                    $res_informaticas = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFORMATICA WHERE id_categoria = $row[id]");

                                    while ($row2 = mysqli_fetch_array($res_informaticas)) {

                                        ?>
                                        <div class="form-check fleft width-32-p">
                                            <input class="form-check-input" type="checkbox" name="informaticas[]" id="info-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>">
                                            <label class="form-check-label"><?php echo $row2["nome"] ?> </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks3">
                                <strong>Selecione ao menos um</strong>
                            </div>
                            <input type="hidden" id="valconhecimentos" name="valconhecimentos"/>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                                <label class="fleft width-300" style="width: 330px">Visibilidade restrita para os dados da empresa?</label>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="visibilidade1" name="visibilidade" value="0" class="form-control-input" checked="checked">
                                    <label class="custom-control-label" for="visibilidade1">Não</label>
                                </div>
                                <div class="form-radio form-control-inline fleft">
                                    <input type="radio" id="visibilidade2" name="visibilidade" value="1" class="form-control-input">
                                    <label class="custom-control-label" for="visibilidade2">Sim</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    </br>
                    <ul class="list-inline">
                        <li class="pull-left">
                            <button type="button" class="btn btn-lg btn-default prev-step">
                                <span class="glyphicon glyphicon-chevron-left"></span> Passo anterior
                            </button>
                        </li>
                        <li class="pull-right">
                            <button type="submit" class="btn btn-lg btn-primary btn-info-full">
                                Finalizar Cadastro <span class="glyphicon glyphicon-ok"></span>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?= PATH_ASSETS ?>/js/geral/wizard.js"></script>
