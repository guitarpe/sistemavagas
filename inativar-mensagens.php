<?php
include "includes/conexao.php";

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');

$sql = "UPDATE TB_VV_USUARIOS SET recebe_email=0 WHERE id=$id AND tipo=$tipo";

$res = mysqli_query($con, $sql);

?>
<html>
    <?php include "includes/cabecalho.php" ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>

        <section class="miolo-conteudo">
            <div class="area-institucional">

                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi1['link'])) { ?>
                            <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
                <section class="institucional">
                    <h1 class="text-center">Mensagens automáticas</h1>
                    <p class="text-center">
                    <div class="box-body">
                        <div class="container">
                            <h3>Não será enviado mais nenhuma mensagem automática para o e-mail informado.</h3>
                        </div>
                    </div>
                    </p>
                </section>

                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi2['link'])) { ?>
                            <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>