<?php
session_start();

include "includes/conexao.php";

$id = filter_input(INPUT_GET, 'id');

$sql = "SELECT
            bl.id_blog, bl.imagem, bl.titulo, bl.descricao, bl.data, bl.texto, cat.nome
        FROM TB_VV_BLOGS bl
            INNER JOIN TB_VV_CATEGORIAS_BLOG cat ON cat.id_cat=bl.categoria
        WHERE bl.status=1 AND cat.status=1 AND id_blog=" . $id;

$res_blogs = mysqli_query($con, $sql)or die(mysqli_error($con));
$blog = mysqli_fetch_array($res_blogs);

$diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');

$mes = array(
    '0' => '',
    '01' => 'Janeiro',
    '02' => 'Fevereiro',
    '03' => 'Março',
    '04' => 'Abril',
    '05' => 'Maio',
    '06' => 'Junho',
    '07' => 'Julho',
    '08' => 'Agosto',
    '09' => 'Setembro',
    '10' => 'Outubro',
    '11' => 'Novembro',
    '12' => 'Dezembro'
);

?>

<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi1['link'])) { ?>
                                    <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-12">
                        <section class="empresas">
                            <?php if (mysqli_num_rows($res_blogs) > 0) { ?>
                                <div class="col-md-9">
                                    <div class="blog">
                                        <div class="col-md-12">
                                            <h2><?php echo $blog['titulo'] ?></h2>
                                        </div>
                                        <?php
                                        $diasemana_numero = date('w', strtotime($blog['data']));
                                        $mes_numero = date('m', strtotime($blog['data']));
                                        $dia = date('d', strtotime($blog['data']));
                                        $ano = date('Y', strtotime($blog['data']));

                                        ?>

                                        <div class="col-md-12">
                                            <i class="fa fa-calendar"></i>
                                            <span><?php echo $diasemana[$diasemana_numero] . ", " . $dia . " de " . $mes[$mes_numero] . " de " . $ano ?></span>
                                            <i class="fa fa-tag"></i>
                                            <span><?php echo $blog['nome'] ?></span>
                                        </div>
                                        <div class="col-md-12">
                                            <img class="img-responsive" src="images/<?php echo $blog['imagem'] ?>">
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <button class="btn-blog" type="button" onclick="javascript:history.go(-1);">
                                                    <i class="fa fa-arrow-left"></i> voltar
                                                </button>
                                            </div>
                                            <div class="col-md-8" align="right">
                                                Tamanho da Fonte &nbsp;
                                                <button class="btn-blog" type="button" id="fontminus">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                                <button class="btn-blog" type="button" id="fontplus">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-12 m-bt-20" id="text-blog">
                                            <?php echo $blog['texto'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 lt">
                                    <div class="item share redes-sociais">
                                        <h3><strong>Compartilhe</strong></h3>
                                        <a href="<?php echo 'https://www.linkedin.com/shareArticle?mini=true&url=' . URL . DIRETORIO . '/blog.php?id=' . $blog['id_blog'] . '&title=' . $blog['titulo'] . '&summary=' . $blog['descricao'] . '&source=' . URL . DIRETORIO . '/blog.php?id=' . $blog['id_blog'] ?>" target="_blank" title="Compartilhar no Linkedin">
                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                        <a href="<?php echo 'https://www.facebook.com/sharer/sharer.php?u= ' . URL . DIRETORIO . '/blog.php?id=' . $blog['id_blog'] . '&summary=' . $blog['descricao'] . '&title=Blog: ' . $blog['titulo'] . '&description=' . $blog['descricao'] . '&picture=' . URL . PATH_ALL . '/images/' . $blog['imagem'] ?>" target="_blank" title="Compartilhar no Facebook">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                        <a href="https://wa.me/?text=<?php echo URL . DIRETORIO . '/blog.php?id=' . $blog['id_blog'] ?>" data-action="share/whatsapp/share" target="_blank" title="Compartilhar no Whattsapp">
                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="comentarios">
                                    <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" ></div>
                                </div>
                            <?php } else { ?>
                                <div class="col-sm-12 text-center">
                                    <h3>Dados Indisponíveis</h3>
                                </div>
                            <?php } ?>
                        </section>
                    </div>
                    <div class="col-sm-12">
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi2['link'])) { ?>
                                    <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
