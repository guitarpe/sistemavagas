<?php
include "includes/conexao.php";

?>

<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="publicidade">
                    <div class="container">
                        <div class="col-sm-12">
                            <span>Publicidade</span>
                            <?php if (!empty($publi1['link'])) { ?>
                                <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </section>
                <section class="institucional">
                    <div class="box-body h-490 text-center">
                        <h2>Senha registrada com sucesso!<br> Agora você já pode realizar o login e e navegar no site</h2>
                    </div>
                </section>
                <section class="publicidade">
                    <div class="container">
                        <div class="col-sm-12">
                            <span>Publicidade</span>
                            <?php if (!empty($publi2['link'])) { ?>
                                <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                            <?php } else { ?>
                                <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>