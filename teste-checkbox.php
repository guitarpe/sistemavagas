<form action="teste-checkbox.php" method="get">
    Which buildings do you want access to?<br />
    <input type="checkbox" name="formDoor[0]" value="A" />Acorn Building<br />
    <input type="checkbox" name="formDoor[1]" value="B" />Brown Hall<br />
    <input type="checkbox" name="formDoor[2]" value="C" />Carnegie Complex<br />
    <input type="checkbox" name="formDoor[3]" value="D" />Drake Commons<br />
    <input type="checkbox" name="formDoor[4]" value="E" />Elliot House

    <input type="submit" name="formSubmit" value="Submit" />
</form>

<?php
$aDoor = $_GET['formDoor'];
if (empty($aDoor)) {
    echo("You didn't select any buildings.");
} else {
    $N = count($aDoor);

    echo("You selected $N door(s): ");
    for ($i = 0; $i < $N; $i++) {
        echo($aDoor[$i] . " ");
    }
}

?>