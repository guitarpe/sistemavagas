<?php
session_start();

include "includes/conexao.php";

$res = mysqli_query($con, "SELECT * FROM TB_VV_INSTITUCIONAL WHERE id_institucional = 1");
$institucional = mysqli_fetch_array($res);

$codigo = filter_input(INPUT_GET, 'codigo');
$email = filter_input(INPUT_GET, 'email');
$tipo = filter_input(INPUT_GET, 'tipo');

?>
<html>
    <?php include "includes/cabecalho.php" ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi1['link'])) { ?>
                            <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
                <section class="institucional">
                    <h1 class="text-center">Recuperação de senha</h1>
                    <div class="box-body">
                        <div class="container">
                            <div class="col-sm-4 col-sm-offset-4">
                                <form action="<?php echo PATH_ALL . '/alterarsenha.php' ?>" method="post">
                                    <input type="hidden" name="tipo" value="<?php echo $tipo ?>"/>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <fieldset class="form-group">
                                                <label>E-mail</label>
                                                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" placeholder="Digite seu E-mail" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <fieldset class="form-group">
                                                <label>Código</label>
                                                <input type="text" name="codigo" class="form-control" value="<?php echo $codigo; ?>" placeholder="Digite o Codigo" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <fieldset class="form-group">
                                                <label>Nova Senha</label>
                                                <input type="password" name="senha" class="form-control txtSenha" placeholder="Digite a Senha" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <fieldset class="form-group">
                                                <label>Confirmar Nova Senha</label>
                                                <input type="password" name="confirmacao" class="form-control confisenha" placeholder="Confirme a Senha" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div align="center"><button type="submit" class="btn btn-success">Recuperar Senha</button></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi2['link'])) { ?>
                            <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
