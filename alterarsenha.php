<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');

include "includes/conexao.php";

$func = new Funcoes();

$codigo = filter_input(INPUT_POST, 'codigo');
$email = filter_input(INPUT_POST, 'email');
$senha = filter_input(INPUT_POST, 'confirmacao');
$tipo = filter_input(INPUT_POST, 'tipo');
$data = date('Y-m-d');
$hora = date('H:i:s');

$res_select = mysqli_query($con, "SELECT id, tipo, email, nome  FROM TB_VV_USUARIOS WHERE email = '$email' AND tipo='$tipo'");
$usu = mysqli_fetch_array($res_select);

$idus = $usu['id'];
$senhausuario = $func->dec_enc(1, $usu['tipo'] . '|' . $senha . '|' . $email);

$sql_recuperacao = mysqli_query($con, "select * FROM TB_VV_RECUPERACAO where codigo = '$codigo' and email = '$email' and data = '$data'");
$vetor = mysqli_fetch_array($sql_recuperacao);

?>
<html>
    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi1['link'])) { ?>
                            <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
                <section class="institucional">
                    <h1 class="text-center">Recuperação de senha</h1>
                    <p class="text-center">
                    <div class="box-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <?php
                                    if (mysqli_num_rows($sql_recuperacao) == 0) {
                                        echo "<h4>Seu código de recuperação não existe ou já expirou, <a href='esqueceuasenha.php'>clique aqui</a> e peça outro envio de senha.</h4>";
                                    } else {
                                        $sql_altera = mysqli_query($con, "UPDATE TB_VV_USUARIOS SET senha='$senhausuario' where email='$email' AND tipo='$tipo' AND id=$idus");
                                        echo "<h4>Senha alterada com sucesso!!!</h4>";
                                    }

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi2['link'])) { ?>
                            <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
            </div>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
