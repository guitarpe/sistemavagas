<?php
session_start();

include "includes/conexao.php";

$func = new Funcoes();

$email = filter_input(INPUT_POST, 'user');
$senha = filter_input(INPUT_POST, 'password');
$tipo = filter_input(INPUT_POST, 'tipo');

$sql = "SELECT * FROM TB_VV_USUARIOS WHERE email='$email' and tipo='$tipo' and status='1'";
$res = mysqli_query($con, $sql);
$qregistros = mysqli_num_rows($res);

if ($qregistros) {

    $array = mysqli_fetch_array($res);

    $senhausuario = $func->dec_enc(1, $array['tipo'] . '|' . $senha . '|' . $email);

    if ($array['status'] == '0') {
        header("Location:usu-inativo.php");
    } elseif ($array['senha'] == $senhausuario) {

        if ($array['tipo'] == 'candidato') {

            $_SESSION['id'] = $array['id'];
            $_SESSION['nome'] = $array['nome'];
            $_SESSION['email'] = $array['email'];
            $_SESSION['senha'] = $array['senha'];
            $_SESSION['tipo'] = $array['tipo'];

            $func->redir("candidatos");
        } elseif ($array['tipo'] == 'empresa') {

            $_SESSION['id'] = $array['id'];
            $_SESSION['nome'] = $array['nome'];
            $_SESSION['email'] = $array['email'];
            $_SESSION['senha'] = $array['senha'];
            $_SESSION['tipo'] = $array['tipo'];

            $func->redir("empresas");
        } elseif ($array['tipo'] == 'admin') {

            $_SESSION['id'] = $array['id'];
            $_SESSION['nome'] = $array['nome'];
            $_SESSION['email'] = $array['email'];
            $_SESSION['senha'] = $array['senha'];
            $_SESSION['tipo'] = $array['tipo'];

            //setar as vagas expiradas para inativas
            $sql_up_vagas = "UPDATE TB_VV_VAGAS SET status=2 WHERE DATE(data_expira) < DATE(NOW())";
            $res_up = mysqli_query($con, $sql_up_vagas);

            $func->redir("admin");
        }
    } else {
        $situacao = 'msg-usuario-senha-inv';
        $func->alert($situacao, 'acao');
        $func->redir("index.php");
    }
} else {
    $situacao = 'msg-usuario-senha-inv';
    $func->alert($situacao, 'acao');
    $func->redir("index.php");
}

