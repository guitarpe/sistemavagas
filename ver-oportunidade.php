<?php
session_start();
include "includes/conexao.php";
$func = new Funcoes();

if (isset($_SESSION["id"]) && $_SESSION["tipo"]) {
    if ($_SESSION["tipo"] == "candidato") {
        $res = mysqli_query($con, "SELECT data FROM TB_VV_CANDIDATURAS WHERE id_candidato = $_SESSION[id] AND id_vaga = $_GET[id]");
        $isCandidato = mysqli_num_rows($res);
    }
}

$id = filter_input(INPUT_GET, "id");

$sql = "SELECT
            id, estado, nome, cargo, cidade, nivel_hierarquico, viajar, veiculo_proprio, categoria,
            faixa_salarial, salario, forma_contratacao, turno, ensino_minimo, nome_empresa,
            numero_vagas, experiencia_minima, data_anuncio, status, data_expira, atividades, atividades_full
        FROM VW_VAGAS
        WHERE id=$id";
$res_vagas = mysqli_query($con, $sql);
$r_vaga = mysqli_query($con, $sql);

$oportunidade = mysqli_fetch_array($r_vaga);

$description = $oportunidade['cargo'];
$title = $oportunidade['atividades'];
$url = URL . DIRETORIO . '/ver-oportunidade.php?id=' . $oportunidade['id'];

$sql_todos = "SELECT
                    count(*) as todos
                FROM VW_VAGAS
                WHERE 1=1 ";
$res_todos = mysqli_query($con, $sql_todos) or die(mysqli_error($con));
$todos = mysqli_fetch_array($res_todos);

?>

<html>

    <?php include "includes/cabecalho.php"; ?>
    <body>
        <header>
            <?php include "includes/navbar.php"; ?>
        </header>
        <section class="miolo-conteudo">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <?php include "includes/vagas-filtro.php"; ?>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi1['link'])) { ?>
                                    <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>
                        <div class="vagas">
                            <ul>
                                <?php while ($vaga = mysqli_fetch_array($res_vagas)) { ?>
                                    <li>
                                        <div class="ttl">
                                            <div class="rt">
                                                <?php
                                                $titulo = $vaga["cargo"];
                                                if ($vaga["numero_vagas"] == 1) {
                                                    $titulo .= " (1 VAGA) - " . $vaga['cidade'] . ' ' . $vaga['estado'];
                                                } else {
                                                    $titulo .= " (" . $vaga['numero_vagas'] . " VAGAS) - " . $vaga['cidade'] . ' ' . $vaga['estado'];
                                                }

                                                ?>
                                                <h6><?php echo $titulo; ?></h6>
                                                <div class="item">
                                                    <i class="fa fa-bookmark" aria-hidden="true"></i>
                                                    <?php echo $vaga['nome_empresa']; ?>
                                                </div>
                                                <div class="item">
                                                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                    <?php echo $vaga['forma_contratacao']; ?>
                                                </div>
                                                <div class="item">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <?php echo $vaga['cidade'] . '/' . $vaga['estado']; ?>
                                                </div>
                                            </div>
                                            <div class="lt">
                                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                                <?php echo $func->formataData($vaga['data_anuncio']); ?>
                                                <div class="item share" style="margin-top: 80px;">
                                                    <a href="<?php echo 'https://www.linkedin.com/shareArticle?mini=true&url=' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] . '&title=' . $titulo . '&summary=' . $vaga['atividades'] . '&source=' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" target="_blank" title="Compartilhar no Linkedin">
                                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="<?php echo 'https://www.facebook.com/sharer/sharer.php?u= ' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] . '&summary=' . $vaga['atividades'] . '&title=Vaga: ' . $titulo . '&description=' . $vaga['atividades'] . '&picture=' . URL . PATH_ASSETS . '/img/logo-menor.jpg' ?>" target="_blank" title="Compartilhar no Facebook">
                                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="https://wa.me/?text=<?php echo 'Vaga: ' . $titulo . '. Segue o link: ' . URL . DIRETORIO . '/ver-oportunidade.php?id=' . $vaga['id'] ?>" data-action="share/whatsapp/share" target="_blank" title="Compartilhar no Whattsapp">
                                                        <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info">
                                            <p><?php echo $vaga['atividades_full'] ?></p>
                                            <div class="caracteristicas">
                                                <div class="col">
                                                    <strong>
                                                        <i class="fa fa-file-text" aria-hidden="true"></i> Forma de contratação: <?php echo $vaga['forma_contratacao']; ?>
                                                    </strong>
                                                    <strong>
                                                        <i class="fa fa-briefcase" aria-hidden="true"></i> Cargo: <?php echo $vaga['cargo']; ?>
                                                    </strong>
                                                    <strong>
                                                        <i class="fa fa-info" aria-hidden="true"></i>
                                                        Quantidade de vagas: <?php echo $vaga['numero_vagas']; ?>
                                                    </strong>
                                                    <strong>
                                                        <i class="fa fa fa-sort-amount-asc" aria-hidden="true"></i> Nível hierárquico: <?php echo $vaga['nivel_hierarquico'] ?>
                                                    </strong>
                                                    <strong>
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Turno: <?php echo $vaga['turno']; ?>
                                                    </strong>
                                                    <strong>
                                                        <i class="fa fa-dollar" aria-hidden="true"></i>
                                                        <?php
                                                        if ($vaga['faixa_salarial'] == "Salário Fixo") {
                                                            echo "Remuneração: R$ " . str_replace(".", ",", $vaga['salario']);
                                                        } else {
                                                            echo "Remuneração: " . $vaga['faixa_salarial'];
                                                        }

                                                        ?>
                                                    </strong>

                                                    <?php if (intval($vaga['viajar']) == 1) { ?>
                                                        <strong>
                                                            <i class="fa fa-bus" aria-hidden="true"></i>
                                                            Disponibilidade para viajar
                                                        </strong>
                                                    <?php } ?>

                                                    <?php if (intval($vaga['veiculo_proprio']) == 1) { ?>
                                                        <strong>
                                                            <i class="fa fa-car" aria-hidden="true"></i>
                                                            Deve possuir veículo próprio
                                                        </strong>
                                                    <?php } ?>

                                                    <?php if (!empty($vaga['categoria'])) { ?>
                                                        <strong>
                                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                            Possuir CNH com categoria mínima <?php echo $vaga['categoria'] ?>
                                                        </strong>
                                                    <?php } ?>

                                                    <?php
                                                    $query_idioma = "SELECT * FROM TB_VV_IDIOMAS_VAGA WHERE id_vaga=" . $vaga['id'];
                                                    $sql_idioma = mysqli_query($con, $query_idioma);

                                                    if (mysqli_num_rows($sql_idioma) > 0) {
                                                        while ($vgidioma = mysqli_fetch_array($sql_idioma)) {

                                                            ?>
                                                            <strong>
                                                                <i class="fa fa-map-o" aria-hidden="true"></i>
                                                                Conhecimento <?php echo $vgidioma['nivel']; ?> em <?php echo $vgidioma['idioma']; ?>
                                                            </strong>
                                                            <?php
                                                        }
                                                    }

                                                    ?>
                                                </div>
                                                <div class="col">
                                                    <strong>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        Experiência mínima:
                                                        <?php echo $vaga['experiencia_minima']; ?>
                                                    </strong>
                                                    <strong>
                                                        <i class="fa fa-graduation-cap" aria-hidden="true"></i> Escolaridade mínima:
                                                        <?php echo $vaga['ensino_minimo']; ?>
                                                    </strong>
                                                    <?php
                                                    $res_benficios = mysqli_query($con, "SELECT descricao FROM TB_VV_BENEFICIOS_VAGA WHERE id_vaga = $_GET[id]");
                                                    if (mysqli_num_rows($res_benficios)) {

                                                        ?>
                                                        <strong>
                                                            <?php
                                                            $beneficios = "";
                                                            while ($beneficio = mysqli_fetch_array($res_benficios)) {
                                                                $beneficios .= $beneficio['descricao'] . ", ";
                                                            }

                                                            ?>
                                                            <i class="fa fa-diamond" aria-hidden="true"></i>
                                                            Benefícios para a vaga: <?php echo chop($beneficios, ", "); ?>
                                                        </strong>
                                                        <?php
                                                    }
                                                    $res_deficiencias = mysqli_query($con, "SELECT nome FROM TB_VV_DEFICIENCIAS_VAGA dv INNER JOIN TB_VV_DEFICIENCIAS df ON df.id=dv.deficiencia WHERE id_vaga=$_GET[id]");
                                                    if (mysqli_num_rows($res_deficiencias)) {

                                                        ?>
                                                        <strong>
                                                            <?php
                                                            $deficiencias = "";
                                                            while ($deficiencia = mysqli_fetch_array($res_deficiencias)) {
                                                                $deficiencias .= $deficiencia['nome'] . ", ";
                                                            }

                                                            ?>
                                                            <i class="fa fa-wheelchair" aria-hidden="true"></i>
                                                            Vaga para Deficientes do tipo: <?php echo chop($deficiencias, ", "); ?>
                                                        </strong>
                                                        <?php
                                                    }
                                                    $res_informaticas = mysqli_query($con, "SELECT inf.nome FROM TB_VV_INFORMATICA_VAGA iv INNER JOIN TB_VV_INFORMATICA inf ON inf.id=iv.nome WHERE id_vaga=$_GET[id]");
                                                    if (mysqli_num_rows($res_informaticas)) {

                                                        ?>
                                                        <strong>
                                                            <?php
                                                            $informaticas = "";
                                                            while ($informatica = mysqli_fetch_array($res_informaticas)) {
                                                                $informaticas .= $informatica['nome'] . ", ";
                                                            }

                                                            ?>
                                                            <i class="fa fa-laptop" aria-hidden="true"></i>
                                                            Conhecimentos em Informática: <?php echo chop($informaticas, ", "); ?>
                                                        </strong>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="foot">
                                            <?php if (isset($isCandidato)) { ?>
                                                <?php if ($isCandidato) { ?>
                                                    <a href="#" class="right" style="font-size: 25px;margin:5px" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                        <i class="fa fa-times-rectangle-o cor-red"></i>
                                                    </a>
                                                    <a href="#" class="btn-enviado" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-mini" data-load-url="<?php echo PATH_ALL ?>/modais/alerta_excluir_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                        <i class="fa fa-check-square-o"></i>
                                                        Currículo Enviado
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="#" class="btn" data-load-title="Candidatura" data-toggle="modal" data-target="#modal-default-alert" data-load-url="<?php echo PATH_CANDIDATOS ?>/actions/recebe_candidatura.php?vaga=<?php echo $vaga['id'] ?>&usuario=<?php echo $_SESSION['id']; ?>">
                                                        Quero esta vaga!
                                                    </a>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <a href="#" class="btn-login" data-load-title="Faça seu login" data-vaga="<?php echo $vaga["id"] ?>" data-toggle="modal" data-target="#modal-default-md" data-load-url="<?php echo PATH_ALL ?>/modais/entrar.php">
                                                    Quero esta vaga!
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <section class="publicidade">
                            <div class="container">
                                <span>Publicidade</span>
                                <?php if (!empty($publi2['link'])) { ?>
                                    <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                                <?php } else { ?>
                                    <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                                <?php } ?>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </section>

        <?php include "includes/footer.php" ?>

        <section class="modal">
            <?php
            ;

            ?>
        </section>
        <?php include "includes/rodape.php" ?>
    </body>

</html>

<?php if (isset($_GET['candidatura'])) { ?>
    <script>
        $('.modal').addClass('active');
        $('.modal .box').removeClass('active');
        $('#candidatura-success').addClass('active');
    </script>
<?php } ?>