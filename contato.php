<?php
session_start();

include "includes/conexao.php";

$sql_perguntas = mysqli_query($con, "select * FROM TB_VV_FAQ ORDER by id_pergunta DESC");

?>

<!DOCTYPE html>
<html>
    <?php include "includes/cabecalho.php" ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <section class="publicidade">
                <div class="container">
                    <div class="col-sm-12">
                        <span>Publicidade</span>
                        <?php if (!empty($publi1['link'])) { ?>
                            <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                        <?php } ?>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="form-area">
                    <h2 class="title">Perguntas Frequentes:</h2>
                    <div class="ol-md-12" align="center">
                        <?php while ($row = mysqli_fetch_array($sql_perguntas)) { ?>
                            <a href="<?php echo '#pergunta' . $row['id_pergunta']; ?>" data-toggle="collapse">
                                <h3><?php echo $row['pergunta']; ?></h3>
                            </a>
                            <div id="<?php echo 'pergunta' . $row['id_pergunta']; ?>" class="collapse">
                                <div class="respostas"><?php echo $row['resposta']; ?></div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
            <section class="publicidade">
                <div class="container">
                    <div class="col-sm-12">
                        <span>Publicidade</span>
                        <?php if (!empty($publi2['link'])) { ?>
                            <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                        <?php } ?>
                    </div>
                </div>
            </section>
        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
