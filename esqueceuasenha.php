<?php
session_start();

include "includes/conexao.php";

$res = mysqli_query($con, "SELECT * FROM TB_VV_INSTITUCIONAL WHERE id_institucional = 1");
$institucional = mysqli_fetch_array($res);

?>

<html>
    <?php include "includes/cabecalho.php" ?>
    <body>
        <header>
            <?php include "includes/navbar.php" ?>
        </header>
        <section class="miolo-conteudo">
            <div class="area-institucional">
                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi1['link'])) { ?>
                            <a href="<?php echo $publi1['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi1['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>
                <section class="institucional">
                    <h1 class="text-center">Esqueceu sua senha?</h1>
                    <div class="box-body">
                        <div class="container">
                            <form action="<?php echo PATH_ALL . '/enviasenha.php' ?>" method="post">
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-xs-12">
                                        <div class="form-group row">
                                            <div class="input-group">
                                                <input type="text" name="email" class="form-control" placeholder="Digite seu e-mail de contato" required>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 area-radio-form">
                                                <label class="fleft">Tipo</label>
                                                <div class="form-radio form-control-inline fleft">
                                                    <input type="radio" id="emp" name="tipo" value="empresa" class="form-control-input" checked="checked">
                                                    <label class="custom-control-label" for="mas">Empresa</label>
                                                </div>
                                                <div class="form-radio form-control-inline fleft">
                                                    <input type="radio" id="can" name="tipo" value="candidato" class="form-control-input">
                                                    <label class="custom-control-label" for="fem">Candidato</label>
                                                </div>
                                                <div class="form-radio form-control-inline fleft">
                                                    <input type="radio" id="adm" name="tipo" value="admin" class="form-control-input">
                                                    <label class="custom-control-label" for="fem">Adminstrativo</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row text-center">
                                            <button type="submit" class="btn button btn-tipo">
                                                Recuperar Senha
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>

                <section class="publicidade">
                    <div class="container">
                        <span>Publicidade</span>
                        <?php if (!empty($publi2['link'])) { ?>
                            <a href="<?php echo $publi2['link'] ?>" target="_blank"><img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>"></a>
                        <?php } else { ?>
                            <img src="<?php echo PATH_IMAGENS . '/' . $publi2['imagem'] ?>">
                        <?php } ?>
                    </div>
                </section>

            </div>

        </section>
        <?php include "includes/footer.php" ?>
        <?php include "includes/rodape.php" ?>
    </body>
</html>
