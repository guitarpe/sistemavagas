<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

$id = filter_input(INPUT_GET, "id");

if ($id == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    $res = mysqli_query($con, "SELECT distinct
                                    can.id,
                                    can.email,
                                    can.nome,
                                    can.cpf,
                                    can.nascimento,
                                    can.sexo,
                                    can.celular,
                                    can.fone,
                                    can.estado,
                                    can.cidade,
                                    can.foto,
                                    can.estado_civil,
                                    coalesce(can.filhos, 0) as filhos,
                                    can.cep,
                                    can.logradouro,
                                    can.numero,
                                    can.complemento,
                                    can.bairro,
									can.pretensao_salarial,
									can.viajar as viajar,
									(CASE WHEN cn.id > 0 THEN 1 ELSE 0 END) AS cnh,
                                    cn.categoria as cnhtipo,
                                    can.veiculo as veiculo,
                                    can.obj_profissional,
                                    (SELECT COUNT(*) FROM TB_VV_EXP_PROFISSIONAIS exp WHERE id_candidato=can.id) as exp,
                                    (CASE WHEN dc.id > 0 THEN 1 ELSE 0 END) AS deficiencia,
                                    (CASE WHEN ic.id IS NOT NULL THEN 1 ELSE 0 END) AS informatica
							  FROM TB_VV_CANDIDATOS can
                                  LEFT OUTER JOIN TB_VV_DEFICIENCIAS_CAND dc ON dc.id_candidato=can.id
                                  LEFT OUTER JOIN TB_VV_INFORMATICA_CAND ic ON ic.id_candidato=can.id
								  LEFT OUTER JOIN TB_VV_CNH_CAND cn on cn.id_candidato=can.id
                              WHERE can.id=" . $id) or die(mysqli_error($con));

    $dados = mysqli_fetch_array($res);

    $res_idiomas_candidato = mysqli_query($con, "SELECT * FROM TB_VV_IDIOMAS_CAND WHERE id_candidato=" . $id);
    $res_exp = mysqli_query($con, "SELECT * FROM TB_VV_EXP_PROFISSIONAIS WHERE id_candidato=" . $id) or die(mysqli_error($con));

    //cargos
    $res_cargos2 = mysqli_query($con, "SELECT id, cargo FROM TB_VV_CARGOS_CAND WHERE id_candidato = $id") or die(mysqli_error($con));

    //situações ensino
    $res_situacao = mysqli_query($con, "SELECT * FROM TB_VV_SITUACOES_ENSINO") or die(mysqli_error($con));

    //deficiências
    $res_deficiencias = mysqli_query($con, "SELECT * FROM TB_VV_DEFICIENCIAS_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));

    //informática
    $res_informaticas = mysqli_query($con, "SELECT * FROM TB_VV_INFORMATICA_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));

    //pegar cidade
    $sql_cidades = "SELECT cid.nome FROM TB_VV_CIDADES cid "
        . "INNER JOIN TB_VV_ESTADOS est ON est.id = cid.id_estado "
        . "WHERE est.uf = '" . $dados['estado'] . "' ORDER BY cid.nome ASC";

    $res_cidades = mysqli_query($con, $sql_cidades) or die(mysqli_error($con));

    $dtn = DateTime::createFromFormat('Y-m-d', $dados['nascimento']);

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Edição de candidato</h1>
            <form action="actions/recebe_alteracadastracandidato.php?id=<?php echo $id ?>" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Nome" id="nome" name="nome" value="<?php echo $dados['nome'] ?>" required>
                            <span class="input-group-addon">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input type="text" placeholder="E-mail" name="repetir_email" id="txtEmail" class="form-control validemail" value="<?php echo $dados['email'] ?>" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="password" placeholder="Nova Senha" name="senha" class="form-control txtSenha">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="password" placeholder="Confirmar nova senha" name="confirmar_senha" class="form-control confisenha nopaste">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <input class="data form-control" type="text" placeholder="Data de Nascimento" id="datanasc" name="datanasc" value="<?php echo $dtn->format('d/m/Y') ?>" required/>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6 col-xs-12 area-radio-form">
                        <label class="fleft width-80 margin-top-7">Sexo</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="mas" name="sexo" value="Masculino" class="form-control-input" <?php echo $dados['sexo'] == "Masculino" ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="mas">Masculino</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="fem" name="sexo" value="Feminino" class="form-control-input" <?php echo $dados['sexo'] == "Feminino" ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="fem">Feminino</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>

                            </span>
                            <input class="cpf form-control verifica_cpf_cadastrado" type="text" placeholder="CPF" name="cpf" value="<?php echo $dados['cpf'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" name="filhos" id="filhos">
                                <option value="">Selecione a quantidade de Filhos</option>
                                <option value="0" <?php echo $dados['filhos'] == 0 ? 'selected' : '' ?>>Não tenho filhos</option>
                                <option value="1" <?php echo $dados['filhos'] == 1 ? 'selected' : '' ?>>Um</option>
                                <option value="2" <?php echo $dados['filhos'] == 2 ? 'selected' : '' ?>>Dois</option>
                                <option value="3" <?php echo $dados['filhos'] == 3 ? 'selected' : '' ?>>Três</option>
                                <option value="4" <?php echo $dados['filhos'] == 4 ? 'selected' : '' ?>>Quatro ou mais</option>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-child" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" name="estadocivil" id="estadocivil">
                                <option value="">Selecione o Estado Civil</option>
                                <option value="Casado" <?php echo $dados['estado_civil'] == 'Casado' ? 'selected' : '' ?>>Casado</option>
                                <option value="Divorciado" <?php echo $dados['estado_civil'] == 'Divorciado' ? 'selected' : '' ?>>Divorciado</option>
                                <option value="Separado" <?php echo $dados['estado_civil'] == 'Separado' ? 'selected' : '' ?>>Separado</option>
                                <option value="Solteiro" <?php echo $dados['estado_civil'] == 'Solteiro' ? 'selected' : '' ?>>Solteiro</option>
                                <option value="União Estável" <?php echo $dados['estado_civil'] == 'União Estável' ? 'selected' : '' ?>>União Estável</option>
                                <option value="Viúvo" <?php echo $dados['estado_civil'] == 'Viúvo' ? 'selected' : '' ?>>Viúvo</option>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input class="form-control fones" type="text" placeholder="Telefone Celular" id="celular" name="celular" value="<?php echo $dados['celular'] ?>" required>
                            <span class="input-group-addon">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input class="form-control fones" type="text" placeholder="Telefone Residencial" id="fone" name="fone" value="<?php echo $dados['fone'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input class="form-control dinheiro" type="text" placeholder="Pretensão Salarial (opcional)" id="pretensao_salarial" name="pretensao_salarial" value="<?php echo $dados['pretensao_salarial'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="input-group">
                            <input class="form-control cep ecep" type="text" placeholder="CEP" id="cep" name="cep" value="<?php echo $dados['cep'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6 text-right">
                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCep.cfm" class="btn btn-warning btn-icon" type="button" target="_blank">
                            Não sabe seu CEP?
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input class="form-control elogradouro" type="text" placeholder="Logradouro" id="logradouro" name="logradouro" value="<?php echo $dados['logradouro'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-road" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Número" id="numero" name="numero" value="<?php echo $dados['numero'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-home" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Complemento" id="complemento" name="complemento" value="<?php echo $dados['complemento'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input class="form-control ebairro" type="text" placeholder="Bairro" id="bairro" name="bairro" value="<?php echo $dados['bairro'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <select class="form-control eestado busca_cidades" name="estado" id="estado">
                                <option value="">Estado</option>
                                <?php
                                $result_est = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                while ($row = mysqli_fetch_array($result_est)) {
                                    if ($dados['estado'] == $row['uf']) {
                                        echo "<option selected value='" . $row['uf'] . "'>" . $row['nome'] . "</option>";
                                    } else {
                                        echo "<option value='" . $row['uf'] . "'>" . $row['nome'] . "</option>";
                                    }
                                }

                                ?>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <select class="form-control ecidade cidades" name="cidade" id="cidade">
                                <option value="">Cidade</option>
                                <?php
                                while ($row = mysqli_fetch_array($res_cidades)) {
                                    if ($row["nome"] == $dados['cidade']) {
                                        echo '<option value="' . $row["nome"] . '" selected>' . $row["nome"] . '</option>';
                                    } else {
                                        echo '<option value="' . $row["nome"] . '">' . $row["nome"] . '</option>';
                                    }
                                }

                                ?>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Tem disponibilidade para viajar?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="viajar1" name="viajar" value="0" class="form-control-input" <?php echo intval($dados['viajar']) == 0 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="viajar1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="viajar2" name="viajar" value="1" class="form-control-input" <?php echo intval($dados['viajar']) == 1 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="viajar2">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Possui veículo próprio?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="veiculo1" name="veiculo" value="0" class="form-control-input" <?php echo intval($dados['veiculo']) == 0 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="veiculo1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="veiculo2" name="veiculo" value="1" class="form-control-input" <?php echo intval($dados['veiculo']) == 1 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="veiculo2">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Possui CNH?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="cnh1" name="cnh" value="0" data-area="#area-cnh" class="habilita form-control-input" <?php echo intval($dados['cnh']) == 0 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="cnh1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="cnh2" name="cnh" value="1" data-area="#area-cnh" class="habilita form-control-input" <?php echo intval($dados['cnh']) == 1 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="cnh2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-cnh" class="form-group row <?php echo intval($dados['cnh']) == 0 ? 'hidden' : '' ?>">
                    <div class="col-sm-12 cnh area-radio-form">
                        <label class="fleft width-290 margin-top-7">Qual categoria deve ser a CNH:</label>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="a" value="A" <?php echo $dados['cnhtipo'] == 'A' ? 'checked="checked"' : '' ?>/>
                            <label class="custom-control-label" for="a">Categoria A</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="b" value="B" <?php echo $dados['cnhtipo'] == 'B' ? 'checked="checked"' : '' ?>/>
                            <label class="custom-control-label" for="b">Categoria B</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="c" value="C" <?php echo $dados['cnhtipo'] == 'C' ? 'checked="checked"' : '' ?>/>
                            <label class="custom-control-label" for="c">Categoria C</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="d" value="D" <?php echo $dados['cnhtipo'] == 'D' ? 'checked="checked"' : '' ?>/>
                            <label class="custom-control-label" for="d">Categoria D</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="e" value="E" <?php echo $dados['cnhtipo'] == 'E' ? 'checked="checked"' : '' ?>/>
                            <label class="custom-control-label" for="e">Categoria E</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">É uma Pessoa com Deficiência?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="deficiencia1" name="deficiencia" value="0" data-area="#area-deficiencia" class="habilita form-control-input" <?php echo intval($dados['deficiencia']) == 0 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="deficiencia1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="deficiencia2" name="deficiencia" value="1" data-area="#area-deficiencia" class="habilita form-control-input" <?php echo intval($dados['deficiencia']) == 1 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="deficiencia2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-deficiencia" class="form-group row <?php echo intval($dados['deficiencia']) == 0 ? 'hidden' : '' ?>">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <label class="fleft">Tipos de Deficiência</label>
                    </div>
                    <?php
                    $res_tipos = mysqli_query($con, "SELECT id, nome FROM TB_VV_TIPOS_DEFICIENCIA");
                    while ($row = mysqli_fetch_array($res_tipos)) {

                        ?>
                        <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                            <label><?php echo $row["nome"] ?> </label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                            <?php
                            $res_def = mysqli_query($con, "SELECT id, nome, descricao, "
                                . "(SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM TB_VV_DEFICIENCIAS_CAND idef WHERE idef.id_deficiencia=ide.id AND idef.id_candidato=" . $id . ") checked "
                                . "FROM TB_VV_DEFICIENCIAS ide WHERE ide.id_tipo=" . $row['id'] . " ORDER BY ide.nome ASC");
                            while ($row2 = mysqli_fetch_array($res_def)) {

                                ?>
                                <div class="form-check fleft width-100">
                                    <input class="form-check-input" id="def-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>" type="checkbox" name="deficiencias[]" <?php echo intval($row2['checked']) == 1 ? 'checked="checked"' : ''; ?>>
                                    <label class="form-check-label" title="<?php echo $row2['descricao'] ?>"><?php echo $row2["nome"] ?></label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Possui conhecimentos em Informática?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="informatica1" name="informatica" value="0" data-area="#area-informatica" class="habilita form-control-input" <?php echo intval($dados['informatica']) == 0 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="informatica1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="informatica2" name="informatica" value="1" data-area="#area-informatica" class="habilita form-control-input" <?php echo intval($dados['informatica']) == 1 ? 'checked="checked"' : '' ?>>
                            <label class="custom-control-label" for="informatica2">Sim</label>
                        </div>

                    </div>
                </div>
                <div id="area-informatica" class="form-group row <?php echo intval($dados['informatica']) == 0 ? 'hidden' : '' ?>">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <label class="fleft">Conhecimentos</label>
                    </div>
                    <?php
                    $res_categorias = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFO_CATEGORIAS");
                    while ($row = mysqli_fetch_array($res_categorias)) {

                        ?>
                        <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                            <label><?php echo $row["nome"] ?> </label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                            <?php
                            $res_info = mysqli_query($con, "SELECT id, nome,"
                                . "(SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM TB_VV_INFORMATICA_CAND ican WHERE ican.id_informatica=inf.id AND ican.id_candidato=" . $id . ") checked "
                                . "FROM TB_VV_INFORMATICA inf WHERE inf.id_categoria=" . $row['id'] . " ORDER BY inf.nome ASC");
                            while ($row2 = mysqli_fetch_array($res_info)) {

                                ?>
                                <div class="form-check fleft width-100">
                                    <input class="form-check-input" type="checkbox" name="informaticas[]" id="info-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>" <?php echo intval($row2['checked']) == 1 ? 'checked="checked"' : ''; ?>>
                                    <label class="form-check-label"><?php echo $row2["nome"] ?> </label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="control-label">Objetivo profissional</label>
                        <textarea class="form-control" placeholder="Objetivo profissional" id="objetivo" name="objetivo"><?php echo $dados['obj_profissional'] ?></textarea>
                    </div>
                </div>
                <hr/>
                <div class="form-group row border">
                    <div class="col-sm-12">
                        <strong>Cargos profissionais</strong>
                    </div>
                </div>
                <div class="div_cargo">
                    <div class="form-group row border">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <select id="select_cargo" name="cargoprofissional[]" class="form-control select2 cargoop" <?php echo (!mysqli_num_rows($res_cargos2)) ? 'required' : '' ?>>
                                    <option value="">Cargo Profissional</option>
                                    <?php
                                    $result_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                    while ($val = mysqli_fetch_array($result_cargos)) {
                                        echo "<option value='$val[nome]'>$val[nome]</option>";
                                    }

                                    ?>
                                    <option>Outro</option>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row border hidden outro_cargo">
                        <div class="col-sm-12">
                            <div class="input-group" class="third hidden" id="label-ncargo">
                                <input class="form-control" type="text" name="cargoprofissional[]" placeholder="Digite o Cargo Profissional" id="novocargo" >
                                <span class="input-group-addon">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row border">
                    <div class="col-sm-3">
                        <button class="btn btn-success form-control btn-icon" type="button" id="add_cargo">
                            Incluir Cargo
                        </button>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-danger form-control btn-icon" type="button" id="rem_cargo" style="display: none">
                            Remover Cargo
                        </button>
                    </div>
                </div>
                <div class="form-group row exp">
                    <div class="col-sm-12 full">
                        <strong>
                            Cargos profissionais cadastrados
                        </strong>
                        <table class="edit" id="cargosgrid" style="width: 100%; margin-top: 15px">
                            <?php
                            if (mysqli_num_rows($res_cargos2) > 0) {
                                while ($cargo = mysqli_fetch_array($res_cargos2)) {

                                    ?>
                                    <tr style="width: 100%; border: #ccc solid 1px">
                                        <td><span><?php echo $cargo['cargo'] ?></span></td>
                                        <td class="width-80">
                                            <button type="button" class="btn btn-danger btn-xs remove_cargos" data-id="<?php echo $cargo['id'] ?>">
                                                <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                Remover
                                            </button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr id="uexp"><td>Nenhum cargo cadastrado!</td></tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <hr/>
                <div class="form-group row border">
                    <div class="col-sm-12 area-radio-form">
                        <span class="fleft width-290 margin-top-7"><strong>Você ja possui experiência profissional?</strong></span>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" name="experiencia" id="expnao" data-area="#area-empresa" class="habilita form-control-input" value="Não" <?php echo intval($dados['exp']) == 0 ? 'checked' : '' ?> required>
                            <label class="form-control-label" for="expnao">Não </label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" name="experiencia" id="expsim" data-area="#area-empresa" class="habilita form-control-input" value="Sim" <?php echo intval($dados['exp']) > 0 ? 'checked' : '' ?> required>
                            <label class="form-control-label" for="expsim">Sim </label>
                        </div>
                    </div>
                </div>
                <div id="area-empresa" class="<?php echo intval($dados['exp']) == 0 ? 'hidden' : '' ?>">
                    <div class="campos_empresa">
                        <div class="form-group row exp">
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Nome da empresa" name="nomeempresa[]" id="nomeempresa">
                                    <span class="input-group-addon">
                                        <i class="fa fa-building-o" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input class="form-control dinheiro" class="dinheiro" type="text" placeholder="Salário (opcional)" name="salario[]" id="salario">
                                    <span class="input-group-addon">
                                        <i class="fa fa-dollar" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row exp">
                            <div class="div_cargo_emp">
                                <div class="col-sm-12" id="label-cargoexp">
                                    <div class="input-group">
                                        <select class="form-control cargo" name="cargoempresa[]" id="cargoempresa">
                                            <option selected value="">Cargo Profissional</option>
                                            <?php
                                            $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                            while ($row = mysqli_fetch_array($res_cargos)) {
                                                echo "<option>" . $row['nome'] . "</option>";
                                            }

                                            ?>
                                            <option>Outro</option>
                                        </select>
                                        <span class="input-group-addon">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-12 outro_cargo hidden" id="label-ncargoexp">
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Digite o Cargo Profissional" id="novocargoexp" name="cargoempresa[]" >
                                        <span class="input-group-addon">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row exp">
                            <div class="col-sm-12">
                                <textarea class="form-control" name="atividades[]" id="atividades" placeholder="Atividades Exercidas"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input class="form-control data_ini data-s" type="text" placeholder="Início (mês/ano)" name="datainicio[]" id="datainicio">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input class="form-control data_fim data-s" type="text" placeholder="Fim (mês/ano)" name="datafim[]" id="datafim">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check">
                                    <input class="form-check-input set_empr_atual" value="1" type="checkbox" name="empregoatual[]">
                                    <label class="form-check-label">
                                        Emprego atual
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <button class="btn btn-success form-control btn-icon" id="adicionar_experiencia" type="button">
                                Incluir Experiência
                            </button>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-danger form-control btn-icon" id="remover_experiencia" type="button" style="display: none">
                                Remover Experiência
                            </button>
                        </div>
                    </div>
                    <div class="form-group row exp">
                        <div class="col-sm-12 full">
                            <strong>
                                Experiências cadastradas
                            </strong>
                            <table class="edit" id="exparea" style="width: 100%; margin-top: 15px ">
                                <?php
                                if (mysqli_num_rows($res_exp) > 0) {
                                    while ($exp = mysqli_fetch_array($res_exp)) {

                                        ?>
                                        <tr style="width: 100%; border: #ccc solid 1px">
                                            <td><span><?php echo $exp['nome_empresa'] ?></span></td>
                                            <td><span><?php echo $exp['cargo'] ?></span></td>
                                            <td class="width-80">
                                                <button type="button" class="btn btn-danger btn-xs remove_experiencia" data-id="<?php echo $exp['id'] ?>">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                    Remover
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {

                                    ?>
                                    <tr id="uexp"><td>Nenhuma experiência cadastrada!</td></tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group row exp">
                    <div class="col-sm-12 full">
                        <strong>Preencha abaixo a sua formação acadêmica/técnica:</strong>
                    </div>
                </div>
                <div class="div_formacao">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <select name="formacao[]" class="form-control slct_formacao">
                                    <option value="">Formação</option>
                                    <?php
                                    $result5 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                                    while ($val = mysqli_fetch_array($result5)) {
                                        echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_instituicao hidden">
                            <div class="input-group">
                                <input type="text" placeholder="Instituição de ensino" name="instituicao[]" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-university" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_situacao hidden">
                            <div class="input-group">
                                <select name="situacao[]" class="form-control slct_situacao">
                                    <option value="">Situação</option>
                                    <?php
                                    $result4 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                                    while ($val = mysqli_fetch_array($result4)) {
                                        if ($val["nome"] == "Trancado") {
                                            echo "<option value='$val[nome]' id='trancado'>$val[nome]</option>";
                                        } else {
                                            echo "<option value='$val[nome]'>$val[nome]</option>";
                                        }
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 campo_nomecurso hidden">
                            <div class="input-group">
                                <input type="text" placeholder="Nome do curso" name="nomeformacao[]" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-bookmark" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 campo_ano_conclusao hidden">
                            <div class="input-group">
                                <input class="data-ano form-control info_ano" type="text" placeholder="Ano de conclusão" name="anofim[]">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_semestre hidden">
                            <div class="input-group">
                                <select name="semestre[]" class="form-control">
                                    <option value="">Semestre</option>
                                    <option value="1">1º semestre</option>
                                    <option value="2">2º semestre</option>
                                    <option value="3">3º semestre</option>
                                    <option value="4">4º semestre</option>
                                    <option value="5">5º semestre</option>
                                    <option value="6">6º semestre</option>
                                    <option value="7">7º semestre</option>
                                    <option value="8">8º semestre</option>
                                    <option value="9">9º semestre</option>
                                    <option value="10">10º semestre</option>
                                    <option value="11">11º semestre</option>
                                    <option value="12">12º semestre</option>
                                    <option value="13">13º semestre</option>
                                    <option value="14">14º semestre</option>
                                    <option value="15">15º semestre</option>
                                    <option value="16">16º semestre</option>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_turno hidden">
                            <div class="input-group">
                                <select name="turno[]" class="form-control">
                                    <option value="">Turno</option>
                                    <option value="Manhã">Manhã</option>
                                    <option value="Tarde">Tarde</option>
                                    <option value="Noite">Noite</option>
                                    <option value="EAD">EAD</option>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <button class="btn btn-success form-control btn-icon" id="adicionar_formacao" type="button">
                            Incluir Formação
                        </button>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-danger form-control btn-icon" id="remover_formacao" type="button" style="display: none">
                            Remover Formação
                        </button>
                    </div>
                </div>
                <div class="form-group row exp">
                    <div class="col-sm-12 full">
                        <strong>
                            Formações cadastradas
                        </strong>
                    </div>
                </div>
                <div class="form-group row exp">
                    <div class="col-sm-12 full">
                        <table class="edit" id="formacoesarea" style="width: 100%; ">
                            <?php
                            $res_formacoes = mysqli_query($con, "SELECT nivel, situacao, curso, id FROM TB_VV_FORMACOES_CAND WHERE id_candidato=$id");
                            if (mysqli_num_rows($res_formacoes) > 0) {
                                while ($row = mysqli_fetch_array($res_formacoes)) {

                                    ?>
                                    <tr style="width: 100%; border: #ccc solid 1px">
                                        <td><span><?php echo $row["nivel"] ?></span><td>
                                        <td><span><?php echo $row["situacao"] ?></span><td>
                                        <td><span><?php echo $row["curso"] ?></span></td>
                                        <td class="width-80">
                                            <button type="button" class="btn btn-danger btn-xs remove_formacao" data-id="<?php echo $row['id'] ?>">
                                                <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                Remover
                                            </button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {

                                ?>
                                <tr id="uformacao"><td>Nenhuma formação cadastrada!</td></tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <hr/>
                <div class="form-group row border">
                    <div class="col-sm-12 full">
                        <strong>Preencha abaixo os seus cursos extracurriculares:</strong>
                    </div>
                </div>
                <div class="div_cursos">
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <input type="text" placeholder="Nome do curso" name="nomecurso[]" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-bookmark" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="text" placeholder="Carga Horária" name="cargahoraria[]" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <input type="text" placeholder="Instituição de ensino" name="instituicao2[]" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-university" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <select name="situacao2[]" class="form-control slct_situacao2" data-length="0">
                                    <option value="">Situação</option>
                                    <?php
                                    $result3 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                                    while ($val = mysqli_fetch_array($result3)) {
                                        echo "<option value='$val[nome]'>$val[nome]</option>";
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 campo_turno hidden">
                            <div class="input-group">
                                <select name="turnoextra[]" class="form-control">
                                    <option value="">Turno</option>
                                    <option value="Manhã">Manhã</option>
                                    <option value="Tarde">Tarde</option>
                                    <option value="Noite">Noite</option>
                                    <option value="EAD">EAD</option>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-asterisk" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 dias_semanas hidden">
                            <div class="input-group">
                                <select class="form-control dias_semana" id="slct_0" multiple>
                                    <option value="Domingo">Domingo</option>
                                    <option value="Segunda-feira">Segunda-feira</option>
                                    <option value="Terca-feira">Terça-feira</option>
                                    <option value="Quarta-feira">Quarta-feira</option>
                                    <option value="Quinta-feira">Quinta-feira</option>
                                    <option value="Sexta-feira">Sexta-feira</option>
                                    <option value="Sábado">Sábado</option>
                                </select>
                                <input type="hidden" class="dias_semana_hiden" id="dias_semana_0" name="diassemana[]"/>
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_ano_conclusao hidden">
                            <div class="input-group">
                                <input class="data-ano form-control ano_conclusao" type="text" placeholder="Ano de conclusão" name="anofim2[]">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row exp">
                    <div class="col-sm-3">
                        <button class="btn btn-success form-control btn-icon" id="adicionar_curso" type="button">
                            Incluir Curso
                        </button>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-danger form-control btn-icon" id="remover_curso" type="button" style="display: none">
                            Remover Curso
                        </button>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 full">
                        <strong>
                            Cursos cadastrados
                        </strong>
                    </div>
                    <div class="col-sm-12 full">
                        <table class="edit" id="cursosarea" style="width: 100%; margin-top: 15px;">
                            <?php
                            $res_cursos = mysqli_query($con, "SELECT situacao, nome, id FROM TB_VV_CURSOS_CAND WHERE id_candidato=$id");
                            if (mysqli_num_rows($res_cursos) > 0) {
                                while ($curso = mysqli_fetch_array($res_cursos)) {

                                    ?>
                                    <tr style="width: 100%; border: #ccc solid 1px">
                                        <td><span><?php echo $curso["nome"] ?></span></td>
                                        <td><span><?php echo $curso["situacao"] ?></span></td>
                                        <td class="width-80">
                                            <button type="button" class="btn btn-xs btn-danger remove_curso" data-id="<?php echo $curso['id'] ?>">
                                                <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                Remover
                                            </button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {

                                ?>
                                <tr id="ucurso"><td>Nenhum curso cadastrado!</td></tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <hr/>
                <div class="form-group row border">
                    <div class="col-sm-12 full">
                        <strong>
                            Idiomas
                        </strong>
                    </div>
                </div>
                <div class="div_idioma">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <select name="idioma[]" class="form-control">
                                    <option value="">Idioma</option>
                                    <?php
                                    $result1 = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                                    while ($val = mysqli_fetch_array($result1)) {
                                        echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select name="nivelidioma[]" class="form-control">
                                    <option value="">Nível</option>
                                    <?php
                                    $result2 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                                    while ($val = mysqli_fetch_array($result2)) {
                                        echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row exp">
                    <div class="col-sm-3">
                        <button class="btn btn-success form-control btn-icon" id="adicionar_idioma" type="button">
                            Incluir Idioma
                        </button>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-danger form-control btn-icon" id="remover_idioma" type="button" style="display: none">
                            Remover Idioma
                        </button>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 full">
                        <strong>
                            Idiomas cadastrados
                        </strong>
                    </div>
                    <div class="col-sm-12 full">
                        <table class="edit" id="idiomasarea" style="width: 100%; margin-top: 15px;">
                            <?php
                            if (mysqli_num_rows($res_idiomas_candidato)) {
                                while ($idiomas = mysqli_fetch_array($res_idiomas_candidato)) {

                                    ?>
                                    <tr style="width: 100%; border: #ccc solid 1px">
                                        <td><span><?php echo $idiomas["idioma"] ?></span></td>
                                        <td><span><?php echo $idiomas["nivel"] ?></span></td>
                                        <td class="width-80">
                                            <button type="button" class="btn btn-xs btn-danger remove_idioma" data-id="<?php echo $idiomas['id'] ?>">
                                                <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                Remover
                                            </button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {

                                ?>
                                <tr id="uidioma"><td>Nenhum idioma cadastrado!</td></tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 text-right">
                        <button class="btn btn-success btn-icon atualizar_outros" data-id="<?php echo $_SESSION["id"] ?>">
                            Alterar Cadastro <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            caminho = '<?php echo DIRETORIO; ?>';
        });
    </script>
    <script src="<?php echo PATH_ASSETS . '/js/geral/cadastrocandidato.js' ?>"></script>
    <?php
    include "includes/footer.php";
}