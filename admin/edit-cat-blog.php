<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $id = $_GET['id'];
    $sql = mysqli_query($con, "select * from TB_VV_CATEGORIAS_BLOG where id_cat = '$id'");
    $vetor = mysqli_fetch_array($sql);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Add Categoria Blog</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Adicionar Categoria Blog</h2>
            <form action="actions/recebe_alteracadastracatblog.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control" value="<?php echo $vetor['nome']; ?>" id="titulo" name="titulo" placeholder="Insira o título" required>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" id="status" name="status">
                            <option value="1" <?php echo intval($vetor['status']) == 1 ? 'selected="selected"' : ''; ?>>Ativa</option>
                            <option value="0" <?php echo intval($vetor['status']) == 0 ? 'selected="selected"' : ''; ?>>Inativa</option>
                        </select>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}