<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $id = $_GET['id'];

    $sql_pergunta = mysqli_query($con, "SELECT * FROM TB_VV_FAQ WHERE id_pergunta = '$id'");
    $vetor = mysqli_fetch_array($sql_pergunta);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Add Pergunta</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Editar Pergunta</h2>
            <form action="actions/recebe_alteracadastrapergunta.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
                <div class="container">
                    <div class="form-group">
                        <label for="titulo">Pergunta</label>
                        <input type="text" class="form-control" id="titulo" name="pergunta" placeholder="Insira a Pergunta" value="<?php echo $vetor['pergunta']; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="images">Resposta</label>
                        <textarea name="resposta" class="ckeditor" id="editor1" required><?php echo $vetor['resposta']; ?></textarea>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}