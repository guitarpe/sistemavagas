<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $sql_vaga = mysqli_query($con, "select * FROM TB_VV_VAGAS where id = '$_GET[id]'");
    $vetor = mysqli_fetch_array($sql_vaga);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Visualizar Vaga</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Visualizar Vaga</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Empresa</label>
                        <select name="id_empresa" class="form-control select2" style="width: 100%;" required>
                            <?php
                            $sql_empresa = mysqli_query($con, "select *  FROM TB_VV_EMPRESAS where id = '$vetor[id_empresa]'");

                            $vetor_empresa = mysqli_fetch_array($sql_empresa);

                            ?>
                            <option value="<?php echo $vetor_empresa['id']; ?>" <?php if (strcasecmp($vetor_empresa['id'], $vetor['id_empresa']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_empresa['nome_empresa']; ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Forma de Contratação</label>
                        <select name="forma_contratacao" id="forma_contratacao" class="form-control select2" required>
                            <?php
                            $res_formas = mysqli_query($con, "SELECT descricao FROM TB_VV_FORMAS_CONTRAT ORDER BY descricao ASC");
                            $formas = mysqli_fetch_array($res_formas);

                            ?>
                            <option value="<?php echo $formas['descricao']; ?>" <?php if (strcasecmp($vetor['forma_contratacao'], $formas['descricao']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor['forma_contratacao']; ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cargo Profissional</label>
                        <select name="cargo_profissional" id="cargo_profissional" class="form-control select2" required>
                            <?php
                            $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                            $cargo = mysqli_fetch_array($res_cargos);

                            ?>
                            <option value="<?php echo $cargo['nome']; ?>" <?php if (strcasecmp($vetor['cargo'], $cargo['nome']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor['cargo']; ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nível Hierárquico</label>
                        <select name="nivel_hierarquico" id="nivel_hierarquico" class="form-control select2" required>
                            <option value="<?php echo $vetor['nivel_hierarquico']; ?>"><?php echo $vetor['nivel_hierarquico']; ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Número de Vagas</label>
                        <input name="numero_vagas" type="text" class="form-control" placeholder="Número de Vagas" id="numero_vagas" value="<?php echo $vetor['numero_vagas'] ?>" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Turno</label>
                        <select name="turnos_vaga" id="turnos_vaga" class="form-control select2" required>

                            <option value="<?php echo $vetor['turno']; ?>"><?php echo $vetor['turno']; ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Atividades a serem desenvolvidas</label>
                        <textarea name="atividades_vaga" placeholder="Atividades a serem desenvolvidas" id="atividades_vaga" class="form-control" rows="8" required><?php echo $vetor['atividades'] ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Remuneração</label>
                        <select name="faixa_salarial" class="form-control" id="tipobusca" required>
                            <option value="<?php echo $vetor['faixa_salarial']; ?>"><?php echo $vetor['faixa_salarial']; ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Salário</label>
                        <input name="valor_salario" class="form-control" type="text" onKeyPress="mascara(this, mvalor)" placeholder="Salário" id="valor_salario" value="<?php echo $vetor['salario'] ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Experiência Mínima</label>
                        <select name="experiencia_min" class="form-control select2" required>
                            <option value="<?php echo $vetor['experiencia_minima']; ?>"><?php echo $vetor['experiencia_minima']; ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Ensino Mínimo</label>
                        <select name="ensino_min" class="form-control select2" required>
                            <option value="<?php echo $vetor['ensino_minimo']; ?>"><?php echo $vetor['ensino_minimo']; ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Benefícios</label>
                        </br>
                        <?php
                        $sql_beneficios = mysqli_query($con, "select * FROM TB_VV_BENEFICIOS order by descricao ASC");

                        while ($vetor_beneficios = mysqli_fetch_array($sql_beneficios)) {

                            $sql_TB_VV_BENEFICIOS_VAGA = mysqli_query($con, "select * from TB_VV_BENEFICIOS_VAGA where id_vaga = '$_GET[id]' and descricao = '$vetor_beneficios[descricao]'");

                            ?>
                            <input type="checkbox" name="beneficios[]" value="<?php echo $vetor_beneficios['descricao']; ?>" <?php mysqli_num_rows($sql_TB_VV_BENEFICIOS_VAGA) == 0 ? "" : "checked" ?>> <?php echo $vetor_beneficios['descricao']; ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Sexo:</label>
                        <label>
                            <input type="radio" name="sexo_vaga" id="indif" <?php if ($vetor['sexo'] == 'Indiferente') { ?> checked <?php } ?> value="Indiferente">
                            <span>Indiferente</span>
                        </label>
                        <label>
                            <input type="radio" name="sexo_vaga" <?php if ($vetor['sexo'] == 'Masculino') { ?> checked <?php } ?> id="masc" value="Masculino">
                            <span>Masculino</span>
                        </label>
                        <label>
                            <input type="radio" name="sexo_vaga" <?php if ($vetor['sexo'] == 'Feminino') { ?> checked <?php } ?> id="femin" value="Feminino">
                            <span>Feminino</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Vaga de Emprego para outro Estado ou Cidade?</label>
                        <label>
                            <select name="vaga_outrolocal" id="tipobusca1" class="form-control">
                                <option value="1" selected="">Não</option>
                                <option value="2_2">Sim</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Estado</label>
                        <select name="estados" id="estados" class="form-control">
                            <option value="">Escolha um Estado</option>
                            <?php
                            $result = mysqli_query($con, "select * FROM TB_VV_ESTADOS order by nome ASC");

                            while ($row = mysqli_fetch_array($result)) {
                                echo "<option value='" . $row['id'] . "'>" . $row['nome'] . "</option>";
                            }

                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Cidade</label>
                        <select name="cidades" id="cidades" class="form-control">
                            <option value="0">Escolha um Estado</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Disponibilidade para viajar?</label>
                        <label>
                            <input type="radio" name="disp_viajar" id="indif" value="Não">
                            <span>Não</span>
                        </label>
                        <label>
                            <input type="radio" name="disp_viajar" id="masc" value="Sim">
                            <span>Sim</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Deve possuir veículo próprio?</label>
                        <label>
                            <input type="radio" name="possuir_veiculo" id="indif" value="Não">
                            <span>Não</span>
                        </label>
                        <label>
                            <input type="radio" name="possuir_veiculo" id="masc" value="Sim">
                            <span>Sim</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Deve possuir CNH?</label>
                        <label>
                            <select name="cnh" class="form-control" id="tipobusca2" required>
                                <option value="Não" selected="">Não</option>
                                <option value="Sim">Sim</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Qual categoria deve ser a CNH:</label>
                        </br>
                        <label class="trd">
                            <input type="checkbox" id="catA">
                            <span>Categoria A</span>
                        </label>
                        <label class="trd">
                            <input type="checkbox" name="cnh[]">
                            <span>Categoria B</span>
                        </label>
                        <label class="trd">
                            <input type="checkbox" name="cnh[]">
                            <span>Categoria C</span>
                        </label>
                        <label class="trd">
                            <input type="checkbox" name="cnh[]">
                            <span>Categoria D</span>
                        </label>
                        <label class="trd">
                            <input type="checkbox" name="cnh[]">
                            <span>Categoria E</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Vaga de Emprego é para Pessoa com Deficiência?</label>
                        <label>
                            <select name="deficiencia" class="form-control" id="tipobusca3" required>
                                <option value="Não_def" selected="">Não</option>
                                <option value="Sim_def">Sim</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?php
                        $res_tipos = mysqli_query($con, "SELECT * FROM TB_VV_TIPOS_DEFICIENCIA");
                        while ($tipo = mysqli_fetch_array($res_tipos)) {

                            ?>
                            <div class="full">
                                <strong>
                                    <span><?php echo $tipo["nome"] ?></span>
                                </strong>
                            </div>
                            <div class="full checks">
                                <?php
                                $res_deficiencias = mysqli_query($con, "SELECT * FROM TB_VV_DEFICIENCIAS WHERE id_tipo = $tipo[id]");

                                while ($deficiencia = mysqli_fetch_array($res_deficiencias)) {

                                    ?>
                                    <label class="trd" title="<?php echo $deficiencia['descricao'] ?>">
                                        <input type="checkbox" name="vaga_def[]" value="<?php echo $deficiencia['nome'] ?>">
                                        <span><?php echo $deficiencia['nome'] ?></span>
                                    </label>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Necessário conhecimento em informática?</label>
                        <label>
                            <select name="informatica" class="form-control" id="tipobusca4" required>
                                <option value="Nao_inf" selected="">Não</option>
                                <option value="Sim_inf">Sim</option>
                            </select>
                        </label>
                        </br>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?php
                        $res_categorias = mysqli_query($con, "SELECT * FROM TB_VV_INFO_CATEGORIAS");
                        while ($categoria = mysqli_fetch_array($res_categorias)) {

                            ?>
                            <div class="full">
                                <strong>
                                    <span><?php echo $categoria["nome"] ?></span>
                                </strong>
                            </div>
                            <div class="full checks">
                                <?php
                                $res_informaticas = mysqli_query($con, "SELECT nome FROM TB_VV_INFORMATICA WHERE id_categoria = $categoria[id]");

                                while ($informatica = mysqli_fetch_array($res_informaticas)) {
                                    $aux = mysqli_query($con, "SELECT id FROM TB_VV_INFORMATICA_VAGA WHERE id_vaga = $_GET[id] AND nome = '$informatica[nome]'");
                                    if (mysqli_num_rows($aux)) {

                                        ?>
                                        <label class="trd">
                                            <input type="checkbox" name="vaga_info[]" value="<?php echo $informatica['nome'] ?>" checked>
                                            <span><?php echo $informatica['nome'] ?></span>
                                        </label>
                                    <?php } else { ?>
                                        <label class="trd">
                                            <input type="checkbox" name="vaga_info[]" value="<?php echo $informatica['nome'] ?>">
                                            <span><?php echo $informatica['nome'] ?></span>
                                        </label>
                                        <?php
                                    }
                                }

                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Será desejável conhecimento em língua estrangeira?</label>
                        <label>
                            <select name="idiomas" class="form-control" id="tipobusca5" required>
                                <option value="Nao_ind" selected="">Não</option>
                                <option value="Sim_ind">Sim</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <select name="idioma_vaga" id="idioma_vaga" class="form-control">
                            <option disabled selected value="">Idioma</option>
                            <?php
                            $res_idiomas = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                            while ($idioma = mysqli_fetch_array($res_idiomas)) {

                                ?>
                                <option value="<?php echo $idioma['descricao'] ?>"><?php echo $idioma['descricao'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <select name="nivelidioma_vaga" id="nivelidioma_vaga" class="form-control">
                            <option disabled selected value="">Nível</option>
                            <?php
                            $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                            while ($nivel = mysqli_fetch_array($res_niveis)) {

                                ?>
                                <option value="<?php echo $nivel['descricao'] ?>"><?php echo $nivel['descricao'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Visibilidade restrita para os dados da empresa?</label>
                        <label>
                            <input type="radio" id="visibnao" name="visidados" value="Não" checked>
                            <span>Não</span>
                        </label>
                        <label>
                            <input type="radio" id="visibsim" name="visidados" value="Sim">
                            <span>Sim</span>
                        </label>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}