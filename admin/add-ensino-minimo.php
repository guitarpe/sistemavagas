<?php
session_start();

include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Adicionar Ensino Mínimo</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Adicionar Ensino Mínimo</h2>
            <form action="actions/recebe_alteracadastraensinominimo.php" method="post">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="ensino">Ensino Mínimo</label>
                        <input type="text" class="form-control" id="ensino" name="ensino" placeholder="Digite o ensino mínimo">
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}