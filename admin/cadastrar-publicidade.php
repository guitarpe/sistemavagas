<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Cadastrar Publicidade</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Cadastrar Publicidade</h2>
            <form action="actions/recebe_alteracadastrapublicidade.php" method="post" enctype="multipart/form-data">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Insira o título">
                    </div>
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" class="form-control" id="link" name="link" placeholder="Insira um link(Opcional)">
                    </div>
                    <div class="form-group">
                        <label for="posicao">Posição</label>
                        <select id="posicao" name="posicao" class="form-control">
                            <option value="1">Superior/Inferior</option>
                            <option value="2">Laterais</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagem</label>
                        <input type="file" class="form-control" id="images" name="images" required/>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}