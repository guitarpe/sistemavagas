<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $nome = filter_input(INPUT_POST, "nome");
    $email = filter_input(INPUT_POST, "email");
    $tipo = filter_input(INPUT_POST, "tipo");

    $query = "SELECT * FROM TB_VV_USUARIOS WHERE id is not null ";

    if (!empty($nome)) {
        $query .= " AND nome LIKE '%$nome%'";
    }

    if (!empty($email)) {
        $query .= " AND email LIKE '%$email%'";
    }

    if (!empty($tipo)) {
        $query .= " AND tipo='$tipo'";
    }

    $query .= " ORDER BY id DESC";

    $res = mysqli_query($con, $query);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Listar Usuários</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Usuários do Sistema</h2>
            </br>
            </br>
            <table class="table">
                <thead class="thead-dark">
                    <tr class="center">
                        <th scope="col">Nome do Candidato</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Tipo</th>
                    </tr>
                </thead>
                <form action="lista-usuarios.php" method="post">
                    <tbody>
                        <tr>
                            <th><input type="text" name="nome" class="form-control"></th>
                            <th><input type="text" name="email" class="form-control"></th>
                            <th>
                                <select name="tipo" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="empresa">Empresa</option>
                                    <option value="candidato">Candidato</option>
                                    <option value="admin">Admin</option>
                                </select>
                            </th>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <th colspan="3">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Buscar
                                </button>
                            </th>
                        </tr>
                    </tbody>
                </form>
            </table>
            </br>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-usuarios" class="table table-bordered table-striped table-pages">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th scope="col">#</th>
                            <th scope="col">Nome do Usuário</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Tipo</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($value = mysqli_fetch_array($res)) {

                            ?>

                            <tr>
                                <th scope="row"><?php echo $i ?></th>
                                <td><?php echo $value['nome'] ?></td>
                                <td><?php echo $value['email'] ?></td>
                                <td><?php echo $value['tipo']; ?></td>
                                <td class="text-center">
                                    <?php if (intval($value['status']) == 0) { ?>
                                        <a href="actions/ativa-desativar-usuario.php?op=1&id=<?php echo $value['id'] . '&tipo=' . $value['tipo'] ?>" class="btn btn-xs btn-success" title="Ativar">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a href="actions/ativa-desativar-usuario.php?op=2&id=<?php echo $value['id'] . '&tipo=' . $value['tipo'] ?> ?>" class="btn btn-xs btn-danger" title="Desativar">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    <?php } ?>
                                    <a href="actions/reenviar-senha.php?id=<?php echo $value['id'] . '&tipo=' . $value['tipo'] ?>" class="btn btn-xs btn-info" title="Reenviar Senha">
                                        <i class="fa fa-send"></i>
                                    </a>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}