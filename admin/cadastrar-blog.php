<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res_categorias = mysqli_query($con, "SELECT * FROM TB_VV_CATEGORIAS_BLOG ORDER BY nome ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Add Blog</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Adicionar Blog</h2>
            <form action="actions/recebe_alteracadastrablog.php" method="post" enctype="multipart/form-data">
                <div class="container">
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Insira o título" required>
                    </div>
                    <div class="form-group">
                        <label for="descricao">Categoria</label>
                        <select name="categoria" id="exampleSelect" class="form-control" required>
                            <option disabled selected>Selecione o Curso</option>
                            <?php while ($vetor_cat = mysqli_fetch_array($res_categorias)) { ?>
                                <option value="<?php echo $vetor_cat['id_cat']; ?>"><?php echo $vetor_cat['nome'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="images">Texto Capa</label>
                        <textarea class="form-control" id="titulo" name="descricao" placeholder="Insira o Texto" rows="5" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="images">Texto</label>
                        <textarea name="texto" class="ckeditor" id="editor1" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagem</label>
                        <input type="file" class="form-control" id="images" name="images" required/>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}