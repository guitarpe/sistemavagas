<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT *  FROM TB_VV_NOTICIAS ORDER BY titulo ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Candidato</a></li>
                <li class="active">Listar Notícias</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Notícias</h2>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-noticias" class="table table-bordered table-striped table-pages">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th scope="col">#</th>
                            <th scope="col">Descrição</th>
                            <th scope="col" width="170"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($value = mysqli_fetch_array($res)) {

                            ?>

                            <tr>
                                <th scope="row"><?php echo $i ?></th>
                                <td><?php echo $value['titulo'] ?></td>
                                <td>
                                    <a class="btn btn-xs btn-default" href="edit-noticia.php?id=<?php echo $value['id'] ?>">
                                        <i class="fa fa-pencil"></i> Editar
                                    </a>
                                    <a class="btn btn-xs btn-danger" href="actions/delete-noticia.php?id=<?php echo $value['id'] ?>">
                                        <i class="fa fa-trash"></i> Excluir
                                    </a>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        }

                        ?>
                    </tbody>
                </table>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}