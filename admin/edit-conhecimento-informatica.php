<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $id = filter_input(INPUT_GET, 'id');

    $res = mysqli_query($con, "SELECT * FROM TB_VV_INFORMATICA WHERE id=$id");
    $value = mysqli_fetch_array($res);

    $res_cat = mysqli_query($con, "SELECT * FROM TB_VV_INFO_CATEGORIAS");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Editar Conhecimento de Informática</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Editar Conhecimento de Informática</h2>
            <form action="actions/recebe_alteracadastraconhecimentoinformatica.php?id=<?php echo $value['id'] ?>" method="post">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="conhecimento">Conhecimento de Informática</label>
                        <input type="text" class="form-control" id="conhecimento" name="conhecimento" placeholder="Digite a situação de ensino" value="<?php echo $value['nome'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <select name="categoria" class="form-control">
                            <?php
                            while ($row = mysqli_fetch_array($res_cat)) {
                                $slct = $row['id'] == $value['id_categoria'] ? 'selected' : '';

                                echo '<option value="' . $row['id'] . '" ' . $slct . '>' . $row['nome'] . '</option>';
                            }

                            ?>
                        </select>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}