<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Listar Situação</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Situação de Ensino Cadastradas</h2>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-situacoes" class="table table-bordered table-striped table-pages">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th scope="col">#</th>
                            <th scope="col">Descrição</th>
                            <th scope="col" width="170"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($value = mysqli_fetch_array($res)) {

                            ?>

                            <tr>
                                <th scope="row"><?php echo $i ?></th>
                                <td><?php echo $value['nome'] ?></td>
                                <td style="width: 80px" class="text-center">
                                    <button class="btn btn-xs btn-warning" onclick="location.href = 'edit-situacao-ensino.php?id=<?php echo $value['id'] ?>'">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button class="btn btn-xs btn-danger" onclick="location.href = 'actions/delete-situacao-ensino.php?id=<?php echo $value['id'] ?>'">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}