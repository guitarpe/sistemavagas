<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo PATH_IMAGENS . '/adm.png' ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>ADMINISTRAÇÃO</p>
                <a href=""><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <br/>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">OPÇÕES DE CURRÍCULO</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-plus-circle"></i>
                    <span>Adicionar</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!--li><a href="<?php echo PATH_ADMIN . '/add-area-profissional.php' ?>"><i class="fa fa-circle"></i>Área Profissional</a></li-->
                    <!--li><a href="<?php echo PATH_ADMIN . '/add-especializacao.php' ?>"><i class="fa fa-circle"></i>Especialização</a></li-->
                    <li><a href="<?php echo PATH_ADMIN . '/add-cargo.php' ?>"><i class="fa fa-circle"></i>Cargo</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-nivel-ensino.php' ?>"><i class="fa fa-circle"></i>Nível Ensino</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-situacao.php' ?>"><i class="fa fa-circle"></i>Situação</a></li>
                </ul>
            </li>
            <li class="header">CANDIDATOS</li>
            <li><a href="<?php echo PATH_ADMIN . '/add-candidato.php' ?>"><i class="fa fa-user"></i>Cadastrar Candidato</a></li>
            <li><a href="<?php echo PATH_ADMIN . '/list-candidatos.php' ?>"><i class="fa fa-list-alt"></i>Listar Candidatos</a></li>
            <li><a href="<?php echo PATH_ADMIN . '/importar-candidatos.php' ?>"><i class="fa fa-plus-circle"></i>Importar Candidatos</a></li>
            <li class="header">EMPRESAS</li>
            <li><a href="<?php echo PATH_ADMIN . '/list-empresas.php' ?>"><i class="fa fa-list-alt"></i>Listar Empresas</a></li>
            <li><a href="<?php echo PATH_ADMIN . '/add-empresa.php' ?>"><i class="fa fa-empire"></i>Cadastrar Empresa</a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span>Listar</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!--li><a href="<?php echo PATH_ALL . '/list-area-profissional.php' ?>"><i class="fa fa-circle"></i>Área Profissional</a></li-->
                    <!--li><a href="<?php echo PATH_ALL . '/list-especializacoes.php' ?>"><i class="fa fa-circle"></i>Especialização</a></li-->
                    <li><a href="<?php echo PATH_ADMIN . '/list-cargo.php' ?>"><i class="fa fa-circle"></i>Cargo</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-nivel-ensino.php' ?>"><i class="fa fa-circle"></i>Nível Ensino</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-situacao.php' ?>"><i class="fa fa-circle"></i>Situação</a></li>
                </ul>
            </li>
            <br/>
            <li class="header">OPÇÕES DE VAGA</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-plus-circle"></i>
                    <span>Adicionar</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/add-vaga.php' ?>"><i class="fa fa-circle"></i>Nova Vaga</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-experiencia-minima.php' ?>"><i class="fa fa-circle"></i>Experiência Mínima</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-ensino-minimo.php' ?>"><i class="fa fa-circle"></i>Ensino Mínimo</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-tipo-deficiencia.php' ?>"><i class="fa fa-circle"></i>Tipo Deficiência</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-deficiencia.php' ?>"><i class="fa fa-circle"></i>Deficiência</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/add-conhecimento-informatica.php' ?>"><i class="fa fa-circle"></i>Conhecimento Informatica</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span>Listar</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/list-vagas.php' ?>"><i class="fa fa-circle"></i>Vagas</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-vagas-ativar.php' ?>"><i class="fa fa-circle"></i>Vagas a Ativar</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-vagas-expiradas.php' ?>"><i class="fa fa-circle"></i>Vagas Expiradas</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-vagas-estado.php' ?>"><i class="fa fa-circle"></i>Vagas por estado</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-vagas-cidade.php' ?>"><i class="fa fa-circle"></i>Vagas por cidade</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-vagas-empresa.php' ?>"><i class="fa fa-circle"></i>Vagas por empresa</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-experiencia-minima.php' ?>"><i class="fa fa-circle"></i>Experiência Mínima</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-ensino-minimo.php' ?>"><i class="fa fa-circle"></i>Ensino Mínimo</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-tipo-deficiencia.php' ?>"><i class="fa fa-circle"></i>Tipo Deficiência</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-deficiencia.php' ?>"><i class="fa fa-circle"></i>Deficiência</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-conhecimento-informatica.php' ?>"><i class="fa fa-circle"></i>Conhecimento Informatica</a></li>
                </ul>
            </li>
            <!--<li><a href="<?php echo PATH_ADMIN . '/'; ?>importar-vagas.php"><i class="fa fa-plus-circle"></i>Importar Vagas</a></li-->
            <br/>
            <li class="header">OUTROS</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i>
                    <span>Notícias</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/cadastrar-noticia.php' ?>"><i class="fa fa-plus-circle"></i>Cadastrar</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-noticias.php' ?>"><i class="fa fa-list-alt"></i>Listar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i>
                    <span>Blog</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/cadastrar-blog.php' ?>"><i class="fa fa-plus-circle"></i>Cadastrar Blog</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-blogs.php' ?>"><i class="fa fa-list-alt"></i>Listar</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/cadastrar-cat-blog.php' ?>"><i class="fa fa-plus-circle"></i>Cadastrar Categoria Blog</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-cat-blogs.php' ?>"><i class="fa fa-list-alt"></i>Listar Categorias</a></li>
                </ul>
            </li>
            <li><a href="<?php echo PATH_ADMIN . '/'; ?>edit-institucional.php"><i class="fa fa-plus-circle"></i>Institucional</a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bullhorn"></i>
                    <span>Publicidades</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/cadastrar-publicidade.php' ?>"><i class="fa fa-plus-circle"></i>Cadastrar</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-publicidades.php' ?>"><i class="fa fa-list-alt"></i>Listar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-text"></i>
                    <span>Perguntas Contato</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/cadastrar-pergunta.php' ?>"><i class="fa fa-plus-circle"></i>Cadastrar</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/list-perguntas.php' ?>"><i class="fa fa-list-alt"></i>Listar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Configurações</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo PATH_ADMIN . '/lista-usuarios.php' ?>"><i class="fa fa-users"></i>Listar Usuários</a></li>
                    <li><a href="<?php echo PATH_ADMIN . '/config-geral.php' ?>"><i class="fa fa-cogs"></i>Informações Gerais</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>

