<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Versão</b> 1.0
    </div>
    <strong>Todos direitos reservados 2018 Vagas & Vagas.
</footer>
</div>
<script src="<?php echo PATH_ASSETS . '/plugins/ckeditor/ckeditor.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/js/jquery/jquery.min.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/bootstrap/js/bootstrap.min.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/plugins/fastclick/lib/fastclick.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/tema/js/adminlte.min.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/plugins/jquery-sparkline/dist/jquery.sparkline.min.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/plugins/jvectormap/jquery-jvectormap-world-mill-en.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/plugins/jquery-slimscroll/jquery.slimscroll.min.js' ?>"></script>

<script src="<?php echo JS_DATATABLE_URL ?>/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo JS_DATATABLE_URL ?>/TableTools.min.js" type="text/javascript"></script>
<script src="<?php echo JS_DATATABLE_URL ?>/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo JS_DATATABLE_URL ?>/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script src="<?php echo JS_DATATABLE_URL ?>/lodash.min.js" type="text/javascript"></script>
<script src="<?php echo JS_DATATABLE_URL ?>/responsive/js/datatables.responsive.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo PATH_ASSETS ?>/js/jquery/jquery.mask.min.js"></script>
<script src="<?php echo PATH_ASSETS . '/plugins/chart.js/Chart.js' ?>"></script>
<script src="<?php echo PATH_ASSETS . '/js/geral/mascaras.js' ?>"></script>
<script>
    $(document).ready(function () {
        caminho = '<?php echo DIRETORIO; ?>';
    });
</script>
<script src="<?php echo PATH_ASSETS . '/js/geral/custom.js' ?>"></script>
<!--script src="<?php //   echo PATH_ASSETS . '/tema/js/pages/dashboard2.js'              ?>"></script-->
<!--script src="<?php //echo PATH_ASSETS . '/tema/js/demo.js'             ?>"></script-->

<script src="<?= URL_JS_UIKIT_PATH ?>/uikit-2.27.2.js" type="text/javascript"></script>
<script src="<?= URL_JS_UIKIT_PATH ?>/notify-2.27.2.js" type="text/javascript"></script>

<script src="<?php echo PATH_ASSETS . '/plugins/select2/select2.min.js' ?>"></script>
</body>
</html>