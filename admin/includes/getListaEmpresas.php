<?php
include "../../includes/conexao.php";

$data = [];
$params = $_REQUEST;

$columns = array(
    0 => 'id',
    1 => 'nome_empresa',
    2 => 'cnpj',
    3 => 'email',
    4 => 'fone',
    5 => 'estado',
    6 => 'cidade'
);

$parametros['id'] = null;
$parametros['nome_empresa'] = null;
$parametros['cnpj'] = null;
$parametros['email'] = null;
$parametros['telefone'] = null;
$parametros['estado'] = null;
$parametros['cidade'] = null;

if (!empty($params['columns'][0]['search']['value'])) {
    $parametros['id'] = $params['columns'][0]['search']['value'];
}

if (!empty($params['columns'][1]['search']['value'])) {
    $parametros['nome_empresa'] = $params['columns'][1]['search']['value'];
}

if (!empty($params['columns'][2]['search']['value'])) {
    $parametros['cnpj'] = $params['columns'][2]['search']['value'];
}

if (!empty($params['columns'][3]['search']['value'])) {
    $parametros['email'] = $params['columns'][3]['search']['value'];
}

if (!empty($params['columns'][4]['search']['value'])) {
    $parametros['telefone'] = $params['columns'][4]['search']['value'];
}

if (!empty($params['columns'][5]['search']['value'])) {
    $parametros['estado'] = $params['columns'][5]['search']['value'];
}

if (!empty($params['columns'][6]['search']['value'])) {
    $parametros['estado'] = $params['columns'][6]['search']['value'];
}

$parametros['order'] = $columns[$params['order'][0]['column']];
$parametros['dir'] = $params['order'][0]['dir'];
$parametros['start'] = $params['start'];
$parametros['length'] = $params['length'];

$sql = "SELECT
            id, nome_empresa, cnpj, email, fone, estado, cidade
        FROM TB_VV_EMPRESAS WHERE 1 ";

if (!empty($parametros['id'])) {
    $sql .= " AND id=" . $parametros['id'];
}

if (!empty($parametros['nome_empresa'])) {
    $sql .= " AND nome_empresa LIKE '%" . $parametros['nome_empresa'] . "%'";
}

if (!empty($parametros['cnpj'])) {
    $sql .= " AND cnpj = '" . $parametros['cnpj'] . "'";
}

if (!empty($parametros['email'])) {
    $sql .= " AND email = '" . $parametros['email'] . "'";
}

if (!empty($parametros['telefone'])) {
    $sql .= " AND fone = '" . $parametros['telefone'] . "'";
}

if (!empty($parametros['estado'])) {
    $sql .= " AND estado='" . $parametros['estado'] . "'";
}

if (!empty($parametros['cidade'])) {
    $sql .= " AND cidade='" . $parametros['cidade'] . "'";
}

$sql .= " ORDER BY " . $parametros['order'] . " " . $parametros['dir'] . " LIMIT " . $parametros['start'] . "," . $parametros['length'];


$res = mysqli_query($con, $sql) or die(mysqli_error($con));

$sqltotal = "SELECT count(*) as totalgeral FROM TB_VV_EMPRESAS";

$restotal = mysqli_query($con, $sqltotal);
$geral = mysqli_fetch_array($restotal);

$count = 0;

while ($vag = mysqli_fetch_array($res)) {

    $row = array();
    $row[] = $vag['id'];
    $row[] = $vag['nome_empresa'];
    $row[] = $vag['cnpj'];
    $row[] = $vag['email'];
    $row[] = $vag['fone'];
    $row[] = $vag['estado'];
    $row[] = $vag['cidade'];
    $row[] = null;

    $data[] = $row;
    $count++;
}

$output = [
    "draw" => intval($params['draw']),
    "recordsTotal" => $count,
    "recordsFiltered" => $geral['totalgeral'],
    "data" => $data,
];

echo json_encode($output);
