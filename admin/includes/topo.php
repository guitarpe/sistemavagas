<header class="main-header">
    <a href="." class="logo">
        <span class="logo-mini"><img src="" width="100px"></span>
        <span class="logo-lg"><img src="" width="100px"></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo PATH_IMAGENS . '/adm.png' ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs">ADMINISTRAÇÃO</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo PATH_IMAGENS . '/adm.png' ?>" class="img-circle" alt="User Image">

                            <p>ADMINISTRAÇÃO</p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo PATH_ADMIN . '/'; ?>meusdados.php" class="btn btn-default btn-flat">Meus Dados</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo PATH_ADMIN . '/actions/'; ?>sair.php" class="btn btn-default btn-flat">Sair</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
