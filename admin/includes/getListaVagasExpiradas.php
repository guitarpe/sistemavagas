<?php
include "../../includes/conexao.php";

$tipo = filter_input(INPUT_GET, 'tipo');

$data = [];
$params = $_REQUEST;

$columns = array(
    0 => 'vg.id',
    1 => 'emp.nome_empresa',
    2 => 'vg.cargo',
    3 => 'vg.estado',
    4 => 'vg.data_anuncio',
    5 => 'vg.data_expira'
);

$parametros['id'] = null;
$parametros['cargo'] = null;
$parametros['empresa'] = null;
$parametros['estado'] = null;
$parametros['data'] = null;

if (!empty($params['columns'][0]['search']['value'])) {
    $parametros['id'] = $params['columns'][0]['search']['value'];
}

if (!empty($params['columns'][1]['search']['value'])) {
    $parametros['cargo'] = $params['columns'][1]['search']['value'];
}

if (!empty($params['columns'][2]['search']['value'])) {
    $parametros['empresa'] = $params['columns'][2]['search']['value'];
}

if (!empty($params['columns'][3]['search']['value'])) {
    $parametros['estado'] = $params['columns'][3]['search']['value'];
}

if (!empty($params['columns'][4]['search']['value'])) {
    $parametros['data_ini'] = $params['columns'][4]['search']['value'];
}

if (!empty($params['columns'][5]['search']['value'])) {
    $parametros['data_fim'] = $params['columns'][5]['search']['value'];
}

$parametros['order'] = $columns[$params['order'][0]['column']];
$parametros['dir'] = $params['order'][0]['dir'];
$parametros['start'] = $params['start'];
$parametros['length'] = $params['length'];

$sql = "SELECT
            vg.id, emp.nome_empresa, vg.cargo, vg.data_anuncio, vg.data_expira, vg.estado, vg.status
        FROM TB_VV_VAGAS vg
        LEFT OUTER JOIN TB_VV_EMPRESAS emp ON emp.id=vg.id_empresa WHERE date(data_expira) < date(now()) ";


if (!empty($parametros['id'])) {
    $sql .= " AND vg.id=" . $parametros['id'];
}

if (!empty($parametros['cargo'])) {
    $sql .= " AND vg.cargo LIKE '%" . $parametros['cargo'] . "%'";
}

if (!empty($parametros['empresa'])) {
    $sql .= " AND emp.nome_empresa LIKE '%" . $parametros['empresa'] . "%'";
}

if (!empty($parametros['estado'])) {
    $sql .= " AND vg.estado = '" . $parametros['estado'] . "'";
}

if (!empty($parametros['data_ini']) && !empty($parametros['data_fim'])) {
    $sql .= " AND DATE(vg.data_expira) BETWEEN DATE('" . $parametros['data_ini'] . "') AND DATE('" . $parametros['data_fim'] . "')";
}

$sql .= " ORDER BY " . $parametros['order'] . " " . $parametros['dir'] . " LIMIT " . $parametros['start'] . "," . $parametros['length'];

$res = mysqli_query($con, $sql) or die(mysqli_error($con));

$sqltotal = "SELECT count(*) as totalgeral FROM TB_VV_VAGAS WHERE date(data_expira) < date(now()) ";

$restotal = mysqli_query($con, $sqltotal);
$geral = mysqli_fetch_array($restotal);

$count = 0;

while ($vag = mysqli_fetch_array($res)) {

    $dt = new DateTime($vag['data_anuncio']);
    $dtex = new DateTime($vag['data_expira']);

    $row = array();
    $row[] = $vag['id'];
    $row[] = $vag['cargo'];
    $row[] = $vag['nome_empresa'];
    $row[] = $vag['estado'];
    $row[] = $dt->format("d/m/Y");
    $row[] = $dtex->format("d/m/Y");
    $row[] = null;

    $data[] = $row;
    $count++;
}

$output = [
    "draw" => intval($params['draw']),
    "recordsTotal" => $count,
    "recordsFiltered" => $geral['totalgeral'],
    "data" => $data,
];

echo json_encode($output);
