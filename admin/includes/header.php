<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Vagas & Vagas - Painel Administrativo</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/plugins/font-awesome/css/font-awesome.min.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/plugins/select2/select2.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/plugins/Ionicons/css/ionicons.min.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/plugins/jvectormap/jquery-jvectormap.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/bootstrap/css/bootstrap.min.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/tema/css/AdminLTE.min.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/tema/css/outros.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/tema/css/skins/_all-skins.min.css' ?>">

        <link href="<?php echo URL_CSS_DATATABLE ?>/dataTable.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?php echo JS_DATATABLE_URL ?>/responsive/css/datatables.responsive.css">

        <link href="<?= URL_CSS_UIKIT_PATH ?>/uikit-2.27.2.css" rel="stylesheet"/>
        <link href="<?= URL_CSS_UIKIT_PATH ?>/notify-2.27.2.css" rel="stylesheet"/>
        <link href="<?= URL_CSS_UIKIT_PATH ?>/customUkit.css" rel="stylesheet"/>

        <script src="<?php echo PATH_ASSETS . '/js/jquery/jquery-1.10.1.min.js' ?>"></script>

        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/plugins/select2/select2-bootstrap.css' ?>">
        <link rel="stylesheet" href="<?php echo PATH_ASSETS . '/plugins/select2/select2.css' ?>">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">