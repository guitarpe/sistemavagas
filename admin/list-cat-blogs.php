<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_CATEGORIAS_BLOG ORDER BY nome ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Outros</a></li>
                <li class="active">Listar Categorias Blogs</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Categorias Blogs</h2>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-cat-blogs" class="table table-bordered table-striped table-pages">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th scope="col">#</th>
                            <th scope="col">Titulo</th>
                            <th scope="col" width="100"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($value = mysqli_fetch_array($res)) {

                            ?>

                            <tr>
                                <th scope="row"><?php echo $i ?></th>
                                <td><?php echo $value['nome'] ?></td>
                                <td class="text-center">
                                    <?php if (intval($value['status']) == 1) { ?>
                                        <a class="btn btn-xs btn-info" href="actions/ativar-cat-blog.php?tipo=0&id=<?php echo $value['id_cat'] ?>" title="Inativar">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a class="btn btn-xs btn-success" href="actions/ativar-cat-blog.php?tipo=1&id=<?php echo $value['id_cat'] ?>" title="Ativar">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    <?php } ?>
                                    <a class="btn btn-xs btn-warning" href="edit-cat-blog.php?id=<?php echo $value['id_cat'] ?>">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger" href="actions/delete-cat-blog.php?id=<?php echo $value['id_cat'] ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}