<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_INSTITUCIONAL WHERE id_institucional = '1'");
    $vetor = mysqli_fetch_array($res);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Institucional</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Atualizando Institucional</h2>
            <form action="actions/recebe_alterarinstitucional.php" method="post" enctype="multipart/form-data">
                <div class="container">
                    <div class="form-group">
                        <label for="images">Texto</label>
                        <textarea name="texto" class="ckeditor" id="editor1" required><?php echo $vetor['texto'] ?></textarea>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}