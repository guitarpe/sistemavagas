<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Listar Candidatos</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Empresas Cadastradas</h2>
            <table class="table">
                <thead class="thead-dark">
                    <tr class="center">
                        <th>Nome da Empresa</th>
                        <th>CNPJ</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Estado</th>
                        <th>Cidade</th>
                    </tr>
                </thead>
                <form>
                    <tbody>
                        <tr>
                            <th><input type="text" name="nome_empresa" id="nome_empresa" class="form-control"></th>
                            <th><input type="text" name="cnpj" id="cnpj" class="form-control"></th>
                            <th><input type="text" name="email" id="email" class="form-control"></th>
                            <th><input type="text" name="telefone" id="telefone" class="form-control"></th>
                            <th><select name="estados" id="estados" class="form-control">
                                    <option value="">Escolha um Estado</option>
                                    <?php
                                    $result = mysqli_query($con, "select * FROM TB_VV_ESTADOS order by nome ASC");

                                    while ($row = mysqli_fetch_array($result)) {
                                        echo "<option value='" . $row['uf'] . "'>" . $row['nome'] . "</option>";
                                    }

                                    ?>
                                </select></th>
                            <th><select name="cidades" id="cidades" class="form-control">
                                    <option value="">Escolha um Estado</option>
                                </select></th>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <th><button type="button" id="btnbuscar" class="btn btn-success">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Buscar
                                </button></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tbody>
                </form>
            </table>
            </br>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-admin-empresas" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th>#</th>
                            <th>Nome da Empresa</th>
                            <th>CNPJ</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th>Estado</th>
                            <th>Cidade</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}