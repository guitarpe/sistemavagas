<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATOS ORDER BY id DESC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Cadastro de candidato</h1>
            <form action="actions/recebe_alteracadastracandidato.php" method="post" data-toggle="validator" role="form">
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input type="text" placeholder="Nome" name="nome" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input type="text" placeholder="E-mail" name="email" id="txtEmail" class="form-control validemail" required>
                            <span id="msg-email" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input type="text" placeholder="Confirmar e-mail" id="repetir_email" class="form-control validemail nopaste" required autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="password" placeholder="Senha" name="senha" class="form-control txtSenha" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="password" placeholder="Confirmar senha" name="confirmar_senha" class="form-control confisenha nopaste" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>

                            </span>
                            <input class="cpf form-control verifica_cpf_cadastrado" type="text" placeholder="CPF" name="cpf" required>
                            <span id="msg-cpf" style="display:none"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input class="form-control dinheiro" type="text" placeholder="Pretensão Salarial (opcional)" id="pretensao_salarial" name="pretensao_salarial">
                            <span class="input-group-addon">
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" name="filhos" id="filhos">
                                <option value="">Selecione a quantidade de Filhos</option>
                                <option value="0">Não tenho filhos</option>
                                <option value="1">Um</option>
                                <option value="2">Dois</option>
                                <option value="3">Três</option>
                                <option value="4">Quatro ou mais</option>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-child" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" name="estadocivil" id="estadocivil">
                                <option value="">Selecione o Estado Civil</option>
                                <option value="Casado">Casado</option>
                                <option value="Divorciado">Divorciado</option>
                                <option value="Separado">Separado</option>
                                <option value="Solteiro">Solteiro</option>
                                <option value="União Estável">União Estável</option>
                                <option value="Viúvo">Viúvo</option>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                            <input class="data form-control" type="text" placeholder="Data de Nascimento" name="datanasc" required>
                            <span id="msg-data" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 area-radio-form">
                        <label class="fleft width-60 margin-top-7">Sexo</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="sexo1" name="sexo" class="form-control-input" value="Masculino" required>
                            <label class="form-control-label" for="sexo1">Masculino </label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="sexo2" name="sexo" class="form-control-input" value="Feminino" required>
                            <label class="form-control-label" for="sexo2">Feminino </label>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-earphone"></i>
                            </span>
                            <input class="form-control fones" type="text" placeholder="Telefone" name="fone" id="telefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                            <span id="msg-fone" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-earphone"></i>
                            </span>
                            <input class="form-control fones" type="text" placeholder="Celular" name="celular" id="celular" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                            <span id="msg-fone" style="display:none"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <select name="estado" id="estado" class="form-control busca_cidades" required>
                                <option disabled selected value="">Estado</option>
                                <?php
                                $result = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                while ($val = mysqli_fetch_array($result)) {
                                    echo "<option value='$val[uf]'>$val[nome]</option>";
                                }

                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <select name="cidade" id="cidades" class="form-control cidades" required>
                                <option value="">Cidade</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Tem disponibilidade para viajar?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="viajar1" name="viajar" value="0" class="form-control-input">
                            <label class="custom-control-label" for="viajar1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="viajar2" name="viajar" value="1" class="form-control-input">
                            <label class="custom-control-label" for="viajar2">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Possui veículo próprio?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="veiculo1" name="veiculo" value="0" class="form-control-input">
                            <label class="custom-control-label" for="veiculo1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="veiculo2" name="veiculo" value="1" class="form-control-input">
                            <label class="custom-control-label" for="veiculo2">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Possui CNH?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="cnh1" name="cnh" value="0" data-area="#area-cnh" class="habilita form-control-input">
                            <label class="custom-control-label" for="cnh1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="cnh2" name="cnh" value="1" data-area="#area-cnh" class="habilita form-control-input">
                            <label class="custom-control-label" for="cnh2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-cnh" class="form-group row hidden">
                    <div class="col-sm-12 cnh area-radio-form">
                        <label class="fleft width-290 margin-top-7">Qual categoria deve ser a CNH:</label>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="a" value="A"/>
                            <label class="custom-control-label" for="a">Categoria A</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="b" value="B"/>
                            <label class="custom-control-label" for="b">Categoria B</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="c" value="C"/>
                            <label class="custom-control-label" for="c">Categoria C</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="d" value="D"/>
                            <label class="custom-control-label" for="d">Categoria D</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="e" value="E"/>
                            <label class="custom-control-label" for="e">Categoria E</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">É uma Pessoa com Deficiência?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="deficiencia1" name="deficiencia" value="0" data-area="#area-deficiencia" class="habilita form-control-input">
                            <label class="custom-control-label" for="deficiencia1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="deficiencia2" name="deficiencia" value="1" data-area="#area-deficiencia" class="habilita form-control-input">
                            <label class="custom-control-label" for="deficiencia2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-deficiencia" class="form-group row hidden">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <label class="fleft">Tipos de Deficiência</label>
                    </div>
                    <?php
                    $res_tipos = mysqli_query($con, "SELECT id, nome FROM TB_VV_TIPOS_DEFICIENCIA");
                    while ($row = mysqli_fetch_array($res_tipos)) {

                        ?>
                        <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                            <label><?php echo $row["nome"] ?> </label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                            <?php
                            $res_def = mysqli_query($con, "SELECT id, nome, descricao FROM TB_VV_DEFICIENCIAS ide WHERE ide.id_tipo=" . $row['id'] . " ORDER BY ide.nome ASC");
                            while ($row2 = mysqli_fetch_array($res_def)) {

                                ?>
                                <div class="form-check fleft width-32-p">
                                    <input class="form-check-input" id="def-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>" type="checkbox" name="deficiencias[]">
                                    <label class="form-check-label" title="<?php echo $row2['descricao'] ?>"><?php echo $row2["nome"] ?></label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Possui conhecimentos em Informática?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="informatica1" name="informatica" value="0" data-area="#area-informatica" class="habilita form-control-input">
                            <label class="custom-control-label" for="informatica1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="informatica2" name="informatica" value="1" data-area="#area-informatica" class="habilita form-control-input">
                            <label class="custom-control-label" for="informatica2">Sim</label>
                        </div>

                    </div>
                </div>
                <div id="area-informatica" class="form-group row hidden">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <label class="fleft">Conhecimentos</label>
                    </div>
                    <?php
                    $res_categorias = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFO_CATEGORIAS");
                    while ($row = mysqli_fetch_array($res_categorias)) {

                        ?>
                        <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                            <label><?php echo $row["nome"] ?> </label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                            <?php
                            $res_info = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFORMATICA inf WHERE inf.id_categoria=" . $row['id'] . " ORDER BY inf.nome ASC");
                            while ($row2 = mysqli_fetch_array($res_info)) {

                                ?>
                                <div class="form-check fleft width-32-p">
                                    <input class="form-check-input" type="checkbox" name="informaticas[]" id="info-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>">
                                    <label class="form-check-label"><?php echo $row2["nome"] ?> </label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="control-label">Objetivo profissional</label>
                        <textarea class="form-control" placeholder="Objetivo profissional" id="objetivo" name="objetivo"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div id="origem">
                        <div class="col-md-12">
                            <div class="input-group div_cargo" style="margin-bottom: 10px">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <select id="select_cargo" name="cargoprofissional[]" class="form-control select2 cargoop" required>
                                    <option value="">Cargo Profissional</option>
                                    <?php
                                    $result1 = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                    while ($val = mysqli_fetch_array($result1)) {
                                        echo "<option value='$val[nome]'>$val[nome]</option>";
                                    }

                                    ?>
                                    <option>Outro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row border hidden outro_cargo">
                                <div class="input-group" class="third hidden" id="label-ncargo">
                                    <input class="form-control" type="text" name="cargoprofissional[]" placeholder="Digite o Cargo Profissional" id="novocargo" >
                                    <span class="input-group-addon">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <input type="button" value="Adicionar Cargo" id="add_cargo" class="btn btn-warning btn-md form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="button" value="Excluir Cargo" id="rem_cargo" class="btn btn-danger form-control" style="display: none">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 area-radio-form">
                        <label class="fleft width-290 margin-top-7">Você ja possui experiência profissional?</label>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="experiencia1" name="experiencia" class="form-control-input" value="Não" checked="checked">
                            <label class="form-control-label" for="experiencia1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline fleft">
                            <input type="radio" id="experiencia2" name="experiencia" class="form-control-input" value="Sim">
                            <label class="form-control-label" for="experiencia2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="origem_empresa" style="display: none">
                    <div class="campos_empresa">
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-user"></i>
                                    </span>
                                    <input type="text" placeholder="Nome da empresa" name="nomeempresa[]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-usd"></i>
                                    </span>
                                    <input class="dinheiro form-control salario" type="text" placeholder="Salário (opcional)" name="salario[]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="div_cargo_emp">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-briefcase"></i>
                                        </span>
                                        <select name="cargoempresa[]" class="form-control" required>
                                            <option  value="">Cargo Profissional</option>
                                            <?php
                                            $result2 = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                            while ($val = mysqli_fetch_array($result2)) {
                                                echo "<option>$val[nome]</option>";
                                            }

                                            ?>
                                            <option>Outro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 outro_cargo hidden" id="label-ncargoexp">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </span>
                                        <input class="form-control" type="text" placeholder="Digite o Cargo Profissional" id="novocargoexp" name="cargoempresa[]" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <textarea name="atividades[]" placeholder="Atividades Exercidas" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    <input class="data-s form-control data_ini" type="text" placeholder="Início (mês/ano)" name="datainicio[]" required>
                                    </br>
                                    <span id="msg-dataexp" style="display:none"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    <input class="data-s form-control data_fim" type="text" placeholder="Fim (mês/ano)" name="datafim[]" required>
                                    </br>
                                    <span id="msg-dataexp2" style="display:none"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input set_empr_atual" type="checkbox" name="empregoatual[]" value="1">
                                    <label class="form-check-label" for="empatual">
                                        Emprego atual
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <input type="button" value="Adicionar Experiência" id="adicionar_experiencia" class="btn btn-warning form-control">
                        </div>
                        <div class="col-md-3">
                            <input type="button" value="Excluir Experiência" id="remover_experiencia" class="btn btn-danger form-control" style="display: none">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <strong>Formação acadêmica/técnica:</strong>
                    </div>
                </div>
                <div class="div_formacao" style="margin-bottom: 20px">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <select name="formacao[]" class="form-control slct_formacao">
                                    <option value="">Formação</option>
                                    <?php
                                    $result5 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                                    while ($val = mysqli_fetch_array($result5)) {
                                        echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_instituicao hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <input type="text" placeholder="Instituição de ensino" name="instituicao[]" class="form-control">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_situacao hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <select name="situacao[]" class="form-control slct_situacao">
                                    <option value="">Situação</option>
                                    <?php
                                    $result4 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                                    while ($val = mysqli_fetch_array($result4)) {
                                        if ($val["nome"] == "Trancado") {
                                            echo "<option value='$val[nome]' id='trancado'>$val[nome]</option>";
                                        } else {
                                            echo "<option value='$val[nome]'>$val[nome]</option>";
                                        }
                                    }

                                    ?>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 campo_nomecurso hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <input type="text" placeholder="Nome do curso" name="nomeformacao[]" class="form-control">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 campo_ano_conclusao hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <input class="data-ano form-control info_ano" type="text" placeholder="Ano de conclusão" name="anofim[]">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_semestre hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <select name="semestre[]" class="form-control">
                                    <option value="">Semestre</option>
                                    <option value="1">1º semestre</option>
                                    <option value="2">2º semestre</option>
                                    <option value="3">3º semestre</option>
                                    <option value="4">4º semestre</option>
                                    <option value="5">5º semestre</option>
                                    <option value="6">6º semestre</option>
                                    <option value="7">7º semestre</option>
                                    <option value="8">8º semestre</option>
                                    <option value="9">9º semestre</option>
                                    <option value="10">10º semestre</option>
                                    <option value="11">11º semestre</option>
                                    <option value="12">12º semestre</option>
                                    <option value="13">13º semestre</option>
                                    <option value="14">14º semestre</option>
                                    <option value="15">15º semestre</option>
                                    <option value="16">16º semestre</option>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_turno hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <select name="turno[]" class="form-control">
                                    <option value="">Turno</option>
                                    <option value="Manhã">Manhã</option>
                                    <option value="Tarde">Tarde</option>
                                    <option value="Noite">Noite</option>
                                    <option value="EAD">EAD</option>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <input type="button" value="Adicionar Formação" id="adicionar_formacao" class="btn btn-warning form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="button" value="Excluir Formação" id="remover_formacao" class="btn btn-danger form-control" style="display: none">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <strong>Preencha abaixo os seus cursos extracurriculares:</strong>
                    </div>
                </div>
                <div class="div_cursos" style="margin-bottom: 20px">
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <input type="text" placeholder="Nome do curso" name="nomecurso[]" class="form-control">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <input type="text" placeholder="Carga Horária" name="cargahoraria[]" class="form-control">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <input type="text" placeholder="Instituição de ensino" name="instituicao2[]" class="form-control">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <select name="situacao2[]" class="form-control slct_situacao2" data-length="0">
                                    <option value="">Situação</option>
                                    <?php
                                    $result3 = mysqli_query($con, "SELECT nome FROM TB_VV_SITUACOES_ENSINO ORDER BY nome ASC");
                                    while ($val = mysqli_fetch_array($result3)) {
                                        echo "<option value='$val[nome]'>$val[nome]</option>";
                                    }

                                    ?>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 campo_turno hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <select name="turnoextra[]" class="form-control">
                                    <option value="">Turno</option>
                                    <option value="Manhã">Manhã</option>
                                    <option value="Tarde">Tarde</option>
                                    <option value="Noite">Noite</option>
                                    <option value="EAD">EAD</option>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4 dias_semanas hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <select class="form-control dias_semana" id="slct_0" multiple>
                                    <option value="Domingo">Domingo</option>
                                    <option value="Segunda-feira">Segunda-feira</option>
                                    <option value="Terca-feira">Terça-feira</option>
                                    <option value="Quarta-feira">Quarta-feira</option>
                                    <option value="Quinta-feira">Quinta-feira</option>
                                    <option value="Sexta-feira">Sexta-feira</option>
                                    <option value="Sábado">Sábado</option>
                                </select>
                                <input type="hidden" class="dias_semana_hiden" id="dias_semana_0" name="diassemana[]"/>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-4 campo_ano_conclusao hidden">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </span>
                                <input class="data-ano form-control ano_conclusao" type="text" placeholder="Ano de conclusão" name="anofim[]">
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <input type="button" value="Adicionar Curso" id="adicionar_curso" class="btn btn-warning form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="button" value="Excluir Curso" id="remover_curso" class="btn btn-danger form-control" style="display: none">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <strong>
                            Idiomas
                        </strong>
                    </div>
                </div>
                <div class="div_idioma" style="margin-bottom: 20px">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <select name="idioma[]" class="form-control">
                                    <option value="">Idioma</option>
                                    <?php
                                    $result1 = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                                    while ($val = mysqli_fetch_array($result1)) {
                                        echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <select name="nivelidioma[]" class="form-control">
                                    <option value="">Nível</option>
                                    <?php
                                    $result2 = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                                    while ($val = mysqli_fetch_array($result2)) {
                                        echo "<option value='$val[descricao]'>$val[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span id="msg-email" style="display:none"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <input type="button" value="Adicionar Idioma" id="adicionar_idioma" class="btn btn-warning form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="button" value="Excluir Idioma" id="remover_idioma" class="btn btn-danger form-control" style="display: none">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-success btn-icon" type="submit">
                            Finalizar Cadastro <span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            caminho = '<?php echo DIRETORIO; ?>';
        });
    </script>
    <script src="<?php echo PATH_ASSETS . '/js/geral/cadastrocandidato.js' ?>"></script>
    <?php
    include "includes/footer.php";
}