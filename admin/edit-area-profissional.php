<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_AREAS_PROF WHERE id = $_GET[id]");
    $value = mysqli_fetch_array($res);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Candidato</a></li>
                <li class="active">Editar Área Profissional</li>
            </ol>
        </section>


        <section class="content">
            <h2 align="center">Editar Área Profissional</h2>
            <form action="actions/recebe_alteracadastraareaprofissional.php?id=<?php echo $value['id'] ?>" method="post">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="areaprof">Área Profissional</label>
                        <input type="text" class="form-control" id="areaprof" name="areaprof" placeholder="Digite a área profissional" value="<?php echo $value['nome'] ?>">
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}