<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'admin') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT US.id, EMP.nome_empresa as nome FROM TB_VV_EMPRESAS EMP INNER JOIN TB_VV_USUARIOS US ON US.id=EMP.id WHERE US.tipo = 'empresa' AND US.status <> 1");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="."><i class="fa fa-dashboard"></i> Início</a></li>
                <li class="active">Empresa</li>
            </ol>
        </section>
        <br/>

        <section class="content">
            <?php if (mysqli_num_rows($res)) { ?>
                <h2 class="text-center">Aguardando Aprovação</h2>
                <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                    <table id="tb-notificacoes" class="table table-bordered table-striped table-pages">
                        <thead class="thead-dark">
                            <tr class="center">
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col" width="100"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            while ($value = mysqli_fetch_array($res)) {

                                ?>

                                <tr>
                                    <th scope="row"><?php echo $i ?></th>
                                    <td><?php echo $value['nome'] ?></td>
                                    <td>
                                        <a class="btn btn-xs btn-success" href="actions/aprova-empresa.php?id=<?php echo $value['id'] ?>">
                                            <i class="fa fa-check"></i> Aprovar
                                        </a>
                                    </td>
                                </tr>

                                <?php
                                $i++;
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
            <?php } else { ?>

                <div class="jumbotron">
                    <div class="container text-center">
                        <i class="fa fa-bell-slash" style="font-size: 58px"></i>
                        <br/><br/>
                        <span>Nenhuma notificação no momento.</span>
                        <br/><br/>
                        <a href=".">Voltar</a>
                    </div>
                </div>

            <?php } ?>
        </section>


    </div>
    <?php
    include "includes/footer.php";
}