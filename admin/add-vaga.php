<?php
session_start();

include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>

    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Add Vaga</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Adicionar Vaga</h2>
            <form action="<?php echo PATH_ADMIN . '/actions/recebe_alteracadastravaga.php' ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Empresa</label>
                            <select name="id_empresa" class="form-control select2" style="width: 100%;" required>
                                <option selected="selected">Selecione uma Empresa</option>
                                <?php
                                $sql_empresa = mysqli_query($con, "select *  FROM TB_VV_EMPRESAS order by nome_empresa ASC");

                                while ($vetor_empresa = mysqli_fetch_array($sql_empresa)) {

                                    ?>
                                    <option value="<?php echo $vetor_empresa['id']; ?>"><?php echo $vetor_empresa['nome_empresa']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Forma de Contratação</label>
                            <select name="formacontratacao" id="formacontratacao" class="form-control select2" required>
                                <option disabled value="">Forma de Contratação</option>
                                <?php
                                $res_formas = mysqli_query($con, "SELECT descricao FROM TB_VV_FORMAS_CONTRAT ORDER BY descricao ASC");
                                while ($formas = mysqli_fetch_array($res_formas)) {

                                    ?>

                                    <option value="<?php echo $formas['descricao']; ?>"><?php echo $formas['descricao']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Cargo Profissional</label>
                            <select name="cargo" id="cargo" class="form-control select2" required>
                                <option disabled value="">Cargo Profissional</option>
                                <?php
                                $res_cargos = mysqli_query($con, "SELECT nome FROM TB_VV_CARGOS ORDER BY nome ASC");
                                while ($cargo = mysqli_fetch_array($res_cargos)) {

                                    ?>

                                    <option value="<?php echo $cargo['nome']; ?>"><?php echo $cargo['nome']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nível Hierárquico</label>
                            <select name="nivelhierarquico" id="nivelhierarquico" class="form-control select2" required>
                                <option disabled value="">Nível Hierárquico</option>
                                <?php
                                $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_HIERARQ ORDER BY descricao ASC");
                                while ($niveis = mysqli_fetch_array($res_niveis)) {

                                    ?>

                                    <option value="<?php echo $niveis['descricao']; ?>"><?php echo $niveis['descricao']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Número de Vagas</label>
                            <input name="nvagas" type="text" class="form-control" placeholder="Número de Vagas" id="nvagas" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Turno</label>
                            <select name="turno" id="turno" class="form-control select2" required>
                                <option disabled value="">Turno</option>
                                <?php
                                $res_turnos = mysqli_query($con, "SELECT descricao FROM TB_VV_TURNOS ORDER BY id ASC");
                                while ($turnos = mysqli_fetch_array($res_turnos)) {

                                    ?>
                                    <option value="<?php echo $turnos['descricao']; ?>"><?php echo $turnos['descricao']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Atividades a serem desenvolvidas</label>
                            <textarea name="atividades" placeholder="Atividades a serem desenvolvidas" id="atividades" class="form-control" rows="8"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Remuneração</label>
                            <select name="faixasalarial" class="form-control" id="tipobusca" required>
                                <option disabled value="">Faixa Salarial</option>
                                <?php
                                $res_sal = mysqli_query($con, "SELECT * FROM TB_VV_FAIXAS_SALARIAL ORDER BY id ASC");
                                while ($sal = mysqli_fetch_array($res_sal)) {

                                    ?>
                                    <option value="<?php echo $sal['descricao']; ?>"><?php echo $sal['descricao']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div id="palco1" style="display: none">
                        <div id="2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Salário</label>
                                    <input name="vsalario" class="form-control" type="text" onKeyPress="mascara(this, mvalor)" placeholder="Salário" id="vsalario">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Experiência Mínima</label>
                            <select name="experienciamin" class="form-control select2" required>
                                <option disabled value="">Nível Hierárquico</option>
                                <?php
                                $res_exp = mysqli_query($con, "SELECT descricao FROM TB_VV_EXP_MINIMAS ORDER BY descricao ASC");
                                while ($exp = mysqli_fetch_array($res_exp)) {

                                    ?>

                                    <option value="<?php echo $exp['descricao']; ?>"><?php echo $exp['descricao']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Ensino Mínimo</label>
                            <select name="ensinomin" class="form-control select2" required>
                                <option disabled value="">Ensino Mínimo</option>
                                <?php
                                $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_ENSINO ORDER BY descricao ASC");
                                while ($nivel = mysqli_fetch_array($res_niveis)) {

                                    ?>

                                    <option value="<?php echo $nivel['descricao']; ?>"><?php echo $nivel['descricao']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 padding-bottom-15">
                        <div class="form-group">
                            <label>Benefícios</label>
                            </br>
                            <?php
                            $sql_beneficios = mysqli_query($con, "select * FROM TB_VV_BENEFICIOS order by descricao ASC");

                            $ct = 0;
                            while ($vetor_beneficios = mysqli_fetch_array($sql_beneficios)) {

                                ?>
                                <div class="form-check op fleft">
                                    <input type="checkbox" class="form-check-input" id="chbbenef<?php echo $ct; ?>" name="beneficios[]" value="<?php echo $vetor_beneficios['descricao']; ?>"/>
                                    <label class="form-check-label" for="chbbenef<?php echo $ct; ?>"><?php echo $vetor_beneficios['descricao']; ?></label>
                                </div>

                                <?php
                                $ct++;
                            }

                            ?>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="fleft width-100 margin-top-10 height-12">Sexo:</label>
                            <div class="form-check op3 fleft">
                                <input type="radio" class="form-radio-input" name="sexo" id="indif" value="Indiferente">
                                <label class="form-check-label">Indiferente</label>
                            </div>
                            <div class="form-check op3 fleft">
                                <input type="radio" class="form-radio-input" name="sexo" id="masc" value="Masculino">
                                <label class="form-check-label">Masculino</label>
                            </div>
                            <div class="form-check op3 fleft">
                                <input type="radio" class="form-radio-input" name="sexo" id="femin" value="Feminino">
                                <label class="form-check-label">Feminino</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Vaga de Emprego para outro Estado ou Cidade?</label>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" data-area="#area-cidades" class="habilita form-control-input" id="outroestnao" name="outroestadocidade" value="0" checked="checked"/>
                            <label class="custom-control-label" for="outroestnao">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" data-area="#area-cidades" class="habilita form-control-input" id="outroestsim" name="outroestadocidade" value="1"/>
                            <label class="custom-control-label" for="outroestsim">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="row hidden" id="area-cidades">
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                        <div class="form-group input-group">
                            <select class="form-control busca_cidades" name="estadovaga" id="estadovaga">
                                <option value="">Estado</option>
                                <?php
                                $res_estados = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                while ($estado = mysqli_fetch_array($res_estados)) {
                                    echo "<option value='$estado[uf]'>$estado[nome]</option>";
                                }

                                ?>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-map" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                        <div class="form-group input-group">
                            <select class="form-control cidades" name="cidadevaga" id="cidadevaga">
                                <option value="">Cidade</option>
                            </select>
                            <span class="input-group-addon">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Tem disponibilidade para viajar?</label>
                        <div class="form-radio form-control-inline op2  fleft">
                            <input type="radio" id="viajar1" name="viajar" value="0" class="form-control-input" checked="checked">
                            <label class="custom-control-label" for="viajar1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="viajar2" name="viajar" value="1" class="form-control-input">
                            <label class="custom-control-label" for="viajar2">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Deve possuir veículo próprio?</label>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="veiculo1" name="veiculo" value="0" class="form-control-input" checked="checked">
                            <label class="custom-control-label" for="veiculo1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="veiculo2" name="veiculo" value="1" class="form-control-input">
                            <label class="custom-control-label" for="veiculo2">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Possui CNH?</label>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="cnh1" name="cnh" value="Não" data-area="#area-cnh" class="habilita form-control-input" checked="checked">
                            <label class="custom-control-label" for="cnh1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="cnh2" name="cnh" value="Sim" data-area="#area-cnh" class="habilita form-control-input">
                            <label class="custom-control-label" for="cnh2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-cnh" class="form-group row hidden">
                    <div class="col-sm-12 cnh area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Qual categoria deve ser a CNH:</label>
                        <div class="form-radio form-control-inline op4 fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="a" value="A" checked="checked"/>
                            <label class="custom-control-label" for="a">Categoria A</label>
                        </div>
                        <div class="form-radio form-control-inline op4 fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="b" value="B"/>
                            <label class="custom-control-label" for="b">Categoria B</label>
                        </div>
                        <div class="form-radio form-control-inline op4 fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="c" value="C"/>
                            <label class="custom-control-label" for="c">Categoria C</label>
                        </div>
                        <div class="form-radio form-control-inline op4 fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="d" value="D"/>
                            <label class="custom-control-label" for="d">Categoria D</label>
                        </div>
                        <div class="form-radio form-control-inline op4 fleft">
                            <input class="form-control-input" name="tipocnh" type="radio" id="e" value="E"/>
                            <label class="custom-control-label" for="e">Categoria E</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">É uma Pessoa com Deficiência?</label>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="deficiencia1" name="deficiencia" value="Não" data-area="#area-deficiencia" class="habilita form-control-input" checked="checked">
                            <label class="custom-control-label" for="deficiencia1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="deficiencia2" name="deficiencia" value="Sim" data-area="#area-deficiencia" class="habilita form-control-input">
                            <label class="custom-control-label" for="deficiencia2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-deficiencia" class="form-group row hidden">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <label class="fleft margin-top-10 height-12">Tipos de Deficiência</label>
                    </div>
                    <?php
                    $res_tipos = mysqli_query($con, "SELECT * FROM TB_VV_TIPOS_DEFICIENCIA");
                    while ($row = mysqli_fetch_array($res_tipos)) {

                        ?>
                        <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                            <label><?php echo $row["nome"] ?> </label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                            <?php
                            $res_deficiencias = mysqli_query($con, "SELECT nome, descricao FROM TB_VV_DEFICIENCIAS WHERE id_tipo = $row[id]");

                            while ($row2 = mysqli_fetch_array($res_deficiencias)) {

                                ?>
                                <div class="form-check fleft op width-32-p">
                                    <input class="form-check-input" id="def-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>" type="checkbox" name="deficiencias[]">
                                    <label class="form-check-label" title="<?php echo $row2['descricao'] ?>"><?php echo $row2["nome"] ?></label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks2">
                        <strong>Selecione ao menos um</strong>
                    </div>
                    <input type="hidden" id="valdeficiencias" name="valdeficiencias"/>
                </div>
                <div class="row">
                    <div class="col-sm-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Possui conhecimentos em Informática?</label>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="informatica1" name="informatica" value="Não" data-area="#area-informatica" class="habilita form-control-input" checked="checked">
                            <label class="custom-control-label" for="informatica1">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" id="informatica2" name="informatica" value="Sim" data-area="#area-informatica" class="habilita form-control-input">
                            <label class="custom-control-label" for="informatica2">Sim</label>
                        </div>
                    </div>
                </div>
                <div id="area-informatica" class="form-group row hidden">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <label class="fleft margin-top-10 height-12">Conhecimentos</label>
                    </div>
                    <?php
                    $res_categorias = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFO_CATEGORIAS");
                    while ($row = mysqli_fetch_array($res_categorias)) {

                        ?>
                        <div class="col-sm-3 col-md-3 col-xs-12 text-right">
                            <label><?php echo $row["nome"] ?> </label>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-12 areas-sub-div">
                            <?php
                            $res_informaticas = mysqli_query($con, "SELECT id, nome FROM TB_VV_INFORMATICA WHERE id_categoria = $row[id]");

                            while ($row2 = mysqli_fetch_array($res_informaticas)) {

                                ?>
                                <div class="form-check fleft op width-32-p">
                                    <input class="form-check-input" type="checkbox" name="informaticas[]" id="info-<?php echo $row2['id'] ?>" value="<?php echo $row2['id'] ?>">
                                    <label class="form-check-label"><?php echo $row2["nome"] ?> </label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 hidden" id="avisochecks3">
                        <strong>Selecione ao menos um</strong>
                    </div>
                    <input type="hidden" id="valconhecimentos" name="valconhecimentos"/>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 area-radio-form">
                        <label class="fleft width-400 margin-top-10 height-12">Será desejável conhecimento em língua estrangeira?</label>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" data-area="#area-idiomas" class="habilita form-control-input" id="idiomanao" name="idiomas" value="Não" checked="checked"/>
                            <label class="custom-control-label" for="idiomanao">Não</label>
                        </div>
                        <div class="form-radio form-control-inline op2 fleft">
                            <input type="radio" data-area="#area-idiomas" class="habilita form-control-input" id="idiomasim" name="idiomas" value="Sim"/>
                            <label class="custom-control-label" for="idiomasim">Sim</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="hidden" id="area-idiomas" >
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                            <strong class="margin-top-10 height-12">Idiomas</strong>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-10">
                            <div class="form-group input-group">
                                <select class="form-control" name="idiomavaga" id="idiomavaga">
                                    <option value="">Idioma</option>
                                    <?php
                                    $res_idiomas = mysqli_query($con, "SELECT descricao FROM TB_VV_IDIOMAS ORDER BY descricao ASC");
                                    while ($idioma = mysqli_fetch_array($res_idiomas)) {
                                        echo "<option>$idioma[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-flag" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-10">
                            <div class="form-group input-group">
                                <select class="form-control" name="nivelidiomavaga" id="nivelidiomavaga">
                                    <option value="">Nível</option>
                                    <?php
                                    $res_niveis = mysqli_query($con, "SELECT descricao FROM TB_VV_NIVEIS_IDIOMA ORDER BY descricao ASC");
                                    while ($nivel = mysqli_fetch_array($res_niveis)) {
                                        echo "<option>$nivel[descricao]</option>";
                                    }

                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-flag" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 margin-top-10">
                            <a href="#" class="btn btn-success btn-sm" id="addidiomavaga">
                                <i class="fa fa-plus" aria-hidden="true"></i> Incluir
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" id="idiomasgrid">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <strong class="margin-top-10 height-12">Idiomas</strong>
                            </div>
                        </div>
                        <input type="hidden" id="validiomas" name="validiomas"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="fleft width-400 margin-top-10 height-12">Visibilidade restrita para os dados da empresa?</label>
                            <div class="form-radio form-control-inline op2 fleft">
                                <input type="radio" id="visibnao" name="visibilidade" value="0" checked>
                                <label class="custom-control-label">Não</label>
                            </div>
                            <div class="form-radio form-control-inline op2 fleft">
                                <input type="radio" id="visibsim" name="visibilidade" value="1">
                                <label class="custom-control-label">Sim</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="fleft width-100 margin-top-10 height-12">Ativo?</label>
                            <div class="form-radio form-control-inline op2 fleft">
                                <input type="radio" id="visibnao" name="status" value="2" checked>
                                <label class="custom-control-label">Não</label>
                            </div>
                            <div class="form-radio form-control-inline op2 fleft">
                                <input type="radio" id="visibsim" name="status" value="1">
                                <label class="custom-control-label">Sim</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            caminho = '<?php echo DIRETORIO; ?>';
        });
    </script>
    <script src="<?php echo PATH_ASSETS . '/js/geral/cadastrovagas.js' ?>"></script>
    <?php
    include "includes/footer.php";
}