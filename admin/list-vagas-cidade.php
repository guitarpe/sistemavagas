<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {
    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    $estado = filter_input(INPUT_POST, 'estados');
    $cidade = filter_input(INPUT_POST, 'cidades');

    if (isset($estado) && isset($cidade)) {
        $sql_vagas = "SELECT
                            vg.id, emp.nome_empresa, vg.cargo, vg.data_anuncio, vg.estado, vg.status
                      FROM TB_VV_VAGAS vg
                            LEFT OUTER JOIN TB_VV_EMPRESAS emp ON emp.id=vg.id_empresa
                      WHERE vg.estado = '$estado' AND vg.cidade LIKE '%" . $cidade . "%' ORDER BY vg.id DESC";

        $res = mysqli_query($con, $sql_vagas);
    }

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="."><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Buscar Vaga por Cidade</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Vaga por Cidade</h2>
            <form action="list-vagas-cidade.php" method="post">
                <div class="container jumbotron">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Estado</label>
                                <select name="estados" id="estados" class="form-control busca_cidades">
                                    <option value="">Selecione</option>
                                    <?php
                                    $res_estados = mysqli_query($con, "select * FROM TB_VV_ESTADOS order by nome ASC");

                                    while ($row = mysqli_fetch_array($res_estados)) {
                                        $slct = $row['uf'] == $estado ? 'selected' : '';

                                        echo "<option value='" . $row['uf'] . "' " . $slct . ">" . $row['nome'] . "</option>";
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cidade</label>
                                <select name="cidades" id="cidades" class="cidades form-control">
                                    <option value="">Selecione</option>
                                    <?php
                                    if (isset($estado)) {
                                        $sql_cidades = "SELECT cid.nome
                                                            FROM TB_VV_CIDADES cid
                                                            INNER JOIN TB_VV_ESTADOS est ON est.id = cid.id_estado
                                                            WHERE est.uf = '" . $estado . "' ORDER BY cid.nome ASC";

                                        $res_cidades = mysqli_query($con, $sql_cidades) or die(mysqli_error($con));

                                        while ($row = mysqli_fetch_array($res_cidades)) {
                                            $slct = $row['nome'] == $cidade ? 'selected' : '';

                                            echo "<option " . $slct . ">" . $row['nome'] . "</option>";
                                        }
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Buscar
                        </button>
                    </div>
                </div>
            </form>
            <br>
            <?php
            if (isset($estado) && isset($cidade)) {

                ?>
                <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                    <table id="tb-vagas-estado" class="table table-bordered table-striped table-pages">
                        <thead class="thead-dark">
                            <tr class="center">
                                <th scope="col">#</th>
                                <th scope="col">Empresa</th>
                                <th scope="col">Descrição</th>
                                <th scope="col">Data</th>
                                <th scope="col">Status</th>
                                <th scope="col" width="100px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            while ($value = mysqli_fetch_array($res)) {

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $i ?></th>
                                    <td><?php echo $value['nome_empresa'] ?></td>
                                    <td><?php echo $value['cargo'] ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($value['data_anuncio'])); ?></td>
                                    <td><?php echo $value['status'] == 1 ? 'Ativo' : 'Inativo' ?></td>
                                    <td>
                                        <a class="btn btn-xs btn-warning" href="'edit-vaga.php?id_vaga=<?php echo $value['id'] ?>'">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" href="'actions/delete-vaga.php?id=<?php echo $value['id'] ?>'">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        <?php if ($value['status'] == 1) { ?>
                                            <a class="btn btn-xs btn-primary" href="'actions/desativar-vaga.php?id_vaga=<?php echo $value['id'] ?>'">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn btn-xs btn-success" href="'actions/ativar-vaga.php?id_vaga=<?php echo $value['id'] ?>'">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }

            ?>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}