<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $id = $_GET['id'];
    $sql = mysqli_query($con, "select * FROM TB_VV_BLOGS where id_blog = '$id'");
    $vetor = mysqli_fetch_array($sql);

    $sql_categoria = mysqli_query($con, "select * from TB_VV_CATEGORIAS_BLOG order by nome ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Add Blg</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Atualizando Blog</h2>
            <form action="actions/recebe_alteracadastrablog.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
                <div class="container">
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control" value="<?php echo $vetor['titulo']; ?>" id="titulo" name="titulo" placeholder="Insira o título" required>
                    </div>
                    <div class="form-group">
                        <label for="descricao">Categoria</label>
                        <select name="categoria" id="exampleSelect" class="form-control" required>
                            <option value="">Selecione a Categoria</option>
                            <?php while ($vetor_cat = mysqli_fetch_array($sql_categoria)) { ?>
                                <option label="<?php echo $vetor_cat['nome']; ?>" value="<?php echo $vetor_cat['id_cat']; ?>" <?php if (strcasecmp($vetor['categoria'], $vetor_cat['id_cat']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_cat['nome']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="images">Texto Capa</label>
                        <input type="text" class="form-control" id="titulo" value="<?php echo $vetor['descricao']; ?>" name="descricao" placeholder="Insira o Texto" required>
                    </div>
                    <div class="form-group">
                        <label for="images">Texto</label>
                        <textarea name="texto" class="ckeditor" id="editor1" required><?php echo $vetor['texto']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagem</label>
                        </br> Alterar Imagem? <img src="<?php echo PATH_IMAGENS . '/' . $vetor['imagem']; ?>" width="80px">
                        <input type="file" class="form-control" id="images" name="images" <?php echo empty($vetor['imagem']) ? 'required' : '' ?>/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}