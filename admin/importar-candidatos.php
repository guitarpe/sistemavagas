<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $sql = "SELECT
                (SELECT COUNT(*) FROM oemprego.oemp_pf WHERE exportada=0) AS para_importar,
                (SELECT COUNT(*) FROM TB_VV_CANDIDATOS WHERE importado=1) as importados
            FROM DUAL";
    $res = mysqli_query($con, $sql)or die(mysqli_error($con));
    $vagas = mysqli_fetch_array($res);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Importar Candidatos - (OEMPREGO)</h1>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6 text-center">
                        <h3><strong>Para Importar</strong></h3>
                        </br>
                        <div><h1><strong><?php echo $vagas['para_importar'] ?></strong></h1></div>
                        </br>
                        <form action="<?php echo 'actions/importar-candidatos-oemprego.php' ?>" method="post">
                            <div class="col-sm-6">
                                <input type="text" class="form-control numbers" name="qtde" placeholder="Quantidade para Importar"/>
                            </div>
                            <div class="col-sm-6 text-left">
                                <button type="submit" class="btn btn-success">Importar Candidatos</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6 text-center">
                        <h3><strong>Candidatos Importados</strong></h3>
                        </br>
                        <div><h1><strong><?php echo $vagas['importados'] ?></strong></h1></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}