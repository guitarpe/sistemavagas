<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_CANDIDATOS ORDER BY id DESC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Cadastro de empresa</h1>
            <form action="actions/recebe_alteracadastraempresa.php" method="post" enctype="multipart/form-data" id="dados">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div style="margin-top: 65px">
                            <label class="form-label">Logotipo: </label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                </span>
                                <input type="file" name="imagem" class="file form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input class="form-control" type="text" placeholder="Nome da empresa" name="nome" id="enome" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input class="cnpj form-control verifica_cnpj_cadastrado" name="cnpj" type="text" placeholder="CNPJ" id="ecnpj" required>
                            <span id="msg-cpf" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                            <input class="form-control" type="text" name="ramoatividade" placeholder="Ramo de atividade" id="eramo">
                            <span id="msg-data" style="display:none"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <textarea placeholder="Descrição da empresa" name="descricao" id="descricao" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input class="form-control numbers" name="nfuncionarios" type="text" placeholder="Número de funcionários" id="nfuncionarios" required>
                            <span id="msg-cpf" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input class="form-control" type="text" name="site" placeholder="Site" id="site">
                            <span id="msg-data" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input class="form-control validemail" type="text" placeholder="E-mail de relacionamento da empresa" name="emailrelacionamento" id="eemailr" required>
                            <span id="msg-email" style="display:none"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-7">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input class="form-control" type="text" placeholder="Nome completo do usuário" name="nomeusuario" id="unome" required>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input class="form-control validemail verifica_email_cadastrado" type="text" placeholder="E-mail do Usuário" name="email" id="eemail" required>
                            <span id="msg-email" style="display:none"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="password" placeholder="Senha" name="senha" class="form-control txtSenha" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="password" placeholder="Confirmar senha" name="confirmar_senha" class="form-control confisenha nopaste" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-earphone"></i>
                            </span>
                            <input class="form-control fones" type="text" placeholder="Telefone" name="fone" id="telefone" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-earphone"></i>
                            </span>
                            <input class="form-control fones" type="text" placeholder="Celular (Opcional)" name="celular" id="telefone2">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input class="cep form-control ecep" name="cep" type="text" placeholder="CEP">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input class="form-control elogradouro" type="text" placeholder="Logradouro" id="logradouro" name="logradouro">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input class="form-control numbers" type="text" name="numero" placeholder="Número" id="enumero">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input class="form-control" type="text" name="complemento" placeholder="Complemento" id="ecomplemento">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input class="form-control ebairro" type="text" name="bairro" placeholder="Bairro" id="ebairro">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <select name="estado" id="estado" class="form-control eestado busca_cidades" required>
                                <option disabled selected value="">Estado</option>
                                <?php
                                $result = mysqli_query($con, "SELECT uf, nome FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                while ($val = mysqli_fetch_array($result)) {
                                    echo "<option value='$val[uf]'>$val[nome]</option>";
                                }

                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <select name="cidade" id="cidade" class="form-control ecidade cidades" required>
                                <option value="">Cidade</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-success btn-icon" type="submit">
                            Finalizar Cadastro <span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            caminho = '<?php echo DIRETORIO; ?>';
        });
    </script>
    <script src="<?php echo PATH_ASSETS . '/js/geral/cadastroempresa.js' ?>"></script>
    <?php
    include "includes/footer.php";
}