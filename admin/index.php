<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
}

if ($_SESSION['tipo'] != 'admin') {
    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT COUNT(status) FROM TB_VV_USUARIOS WHERE tipo = 'empresa' AND status <> '1'");
    $nsolicitacao = mysqli_fetch_array($res, MYSQLI_NUM);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <h1>Seja Bem vindo, Administrador</h1>
            <ol class="breadcrumb">
                <li><a href="."><i class="fa fa-dashboard"></i> Início</a></li>
            </ol>
        </section>
        <br/>

        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <?php if ($nsolicitacao[0] > 0) { ?>
                        <span class="label label-danger">
                            <?php echo $nsolicitacao[0]; ?>
                        </span>
                    <?php } ?>
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>Empresa</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-building"></i>
                        </div>
                        <a href="<?php echo PATH_ADMIN; ?>/notificacoes-empresas.php" class="small-box-footer">
                            Notificações <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}