<?php
session_start();

include "../includes/conexao.php";
$func = new Funcoes();

$id = filter_input(INPUT_GET, "id");

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    $res = mysqli_query($con, "SELECT * FROM TB_VV_EMPRESAS WHERE id = $id");
    $dados = mysqli_fetch_array($res);

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Editar de empresa</h1>
            <form action="actions/recebe_alteracadastraempresa.php?id=<?php echo $id ?>" method="post" enctype="multipart/form-data" id="dados">
                <div class="form-group row">
                    <div class="col-sm-2">
                        <div class="input-group">
                            <img src="<?php echo PATH_EMP_IMAGENS . '/' . $dados['imagem'] ?>" class="img-lg img-responsive"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div style="margin-top: 65px">
                            <label class="form-label">Logotipo: </label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                </span>
                                <input type="file" name="imagem" class="file form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-building" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Nome da empresa" id="nomeempresa" name="nomeempresa" value="<?php echo $dados['nome_empresa'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-id-card" aria-hidden="true"></i>
                            </span>
                            <input class="form-control cnpj" type="text" placeholder="CNPJ" id="cnpj" name="cnpj" value="<?php echo $dados['cnpj'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gear" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Ramo de atividade" id="ramoatividade" name="ramoatividade" value="<?php echo $dados['ramo_atividade'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <textarea class="form-control" placeholder="Descrição da empresa" id="descricaoempresa" name="descricaoempresa"><?php echo $dados['descricao'] ?></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-group" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Número de funcionários" id="nfuncionarios" name="nfuncionarios" value="<?php echo $dados['qtd_funcionarios'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-laptop" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Site" id="siteempresa" name="siteempresa" value="<?php echo $dados['site'] ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control validemail" placeholder="E-mail de relacionamento da empresa" id="emailempresa" name="emailempresa" value="<?php echo $dados['email_empresa'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-7">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Nome completo do usuário" id="nomeusuario" name="nomeusuario" value="<?php echo $dados['nome_usuario'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control validemail" placeholder="E-mail/login" id="email" name="email" value="<?php echo $dados['email'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true" title="ver" id="ver"></i>
                            </span>
                            <input class="form-control txtSenha" type="password" placeholder="Nova Senha" id="senha" name="senha" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true" title="ver" id="ver"></i>
                            </span>
                            <input type="password" placeholder="Confirmar senha" name="confirmar_senha" class="form-control confisenha nopaste">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input class="form-control fones" type="text" placeholder="Telefone" id="foneempresa" name="foneempresa" value="<?php echo $dados['fone'] ?>" required>
                            <span class="input-group-addon">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                            <input class="form-control fones" type="text" placeholder="Celular (Opcional)" id="celular" name="celular" value="<?php echo $dados['fax'] ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </span>
                            <input class="form-control cep" type="text" placeholder="CEP" id="cepempresa" name="cepempresa" value="<?php echo $dados['cep'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-3 text-right">
                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCep.cfm" class="btn btn-warning btn-icon" type="button" target="_blank">
                            Não sabe seu CEP?
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-road" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Logradouro" id="logradouroempresa" name="logradouroempresa" value="<?php echo $dados['logradouro'] ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-home" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Número" id="numero" name="numero" value="<?php echo $dados['numero'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Complemento" id="complemento" name="complemento" value="<?php echo $dados['complemento'] ?>">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-asterisk" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="Bairro" id="bairro" name="bairro" value="<?php echo $dados['bairro'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </span>
                            <select name="estado" id="estado" class="form-control busca_cidades">
                                <option disabled value="">Estado</option>
                                <?php
                                $res_estados = mysqli_query($con, "SELECT nome, uf FROM TB_VV_ESTADOS ORDER BY nome ASC");
                                while ($val = mysqli_fetch_array($res_estados)) {
                                    if ($dados['estado'] == $val['uf']) {
                                        echo "<option selected value='$val[uf]'>$val[nome]</option>";
                                    } else {
                                        echo "<option value='$val[uf]'>$val[nome]</option>";
                                    }
                                }

                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </span>
                            <select name="cidade" class="form-control cidades" id="cidade" required>
                                <option disabled value="">Cidade</option>
                                <option><?php echo $dados['cidade'] ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-success btn-icon">
                            Alterar cadastro <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            caminho = '<?php echo DIRETORIO; ?>';
        });
    </script>
    <script src="<?php echo PATH_ASSETS . '/js/geral/cadastroempresa.js' ?>"></script>
    <?php
    include "includes/footer.php";
}