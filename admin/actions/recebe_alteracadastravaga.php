<?php
include "../../includes/conexao.php";

$func = new Funcoes();

$id = filter_input(INPUT_GET, 'id_vaga');

$id_empresa = filter_input(INPUT_POST, 'id_empresa');
$formacontratacao = filter_input(INPUT_POST, 'formacontratacao');
$nivelhierarquico = filter_input(INPUT_POST, 'nivelhierarquico');

$cargo = filter_input(INPUT_POST, 'cargo');
$nvagas = filter_input(INPUT_POST, 'nvagas');
$turno = filter_input(INPUT_POST, 'turno');
$atividades = filter_input(INPUT_POST, 'atividades');

$faixasalarial = filter_input(INPUT_POST, 'faixasalarial');
$vsalario = empty(filter_input(INPUT_POST, "vsalario")) ? 0 : filter_input(INPUT_POST, "vsalario");

$sexo = filter_input(INPUT_POST, 'sexo');
$experienciamin = filter_input(INPUT_POST, 'experienciamin');
$ensinomin = filter_input(INPUT_POST, 'ensinomin');

$idiomas = filter_input(INPUT_POST, "idiomas");
$validiomas = filter_input(INPUT_POST, "validiomas");

$outroestadocidade = filter_input(INPUT_POST, 'outroestadocidade');
$cidadevaga = filter_input(INPUT_POST, 'cidadevaga');
$estadovaga = filter_input(INPUT_POST, 'estadovaga');
$viajar = filter_input(INPUT_POST, 'viajar');
$veiculo = filter_input(INPUT_POST, 'veiculo');

$cnh = filter_input(INPUT_POST, "cnh");
$tipocnh = filter_input(INPUT_POST, 'tipocnh');

$status = filter_input(INPUT_POST, 'status');
$deficiencia = filter_input(INPUT_POST, "deficiencia");
$informatica = filter_input(INPUT_POST, "informatica");
$visibilidade = filter_input(INPUT_POST, "visibilidade");

$beneficios = filter_input(INPUT_POST, 'beneficios', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$deficiencias = filter_input(INPUT_POST, 'deficiencias', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$informaticas = filter_input(INPUT_POST, 'informaticas', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

//vaga para outro estado
if (intval($outroestadocidade) === 1) {
    $cidade = $cidadevaga;
    $estado = $estadovaga;
} else if (intval($outroestadocidade) === 0) {
    $res_local = mysqli_query($con, "SELECT cidade, estado  FROM TB_VV_EMPRESAS WHERE id=" . $id_empresa);
    $local = mysqli_fetch_array($res_local);
    $cidade = $local["cidade"];
    $estado = $local["estado"];
}

if ($faixasalarial == "Salário Fixo") {
    $salario = $func->returnValor($vsalario);
} else {
    $salario = null;
}

if (!isset($id)) {

    //$status = $_SESSION['visidados'];
    $data = date('Y-m-d');
    $expira = date('Y-m-d', strtotime($data . ' + 90 days'));

    $sql = "INSERT INTO TB_VV_VAGAS (
                                id_empresa,
                                forma_contratacao,
                                cargo,
                                numero_vagas,
                                nivel_hierarquico,
                                turno,
                                atividades,
                                faixa_salarial,
                                salario,
                                sexo,
                                experiencia_minima,
                                ensino_minimo,
                                vaga_outrolocal,
                                cidade,
                                estado,
                                viajar,
                                veiculo_proprio,
                                visibilidade_dados,
                                data_anuncio,
                                data_expira,
                                status,
                                numero_candidatos
                            ) VALUES (
                                $id_empresa,
                                '$formacontratacao',
                                '$cargo',
                                '$nvagas',
                                '$nivelhierarquico',
                                '$turno',
                                '$atividades',
                                '$faixasalarial',
                                $vsalario,
                                '$sexo',
                                '$experienciamin',
                                '$ensinomin',
                                '$outroestadocidade',
                                '$cidade',
                                '$estado',
                                $viajar,
                                $veiculo,
                                $visibilidade,
                                '$data',
                                '$expira',
                                $status,
                                '$nvagas')";

    $res = mysqli_query($con, $sql) or die(mysqli_error($con));

    $id_vaga = $con->insert_id;

    //BENEFÍCIOS
    if (!empty($beneficios[0])) {
        foreach ($beneficios as $value) {
            $sql_beneficio = mysqli_query($con, "INSERT into TB_VV_BENEFICIOS_VAGA (id_vaga, descricao) VALUES ('$id_vaga', '$value')");
        }
    }

    //CNH
    if ($cnh == "Sim") {
        $res_delete = mysqli_query($con, "DELETE FROM TB_VV_CNH WHERE id_vaga=$id_vaga");

        mysqli_query($con, "INSERT INTO TB_VV_CNH (id_vaga, categoria) VALUES ($id_vaga, '$tipocnh') ON DUPLICATE KEY UPDATE id_vaga=$id_vaga, categoria='$tipocnh'");
    }

    //DEFICIÊNCIAS
    if ($deficiencia == "Sim") {
        if (!empty($deficiencias[0])) {
            foreach ($deficiencias as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_DEFICIENCIAS_VAGA (id_vaga, deficiencia) VALUES ($id_vaga, '$value') ON DUPLICATE KEY UPDATE id_vaga=$id_vaga, deficiencia='$value'");
            }
        }
    }

    //IDIOMAS
    if ($idiomas == "Sim") {

        $res_delete = mysqli_query($con, "DELETE FROM TB_VV_IDIOMAS_VAGA WHERE id_vaga=$id_vaga");

        $idiomasops = explode(",", $validiomas);

        if (count($idiomasops) > 0) {
            foreach ($idiomasops as $value) {

                if (!empty($value)) {
                    $idi = explode(":", $value);

                    mysqli_query($con, "INSERT INTO TB_VV_IDIOMAS_VAGA (id_vaga, idioma, nivel) VALUES ($id_vaga, '$idi[0]', '$idi[1]')");
                }
            }
        }
    }

    //INFORMÁTICA
    if ($informatica == "Sim") {
        if (!empty($informaticas[0])) {
            foreach ($informaticas as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_INFORMATICA_VAGA (id_vaga, nome) VALUES ($id_vaga, '$value') ON DUPLICATE KEY UPDATE id_vaga=$id_vaga, nome='$value'");
            }
        }
    }

    header("Location:../list-vagas.php");
} else {

    $sql_up = "UPDATE TB_VV_VAGAS SET id_empresa='$id_empresa',
                                    forma_contratacao='$formacontratacao',
                                    cargo='$cargo',
                                    numero_vagas='$nvagas',
                                    nivel_hierarquico='$nivelhierarquico',
                                    turno='$turno',
                                    atividades='$atividades',
                                    faixa_salarial='$faixasalarial',
                                    salario='$salario',
                                    sexo='$sexo',
                                    experiencia_minima='$experienciamin',
                                    ensino_minimo='$ensinomin',
                                    vaga_outrolocal=$outroestadocidade,
                                    cidade='$cidade',
                                    estado='$estado',
                                    viajar=$viajar,
                                    veiculo_proprio=$veiculo,
                                    visibilidade_dados=$visibilidade,
                                    status=$status,
                                    numero_candidatos='$nvagas'
                                where id = '$id'";

    $sql = mysqli_query($con, $sql_up);

    //EXCLUI OS DADOS
    $sql_beneficio_exclui = mysqli_query($con, "DELETE FROM TB_VV_BENEFICIOS_VAGA WHERE id_vaga = '$id'");
    $sql_deficiente_exclui = mysqli_query($con, "DELETE FROM TB_VV_DEFICIENCIAS_VAGA WHERE id_vaga = '$id'");
    $sql_idioma_exclui = mysqli_query($con, "DELETE FROM TB_VV_IDIOMAS_VAGA WHERE id_vaga = '$id'");
    $sql_info_exclui = mysqli_query($con, "DELETE FROM TB_VV_INFORMATICA_VAGA WHERE id_vaga = '$id'");
    $sql_cnh_exclui = mysqli_query($con, "DELETE FROM TB_VV_CNH WHERE id_vaga = '$id'");

    //BENEFÍCIOS
    if (!empty($beneficios[0])) {
        foreach ($beneficios as $value) {
            $sql_beneficio = mysqli_query($con, "INSERT into TB_VV_BENEFICIOS_VAGA (id_vaga, descricao) VALUES ('$id', '$value')");
        }
    }

    //CNH
    if ($cnh == "Sim") {
        $res_delete = mysqli_query($con, "DELETE FROM TB_VV_CNH WHERE id_vaga=$id");

        mysqli_query($con, "INSERT INTO TB_VV_CNH (id_vaga, categoria) VALUES ($id, '$tipocnh') ON DUPLICATE KEY UPDATE id_vaga=$id, categoria='$tipocnh'");
    }

    //DEFICIÊNCIAS
    if ($deficiencia == "Sim") {
        if (!empty($deficiencias[0])) {
            foreach ($deficiencias as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_DEFICIENCIAS_VAGA (id_vaga, deficiencia) VALUES ($id, '$value') ON DUPLICATE KEY UPDATE id_vaga=$id, deficiencia='$value'");
            }
        }
    }

    //IDIOMAS
    if ($idiomas == "Sim") {

        $res_delete = mysqli_query($con, "DELETE FROM TB_VV_IDIOMAS_VAGA WHERE id_vaga=$id");

        $idiomasops = explode(",", $validiomas);

        if (count($idiomasops) > 0) {
            foreach ($idiomasops as $value) {

                if (!empty($value)) {
                    $idi = explode(":", $value);

                    mysqli_query($con, "INSERT INTO TB_VV_IDIOMAS_VAGA (id_vaga, idioma, nivel) VALUES ($id, '$idi[0]', '$idi[1]')");
                }
            }
        }
    }

    //INFORMÁTICA
    if ($informatica == "Sim") {
        if (!empty($informaticas[0])) {
            foreach ($informaticas as $value) {
                mysqli_query($con, "INSERT INTO TB_VV_INFORMATICA_VAGA (id_vaga, nome) VALUES ($id, '$value') ON DUPLICATE KEY UPDATE id_vaga=$id, nome='$value'");
            }
        }
    }

    header("Location:../list-vagas.php");
}