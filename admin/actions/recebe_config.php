<?php
include "../../includes/conexao.php";

$func = new Funcoes();

$id = filter_input(INPUT_POST, "id");
$host = filter_input(INPUT_POST, "smtp_host");
$user = filter_input(INPUT_POST, "smtp_user");
$pass = filter_input(INPUT_POST, "smtp_pass");
$porta = filter_input(INPUT_POST, "smtp_port");
$email_sistema = filter_input(INPUT_POST, "email_sistema");
$email_copia = filter_input(INPUT_POST, "emails_copia");
$remetente_copia = filter_input(INPUT_POST, "emails_copia_remetentes");


$query = "UPDATE
               TB_VV_CONFIG
          SET
               SMTP_HOST='$host',
               SMTP_USER='$user',
               SMTP_PASS='$pass',
               SMTP_PORT='$porta',
               MAIL_FROM='$email_sistema',
               MAIL_COPY='$email_copia',
               COPY_TXT='$remetente_copia'
           WHERE id=$id";

$sql = mysqli_query($con, $query) or die(mysqli_error($con));

header("Location:../index.php");

