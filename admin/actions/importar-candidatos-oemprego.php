<?php
include "../../includes/conexao.php";

$qtde = intval(filter_input(INPUT_POST, 'qtde'));

$parametros = [
    'I_QTDE' => $qtde
];
$procedure = "CALL IMPORTACAO_O_EMPREGO_CANDIDATOS(:I_QTDE)";
$return = $conn->executeProcedure($procedure, $parametros);

if (intval($return[0]['O_COD_RETORNO']) == 0) {
    header("Location:../importar-candidatos.php");
}

