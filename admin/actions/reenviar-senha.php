<?php
include "../../includes/conexao.php";

$tipo = filter_input(INPUT_GET, "tipo");
$id = filter_input(INPUT_GET, "id");

$query = "UPDATE TB_VV_USUARIOS SET status='0', senha='' WHERE tipo='$tipo' AND id=$id";
$res = mysqli_query($con, $query);

$func = new Funcoes();

if ($res) {
    $query_us = "SELECT * FROM TB_VV_USUARIOS WHERE tipo='$tipo' AND id=$id";
    $res_us = mysqli_query($con, $query_us) or die(mysqli_error($con));
    $usuario = mysqli_fetch_array($res_us);

    $email = $usuario['email'];
    $nome = $usuario['nome'];

    $to = $email;
    $to_txt = $nome;

    $subject = 'Reset de senha do usuário ' . $nome . ' no portal Vagas e Vagas';

    $message = '
            <html>
            <head>
            </head>
            <body>
                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                    <img src="' . HOME . '/images/top.png">
                    <div class="container" style="padding:20px">
                        <table>
                            <tr>
                                <td>
                                    <h2><b></b></h2>
                                    <br/>Olá ' . $nome . ', sua senha foi resetada!
                                    <br/>
                                    <br/>
                                    Bem, para que você possa entrar no site novamente, basta clicar no link abaixo e registrar uma nova senha
                                    <br/>
                                    <br/>
                                    <a href="' . HOME . '/registrar-nova-senha.php?id=' . $id . '&tipo=' . $tipo . '">Registrar Nova Senha</a>
                                    <br/>
                                    <br/>
                                    Aguardamos seu novo registro e torcemos muito para que encontre a vaga que deseja!
                                    <br/>
                                    <br/>
                                </td>
                                <td>
                                    <img src="' . HOME . '/images/confirmado.png">
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                        PS. Ajude divulgar este trabalho especial para seus colegas! É muito importante para todos.
                        <br/>
                        <br/>
                        Contamos muito com você!
                        <br/>
                        <br/>
                        Equipe Vagas e Vagas.
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <div class="row" align="right">
                            <a href="' . FACEBOOK . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/facebook.png">
                            </a>
                            <a href="' . TWITTER . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/twitter.png">
                            </a>
                            <a href="' . PLUS . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/googlep.png">
                            </a>
                        </div>
                    </div>
                    <img src="' . HOME . '/images/top.png">
                <div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $usuario['id'] . '&tipo=candidato" target="_blank">Não quero receber mais mensagens</a></div>
                </div>
            </body>
            </html>';

    $dados = [
        'HOST' => $configuracoes_sis['SMTP_HOST'],
        'USER' => $configuracoes_sis['SMTP_USER'],
        'PASS' => $configuracoes_sis['SMTP_PASS'],
        'PORT' => $configuracoes_sis['SMTP_PORT'],
        'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
        'FROM_TXT' => 'Vagas e Vagas',
        'TO_MAIL' => $to,
        'TO_TXT' => $to_txt,
        'SUBJECT' => $subject,
        'MENSAGE' => $message
    ];

    include '../../includes/enviar-email.php';
}

