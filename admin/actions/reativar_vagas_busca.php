<?php
include "../../includes/conexao.php";

$cargo = $_POST['cargo'];
$empresa = $_POST['empresa'];
$estados = $_POST['estados'];
$dt_ini = $_POST['dt_ini'];
$dt_fim = $_POST['dt_fim'];

$sql = "SELECT
            vg.id, emp.nome_empresa, vg.cargo, vg.data_anuncio, vg.data_expira, vg.estado, vg.status
        FROM TB_VV_VAGAS vg
        LEFT OUTER JOIN TB_VV_EMPRESAS emp ON emp.id=vg.id_empresa WHERE date(data_expira) < date(now()) ";


if (!empty($cargo)) {
    $sql .= " AND vg.cargo LIKE '%" . $cargo . "%'";
}

if (!empty($empresa)) {
    $sql .= " AND emp.nome_empresa LIKE '%" . $empresa . "%'";
}

if (!empty($estados)) {
    $sql .= " AND vg.estado = '" . $estados . "'";
}

if (!empty($dt_ini) && !empty($dt_fim)) {
    $sql .= " AND DATE(vg.data_expira) BETWEEN DATE('" . $dt_ini . "') AND DATE('" . $dt_fim . "')";
}

$data = date('Y-m-d');
$expira = date('Y-m-d', strtotime($data . ' + 90 days'));

$res_vagas = mysqli_query($con, $sql);
while ($value = mysqli_fetch_array($res_vagas)) {
    $res = mysqli_query($con, "UPDATE TB_VV_VAGAS SET status = '1', data_anuncio='$data', data_expira='$expira' where id = " . $value['id']);
}
