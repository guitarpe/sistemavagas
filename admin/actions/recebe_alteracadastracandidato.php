<?php
include "../../includes/conexao.php";

$func = new Funcoes();

$dtnasc = filter_input(INPUT_POST, 'datanasc');

$dtn = DateTime::createFromFormat('d/m/Y', $dtnasc);

$email = filter_input(INPUT_POST, "repetir_email");
$senha = filter_input(INPUT_POST, "confirmar_senha");
$nome = filter_input(INPUT_POST, "nome");
$cpf = filter_input(INPUT_POST, "cpf");
$data = $dtn->format("Y-m-d");
$sexo = filter_input(INPUT_POST, "sexo");
$fone = filter_input(INPUT_POST, "fone");
$celular = filter_input(INPUT_POST, "celular");
$cep = filter_input(INPUT_POST, 'cep');
$logradouro = filter_input(INPUT_POST, 'logradouro');
$numero = filter_input(INPUT_POST, 'numero');
$filhos = filter_input(INPUT_POST, 'filhos');
$estadocivil = filter_input(INPUT_POST, "estadocivil");
$pretensao_salarial = filter_input(INPUT_POST, "pretensao_salarial");
$objetivo = filter_input(INPUT_POST, "objetivo");
$complemento = filter_input(INPUT_POST, 'complemento');
$bairro = filter_input(INPUT_POST, 'bairro');
$estado = filter_input(INPUT_POST, "estado");
$cidade = filter_input(INPUT_POST, "cidade");
$viajar = filter_input(INPUT_POST, "viajar");
$veiculo = filter_input(INPUT_POST, "veiculo");

$senhausuario = $func->dec_enc(1, 'candidato' . '|' . $senha . '|' . $email);

$id = filter_input(INPUT_GET, "id");

if (empty($id)) {
    $sql_candidato = mysqli_query($con, "SELECT COUNT(*) as total, id FROM TB_VV_CANDIDATOS WHERE email='$email' AND cpf='$cpf'") or die(mysqli_error($con));
    $res_cand = mysqli_fetch_array($sql_candidato);

    if (intval($res_cand['total']) == 0) {

        $query = "INSERT INTO TB_VV_CANDIDATOS "
            . "(email, senha, nome, cpf, nascimento, sexo, fone, celular, cep, logradouro, numero, bairro, estado, cidade, filhos, estado_civil, pretensao_salarial, viajar, veiculo, obj_profissional) "
            . "VALUES "
            . "('$email', '$senhausuario', '$nome', '$cpf', '$data', '$sexo', '$fone', '$celular', '$cep', '$logradouro', '$numero', '$bairro', '$estado', '$cidade', '$filhos', '$estadocivil', '$pretensao_salarial', '$viajar', '$veiculo', '$objetivo')";

        $sql = mysqli_query($con, $query) or die(mysqli_error($con));

        $id_candidato = mysqli_insert_id($con);

        $cargos = filter_input(INPUT_POST, 'cargoprofissional', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $nomepresa = filter_input(INPUT_POST, 'nomeempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        $nomeempresa = filter_input(INPUT_POST, 'nomeempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $salario = filter_input(INPUT_POST, 'salario', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $cargoempresa = filter_input(INPUT_POST, 'cargoempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $atividades = filter_input(INPUT_POST, 'atividades', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $datainicio = filter_input(INPUT_POST, 'datainicio', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $datafim = filter_input(INPUT_POST, 'datafim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $empregoatual = filter_input(INPUT_POST, 'empregoatual', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($cargos[0])) {
            foreach ($cargos as $key) {
                if (!empty($key)) {
                    $sql = "INSERT INTO TB_VV_CARGOS_CAND (id_candidato, cargo) VALUES ($id_candidato, '$key') "
                        . "ON DUPLICATE KEY UPDATE id_candidato='$id_candidato', cargo='$key'";
                    $res = mysqli_query($con, $sql) or die(mysqli_error($con));
                }
            }
        }

        if (!empty($nomepresa[0])) {

            foreach ($nomepresa as $key => $value) {

                $valsalario = $func->returnValor($salario[$key]);
                $empatual = isset($empregoatual[$key]) ? intval($empregoatual[$key]) : 0;

                $sql = "INSERT INTO TB_VV_EXP_PROFISSIONAIS "
                    . "(id_candidato, nome_empresa, cargo, atividades, inicio, fim, emprego_atual, salario) "
                    . "VALUES "
                    . "($id_candidato, '$nomeempresa[$key]', '$cargoempresa[$key]', '$atividades[$key]', '$datainicio[$key]', '$datafim[$key]', $empatual, '$valsalario') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', nome_empresa='$nomeempresa[$key]', cargo='$cargoempresa[$key]', atividades='$atividades[$key]', "
                    . "inicio='$datainicio[$key]', fim='$datafim[$key]', emprego_atual=$empatual, salario='$valsalario'";

                $res = mysqli_query($con, $sql) or die(mysqli_error($con));
            }
        }

        $formacao = filter_input(INPUT_POST, 'formacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($formacao[0])) {

            $formacao = filter_input(INPUT_POST, 'formacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $instituicao = filter_input(INPUT_POST, 'instituicao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $situacao = filter_input(INPUT_POST, 'situacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $nomeformacao = filter_input(INPUT_POST, 'nomeformacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $anofim = filter_input(INPUT_POST, 'anofim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $semestre = filter_input(INPUT_POST, 'semestre', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $turno = filter_input(INPUT_POST, 'turno', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

            foreach ($formacao as $key => $value) {

                $sql_formacao = "INSERT INTO TB_VV_FORMACOES_CAND "
                    . "(id_candidato, nivel, instituicao, curso, semestre, situacao, fim, turno) "
                    . "VALUES "
                    . "($id_candidato, '$formacao[$key]', '$instituicao[$key]', '$nomeformacao[$key]', '$semestre[$key]', '$situacao[$key]', '$anofim[$key]', '$turno[$key]')";

                $res_formacao = mysqli_query($con, $sql_formacao) or die(mysqli_error($con));
            }
        }

        $nomecurso = filter_input(INPUT_POST, 'nomecurso', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($nomecurso[0])) {

            $nomecurso = filter_input(INPUT_POST, 'nomecurso', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $instituicao2 = filter_input(INPUT_POST, 'instituicao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $situacao2 = filter_input(INPUT_POST, 'situacao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $turnoextra = filter_input(INPUT_POST, 'turnoextra', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $diassemana = filter_input(INPUT_POST, 'diassemana', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $cargahoraria = filter_input(INPUT_POST, 'cargahoraria', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $anofim = filter_input(INPUT_POST, 'anofim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

            foreach ($nomecurso as $key => $value) {
                $sql_curso = "INSERT INTO TB_VV_CURSOS_CAND "
                    . "(id_candidato, nome, instituicao, situacao, turnoextra, diassemana, cargahoraria, anofim ) "
                    . "VALUES "
                    . "($id_candidato, '$nomecurso[$key]', '$instituicao2[$key]', '$situacao2[$key]', '$turnoextra[$key]', '$diassemana[$key]', '$cargahoraria[$key]', '$anofim[$key]')";

                $res_curso = mysqli_query($con, $sql_curso) or die(mysqli_error($con));
            }
        }

        $idioma = filter_input(INPUT_POST, 'idioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($idioma[0])) {

            $idioma = filter_input(INPUT_POST, 'idioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $nivelidioma = filter_input(INPUT_POST, 'nivelidioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

            foreach ($idioma as $key => $value) {

                $sql_idioma = "INSERT INTO TB_VV_IDIOMAS_CAND "
                    . "(id_candidato, idioma, nivel) "
                    . "VALUES "
                    . "($id_candidato, '$idioma[$key]', '$nivelidioma[$key]')";

                $res_idioma = mysqli_query($con, $sql_idioma) or die(mysqli_error($con));
            }
        }

        $sql_candidato = mysqli_query($con, "select * FROM TB_VV_CANDIDATOS where id = '$id_candidato'") or die(mysqli_error($con));
        $res_cand = mysqli_fetch_array($sql_candidato);

        $cnome = $res_cand['nome'];
        $cemail = $res_cand['email'];
        $cesenha = $res_cand['senha'];

        $sql_usuario = "INSERT INTO TB_VV_USUARIOS "
            . "(id, nome, email, senha, tipo, status) "
            . "VALUES "
            . "('$id_candidato', '$cnome', '$cemail', '$cesenha', 'candidato', '0')'";

        $sql_usuario = mysqli_query($con, $sql_usuario) or die(mysqli_error($con));

        $sql_us = "SELECT * FROM TB_VV_USUARIOS WHERE nome='$nome' AND email='$email' AND tipo='candidato'";
        $res_us = mysqli_query($con, $sql_us) or die(mysqli_error($con));
        $usuario = mysqli_fetch_array($res_us);

        $to = $email;
        $to_txt = $nome;

        $subject = 'Olá ' . $nome . ', seja bem vindo ao Vagas e Vagas!';

        $message = '
		<html>
            <head>
            </head>
            <body>
                <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                    <img src="' . HOME . '/images/top.png">
                    <div class="container" style="padding:20px">
                        <table>
                            <tr>
                                <td>
                                    <h2><b></b></h2>
                                    <br/>Olá ' . $nome . ', agora você já faz quase parte da família Vagas e Vagas!
                                    <br/>
                                    Um portal totalmente livre para pesquisar e inserir vagas!
                                    <br/>
                                    <br/>
                                    Bem, para que possa entrar no site, basta confirmar que seu e-mail está correto e, para isso, use o link abaixo:
                                    <br/>
                                    <br/>
                                    <a href="' . HOME . '/candidatos/activate.php?token=' . $token . '">' . HOME . '/candidatos/activate.php?token=' . $token . '</a>
                                    <br/>
                                    <br/>
                                    Aguardamos sua confirmação e torcemos muito para que encontre a vaga que deseja!
                                    <br/>
                                    <br/>
                                </td>
                                <td>
                                    <img src="' . HOME . '/images/confirmado.png">
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                        PS. Ajude divulgar este trabalho especial para seus colegas! É muito importante para todos.
                        <br/>
                        <br/>
                        Contamos muito com você!
                        <br/>
                        <br/>
                        Equipe Vagas e Vagas.
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <div class="row" align="right">
                            <a href="' . FACEBOOK . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/facebook.png">
                            </a>
                            <a href="' . TWITTER . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/twitter.png">
                            </a>
                            <a href="' . PLUS . '" style="text-decoration:none">
                                <img src="' . HOME . '/images/googlep.png">
                            </a>
                        </div>
                    </div>
                    <img src="' . HOME . '/images/top.png">
                <div class="text-align:center; font-size:9px"><a href="' . URL . PATH_ALL . '/inativar-mensagens.php?id=' . $usuario['id'] . '&tipo=candidato" target="_blank">Não quero receber mais mensagens</a></div>
                </div>
            </body>
            </html>';

        $dados = [
            'HOST' => $configuracoes_sis['SMTP_HOST'],
            'USER' => $configuracoes_sis['SMTP_USER'],
            'PASS' => $configuracoes_sis['SMTP_PASS'],
            'PORT' => $configuracoes_sis['SMTP_PORT'],
            'FROM_MAIL' => $configuracoes_sis['MAIL_FROM'],
            'FROM_TXT' => 'Vagas e Vagas',
            'TO_MAIL' => $to,
            'TO_TXT' => $to_txt,
            'SUBJECT' => $subject,
            'MENSAGE' => $message
        ];

        include_once '../../includes/enviar-email.php';
    }
} else {
    if (!empty($senha)) {
        $query_can = "UPDATE TB_VV_CANDIDATOS SET "
            . "nome = '$nome', "
            . "senha = '$senhausuario', "
            . "nascimento = '$data', "
            . "sexo = '$sexo', "
            . "filhos = '$filhos', "
            . "estado_civil = '$estadocivil', "
            . "celular = '$celular', "
            . "fone = '$fone', "
            . "pretensao_salarial = '$pretensao_salarial', "
            . "cep = '$cep', "
            . "logradouro = '$logradouro', "
            . "numero = '$numero', "
            . "complemento = '$complemento', "
            . "bairro = '$bairro', "
            . "estado = '$estado', "
            . "cidade = '$cidade', "
            . "viajar = '$viajar', "
            . "veiculo = '$veiculo', "
            . "obj_profissional = '$objetivo' "
            . "WHERE id = $id";
    } else {
        $query_can = "UPDATE TB_VV_CANDIDATOS SET "
            . "nome = '$nome', "
            . "nascimento = '$data', "
            . "sexo = '$sexo', "
            . "filhos = '$filhos', "
            . "estado_civil = '$estadocivil', "
            . "celular = '$celular', "
            . "fone = '$fone', "
            . "pretensao_salarial = '$pretensao_salarial', "
            . "cep = '$cep', "
            . "logradouro = '$logradouro', "
            . "numero = '$numero', "
            . "complemento = '$complemento', "
            . "bairro = '$bairro', "
            . "estado = '$estado', "
            . "cidade = '$cidade', "
            . "viajar = '$viajar', "
            . "veiculo = '$veiculo', "
            . "obj_profissional = '$objetivo' "
            . "WHERE id = $id";
    }

    $res = mysqli_query($con, $query_can) or die(mysqli_error($con));

    if (!empty($senha)) {
        $sql_usuario = "UPDATE TB_VV_USUARIOS SET nome='$nome', senha='$senhausuario', email='$email' WHERE id=$id and tipo='candidato'";
    } else {
        $sql_usuario = "UPDATE TB_VV_USUARIOS SET nome='$nome', email='$email' WHERE id=$id and tipo='candidato'";
    }
    $res_usuario = mysqli_query($con, $sql_usuario) or die(mysqli_error($con));

    /* -------------- CNH ------------- */
    $cnh = filter_input(INPUT_POST, "cnh");
    if (intval($cnh) == 1) {

        $tipo = filter_input(INPUT_POST, "tipocnh");

        $res_cat_cnh = mysqli_query($con, "SELECT * FROM TB_VV_CNH_CAND WHERE id_candidato=" . $id) or die(mysqli_error($con));

        if (mysqli_num_rows($res_cat_cnh)) {
            $sql_cnh = "UPDATE TB_VV_CNH_CAND SET categoria='$tipo' WHERE id_candidato='$id'";
            $res_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));
        } else {
            $sql_cnh = "INSERT INTO TB_VV_CNH_CAND (id_candidato, categoria) VALUES ($id, '$tipo')";
            $res_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));
        }
    } else {
        $sql_cnh = "DELETE FROM TB_VV_CNH_CAND WHERE id_candidato = $id";
        $res_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));
    }

    /* -------------- DEFICIENCIAS ------------- */
    $deficiencia = filter_input(INPUT_POST, "deficiencia");
    if (intval($deficiencia) == 1) {

        $defs = filter_input(INPUT_POST, 'deficiencias', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($defs[0])) {
            foreach ($defs as $key) {
                $sql_defs = "INSERT INTO TB_VV_DEFICIENCIAS_CAND (id_candidato, id_deficiencia) VALUES ($id, '$key') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', id_deficiencia='$key'";
                $res_defs = mysqli_query($con, $sql_defs) or die(mysqli_error($con));
            }
        }
    }

    /* -------------- CONHECIMENTOS INFO ------------- */
    $conhecimentos = filter_input(INPUT_POST, "informatica");

    if (intval($conhecimentos) == 1) {
        $infos = filter_input(INPUT_POST, 'informaticas', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($infos[0])) {
            foreach ($infos as $key) {
                $sql_infos = "INSERT INTO TB_VV_INFORMATICA_CAND (id_candidato, id_informatica) VALUES ($id, '$key') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', id_informatica='$key'";
                $res_infos = mysqli_query($con, $sql_infos) or die(mysqli_error($con));
            }
        }
    }

    /* -------------- CARGOS ------------- */
    $cargos = filter_input(INPUT_POST, 'cargoprofissional', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($cargos[0])) {
        foreach ($cargos as $key) {
            if (!empty($key)) {
                $sql = "INSERT INTO TB_VV_CARGOS_CAND (id_candidato, cargo) VALUES ($id, '$key') "
                    . "ON DUPLICATE KEY UPDATE id_candidato='$id', cargo='$key'";
                $res = mysqli_query($con, $sql) or die(mysqli_error($con));
            }
        }
    }

    /* -------------- EXPERIÊNCIAS ------------- */
    $nomeempresa = filter_input(INPUT_POST, 'nomeempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $salario = filter_input(INPUT_POST, 'salario', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $cargoempresa = filter_input(INPUT_POST, 'cargoempresa', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $atividades = filter_input(INPUT_POST, 'atividades', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $datainicio = filter_input(INPUT_POST, 'datainicio', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $datafim = filter_input(INPUT_POST, 'datafim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $empregoatual = filter_input(INPUT_POST, 'empregoatual', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($nomeempresa[0])) {

        foreach ($nomeempresa as $key => $value) {

            $valsalario = $func->returnValor($salario[$key]);
            $empatual = isset($empregoatual[$key]) ? intval($empregoatual[$key]) : 0;

            $sql = "INSERT INTO TB_VV_EXP_PROFISSIONAIS "
                . "(id_candidato, nome_empresa, cargo, atividades, inicio, fim, emprego_atual, salario) "
                . "VALUES "
                . "($id, '$nomeempresa[$key]', '$cargoempresa[$key]', '$atividades[$key]', '$datainicio[$key]', '$datafim[$key]', $empatual, '$valsalario') "
                . "ON DUPLICATE KEY UPDATE id_candidato='$id', nome_empresa='$nomeempresa[$key]', cargo='$cargoempresa[$key]', atividades='$atividades[$key]', "
                . "inicio='$datainicio[$key]', fim='$datafim[$key]', emprego_atual=$empatual, salario='$valsalario'";

            $res = mysqli_query($con, $sql) or die(mysqli_error($con));
        }
    }

    /* ---------- FORMAÇÃO -------- */
    $formacao = filter_input(INPUT_POST, 'formacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $instituicao = filter_input(INPUT_POST, 'instituicao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $nomeformacao = filter_input(INPUT_POST, 'nomeformacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $situacao = filter_input(INPUT_POST, 'situacao', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $anofim = filter_input(INPUT_POST, 'anofim', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $semestre = filter_input(INPUT_POST, 'semestre', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $turno = filter_input(INPUT_POST, 'turno', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($formacao[0])) {

        foreach ($formacao as $key => $value) {

            $sql_formacao = "INSERT INTO TB_VV_FORMACOES_CAND "
                . "(id_candidato, nivel, instituicao, curso, semestre, situacao, fim, turno) "
                . "VALUES "
                . "($id, '$formacao[$key]', '$instituicao[$key]', '$nomeformacao[$key]', '$semestre[$key]', '$situacao[$key]', '$anofim[$key]', '$turno[$key]')";

            $res_formacao = mysqli_query($con, $sql_formacao) or die(mysqli_error($con));
        }
    }

    /* ---------- CURSOS -------- */
    $nomecurso = filter_input(INPUT_POST, 'nomecurso', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $instituicao2 = filter_input(INPUT_POST, 'instituicao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $anofim2 = filter_input(INPUT_POST, 'anofim2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $situacao2 = filter_input(INPUT_POST, 'situacao2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $turnoextra = filter_input(INPUT_POST, 'turnoextra', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $diassemana = filter_input(INPUT_POST, 'diassemana', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $cargahoraria = filter_input(INPUT_POST, 'cargahoraria', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($nomecurso[0])) {

        foreach ($nomecurso as $key => $value) {
            $sql_curso = "INSERT INTO TB_VV_CURSOS_CAND "
                . "(id_candidato, nome, instituicao, situacao, turnoextra, diassemana, cargahoraria, anofim ) "
                . "VALUES "
                . "($id, '$nomecurso[$key]', '$instituicao2[$key]', '$situacao2[$key]', '$turnoextra[$key]', '$diassemana[$key]', '$cargahoraria[$key]', '$anofim2[$key]')";

            $res_curso = mysqli_query($con, $sql_curso) or die(mysqli_error($con));
        }
    }

    /* ---------- IDIOMAS -------- */
    $idioma = filter_input(INPUT_POST, 'idioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $nivelidioma = filter_input(INPUT_POST, 'nivelidioma', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (!empty($idioma[0])) {

        foreach ($idioma as $key => $value) {

            $sql_idioma = "INSERT INTO TB_VV_IDIOMAS_CAND "
                . "(id_candidato, idioma, nivel) "
                . "VALUES "
                . "($id, '$idioma[$key]', '$nivelidioma[$key]')";

            $res_idioma = mysqli_query($con, $sql_idioma) or die(mysqli_error($con));
        }
    }
}

header("Location:../list-candidatos.php");

