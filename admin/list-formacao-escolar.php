<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_FORM_ESCOLAR ORDER BY nome ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Listar Formações Escolares</li>
            </ol>
        </section>


        <section class="content">
            <h2 align="center">Formações Escolares</h2>

            <table class="table">
                <thead class="thead-dark">
                    <tr class="center">
                        <th scope="col">#</th>
                        <th scope="col">Descrição</th>
                        <th scope="col" width="170"></th>
                    </tr>
                </thead>
                <?php
                $i = 1;
                while ($value = mysqli_fetch_array($res)) {

                    ?>
                    <tbody>
                        <tr>
                            <th scope="row"><?php echo $i ?></th>
                            <td><?php echo $value['nome'] ?></td>
                            <td>
                                <button class="btn btn-xs btn-default" onclick="location.href = 'edit-formacao-escolar.php?id=<?php echo $value['id'] ?>'">
                                    <i class="fa fa-pencil"></i> Editar
                                </button>
                                <button class="btn btn-xs btn-danger" onclick="location.href = 'actions/delete-formacao-escolar.php?id=<?php echo $value['id'] ?>'">
                                    <i class="fa fa-trash"></i> Excluir
                                </button>
                            </td>
                        </tr>
                    </tbody>
                    <?php
                    $i++;
                }

                ?>
            </table>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}