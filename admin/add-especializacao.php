<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT id, nome FROM TB_VV_AREAS_PROF ORDER BY nome ASC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="."><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Adicionar Especialização</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Adicionar Especialização</h2>
            <form action="actions/recebe_alteracadastraespecializacao.php" method="post">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="area">Área Profissional</label>
                        <select class="form-control" id="area" name="area">
                            <option disabled selected>Selecione a área profissional</option>
                            <?php
                            while ($area = mysqli_fetch_array($res)) {
                                echo "<option value='$area[id]'>$area[nome]</option>";
                            }

                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="especializacao">Especialização</label>
                        <input type="text" class="form-control" id="especializacao" name="especializacao" placeholder="Digite a especialização">
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}