<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT *  FROM TB_VV_NOTICIAS WHERE id = $_GET[id]");
    $value = mysqli_fetch_array($res);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Editar Notícia</li>
            </ol>
        </section>

        <section class="content">
            <h2 align="center">Editar Notícia</h2>
            <form action="actions/recebe_alteracadastranoticia.php?id=<?php echo $value['id'] ?>" method="post" enctype="multipart/form-data">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="images">Imagem</label>
                        <input type="file" class="form-control" id="images" name="images">
                    </div>
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Insira o título" value="<?php echo $value['titulo'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" id="descricao" name="descricao" placeholder="Descreva a notícia..." rows="10" required><?php echo $value['descricao'] ?></textarea>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </section>
    </div>
    <?php
    include "includes/footer.php";
}