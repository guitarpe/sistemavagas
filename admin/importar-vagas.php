<?php
/*
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $sql = "SELECT
                SUM(CASE WHEN DATE(oem.finish_date_vaga) >= CURDATE() THEN 1 ELSE 0 END) AS n_expiradas,
                SUM(CASE WHEN DATE(oem.finish_date_vaga) < CURDATE() THEN 1 ELSE 0 END) AS expiradas,
                (SELECT COUNT(*) as total FROM TB_VV_VAGAS vg WHERE vg.importada=1) as importadas
            FROM oemprego.oemp_vaga oem
            WHERE NOT EXISTS(
                SELECT 1 FROM TB_VV_VAGAS vg
                WHERE vg.id_antigo=oem.id_vaga AND vg.importada=1
            )
            AND oem.id_vaga > 0";
    $res = mysqli_query($con, $sql)or die(mysqli_error($con));
    $vagas = mysqli_fetch_array($res);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Importar Vagas - (OEMPREGO)</h1>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-4 text-center">
                        <h3><strong>Para Importar (Não Expiradas)</strong></h3>
                        </br>
                        <div><h1><strong><?php echo $vagas['n_expiradas'] ?></strong></h1></div>
                        </br>
                        <form action="<?php echo 'actions/importar-vagas-oemprego.php?tipo=1' ?>" method="post">
                            <div class="col-sm-6">
                                <input type="text" name="qtde" placeholder="Qtde para importar" class="form-control numbers"/>
                            </div>
                            <div class="col-sm-6 text-left">
                                <button type="submit" class="btn btn-success">Importar Vagas</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4 text-center">
                        <h3><strong>Para Importar (Expiradas)</strong></h3>
                        </br>
                        <div><h1><strong><?php echo $vagas['expiradas'] ?></strong></h1></div>
                        </br>
                        <form action="<?php echo 'actions/importar-vagas-oemprego.php?tipo=2' ?>" method="post">
                            <div class="col-sm-6">
                                <input type="text" name="qtde" placeholder="Qtde para importar" class="form-control numbers"/>
                            </div>
                            <div class="col-sm-6 text-left">
                                <button type="submit" class="btn btn-success">Importar Vagas</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4 text-center">
                        <h3><strong>Vagas Importadas</strong></h3>
                        </br>
                        <div><h1><strong><?php echo $vagas['importadas'] ?></strong></h1></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}