<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">
        <section class="content">
            <h1 class="text-center">Informações Gerais</h1>
            <form action="actions/recebe_config.php" method="post" role="form">
                <input type="hidden" name="id"  value="<?php echo $configuracoes_sis['ID'] ?>"/>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="control-label">SMTP Host:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-globe"></i>
                            </span>
                            <input type="text" placeholder="SMTP Host" name="smtp_host" class="form-control" value="<?php echo $configuracoes_sis['SMTP_HOST'] ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">SMTP Usuário:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input type="text" placeholder="SMTP User" name="smtp_user" class="form-control" value="<?php echo $configuracoes_sis['SMTP_USER'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="control-label">SMTP Senha:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </span>
                            <input type="text" placeholder="SMTP Pass" name="smtp_pass" class="form-control" value="<?php echo $configuracoes_sis['SMTP_PASS'] ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">SMTP Porta:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-open"></i>
                            </span>
                            <input type="text" placeholder="SMTP Port" name="smtp_port" class="form-control" value="<?php echo $configuracoes_sis['SMTP_PORT'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="control-label">E-mail de Envio do Sistema:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input type="text" placeholder="E-mail de Envio do Sistema" name="email_sistema" class="form-control" value="<?php echo $configuracoes_sis['MAIL_FROM'] ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">E-mails de Cópia:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </span>
                            <input type="text" placeholder="E-mails de Cópia" name="emails_copia" class="form-control" value="<?php echo $configuracoes_sis['MAIL_COPY'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="control-label">Remetente dos emails de cópia:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input type="text" placeholder="Remetente dos emails de cópia" name="emails_copia_remetentes" class="form-control" value="<?php echo $configuracoes_sis['COPY_TXT'] ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 text-left">
                        <button class="btn btn-success btn-icon" type="submit">
                            Salvar Informações<span class="glyphicon glyphicon-ok"></span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            caminho = '<?php echo DIRETORIO; ?>';
        });
    </script>
    <script src="<?php echo PATH_ASSETS . '/js/geral/cadastrocandidato.js' ?>"></script>
    <?php
    include "includes/footer.php";
}