<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {
    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    $empresa = filter_input(INPUT_POST, 'id_empresa');

    if (isset($empresa)) {
        $sql_vagas = "SELECT
                            vg.id, emp.nome_empresa, vg.cargo, vg.data_anuncio, vg.estado, vg.status
                      FROM TB_VV_VAGAS vg
                            LEFT OUTER JOIN TB_VV_EMPRESAS emp ON emp.id=vg.id_empresa
                      WHERE vg.id_empresa=$empresa ORDER BY vg.id DESC";

        $res = mysqli_query($con, $sql_vagas);
    }

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="."><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Buscar Vaga por Empresa</li>
            </ol>
        </section>


        <section class="content">
            <h2 align="center">Vaga por Empresa</h2>
            <form action="list-vagas-empresa.php" method="post">
                <div class="container jumbotron">
                    <div class="form-group">
                        <label for="areaprof">Empresa</label>
                        <select name="id_empresa" class="form-control select2" style="width: 100%;" required>
                            <option selected="selected">Selecione uma Empresa</option>
                            <?php
                            $sql_empresa = mysqli_query($con, "select *  FROM TB_VV_EMPRESAS order by nome_empresa ASC");

                            while ($vetor_empresa = mysqli_fetch_array($sql_empresa)) {

                                ?>
                                <option value="<?php echo $vetor_empresa['id']; ?>"><?php echo $vetor_empresa['nome_empresa']; ?></option>

                            <?php } ?>

                        </select>
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Buscar
                        </button>
                    </div>
                </div>
            </form>
            <br>
            <?php
            if (isset($estado) && isset($cidade)) {

                ?>
                <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                    <table id="tb-vagas-estado" class="table table-bordered table-striped table-pages">
                        <thead class="thead-dark">
                            <tr class="center">
                                <th scope="col">#</th>
                                <th scope="col">Empresa</th>
                                <th scope="col">Descrição</th>
                                <th scope="col">Data</th>
                                <th scope="col">Status</th>
                                <th scope="col" width="100px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            while ($value = mysqli_fetch_array($res)) {

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $i ?></th>
                                    <td><?php echo $value['nome_empresa'] ?></td>
                                    <td><?php echo $value['cargo'] ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($value['data_anuncio'])); ?></td>
                                    <td><?php echo $value['status'] == 1 ? 'Ativo' : 'Inativo' ?></td>
                                    <td>
                                        <a class="btn btn-xs btn-warning" href="'edit-vaga.php?id_vaga=<?php echo $value['id'] ?>'">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" href="'actions/delete-vaga.php?id=<?php echo $value['id'] ?>'">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        <?php if ($value['status'] == 1) { ?>
                                            <a class="btn btn-xs btn-primary" href="'actions/desativar-vaga.php?id_vaga=<?php echo $value['id'] ?>'">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn btn-xs btn-success" href="'actions/ativar-vaga.php?id_vaga=<?php echo $value['id'] ?>'">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }

            ?>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}