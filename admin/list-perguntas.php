<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {

    $res = mysqli_query($con, "SELECT * FROM TB_VV_FAQ ORDER BY id_pergunta DESC");

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Outros</a></li>
                <li class="active">Listar Perguntas</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Perguntas</h2>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-perguntas" class="table table-bordered table-striped table-pages">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th scope="col">#</th>
                            <th scope="col">Pergunta</th>
                            <th scope="col" width="100"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($value = mysqli_fetch_array($res)) {

                            ?>

                            <tr>
                                <th scope="row"><?php echo $i ?></th>
                                <td><?php echo $value['pergunta'] ?></td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-warning" href="edit-pergunta.php?id=<?php echo $value['id_pergunta'] ?>">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger" href="actions/delete-pergunta.php?id=<?php echo $value['id_pergunta'] ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <?php
    include "includes/footer.php";
}