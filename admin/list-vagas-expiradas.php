<?php
session_start();
include "../includes/conexao.php";
$func = new Funcoes();

if ($_SESSION['id'] == NULL && $_SESSION['nome'] == NULL && $_SESSION['email'] == NULL && $_SESSION['senha'] == NULL && $_SESSION['tipo'] == NULL) {

    $situacao = 'msg-sem-acesso';
    $func->alert($situacao, 'acao');
    $func->redir('index.php');
} else {
    $res = mysqli_query($con, "SELECT count(*) total FROM TB_VV_VAGAS where data_expira < date(now()) ORDER BY id DESC");
    $geral = mysqli_fetch_array($res);

    include "includes/header.php";
    include "includes/topo.php";
    include "includes/menu_sistema.php";

    ?>
    <div class="content-wrapper">

        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Início</a></li>
                <li class="active">Vagas</li>
            </ol>
        </section>
        <section class="content">
            <h2 align="center">Vagas Expiradas</h2>
            <table class="table">
                <thead class="thead-dark">
                    <tr class="center">
                        <th scope="col">Código</th>
                        <th scope="col">Nome da Vaga</th>
                        <th scope="col">Empresa</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Expiradas de:</th>
                        <th scope="col">Até:</th>
                    </tr>
                </thead>
                <form>
                    <tbody>
                        <tr>
                            <th><input type="text" id="codigo-vaga" class="form-control"></th>
                            <th>
                                <input type="text" id="nome-cargo" class="form-control">
                            </th>
                            <th>
                                <input type="text" id="nome-empresa" class="form-control">
                            </th>
                            <th>
                                <select name="estados" id="estados" class="form-control">
                                    <option value="">Escolha um Estado</option>
                                    <?php
                                    $result = mysqli_query($con, "select * FROM TB_VV_ESTADOS order by nome ASC");

                                    while ($row = mysqli_fetch_array($result)) {
                                        echo "<option value='" . $row['uf'] . "'>" . $row['nome'] . "</option>";
                                    }

                                    ?>
                                </select>
                            </th>
                            <th>
                                <input type="date" id="data_ini" name="data_ini" class="form-control">
                            </th>
                            <th>
                                <input type="date" id="data_fim" name="data_fim" class="form-control">
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <button type="button" class="btn btn-success" id="btnbuscar">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Buscar
                                </button>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>
                                <a href="#" class="btn btn-success" id="reativarbusca">
                                    Reativar Vagas Buscadas
                                </a>
                            </th>
                        </tr>
                    </tbody>
                </form>
            </table>
            <div>
                <div class="col-sm-12 text-right" style="height: 50px">
                    <?php if (intval($geral['total']) > 0) { ?>
                        <a href="<?php echo PATH_ADMIN . '/actions/reativar_todas_vagas.php' ?> " class="btn btn-success">
                            Reativar Todas as Vagas Expiradas
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table id="tb-admin-vagas-expiradas" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr class="center">
                            <th scope="col">#</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">Empresa</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Data</th>
                            <th scope="col" width="90px">Expirada em</th>
                            <th scope="col" width="100px"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>

    <?php
    include "includes/footer.php";
}