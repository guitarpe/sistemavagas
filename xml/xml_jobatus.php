<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '512M');
include('../views/system/includes/joomla.inc.php');

$obj = new OempregoDynamicSelect();

class OempregoDynamicSelect
{

    var $_db = null;
    var $_data = null;
    var $_xml = null;
    var $_xmlItem = null;
    var $_xmlRoot = null;

    function __construct()
    {
        try {
            $lang = JFactory::getLanguage();
            $extension = 'tpl_system';
            $base_dir = JPATH_SITE;
            $language_tag = 'pt-BR';

            $lang->load($extension, $base_dir, $language_tag, $reload);
            $this->_db = JFactory::getDBO();

            $query = $this->_db->getQuery(true);
            $query->select($this->_db->quoteName(array('id_vaga',
                    'cargo_vaga',
                    'descricao_vaga',
                    'name_cidade',
                    'bairro_pj',
                    'sigla_estado',
                    'cep_pj',
                    'id_experiencia',
                    'name_experiencia',
                    'deficiencia_vaga',
                    'sexo_vaga',
                    'viajar_vaga',
                    'veiculo_vaga',
                    'cnh_vaga',
                    'idioma_vaga',
                    'informatica_vaga',
                    'id_ensino',
                    'name_ensino',
                    'salario_vaga',
                    'name_salario',
                    'name_turno',
                    'name_tipo_contratacao',
                    'name_profissional_area',
                    'razao_social_pj',
                    'nome_fantasia_pj',
                    'default_name_pj',
                    'renovada_vaga',
                    'publish_date_vaga',
                    'renovada_date_vaga',
                    'privacidade_vaga'
            )));

            $query->from($this->_db->quoteName('#__vaga'));
            $query->innerJoin($this->_db->quoteName('#__estado') . ' USING(' . $this->_db->quoteName('id_estado') . ')');
            $query->innerJoin($this->_db->quoteName('#__cidade') . ' USING(' . $this->_db->quoteName('id_cidade') . ')');
            $query->innerJoin($this->_db->quoteName('#__pj') . ' USING(' . $this->_db->quoteName('id_pj') . ')');
            $query->leftJoin($this->_db->quoteName('#__experiencia') . ' USING(' . $this->_db->quoteName('id_experiencia') . ')');
            $query->innerJoin($this->_db->quoteName('#__ensino') . ' USING(' . $this->_db->quoteName('id_ensino') . ')');
            $query->innerJoin($this->_db->quoteName('#__salario') . ' USING(' . $this->_db->quoteName('id_salario') . ')');
            $query->innerJoin($this->_db->quoteName('#__turno') . ' USING(' . $this->_db->quoteName('id_turno') . ')');
            $query->innerJoin($this->_db->quoteName('#__tipo_contratacao') . ' USING(' . $this->_db->quoteName('id_tipo_contratacao') . ')');
            $query->innerJoin($this->_db->quoteName('#__profissional_area') . ' USING(' . $this->_db->quoteName('id_profissional_area') . ')');
            $query->where('((' .
                $this->_db->quoteName('#__vaga.publish_date_vaga') . ' >= ' . $this->_db->quote(JFactory::getDate('now -60 day')->toFormat('%Y-%m-%d')) .
                ') OR (' .
                $this->_db->quoteName('#__vaga.renovada_vaga') . ' =  1 AND ' .
                $this->_db->quoteName('#__vaga.renovada_date_vaga') . ' >= ' . $this->_db->quote(JFactory::getDate('now -30 day')->toFormat('%Y-%m-%d')) .
                '))');
            $query->where($this->_db->quoteName('#__pj.block_pj') . '  = 0');
            $query->where($this->_db->quoteName('#__pj.status_pj') . '  = 1');
            $query->where($this->_db->quoteName('#__vaga.status_vaga') . '  = 1');
            $query->where($this->_db->quoteName('#__vaga.id_vaga_fase') . '  = 1');
            $query->order('IFNULL(renovada_date_vaga, publish_date_vaga) DESC');
            $query->limit(1);

            $this->_db->setQuery($query);
            $list_vagas = $this->_db->loadObjectList();
            $base_url = str_replace('/xml/', '', JURI::base());

            if (count($list_vagas) > 0) {
                $this->_xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8'?><jobatus></jobatus>");

                foreach ($list_vagas as $vaga) {
                    $this->_xmlRoot = $this->_xml->addChild("ad");

                    $query = $this->_db->getQuery(true);
                    $query->select($this->_db->quoteName('name_beneficio'));
                    $query->from($this->_db->quoteName('#__vaga_beneficio_map'));
                    $query->innerJoin($this->_db->quoteName('#__beneficio') . ' USING(' . $this->_db->quoteName('id_beneficio') . ')');
                    $query->where($this->_db->quoteName('id_vaga') . ' = ' . $this->_db->quote($vaga->id_vaga));
                    $this->_db->setQuery($query);
                    $beneficios = $this->_db->loadResultArray();

                    $requirements = array();
                    if ($vaga->deficiencia_vaga == 1) {
                        $requirements[] = JText::_('OEMPREGO_XML_DEFICIENTE');
                    }

                    if ($vaga->viajar_vaga == 1) {
                        $requirements[] = JText::_('OEMPREGO_XML_VIAJAR');
                    }

                    if ($vaga->veiculo_vaga == 1) {
                        $requirements[] = JText::_('OEMPREGO_XML_VEICULO');
                    }

                    $descricao = array();

                    if (!empty($beneficios)) {
                        $descricao[] = JText::sprintf('OEMPREGO_XML_BENEFICIOS', implode(", ", $beneficios));
                    }

                    if (!empty($requirements)) {
                        $descricao[] = JText::sprintf('OEMPREGO_XML_REQUISITE', implode(", ", $requirements));
                    }

                    $_nameEmp = (($vaga->default_name_pj == 1) ? $vaga->nome_fantasia_pj : $vaga->razao_social_pj);
                    $_descricao_vaga = str_replace('	', '', str_replace('•', '', $vaga->descricao_vaga));

                    $this->_xmlItem = $this->addChildWithCDATA("url", $base_url . str_replace('/xml', '', JRoute::_('index.php?view=opportunity&cid=' . $vaga->id_vaga . ':' . JFilterOutput::stringURLSafe($vaga->cargo_vaga))));
                    $this->_xmlItem = $this->addChildWithCDATA("title", JText::sprintf('OEMPREGO_XML_TITULO', $vaga->cargo_vaga, $vaga->name_cidade, $vaga->sigla_estado));
                    $this->_xmlItem = $this->addChildWithCDATA("content", $_descricao_vaga . ((!empty($descricao)) ? implode(" ", $descricao) : "") . '. Cargo: ' . (($vaga->cargo_vaga) ? $vaga->cargo_vaga : "") . ' | Salario: ' . (($vaga->salario_vaga) ? JText::sprintf('OEMPREGO_XML_MOEDA', number_format($vaga->salario_vaga, 2, ',', '.')) : $vaga->name_salario) . '. | Ensino: ' . (($vaga->id_ensino > 0 && !empty($vaga->name_ensino)) ? $vaga->name_ensino : "") . '. | Turno: ' . (($vaga->name_turno) ? $vaga->name_turno : "") . '. | Area: ' . (($vaga->name_profissional_area) ? $vaga->name_profissional_area : "") . '. | Experiencia: ' . (($vaga->id_experiencia > 0) ? $vaga->name_experiencia : "") . '. | Cidade: ' . (($vaga->name_cidade) ? $vaga->name_cidade : "") . ' / ' . (($vaga->sigla_estado) ? $vaga->sigla_estado : ""));
                    $this->_xmlItem = $this->addChildWithCDATA("company", (($vaga->privacidade_vaga == 0 ) ? $_nameEmp : ""));
                    $this->_xmlItem = $this->addChildWithCDATA("contract", $vaga->name_tipo_contratacao);
                    $this->_xmlItem = $this->addChildWithCDATA("salary", (($vaga->salario_vaga) ? JText::sprintf('OEMPREGO_XML_MOEDA', number_format($vaga->salario_vaga, 2, ',', '.')) : $vaga->name_salario));
                    $this->_xmlItem = $this->addChildWithCDATA("city", $vaga->name_cidade);
                    $this->_xmlItem = $this->addChildWithCDATA("region", $vaga->sigla_estado);
                }
            }

            header("Content-type: text/xml");
            echo $this->_xml->asXML();
        } catch (Exception $e) {
            echo 'Exceção capturada: ', $e->getMessage(), "\n";
        }
    }

    function addChildWithCDATA($name, $value = NULL)
    {
        $this->_xmlItem = $this->_xmlRoot->addChild($name);

        $node = dom_import_simplexml($this->_xmlItem);
        $no = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($value));
    }
}
