<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '512M');

header("Content-type: text/xml");

require_once '../includes/SimpleXmlElementComplement.php';
include "../includes/conexao.php";

$nomedoc = 'vagas_' . date('d_m_Y') . '.xml';

if (file_exists($nomedoc)) {
    if (unlink($nomedoc)) {
        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->save($nomedoc);
    }
} else {
    $doc = new DOMDocument('1.0', 'UTF-8');
    $doc->save($nomedoc);
}

$sql = "SELECT DISTINCT
            vg.id, vg.cargo, vg.atividades, vg.cidade, pj.bairro, vg.estado, pj.cep, vg.experiencia_minima,'' as area_profissional,
            vg.sexo, vg.viajar, vg.veiculo_proprio, cn.categoria, vg.ensino_minimo, vg.faixa_salarial, vg.salario, vg.turno, vg.forma_contratacao,
            pj.nome_empresa, vg.data_anuncio
        FROM TB_VV_VAGAS vg
            INNER JOIN TB_VV_ESTADOS est ON est.uf=vg.estado
            INNER JOIN TB_VV_CIDADES cid ON cid.nome=vg.cidade
            INNER JOIN TB_VV_EMPRESAS pj ON pj.id=vg.id_empresa
            LEFT OUTER JOIN TB_VV_CNH cn ON cn.id_vaga=vg.id
            LEFT OUTER JOIN TB_VV_EXP_MINIMAS exp ON exp.descricao=vg.experiencia_minima
            INNER JOIN TB_VV_NIVEIS_ENSINO ens ON ens.descricao=vg.ensino_minimo
            INNER JOIN TB_VV_TURNOS tur ON tur.descricao=vg.turno
            INNER JOIN TB_VV_FORMAS_CONTRAT cont ON cont.descricao=vg.forma_contratacao
        WHERE 1=1 /*vg.data_anuncio >= DATE_SUB(NOW(), INTERVAL 45 DAY)
            AND pj.status = 1*/
            AND vg.status = 1
        ORDER BY data_anuncio DESC LIMIT 8000";

$res_vagas = mysqli_query($con, $sql) or die(mysqli_error($con));

if (mysqli_num_rows($res_vagas) > 0) {

    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');
    $semana = date('w');
    $hora = date('H:i:s');

    // configuração mes
    switch ($mes) {

        case 1: $mes = "Jan";
            break;
        case 2: $mes = "Fev";
            break;
        case 3: $mes = "Mar";
            break;
        case 4: $mes = "Abr";
            break;
        case 5: $mes = "Mai";
            break;
        case 6: $mes = "Jun";
            break;
        case 7: $mes = "Jul";
            break;
        case 8: $mes = "Ago";
            break;
        case 9: $mes = "Set";
            break;
        case 10: $mes = "Out";
            break;
        case 11: $mes = "Nov";
            break;
        case 12: $mes = "Dez";
            break;
    }

    // configuração semana
    switch ($semana) {

        case 0: $semana = "Dom";
            break;
        case 1: $semana = "Seg";
            break;
        case 2: $semana = "Ter";
            break;
        case 3: $semana = "Qua";
            break;
        case 4: $semana = "Qui";
            break;
        case 5: $semana = "Sex";
            break;
        case 6: $semana = "Sab";
            break;
    }

    $xml = new XmlWriter();
    $xml->openMemory();
    //$xml->startDocument('1.0', 'utf-8');
    $xml->startElement('source');
    $xml->setIndent(true);
    $xml->writeElement("publisher", "VAGAS E VAGAS");
    $xml->writeElement("publisherurl", "https://www.vagasevagas.com.br");
    $xml->writeElement("lastBuildDate", $semana . ' ' . $dia . ' ' . $mes . ' ' . $ano . ' ' . $hora . "GMT");

    while ($vetor = mysqli_fetch_array($res_vagas)) {

        if ($vetor['faixa_salarial'] == 'Salário Fixo') {

            $salario = $vetor['salario'];
        } else {

            $salario = $vetor['faixa_salarial'];
        }

        $sql_beneficios = mysqli_query($con, "select descricao from TB_VV_BENEFICIOS_VAGA where id_vaga=" . $vetor['id']);
        $beneficios = mysqli_fetch_array($sql_beneficios);

        $requirements = array();

        $sql_def = "SELECT deficiencia FROM TB_VV_DEFICIENCIAS_VAGA def WHERE def.id_vaga = " . $vetor['id'];
        $result_def = mysqli_query($con, $sql_def) or die(mysqli_error($con));

        if (intval(mysqli_num_rows($result_def) > 0)) {
            $requirements[] = '- Vaga de Emprego para Pessoas com Deficiência (PCD) ';

            $deficiencias = mysqli_fetch_array($result_def);
        }

        if (!empty($deficiencias)) {
            $requirements[] = '- Deficiências permitidas: ' . implode(", ", $deficiencias) . '. ';
        }

        if (intval($vetor['viajar']) == 1) {
            $requirements[] = '- Deve ter disponibilidade para viajar ';
        }

        if (intval($vetor['veiculo_proprio']) == 1) {
            $requirements[] = '- Deve possuir veículo próprio ';
        }

        $sql_cnh = "SELECT categoria FROM TB_VV_CNH vgc WHERE vgc.id_vaga=" . $vetor['id'] . " ORDER BY id DESC LIMIT 1";
        $result_cnh = mysqli_query($con, $sql_cnh) or die(mysqli_error($con));

        if (intval(mysqli_num_rows($result_cnh) > 0)) {
            $cnh = mysqli_fetch_array($result_cnh);
            $requirements[] = '- Deve possuir CNH de categoria ' . $cnh['categoria'] . ' ';
        }

        $sql_idiomas = "SELECT idioma, nivel FROM TB_VV_IDIOMAS_VAGA vgi WHERE vgi.id_vaga = " . $vetor['id'];
        $result_idioma = mysqli_query($con, $sql_idiomas) or die(mysqli_error($con));

        if (mysqli_num_rows($result_idioma) > 0) {
            $textIdiomas = array();

            while ($vetor_id = mysqli_fetch_assoc($result_idioma)) {
                $textIdiomas[] = $vetor_id['idioma'] . " nível " . $vetor_id['nivel'];
            }

            $requirements[] = '-  Deve ter conhecimento de idioma ' . implode(", ", $textIdiomas) . ' ';
        }

        $sql_info = "SELECT nome FROM TB_VV_INFORMATICA_VAGA vgif WHERE vgif.id_vaga = " . $vetor['id'];
        $result_info = mysqli_query($con, $sql_info) or die(mysqli_error($con));

        if (intval(mysqli_num_rows($result_cnh) > 0)) {
            $informatica_tipo = mysqli_fetch_array($result_info);

            $requirements[] = '- Conhecimentos de informática em ' . implode(", ", $informatica_tipo) . ' ';
        }

        $descricao = '';

        if (!empty($requirements)) {
            $descricao = ' | Requisitos solicitados pela empresa: ' . implode(", ", $requirements) . ' ';
        }

        $xml->startElement('job');

        $xml->startElement('title');
        $xml->writeCdata($vetor['cargo'] . ' em ' . $vetor['cidade'] . '/' . $vetor['estado']);
        $xml->endElement();

        $xml->startElement('date');
        $xml->writeCdata(date('D M Y H:i:s', strtotime($vetor['data_anuncio'])) . " GMT");
        $xml->endElement();

        $xml->startElement('referencenumber');
        $xml->writeCdata($vetor['id']);
        $xml->endElement();

        $xml->startElement('url');
        $xml->writeCdata('https://www.vagasevagas.com.br/ver-oportunidade.php?id=' . $vetor['id']);
        $xml->endElement();

        $xml->startElement('company');
        $xml->writeCdata($vetor['nome_empresa']);
        $xml->endElement();

        $xml->startElement('city');
        $xml->writeCdata($vetor['cidade']);
        $xml->endElement();

        $xml->startElement('state');
        $xml->writeCdata($vetor['estado']);
        $xml->endElement();

        $xml->startElement('country');
        $xml->writeCdata('BR');
        $xml->endElement();

        $xml->startElement('postalcode');
        $xml->writeCdata($vetor['cep']);
        $xml->endElement();

        $xml->startElement('description');

        if (!empty($beneficios)) {
            $descbeneficios = '. - Benefícios oferecidos pela empresa: ' . implode(", ", $beneficios);
        } else {
            $descbeneficios = '.';
        }

        $xml->writeCdata($vetor['atividades'] . $descbeneficios . '. Cargo: ' . $vetor['cargo'] . ' | Salario: ' . $salario . '. | Ensino: ' . $vetor['ensino_minimo'] . '. | Turno: ' . $vetor['turno'] . '. | Experiencia: ' . $vetor['experiencia_minima'] . '. | Cidade: ' . $vetor['cidade'] . ' / ' . $vetor['estado'] . '' . $descricao);
        $xml->endElement();

        $xml->startElement('salary');
        $xml->writeCdata($salario);
        $xml->endElement();

        $xml->startElement('education');
        $xml->writeCdata($vetor['ensino_minimo']);
        $xml->endElement();

        $xml->startElement('jobtype');
        $xml->writeCdata($vetor['turno']);
        $xml->endElement();

        $xml->startElement('category');
        $xml->writeCdata($vetor['atividades']);
        $xml->endElement();

        $xml->startElement('experience');
        $xml->writeCdata($vetor['experiencia_minima']);
        $xml->endElement();

        $xml->endElement();

        file_put_contents($nomedoc, $xml->flush(true), FILE_APPEND);
    }

    $xml->endElement();

    file_put_contents($nomedoc, $xml->flush(true), FILE_APPEND);
}

header("Location:$nomedoc");
